<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebVisit extends Model
{
    protected $table = 'web_visit';
}
