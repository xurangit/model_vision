
    <ul class="right_nav_bar wf-mobile-hidden" style="margin-top:-90px;" data-pop='dark' data-hovercolor="#000000"
        data-bgcolor="#333333">
        <li style="margin-bottom: 1px" class="phone">
            <div style="background-color:#333333;cursor: pointer" class="phone-icon icons" data-file="" data-desc="" data-color=""
                 data-hovercolor=""><i class="icon"></i><span class="title"></span></div>
            <!--电话-->
            <div class="iphone" style="height: 150px">
                <h3 class="tit">
                    @if($lang == 'en')
                        Contact Information
                    @else
                        联系方式
                    @endif
                </h3>
                <ul>
                    <li style="float: none;height: 45px;">
                        @if($lang == 'en')
                            Tel
                        @else
                            固定电话
                        @endif：{{$company['tel']}}</li>
                    <li style="float: none;height: 45px;">
                        @if($lang == 'en')
                            Phone
                        @else
                            移动电话
                        @endif：{{$company['phone']}}</li>
                </ul>
            </div>
        </li>
        <li  style="margin-bottom: 1px;cursor: pointer" class="wechat">
            <div style="background-color:#333333;" class="weixin-icon icons" data-file="" data-desc=""
                 data-color="" data-hovercolor=""><i class="icon"></i><span class="title"></span></div>
            <!--微信-->
            <div class="qr_codes">
                <h3 style="text-align: center;font-size: 16px;margin-top: 10px">@if($lang == 'en')
                        WeChat
                    @else
                        扫码关注
                    @endif</h3>
                <div style="width: 147px;height: 147px;margin: 0 auto">
                    <img src="/uploads/{{$company['wechat_code']}}" alt="" width="147" height="147">
                </div>
            </div>
        </li>
        <li  style="margin-bottom: 1px;cursor: pointer">
            <div style="background-color:#333333;line-height: 54px;text-align: center;" class="weixin-icon icons" data-file="" data-desc=""
                 data-color="" data-hovercolor="">
                @if($lang == 'en')
                <a href="{{url($route,['lang'=>'zh_cn'])}}" style="color: #fff;" >中</a>
                @else
                <a href="{{url($route,['lang'=>'en'])}}" style="color: #fff">EN</a>
                @endif
            </div>
        </li>
        <li>
            <div style="background-color:#333333;cursor:pointer" class="totop-icon icons"><i class="icon"></i></div>
        </li>
    </ul>
