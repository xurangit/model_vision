<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    use ModelTree, AdminBuilder;

    protected $table = "categories";

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('parent_id');
        $this->setOrderColumn('sort');
        $this->setTitleColumn('name');
    }

//    public function link()
//    {
//        return $this->belongsTo(Link::class);
//    }

    public function getList()
    {
//        $list = Category::where('is_show',1)->orderBy('sort','asc')->get();
        return DB::table("categories as c")
            ->leftjoin('link as l','c.link','=','l.id')
            ->where('c.is_show','=',1)
            ->select('c.title','c.name','c.name_en','c.picture','l.url')
            ->orderBy('c.sort','asc')
            ->get();
    }
}
