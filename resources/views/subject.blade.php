<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" class="ancient-ie old-ie bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" class="ancient-ie old-ie  bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" class="old-ie bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 9]>
<html id="ie9" class="old-ie9 bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html class="bit-html" dir="ltr" lang="zh-CN">
<!--<![endif]-->
<head>
    <!-- for 360 -->
    <meta name="renderer" content="webkit">
    <meta name="applicable-device" content="pc,mobile"> <!-- for baidu -->
    <meta http-equiv="Cache-Control" content="no-transform"/> <!-- for baidu -->
    <meta name="MobileOptimized" content="width"/><!-- for baidu -->
    <meta name="HandheldFriendly" content="true"/><!-- for baidu -->
    <meta name="baidu-site-verification" content="ZJTYs4ZAXD"/>
    <meta name="baidu-site-verification" content="QKVudTvBdU"/>
    <meta name="google-site-verification" content="emG8OWshwRUgZawA85PaCbo0RaLf5X8vpqeRuwiTsO8"/>
    <!-- customer header -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>超模专题</title>
    <link rel='stylesheet'  href='{{asset("/home/css/bootstrap3.min.css")}}' type='text/css'/>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="http://www.neverends.cn/xmlrpc.php"/>
    <!--[if lt IE 9]>
    <script src="{{asset('/home/js/html5shiv.min.js')}}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{asset('/home/js/respond.min.js')}}"></script>
    <![endif]-->
    <style type="text/css" id="static-stylesheet"></style>
    <link rel='stylesheet' id='iphorm-css' href='{{asset("/home/css/styles.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dashicons-css' href='{{asset("/home/css/dashicons.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='thickbox-css' href='{{asset("/home/css/thickbox.css")}}' type='text/css' media='all'/>
    <style type='text/css'>
        .qfe_gmaps_widget .qfe_map_wraper {
            background-color: #f7f7f7;
            padding: 0px !important;
        }

         .fiximage{
             position: fixed;
             top: 50%;
             left: 50%;
             z-index: 9;
         }

        .fiximage_back{
            width: 100%;
            height: 100%;
            background: black;
            opacity: 0.7;
            position: fixed;
            top:0%;
            left:0%;
            z-index: 8;
        }

        .image_hidden{
            display: none;
        }
        .image_show{
            display: block;
        }

    </style>
    <link rel='stylesheet' id='FeiEditor_respond-css' href='{{asset("/home/css/respond.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='open_social_css-css' href='{{asset("/home/css/os.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfy_editor_front-css' href='{{asset("/home/css/qfy_editor_front.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='vc_carousel_css-css' href='{{asset("/home/css/vc_carousel.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfyuser_font-css' href='{{asset("/home/css/qfyuser-font.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfyuser_min-css' href='{{asset("/home/css/qfyuser.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfyuser_skin_min-css' href='{{asset("/home/css/style.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='main-fonts-css' href='{{asset("/home/css/main.font.cdn.default.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-main-css' href='{{asset("/home/css/main.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-custom-less-css' href='{{asset("/home/css/custom-d566645bd5.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-bit-custom-less-css' href='{{asset("/home/css/bit-custom-15bb1788e8.css")}}' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='qfy_dynamic_css-css' href='{{asset("/home/css/qfy-custom-style.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontawesome_css-css' href='{{asset("/home/css/font-awesome.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-awsome-fonts-boot-css' href='{{asset("/home/css/bootstrap.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{asset("/home/css/style_1.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-media-op-css-css' href='{{asset("/home/css/opentip.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-main-roya-css-css' href='{{asset("/home/css/royalslider.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-main-roya-default-css-css' href='{{asset("/home/css/rs-default.css")}}' type='text/css' media='all'/>
    <script type='text/javascript' src='{{asset("/home/js/jquery.js")}}'></script>
    <script type='text/javascript' src='{{asset("/home/js/jquery-migrate.min.js")}}'></script>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.neverends.cn/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://www.neverends.cn/qfy-includes/wlwmanifest.xml"/>

    <link rel='canonical' href='http://www.neverends.cn/?page_id=13351'/>
    <link rel='shortlink' href='http://www.neverends.cn/?p=13351'/>
    <style class='style_0'>.bitRightSider .widget-title, .bitLeftSider .widget-title {
        padding: 0 0 0 10px;
        height: 28px;
        line-height: 28px;
        background-color: #024886;
        margin: 0px;
        font-family:;
        font-size: px;
        font-weight: normal;
        font-style: normal;
        text-decoration: none;
        color: #fff;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        border-bottom: 0;
        background-image: url(images/bg.png);
        background-repeat: repeat;
        -webkit-border-top-left-radius: 0;
        -webkit-border-top-right-radius: 0;
        -moz-border-radius-topleft: 0;
        -moz-border-radius-topright: 0;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .bitRightSider .bitWidgetFrame, .bitLeftSider .bitWidgetFrame {
        border-top: 0;
        border-bottom: 1px solid #bababa;
        border-left: 1px solid #bababa;
        border-right: 1px solid #bababa;
        padding: 4px 10px 4px 10px;
        -webkit-border-bottom-left-radius: 0;
        -webkit-border-bottom-right-radius: 0;
        -moz-border-radius-bottomleft: 0;
        -moz-border-radius-bottomright: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    .bitRightSider .site_tooler, .bitLeftSider .site_tooler {
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame, .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame {
        background-color: transparent;
        background-image: none;
        -webkit-border-bottom-left-radius: 0px;
        border-bottom-left-radius: 0px;
        -webkit-border-bottom-right-radius: 0px;
        border-bottom-right-radius: 0px;
    }</style>
    <style class='style_0'>.footer .widget-title {
        padding: 0 0 0 10px;
        height: 28px;
        line-height: 28px;
        background-color: #024886;
        margin: 0px;
        font-family:;
        font-size: px;
        font-weight: normal;
        font-style: normal;
        text-decoration: none;
        color: #fff;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        border-bottom: 0;
        background-image: none;
        -webkit-border-top-left-radius: 4px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -moz-border-radius-topright: 4px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }

    .footer .widget-title {
        border-top: 0;
        border-left: 0;
        border-right: 0;
    }

    .footer .bitWidgetFrame {
        border-bottom: 0;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        padding: 4px 10px 4px 10px;
    }

    .footer .site_tooler {
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .footer .site_tooler .bitWidgetFrame {
        background-color: transparent;
        background-image: none;
        -webkit-border-bottom-left-radius: 4px;
        border-bottom-left-radius: 4px;
        -webkit-border-bottom-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }</style>
    <script type="text/javascript">
        var qfyuser_ajax_url = 'http://www.neverends.cn/admin/admin-ajax.php';
    </script>
    <script type="text/javascript">
        var qfyuser_upload_url = 'http://www.neverends.cn/qfy-content/plugins/qfyuser/lib/fileupload/fileupload.php';
    </script>
    <link rel="icon" href="http://www.neverends.cn/qfy-content/uploads/2017/11/3a1f4031747c19d0318da97e1ba753c1.jpg"
          type="image/jpg"/>
    <link rel="apple-touch-icon-precomposed"
          href="http://www.neverends.cn/qfy-content/uploads/2017/11/ad36ec2ac475c2e86e42fdfe9c24da3c.jpg"/>
    <link rel="apple-touch-icon"
          href="http://www.neverends.cn/qfy-content/uploads/2017/11/ad36ec2ac475c2e86e42fdfe9c24da3c.jpg"/>
    <meta name="robots" content="index, follow"/>
    <meta name="description" content="敏感，且富有创意"/>
    <meta name="keywords" content="无尽方式NEVERENDS"/>
    <style type="text/css">
        .title_introduce{
            position: relative;
        }
        .title_introduce:after{
            position: absolute;
            content: '';
            width: 50px;
            height: 2px;
            bottom: -20px;
            left: 0;
            background: #909092;
        }
    </style>
    <script type="text/javascript">
        dtGlobals.logoEnabled = 1;
        dtGlobals.curr_id = '13351';
        dtGlobals.logoURL = '';
        dtGlobals.logoW = '0';
        dtGlobals.logoH = '0';
        dtGlobals.qfyname = '起飞页';
        dtGlobals.id = '5a0e68a51e89c';
        dtGlobals.language = '';
        smartMenu = 0;
        document.cookie = 'resolution=' + Math.max(screen.width, screen.height) + '; path=/';
        dtGlobals.gallery_bgcolor = 'rgba(51,51,51,1)';
        dtGlobals.gallery_showthumbs = '0';
        dtGlobals.gallery_style = '';
        dtGlobals.gallery_autoplay = '0';
        dtGlobals.gallery_playspeed = '3';
        dtGlobals.gallery_imagesize = '100';
        dtGlobals.gallery_stopbutton = '';
        dtGlobals.gallery_thumbsposition = '';
        dtGlobals.gallery_tcolor = '#fff';
        dtGlobals.gallery_tsize = '16';
        dtGlobals.gallery_dcolor = '#fff';
        dtGlobals.gallery_dsize = '14';
        dtGlobals.gallery_tfamily = '';
        dtGlobals.gallery_dfamily = '';
        dtGlobals.gallery_blankclose = '0';
        dtGlobals.fm_showstyle = '';
        dtGlobals.fm_showspeed = '';
        dtGlobals.cdn_url = 'https://cdn.goodq.top';
        dtGlobals.qfymodel = "";
        var socail_back_url = '';

    </script>


</head>


<body class="page page-id-13351 page-template-default image-blur  mobilehide_menu  mobilehide_mobile_menu widefull_header2 widefull_footer2 btn-flat content-fullwidth qfe-js-composer js-comp-ver-4.0.1 vc_responsive"
      data-pid="13351" data-pkey="18f5ff452c8ecbeaacccee53cc9b452f">

<div id="page" class=' hidetopbar hideheader hidefooter hidebottombar wide '>


    <!-- left, center, classical, classic-centered -->
    <!-- !Header -->
    <header id="header" class="logo-left wf-mobile-hidden headerPM menuPosition transparent" role="banner">
        <!-- class="overlap"; class="logo-left", class="logo-center", class="logo-classic" -->
        <div class="wf-wrap">
            <div class="wf-table">


                <div id="branding" class="wf-td bit-logo-bar" style="vertical-align: bottom;">
                    <a class="bitem logo small" style="display: table-cell;" href="http://www.neverends.cn/"><span
                            class="logospan"><img class="preload-me"
                                                  src="{{asset('/home/picture/ahr0cdovl3d3dy5uzxzlcmvuzhmuy24vcwz5lwnvbnrlbnqvdxbsb2fkcy8ymde3lzexl2q3zdzlyjg3ndm1mwm5n2iyyte1yta0mde3odi4njvklnbuzw_p_p100_p_3d_p_p100_p_3d.png')}}"
                                                  width="845" height="122" alt=""/></span></a>
                    <div class="bitem text" style="display: table-cell;vertical-align: middle;"><a
                            href='http://www.neverends.cn/' style='text-decoration: none;'>
                        <div id='bit-logoText' data-margin-left=0 style='position:relative;font-size:16px;  '>
                            <div class='logotext_outner'>
                                <div class='logotext_inner'>
                                    <div><span style="color:#FFFFFF;">&nbsp;</span><span style="font-family:黑体;"><span
                                            style="font-family:raleway;"></span></span></div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <!-- <div id="site-title" class="assistive-text"></div>
	<div id="site-description" class="assistive-text"></div> -->
                </div>
                <div class="wf-mobile-visible wf-td assistive-info    " role="complementary">
                    <div class="top-bar-right right bit_widget_more" bitdatamarker="bitHeader-2"
                         bitdataaction="site_fix_container" bitdatacolor="white">
                    </div>
                </div>


                <!-- !- Navigation -->
                <nav style="vertical-align: bottom;" id="navigation" class="wf-td" bitDataAction="site_menu_container"
                     bitDataLocation="primary">
                    <ul id="main-nav" data-st="0" data-sp="0" data-fh="0" data-mw="0" data-lh="31"
                        class="mainmenu fancy-rollovers wf-mobile-hidden bit-menu-default underline-hover position-text-right"
                        data-bit-menu=underline-hover data-bit-float-menu=underline-hover>
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page bit-menu-post-id-13599 menu-item-13688 first">
                            <a href="http://www.neverends.cn/"><span>HOME</span></a></li>
                    </ul>

                    <a data-padding='' data-top='8' data-right='8' rel="nofollow" id="mobile-menu" style="display:none;"
                       class="glyphicon glyphicon-icon-align-justify dropTopStyle positionFixed center">
                        <span class="menu-open  phone-text">联络</span>
                        <span class="menu-close">关闭</span>
                        <span class="menu-back">返回上一级</span>
                        <span class="wf-phone-visible">&nbsp;</span>
                    </a>

                </nav>
                <div style="display:none;" id="main-nav-slide">
                    <div class="main-nav-slide-inner" data-class="">
                        <div class="floatmenu-bar-right bit_widget_more" bitdatamarker="bitHeader-3"
                             bitdataaction="site_fix_container" bitdatacolor="white">
                        </div>
                    </div>
                </div>


            </div><!-- #branding -->
        </div><!-- .wf-wrap -->
    </header><!-- #masthead -->

    <section class="bitBanner" id="bitBanner" bitdatamarker="bitBanner" bitdataaction="site_fix_container">
    </section>
    <div id="main" class="bit_main_content">


        <div class="main-gradient"></div>

        <div class="wf-wrap">
            <div class="wf-container-main">


                <div id="content" class="content" role="main">


                    <div class="main-outer-wrapper ">
                        <div class="bit_row">

                            <div class="twelve columns no-sidebar-content ">

                                <div class="bit_row">

                                    <div class="content-wrapper twelve columns ">
                                        <!--顶部视频-->

                                        <section data-fixheight=""
                                                 class="qfy-row-1-5aec7054bb124341239 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal"
                                                 id="bit_rwkws"
                                                 style='margin-bottom:0;border-radius:0px;color:transparent;width: 100%;'>
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-1-5aec7054bb124341239 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 20vh;
                                                        padding-bottom: 20vh;
                                                        margin-top: 0;
                                                        min-height:520px;
                                                    }

                                                    section.section.qfy-row-1-5aec7054bb124341239 > .container {
                                                        max-width: 1024px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-1-5aec7054bb124341239 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 20vh;
                                                        padding-bottom: 20vh;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div data-time="3" data-imagebgs="" class="background-media   "
                                                 backgroundSize='true'
                                                @if($subject_basic->is_video == 0)
                                                    style="background-image: url('/uploads/{{$subject_basic->file}}'); background-repeat:no-repeat; background-size:cover; background-position:center center;"
                                                @endif >
                                                @if($subject_basic->is_video == 1)
                                                <video autoplay="autoplay" muted="muted" volume="0"
                                                       poster=" /uploads/{{$subject_basic->file}}"
                                                       loop="loop"
                                                       style="width: 100%; height: 100%;object-fit:cover;object-position:center center"
                                                       class="section-background-video">
                                                    {{--<source type="video/webm" src="https://cdn.goodq.top/caches/a72a582b36bc92b650d2f4f0a13de6a1/aHR0cDovL3d3dy5uZXZlcmVuZHMuY24vcWZ5LWNvbnRlbnQvdXBsb2Fkcy8yMDE3LzExLzE1NGMyNDgzNjVhNDkzMWU4ZjM1ZmIzNzhjN2Q5YmY4LndlYm0_p_p100_p_3D.webm"/>--}}
                                                    <source type="video/mp4" src="/uploads/{{$subject_basic->file}}"/>
                                                </video>
                                                @endif
                                            </div>
                                            @if($subject_basic->is_video == 0)
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: rgba(0,0,0,0.4);"></div>

                                            <div class="container" style="top:-140px">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="" data-delay=""
                                                         class=" qfy-column-1-5aec70151cf23844572 qfy-column-inner  vc_span12  text-Default small-screen-undefined fullrow"
                                                         data-dw="1/1" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_column_text_n8nrq-c-w3"
                                                                     data-anitime='2'
                                                                     class="qfy-element qfy-text qfy-text-45760 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_left-to-right "
                                                                     style="position: relative;line-height:1.5em;background-position:left top;background-repeat: no-repeat;margin-top:30px;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="position: relative;top: 32px;">
                                                                            <div style="text-align: center;"><span
                                                                                        style="color:#ff6d62;"><span
                                                                                            style="font-size: 48px;color: #ffffff;opacity: 0.3"><span
                                                                                                style="font-family: aviano sans;">{{$cate->title}}</span></span></span><br/>
                                                                                &nbsp;
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_column_text_637v2-c-w3"
                                                                     data-anitime='2'
                                                                     class="qfy-element qfy-text qfy-text-73424 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_right-to-left "
                                                                     style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="position: relative;">
                                                                            <div style="text-align: center;">
                                                                                <strong>
                                                                                    <span style="font-family:华文仿宋;">
                                                                                        <span style="letter-spacing: 10px;">
                                                                                            <span style="font-size: 36px;">
                                                                                                <div style="border-bottom:2px white solid;width: 3em;opacity: 0.3;position: absolute;left: 250px;top:14px"></div>
                                                                                                <span style="color: rgb(255, 255, 255);">
                                                                                                    <a  style="color: rgb(255, 255, 255);" href="">
                                                                                                        @if($lang == 'en')
                                                                                                            {{$cate->name_en}}
                                                                                                        @else
                                                                                                            {{$cate->name}}
                                                                                                        @endif
                                                                                                    </a>
                                                                                                </span>
                                                                                                <div style="border-bottom:2px white solid;width: 3em;opacity: 0.3;position: absolute;right: 250px;top:14px"></div>
                                                                                            </span>
                                                                                        </span>
                                                                                    </span>
                                                                                </strong>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <!--<div id="qfy-btn-5aec70151df5f2468"
                                                                     style="margin-top:0;margin-bottom:0;padding-top:54px;padding-bottom:0;padding-right:0;padding-left:0;"
                                                                     m-padding="54px 0px 0px 0px" p-padding="54px 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_btn_ywgbh-c-w3" data-anitime='2'
                                                                     class="qfy-element vc_btn3-container  qfe_animate_when_almost_visible qfe_bottom-to-top vc_btn3-center">
                                                                    <a style="font-family:Aviano Sans; font-size:20px;"
                                                                       class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-square vc_btn3-style-modern vc_btn3-color-danger"
                                                                       href="http://www.neverends.cn/?page_id=13280"
                                                                       target="">Read More</a></div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-1-5aec70151cf23844572 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 141px;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-1-5aec70151cf23844572 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-1-5aec70151cf23844572 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-1-5aec70151cf23844572 {
                                                            }

                                                            .qfy-column-1-5aec70151cf23844572 > .column_inner > .background-overlay, .qfy-column-1-5aec70151cf23844572 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                </div>
                                            </div>
                                            @endif

                                        </section>

                                        @foreach($subject as $key => $value)
                                            @if($key%2 == 0)
                                            <!--学员介绍-->
                                            <section data-fixheight=""
                                                     class="qfy-row-4-5aec7054c4266181531 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
                                                     id="bit_y2753"
                                                     style='margin-bottom:0;border-radius:0px;color:#ffffff;'>
                                                <style class="row_class qfy_style_class">
                                                    @media only screen and (min-width: 992px) {
                                                        section.section.qfy-row-4-5aec7054c4266181531 {
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top: 20px;
                                                            padding-bottom: 0;
                                                            margin-top: 152px;
                                                            min-height: 617px;
                                                        }
                                                    }

                                                    @media only screen and (max-width: 992px) {
                                                        .bit-html section.section.qfy-row-4-5aec7054c4266181531 {
                                                            padding-left: 15px;
                                                            padding-right: 15px;
                                                            padding-top: 20px;
                                                            padding-bottom: 0;
                                                            margin-top: 0;
                                                            min-height: 0;
                                                        }
                                                    }
                                                </style>
                                                <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                     style="background-color: #fff;"></div>


                                                <!--测试改写页面  left-->
                                                <div class="container">
                                                    <div class="row qfe_row 99999">
                                                        <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                             data-duration="" data-delay=""
                                                             class=" qfy-column-5-5aec7054c44a4416314 qfy-column-inner  vc_span_class  vc_span6  text-default small-screen-undefined notfullrow"
                                                             data-dw="1/2" data-fixheight="">
                                                            <div style=";position:relative;" class="column_inner ">
                                                                <div class=" background-overlay grid-overlay-"
                                                                     style="background-color:transparent;width:100%;"></div>
                                                                <div class="column_containter"
                                                                     style="z-index:3;position:relative;">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                        .single_image_text-5aec7054c5aa6601 .head {
                                                                            font-size: 16px !important;
                                                                        }

                                                                        .single_image_text-5aec7054c5aa6601 .content {
                                                                            font-size: 16px !important;
                                                                        }
                                                                    }</style>
                                                                    <div id="vc_img_5aec7054c59d1309"
                                                                         style="width:100%;padding:0px;margin:0px;height:100%;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:4px;padding-right:0;padding-left:0;background-repeat: no-repeat;"
                                                                         m-padding="0px 0px 4px 0px" p-padding="0 0 4px 0"
                                                                         css_animation_delay="0"
                                                                         qfyuuid="qfy_single_image_fg7p5" data-anitime='1'
                                                                         class="qfy-element bitImageControlDiv "><a
                                                                            class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_left-to-right vc_align_center">
                                                                            <div class="qfe_wrapper"><span></span>
                                                                                @if($value->is_video != 1)
                                                                                <img
                                                                                    class="front_image   ag_image"
                                                                                    src="/uploads/{{$value->file}}"
                                                                                    alt="" description=""
                                                                                    data-attach-id="12885"
                                                                                    data-title="" title="" src-img=""
                                                                                    style=''/>
                                                                                @else
                                                                                <video autoplay="autoplay" muted="muted" volume="0"
                                                                                       poster=""
                                                                                       loop="loop"
                                                                                       style="width: 100%; height: 100%;object-fit:cover;object-position:center center"
                                                                                       class="section-background-video">
                                                                                    <source type="video/webm"
                                                                                            src="/uploads/{{$value->file}}" autoplay="autoplay"/>
                                                                                    <source type="video/mp4" src="/uploads/{{$value->file}}" autoplay="autoplay"/>
                                                                                    {{--<source type="video/mp4" src="https://yijqi.com/video/v1.mp4" autoplay="autoplay"/>--}}

                                                                                </video>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    </a></div>
                                                            </div>
                                                        </div>
                                                        <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-5-5aec7054c44a4416314 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-5-5aec7054c44a4416314 {
                                                                width: 53.51851851851852%;
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-5-5aec7054c44a4416314 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top:;
                                                                padding-bottom:;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-5-5aec7054c44a4416314 {
                                                                width: 53.51851851851852%;
                                                            }

                                                            .qfy-column-5-5aec7054c44a4416314 > .column_inner > .background-overlay, .qfy-column-5-5aec7054c44a4416314 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                        <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                             data-duration="" data-delay=""
                                                             class=" qfy-column-6-5aec7054c5c16108347 qfy-column-inner  vc_span_class  vc_span6  text-default small-screen-undefined notfullrow"
                                                             data-dw="1/2" data-fixheight="">
                                                            <div style=";position:relative;" class="column_inner ">
                                                                <div class=" background-overlay grid-overlay-"
                                                                     style="background-color:transparent;width:100%;"></div>
                                                                <div class="column_containter"
                                                                     style="z-index:3;position:relative;">
                                                                    <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                         css_animation_delay="0"
                                                                         qfyuuid="qfy_column_text_mznvt-c-fd"
                                                                         data-anitime='1'
                                                                         class="qfy-element qfy-text qfy-text-20301 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_left-to-right "
                                                                         style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                        <div class="qfe_wrapper">
                                                                            <div style="position: relative;">
                                                                                <div><span style="color:#231815;"><span
                                                                                        style="font-size: 36px;"><span
                                                                                        style="letter-spacing: 3px;"><span
                                                                                        style="font-family: aviano sans;">
                                                                                                    @if($lang == 'en')
                                                                                                        {{$value->name_en}}
                                                                                                        @else
                                                                                                        {{$value->name}}
                                                                                                    @endif
                                                                                                </span></span></span></span>
                                                                                </div>

                                                                                <div>&nbsp;</div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                         css_animation_delay="0"
                                                                         qfyuuid="qfy_column_text_1oobh" data-anitime='1'
                                                                         class="qfy-element qfy-text qfy-text-1104 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_right-to-left "
                                                                         style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                        <div class="qfe_wrapper">
                                                                            <div style="position: relative;color: #304040">
                                                                                <div>
                                                                                    <div><span style="font-size:18px;">
                                                                                          @if($lang == 'en')
                                                                                                {!! $value->introduce_en !!}
                                                                                          @else
                                                                                                {!! $value->introduce !!}
                                                                                          @endif
                                                                                        </span>
                                                                                    </div>

                                                                                    {{--<div><span style="color:#666666;"></span><span><span style="font-size: 18px;">眼下的一切都不是自己想要的</span></span>--}}
                                                                                    {{--</div>--}}

                                                                                   {{-- <div>
                                                                                        <p><span
                                                                                        ><span
                                                                                                style="font-size: 18px;">于是他们的人生就在这一站和下一站之间悄然逝去</span></span>
                                                                                        </p>

                                                                                        <p><span
                                                                                        ><span
                                                                                                style="font-size: 18px;">他们不断寻找，却永远也不知道</span></span>
                                                                                        </p>

                                                                                        <p><span
                                                                                        ><span
                                                                                                style="font-size: 18px;">自己真正想要的是什么</span></span>
                                                                                        </p>
                                                                                        <span ><span
                                                                                                style="font-size: 18px;"></span></span>
                                                                                    </div>


                                                                                    <div><span
                                                                                            style="color:#666666;"></span>
                                                                                    </div>--}}
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-6-5aec7054c5c16108347 > .column_inner {
                                                                padding-left: 37px;
                                                                padding-right: 0;
                                                                padding-top: 167px;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-6-5aec7054c5c16108347 {
                                                                width: 46.4%;
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-6-5aec7054c5c16108347 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 167px;
                                                                padding-bottom:;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-6-5aec7054c5c16108347 {
                                                                width: 46.4%;
                                                            }

                                                            .qfy-column-6-5aec7054c5c16108347 > .column_inner > .background-overlay, .qfy-column-6-5aec7054c5c16108347 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                        <div class="wf-mobile-hidden qfy-clumn-clear"
                                                             style="clear:both;"></div>
                                                    </div>
                                                </div>
                                                <!--********************-->

                                            </section>
                                            @else
                                            <!--学员介绍-->
                                            <section data-fixheight=""
                                                     class="qfy-row-3-5aec7054bf720194112 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
                                                     id="bit_ai9aq"
                                                     style='margin-bottom:0;border-radius:0px;color:#ffffff;'>
                                                <style class="row_class qfy_style_class">
                                                    @media only screen and (min-width: 992px) {
                                                        section.section.qfy-row-3-5aec7054bf720194112 {
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top: 20px;
                                                            padding-bottom: 0;
                                                            min-height: 603px;
                                                        }
                                                    }

                                                    @media only screen and (max-width: 992px) {
                                                        .bit-html section.section.qfy-row-3-5aec7054bf720194112 {
                                                            padding-left: 15px;
                                                            padding-right: 15px;
                                                            padding-top: 20px;
                                                            padding-bottom: 0;
                                                            margin-top: 0;
                                                            min-height: 0;
                                                        }
                                                    }
                                                </style>
                                                <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                     style="background-color: #fff;"></div>





                                                <!--测试改写页面  right-->
                                                <div class="container">
                                                    <div class="row qfe_row 7777777">
                                                        <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                             data-duration="" data-delay=""
                                                             class=" qfy-column-3-5aec7054c0968412353 qfy-column-inner  vc_span_class  vc_span6  text-default small-screen-undefined notfullrow"
                                                             data-dw="1/2" data-fixheight="">
                                                            <div style=";position:relative;" class="column_inner ">
                                                                <div class=" background-overlay grid-overlay-"
                                                                     style="background-color:transparent;width:100%;"></div>
                                                                <div class="column_containter"
                                                                     style="z-index:3;position:relative;">
                                                                    <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                         css_animation_delay="0"
                                                                         qfyuuid="qfy_column_text_mznvt" data-anitime='0.7'
                                                                         class="qfy-element qfy-text qfy-text-53262 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_left-to-right "
                                                                         style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                        <div class="qfe_wrapper">
                                                                            <div style="position: relative;">
                                                                                <div style="text-align: right;"><span
                                                                                        style="color:#231815;"><span
                                                                                        style="font-size: 36px;"><span
                                                                                        style="letter-spacing: 3px;"><span
                                                                                        style="font-family: aviano sans;">
                                                                                                    @if($lang == 'en')
                                                                                                        {{$value->name_en}}
                                                                                                        @else
                                                                                                        {{$value->name}}
                                                                                                    @endif
                                                                                                </span></span></span></span>
                                                                                </div>

                                                                                <div style="text-align: right;">&nbsp;</div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                         css_animation_delay="0"
                                                                         qfyuuid="qfy_column_text_4zll3" data-anitime='1'
                                                                         class="qfy-element qfy-text qfy-text-35497 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_left-to-right "
                                                                         style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                        <div class="qfe_wrapper" style="color: #304040">
                                                                            <div style="text-align: right;"><span
                                                                                    style="font-size:18px;">
                                                                                    @if($lang == 'en')
                                                                                        {!! $value->introduce_en !!}
                                                                                    @else
                                                                                        {!! $value->introduce !!}
                                                                                    @endif
                                                                                </span>
                                                                            </div>

                                                                            {{--<div style="text-align: right;"><span--}}
                                                                                    {{--style="font-size:18px;">他们会不屈不挠地、一步步地朝着自己梦想的样子努力</span>--}}
                                                                            {{--</div>--}}

                                                                            {{--<div style="text-align: right;"><span--}}
                                                                                    {{--style="font-size:18px;">不虚度光阴、不得过且过</span>--}}
                                                                            {{--</div>--}}

                                                                            {{--<div style="text-align: right;"><span--}}
                                                                                    {{--style="font-size:18px;">过着因充实而幸福的日子</span>--}}
                                                                            {{--</div>--}}

                                                                            {{--<div style="text-align: right;"><span--}}
                                                                                    {{--style="font-size:18px;">对于他们来说，这才是真正的生活</span>--}}
                                                                            {{--</div>--}}

                                                                            {{--<div style="text-align: right;"><span--}}
                                                                                    {{--style="font-size:18px;">他们往往也将伴随着自己的努力一天天接近梦想</span>--}}
                                                                            {{--</div>--}}

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-3-5aec7054c0968412353 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 60px;
                                                                padding-top: 200px;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-3-5aec7054c0968412353 {
                                                                width: 45.879999999999995%;
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-3-5aec7054c0968412353 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 200px;
                                                                padding-bottom:;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-3-5aec7054c0968412353 {
                                                                width: 45.879999999999995%;
                                                            }

                                                            .qfy-column-3-5aec7054c0968412353 > .column_inner > .background-overlay, .qfy-column-3-5aec7054c0968412353 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                        <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                             data-duration="" data-delay=""
                                                             class=" qfy-column-4-5aec7054c14e4912393 qfy-column-inner  vc_span_class  vc_span6  text-default small-screen-undefined notfullrow"
                                                             data-dw="1/2" data-fixheight="">
                                                            <div style=";position:relative;" class="column_inner ">
                                                                <div class=" background-overlay grid-overlay-"
                                                                     style="background-color:transparent;width:100%;"></div>
                                                                <div class="column_containter"
                                                                     style="z-index:3;position:relative;">
                                                                    <style>@media only screen and (max-width: 768px) {
                                                                        .single_image_text-5aec7054c3c8b561 .head {
                                                                            font-size: 16px !important;
                                                                        }

                                                                        .single_image_text-5aec7054c3c8b561 .content {
                                                                            font-size: 16px !important;
                                                                        }
                                                                    }</style>
                                                                    <div id="vc_img_5aec7054c355d81"
                                                                         style="width:100%;padding:0px;margin:0px;height:100%;clear:both;position:relative;margin-top:0;margin-bottom:0;padding-top:53px;padding-bottom:6px;padding-right:69px;padding-left:0;background-repeat: no-repeat;"
                                                                         m-padding="53px 0px 6px 0px"
                                                                         p-padding="53px 69px 6px 0" css_animation_delay="0"
                                                                         qfyuuid="qfy_single_image_zhuhk" data-anitime='1'
                                                                         class="qfy-element bitImageControlDiv "><a
                                                                            class="bitImageAhover  ">
                                                                        <div class="bitImageParentDiv qfe_single_image qfe_content_element qfe_animate_when_almost_visible qfe_right-to-left vc_align_center">
                                                                            <div class="qfe_wrapper"><span></span>
                                                                                @if($value->is_video != 1)
                                                                                <!--<img
                                                                                    width="1024" height="654"
                                                                                    src="{{asset('/home/images/img.png')}}"
                                                                                    class="front_image  attachment-large"
                                                                                    alt="任睿" title="" description=""
                                                                                    data-title="任睿" src-img="" style=''/>-->
                                                                                <!--<video src="https://yijqi.com/video/v1.mp4" autoplay="autoplay"></video>-->
                                                                                <img
                                                                                        class="front_image   ag_image"
                                                                                        src="/uploads/{{$value->file}}"
                                                                                        alt="" description=""
                                                                                        data-attach-id="12885"
                                                                                        data-title="" title="" src-img=""
                                                                                        style=''/>
                                                                                @else
                                                                                <video autoplay="autoplay" muted="muted" volume="0"
                                                                                       poster=""
                                                                                       loop="loop"
                                                                                       style="width: 100%; height: 100%;object-fit:cover;object-position:center center"
                                                                                       class="section-background-video">
                                                                                    <source type="video/webm"
                                                                                            src="/uploads/{{$value->file}}" autoplay="autoplay"/>
                                                                                    <source type="video/mp4"
                                                                                            src="/uploads/{{$value->file}}" autoplay="autoplay"/>

                                                                                </video>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    </a></div>
                                                            </div>
                                                        </div>
                                                        <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-4-5aec7054c14e4912393 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-4-5aec7054c14e4912393 {
                                                                width: 54.1%;
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-4-5aec7054c14e4912393 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top:;
                                                                padding-bottom:;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-4-5aec7054c14e4912393 {
                                                                width: 54.1%;
                                                            }

                                                            .qfy-column-4-5aec7054c14e4912393 > .column_inner > .background-overlay, .qfy-column-4-5aec7054c14e4912393 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                        <div class="wf-mobile-hidden qfy-clumn-clear"
                                                             style="clear:both;"></div>
                                                    </div>
                                                </div>



                                                <!--*******************************-->

                                            </section>
                                            @endif
                                        @endforeach

                                        <section data-fixheight=""
                                                 class="qfy-row-2-5aec7054bd876386281 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
                                                 style='margin-bottom:0;border-radius:0px;color:#ffffff;'>
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 991px) {
                                                    section.section.qfy-row-2-5aec7054bd876386281 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 20px;
                                                        padding-bottom: 0;
                                                        margin-top: 89px;
                                                    }

                                                    section.section.qfy-row-2-5aec7054bd876386281 > .container {
                                                        max-width: 1500px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-2-5aec7054bd876386281 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 20px;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }

                                            </style>
                                            <!--<div class="section-background-overlay background-overlay grid-overlay-0 " style="background-color: #252525;"></div>-->
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style=""></div>
                                            <div class="container">
                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                     css_animation_delay="0"
                                                     qfyuuid="qfy_column_text_1oobh" data-anitime='1'
                                                     class="qfy-element qfy-text qfy-text-1104 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_right-to-left "
                                                     style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                </div>
                                            </div>
                                        </section>
                                        <!--底部 联系方式-->
                                        @include('common.footer')
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div><!-- END .page-wrapper -->
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

            </div><!-- .wf-container -->
        </div><!-- .wf-wrap -->
    </div><!-- #main -->

    <div class="fiximage_back image_hidden"></div>
    <div class="fiximage image_hidden">
        <img src="" alt="">
    </div>

    <footer id="footer" class="footer">
        <div class="wf-wrap">
            <div class="wf-container qfe_row footer1" bitDataAction='site_widget_container' bitdatamarker="sidebar_2">
                <section id="text-11" style="margin-top:0;margin-bottom:0;" class="widget widget_text site_tooler">
                    <style class='style_text-11'>#text-11 .widget-title {
                        padding: 0 0 0 10px;
                        height: 28px;
                        line-height: 28px;
                        background-color: transparent;
                        margin: 0px;
                        font-family:;
                        font-size: px;
                        font-weight: normal;
                        font-style: normal;
                        text-decoration: none;
                        color: #ffffff;
                        border-top: 1px solid transparent;
                        border-left: 1px solid transparent;
                        border-right: 1px solid transparent;
                        border-bottom: 0px solid transparent;
                        background-image: none;
                        -webkit-border-top-left-radius: 4px;
                        -webkit-border-top-right-radius: 4px;
                        -moz-border-radius-topleft: 4px;
                        -moz-border-radius-topright: 4px;
                        border-top-left-radius: 4px;
                        border-top-right-radius: 4px;
                    }

                    #text-11 .bitWidgetFrame {
                        border-top: 0;
                        border-bottom: 1px solid transparent;
                        border-left: 1px solid transparent;
                        border-right: 1px solid transparent;
                        padding: 4px 10px 4px 10px;
                        -webkit-border-bottom-left-radius: 4px;
                        -webkit-border-bottom-right-radius: 4px;
                        -moz-border-radius-bottomleft: 4px;
                        -moz-border-radius-bottomright: 4px;
                        border-bottom-left-radius: 4px;
                        border-bottom-right-radius: 4px;
                    }

                    #text-11 {
                        -webkit-box-shadow: none;
                        box-shadow: none;
                    }

                    #text-11 .bitWidgetFrame {
                        background-color: transparent;
                        background-image: none;
                        -webkit-border-bottom-left-radius: 4px;
                        border-bottom-left-radius: 4px;
                        -webkit-border-bottom-right-radius: 4px;
                        border-bottom-right-radius: 4px;
                    }

                    body #text-11 .bitWidgetFrame {
                        padding-top: 10px !important;
                        padding-bottom: 10px !important;
                    }</style>
                    <div class="textwidget ckeditorInLine bitWidgetFrame" bitRlt="text" bitKey="text" wid="text-11">
                        <div style="text-align: center;"><span style="font-size:16px;"><span style="font-family: 微软雅黑;"><span
                                style="color:#FFFFFF;"></span><a rel="" target="_blank"><span
                                style="color:#FFFFFF;"></span></a></span></span></div>
                    </div>
                </section>
                <section id="simplepage-2" style="margin-bottom:20px;" class="widget simplepage site_tooler">
                    <style class='style_simplepage-2'>#simplepage-2 .widget-title {
                        padding: 0 0 0 10px;
                        height: 28px;
                        line-height: 28px;
                        background-color: transparent;
                        margin: 0px;
                        font-family:;
                        font-size: px;
                        font-weight: normal;
                        font-style: normal;
                        text-decoration: none;
                        color: #ffffff;
                        border-top: 1px solid transparent;
                        border-left: 1px solid transparent;
                        border-right: 1px solid transparent;
                        border-bottom: 0px solid transparent;
                        background-image: none;
                        -webkit-border-top-left-radius: 4px;
                        -webkit-border-top-right-radius: 4px;
                        -moz-border-radius-topleft: 4px;
                        -moz-border-radius-topright: 4px;
                        border-top-left-radius: 4px;
                        border-top-right-radius: 4px;
                    }

                    #simplepage-2 .widget-title {
                        border-top: 0;
                        border-left: 0;
                        border-right: 0;
                    }

                    #simplepage-2 .bitWidgetFrame {
                        border-bottom: 0;
                        border-top: 0;
                        border-left: 0;
                        border-right: 0;
                        padding: 4px 10px 4px 10px;
                    }

                    #simplepage-2 {
                        -webkit-box-shadow: none;
                        box-shadow: none;
                    }

                    #simplepage-2 .bitWidgetFrame {
                        background-color: transparent;
                        background-image: none;
                        -webkit-border-bottom-left-radius: 4px;
                        border-bottom-left-radius: 4px;
                        -webkit-border-bottom-right-radius: 4px;
                        border-bottom-right-radius: 4px;
                    }

                    #simplepage-2 .bitWidgetFrame {
                        padding-left: 0px;
                        padding-right: 0px;
                    }

                    body #simplepage-2 .bitWidgetFrame {
                        padding-top: 0px !important;
                        padding-bottom: 0px !important;
                    }</style>
                    <div class='simplepage_container bitWidgetFrame' data-post_id='10379'>
                        <section data-fixheight=""
                                 class="qfy-row-12-5aec7054e2e90354551 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
                                 id="bit_4ami8" style='margin-bottom:0;border-radius:0px;color:#333333;'>
                            <style class="row_class qfy_style_class">
                                @media only screen and (min-width: 992px) {
                                    section.section.qfy-row-12-5aec7054e2e90354551 {
                                        padding-left: 0;
                                        padding-right: 0;
                                        padding-top: 20px;
                                        padding-bottom: 0;
                                        margin-top: 0;
                                    }

                                    section.section.qfy-row-12-5aec7054e2e90354551 > .container {
                                        max-width: 1280px;
                                        margin: 0 auto;
                                    }
                                }

                                @media only screen and (max-width: 992px) {
                                    .bit-html section.section.qfy-row-12-5aec7054e2e90354551 {
                                        padding-left: 15px;
                                        padding-right: 15px;
                                        padding-top: 20px;
                                        padding-bottom:;
                                        margin-top: 0;
                                    }
                                }
                            </style>
                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                 style="background-color: #efefef;"></div>

                            <div class="container">
                                <div class="row qfe_row">
                                    <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp"
                                         data-duration="" data-delay=""
                                         class=" qfy-column-17-5aec7054e45a317990 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                         data-dw="1/1" data-fixheight="">
                                        <div style=";position:relative;" class="column_inner ">
                                            <div class=" background-overlay grid-overlay-"
                                                 style="background-color:transparent;width:100%;"></div>
                                            <div class="column_containter" style="z-index:3;position:relative;">
                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                     css_animation_delay="0" qfyuuid="qfy_column_text_7w7z9"
                                                     data-anitime='0.7'
                                                     class="qfy-element qfy-text qfy-text-4022 qfe_text_column qfe_content_element  "
                                                     style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                    <div class="qfe_wrapper">
                                                        <div style="text-align: center;"><span
                                                                style="font-family:raleway;"><span
                                                                style="display: inline-block; line-height: 1.2;"><span
                                                                style="font-size: 13px; color: rgb(0, 0, 0); word-spacing: normal;">Copyright © Stanley Zhai</span></span></span>
                                                        </div>

                                                        <div style="text-align: center;">
                                                            <div style="background-color: rgb(255, 255, 255); text-align: center;">
                                                                <span style="font-family:raleway;"><span
                                                                        style="display: inline-block; line-height: 1.2;"><span
                                                                        style="color: rgb(0, 0, 0);"><span
                                                                        style="font-size: 13px;">info@StanleyZhai.com</span></span></span></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                        .qfy-column-17-5aec7054e45a317990 > .column_inner {
                                            padding-left: 0;
                                            padding-right: 0;
                                            padding-top: 0;
                                            padding-bottom: 0;
                                        }

                                        .qfe_row .vc_span_class.qfy-column-17-5aec7054e45a317990 {
                                        }

                                    ;
                                    }

                                    @media only screen and (max-width: 992px) {
                                        .qfy-column-17-5aec7054e45a317990 > .column_inner {
                                            margin: 0 auto 0 !important;
                                            padding-left: 0;
                                            padding-right: 0;
                                            padding-top:;
                                            padding-bottom:;
                                        }

                                        .display_entire .qfe_row .vc_span_class.qfy-column-17-5aec7054e45a317990 {
                                        }

                                        .qfy-column-17-5aec7054e45a317990 > .column_inner > .background-overlay, .qfy-column-17-5aec7054e45a317990 > .column_inner > .background-media {
                                            width: 100% !important;
                                            left: 0 !important;
                                            right: auto !important;
                                        }
                                    }</style>
                                </div>
                            </div>

                        </section>
                    </div>
                </section>
            </div><!-- .wf-container -->
        </div><!-- .wf-wrap -->
        <!--  ************begin************* -->
        <style type="text/css" id="static-stylesheet-footer">
            #footer.footer .footer1 .widget {
                width: 99%;
            }

            #footer.footer .footer1 .widget.simplepage {
                width: 100%;
            }

            #footer.footer .footer2 .widget {
                width: 99%;
            }

            #footer.footer .footer2 .widget.simplepage {
                width: 100%;
            }

            #footer.footer .footer3 .widget {
                width: 99%;
            }

            #footer.footer .footer3 .widget.simplepage {
                width: 100%;
            }

            .bit_main_content {
                margin-top: 0px;
                margin-bottom: 50px
            }

            @media screen and (min-width: 760px) {
                .bit_main_content {
                    min-height: 100px;
                }
            }
        </style>
        <!--  ************end************* -->
    </footer>


    <style>
        .iphone{
            display: none;
            width: 240px;
            height: 200px;
            border: 1px solid #cfcfcf;
            position: absolute;
            top:0;
            left: -240px;
            background: #fff;
        }
        .iphone .tit{
            height: 40px;padding: 0 20px;font-size: 14px;background:#f7f7f7;border-bottom: 1px solid #cfcfcf;line-height: 40px;font-family: "Microsoft YaHei";color: #616161;
        }
        .iphone ul li{
            font-size: 15px;width: 100%;padding: 0 20px;box-sizing: border-box;
            line-height: 45px;
        }
        .qr_codes{
            display: none;
            width: 200px;
            height: 200px;
            border: 1px solid #cfcfcf;
            position: absolute;
            top:0;
            left: -200px;
            background: #fff;
        }
    </style>
    @include('common.button')
</div>
<a href="#" class="scroll-top displaynone"></a>

</div><!-- #page -->

<link rel='stylesheet' id='qfy-gf-f8ac7b0ae9ec6d45be568052f50b9149-css' href='{{asset("/home/css/786031f43b3b41b190923e0f06009df0.css")}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='qfyuser_jquery_ui_style-css' href='{{asset("/home/css/qfyuser-jquery-ui.css")}}' type='text/css' media='all'/>
<link rel='stylesheet' id='qtip-css' href='{{asset("/home/css/jquery.qtip.min.css")}}' type='text/css' media='all'/>
{{--<link rel='stylesheet' id='iphorm-uniform-theme-66-css' href='http://www.neverends.cncss/default.css?ver=4.851' type='text/css' media='all'/>--}}
<script type='text/javascript'>
    /* <![CDATA[ */
    var thickboxL10n = {
        "next": "\u4e0b\u4e00\u5f20 >",
        "prev": "< \u4e0a\u4e00\u5f20",
        "image": "\u56fe\u7247",
        "of": "\/",
        "close": "\u5173\u95ed",
        "noiframes": "\u8fd9\u4e2a\u529f\u80fd\u9700\u8981iframe\u7684\u652f\u6301\u3002\u60a8\u53ef\u80fd\u7981\u6b62\u4e86iframe\u7684\u663e\u793a\uff0c\u6216\u60a8\u7684\u6d4f\u89c8\u5668\u4e0d\u652f\u6301\u6b64\u529f\u80fd\u3002",
        "loadingAnimation": "\/\/fast.qifeiye.com\/FeiEditor\/bitSite\/images\/preloader.gif"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("/home/js/thickbox.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/qfy_editor_front.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/transition.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/vc_carousel.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/waypoints.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.blockui.min.js")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var dtLocal = {
        "passText": "\u67e5\u770b\u8fd9\u4e2a\u52a0\u5bc6\u8d44\u8baf\uff0c\u8bf7\u5728\u4e0b\u9762\u8f93\u5165\u5bc6\u7801\uff1a",
        "moreButtonAllLoadedText": "\u5168\u90e8\u5df2\u52a0\u8f7d",
        "postID": "13351",
        "ajaxurl": "http:\/\/www.neverends.cn\/admin\/admin-ajax.php",
        "contactNonce": "47909ff2a7",
        "ajaxNonce": "72af02f6a2",
        "pageData": {"type": "page", "template": "page", "layout": null},
        "themeSettings": {"smoothScroll": "on"}
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("/home/js/plugins.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/opentip-jquery.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/language.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/main.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.lazy.min.js")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var iphormL10n = {
        "error_submitting_form": "\u5728\u63d0\u4ea4\u8868\u5355\u65f6\u6709\u9519\u8bef",
        "swfupload_flash_url": "http:\/\/www.neverends.cn\/qfy-includes\/js\/swfupload\/swfupload.swf",
        "swfupload_upload_url": "http:\/\/www.neverends.cn\/?iphorm_swfupload=1",
        "swfupload_too_many": "\u961f\u5217\u4e2d\u7684\u6587\u4ef6\u592a\u591a\u4e86",
        "swfupload_file_too_big": "\u6587\u4ef6\u592a\u5927\u4e86",
        "swfupload_file_empty": "\u4e0d\u80fd\u4e0a\u4f20\u7a7a\u6587\u4ef6",
        "swfupload_file_type_not_allowed": "\u8be5\u6587\u4ef6\u7684\u7c7b\u578b\u662f\u4e0d\u5141\u8bb8\u4e0a\u4f20\u7684",
        "swfupload_unknown_queue_error": "\u672a\u77e5\u961f\u5217\u9519\u8bef\uff0c\u8bf7\u7a0d\u5019\u518d\u8bd5",
        "swfupload_upload_error": "\u4e0a\u4f20\u9519\u8bef",
        "swfupload_upload_failed": "\u4e0a\u4f20\u5931\u8d25",
        "swfupload_server_io": "\u670d\u52a1\u5668IO\u9519\u8bef",
        "swfupload_security_error": "\u5b89\u5168\u9519\u8bef",
        "swfupload_limit_exceeded": "\u4e0a\u4f20\u8d85\u8fc7\u9650\u5236",
        "swfupload_validation_failed": "\u9a8c\u8bc1\u5931\u8d25",
        "swfupload_upload_stopped": "\u4e0a\u4f20\u88ab\u7ec8\u6b62",
        "swfupload_unknown_upload_error": "\u6587\u4ef6\u4e0a\u4f20\u88ab\u610f\u5916\u7ec8\u6b62",
        "plugin_url": "http:\/\/www.neverends.cn\/qfy-content\/plugins\/qfy_form",
        "preview_no_submit": "\u5728\u9884\u89c8\u6a21\u5f0f\u4e0b\u4e0d\u80fd\u63d0\u4ea4\u8868\u5355",
        "iphorm_required": "\u8fd9\u662f\u4e00\u4e2a\u5fc5\u586b\u9879\u54e6"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("/home/js/jquery.iphorm.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.form.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.smooth-scroll.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.qtip.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.uniform.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.infieldlabel.min.js")}}'></script>
<script>
    $('.wechat').hover(function () {
        $('.qr_codes').show();
    },function () {
        $('.qr_codes').hide();
    });
    $('.phone').hover(function () {
        $('.iphone').show();
    },function () {
        $('.iphone').hide();
    })


    //点击放大图片
    $(".bgimages").click(function () {
        var url = $(this).attr('src');
        $(".fiximage img").attr('src',url);
        $(".fiximage").addClass('image_show');
        var width = $(".fiximage").width();
        var height = $(".fiximage").height();
        $(".fiximage").css('margin-top',-height/2);
        $(".fiximage").css('margin-left',-width/2);
        $(".fiximage_back").addClass('image_show');
    });

    $(".fiximage_back").click(function () {
        $(".fiximage").removeClass('image_show');
        $(".fiximage_back").removeClass('image_show');
    });

    $(".fiximage").click(function () {
        $(".fiximage").removeClass('image_show');
        $(".fiximage_back").removeClass('image_show');
    });
</script>
<div style='display:none;'></div>
</body>

</html>
