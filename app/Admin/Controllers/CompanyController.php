<?php

namespace App\Admin\Controllers;

use App\Models\Company;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CompanyController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->edit(1);
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('公司设置');
            $content->description('编辑');

            $content->body($this->form()->edit($id));
        });
    }

    public function update()
    {
        return $this->form()->update(1);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Gene::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Company::class, function (Form $form) {
            $form->setAction('/admin/company/update');

            $form->text('name','公司名称（中文）')->rules('required',['required'=>'公司名称为必填项']);
            $form->text('name_en','公司名称（英文）')->rules('required',['required'=>'公司名称为必填项']);
            $form->text('tel','电话')->rules('required',['required'=>'电话为必填项']);
            $form->text('address','地址')->rules('required',['required'=>'地址为必填项']);
            $form->text('phone','手机')->rules('required',['required'=>'手机为必填项']);
            $form->text('fax','传真')->rules('required',['required'=>'传真为必填项']);
            $form->email('email','邮箱')->rules('required',['required'=>'邮箱为必填项']);
            $form->text('code','邮编')->rules('required',['required'=>'邮编为必填项']);
            $form->image('app_code','APP二维码')->uniqueName()->rules('required',['required'=>'APP二维码为必填项'])->help("建议尺寸为： 150 X 150 ，若不是，图片会被压缩或拉伸");
            $form->image('wechat_code','微信公众号二维码')->uniqueName()->rules('required',['required'=>'微信公众号二维码为必填项'])->help("建议尺寸为： 150 X 150 ，若不是，图片会被压缩或拉伸");
            $form->text('icp','备案号')->help("【选填】不填默认不显示");

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
            $form->tools(function (Form\Tools $tools) {
                // 去掉返回按钮
                $tools->disableBackButton();
                // 去掉跳转列表按钮
                $tools->disableListButton();
            });
        });
    }
}
