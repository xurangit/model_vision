<?php

namespace App\Admin\Controllers;

use App\Models\First;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Widgets\Box;
use Illuminate\Http\Request;

class FirstController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function home()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function index($id = 1)
    {
//        admin_toastr(trans('admin.update_succeeded'));
//        admin_toastr(trans('admin.update_succeeded'));
        return Admin::content(function (Content $content) use ($id) {

            $content->header('首页');
            $content->description('设置');
            $form = $this->form();

            $content->body($form->edit(1));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id = 1)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('首页');
            $content->description('设置');

            $content->body($this->form()->edit($id));
        });
    }

    public function update()
    {
        return $this->form()->update(1);
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(First::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(First::class, function (Form $form) {
            $form->setAction('/admin/first/update');
            $form->text('title','标题');
            $states = [
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '否', 'color' => 'danger'],
            ];
            $form->switch('is_video','是否设置视频')->states($states)->help('是（显示视频） 否（显示图片）按钮要与下方图片/视频链接对应');

//            $form->file('file','图片/视频');
            $form->file('file','图片/视频')->removable();

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
            $form->tools(function (Form\Tools $tools) {
                // 去掉返回按钮
                $tools->disableBackButton();
                // 去掉跳转列表按钮
                $tools->disableListButton();
            });
        });
    }
}
