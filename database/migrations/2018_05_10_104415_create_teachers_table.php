<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->comment('姓名-中文');
            $table->string('name_en')->comment('姓名-英文');
            $table->text('introduce')->comment('简介-中文');
            $table->text('introduce_en')->comment('简介-英文');
            $table->string('image')->comment('教师照片');
            $table->integer('sort')->default(10)->comment("排序");
            $table->tinyInteger('is_show')->default(1)->comment("是否显示（1-显示-默认 0-不显示）");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
