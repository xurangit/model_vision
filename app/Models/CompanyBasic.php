<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyBasic extends Model
{
    protected $table = "company_basic";
}
