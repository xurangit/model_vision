<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'     => config('admin.route.prefix'),
    'namespace'  => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index');

    $router->resource('category', CategoryController::class);//分类
    $router->resource('first', FirstController::class);//首页
    $router->resource('gene', GeneController::class);//基因故事
    $router->resource('story_list', StoryController::class);//基因故事 -- 列表
    $router->resource('teacher', TeacherController::class);//导师
    $router->resource('student', StudentController::class);//优秀学员
    $router->resource('student_ordinary', StudentOrdinaryController::class);//普通学员
    $router->resource('course', CourseController::class);//课程
    $router->resource('company', CompanyController::class);//公司设置
    $router->resource('company_basic', CompanyBasicController::class);//公司设置
    $router->resource('tourist', TouristController::class);//提交的
    $router->resource('subject_basic', SubjectBasicController::class);//专题 - 基本
    $router->resource('subject', SubjectController::class);//专题 - 列表
});
