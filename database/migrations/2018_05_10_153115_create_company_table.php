<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->comment('公司名称-中文');
            $table->string('name_en',255)->comment('公司名称-英文');
            $table->string('tel',255)->comment('电话');
            $table->string('phone',255)->comment('手机');
            $table->string('fax',255)->comment('传真');
            $table->string('email',255)->comment('邮箱');
            $table->string('code',255)->comment('邮编');
            $table->string('app_code',255)->comment('app二维码');
            $table->string('wechat_code',255)->comment('微信公众号二维码');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
