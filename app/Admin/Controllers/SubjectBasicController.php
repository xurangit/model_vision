<?php

namespace App\Admin\Controllers;

use App\Models\SubjectBasic;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SubjectBasicController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->edit(1);
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    public function update()
    {
        return $this->form()->update(1);
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('超模专题');
            $content->description('基本');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(SubjectBasic::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(SubjectBasic::class, function (Form $form) {

            $form->setAction('/admin/subject_basic/update');
//            $form->display('id', 'ID');
//            $cates = Category::all()->toArray();
//            $catesNew = [];
//            foreach ($cates as $key => $value) {
//                $catesNew[$value['id']] = $value['name'];
//            }
//            $form->select('cate_id', '分类')->options($catesNew);
//            $form->text('title','标题（中文）');
//            $form->text('title_en','标题（英文）');
//            $form->textarea('name','名称（中文）')->rows(2);
//            $form->textarea('name_en','名称（英文）')->rows(2);

            $states = [
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '否', 'color' => 'danger'],
            ];
            $form->switch('is_video','是否设置视频')->states($states)->help('是（显示视频） 否（显示图片）按钮要与下方图片/视频链接对应,当选择视频时，前台不展示标题和名称');

            $form->file('file','图片/视频')->uniqueName();

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
            $form->tools(function (Form\Tools $tools) {
                // 去掉返回按钮
                $tools->disableBackButton();
                // 去掉跳转列表按钮
                $tools->disableListButton();
            });
        });
    }
}
