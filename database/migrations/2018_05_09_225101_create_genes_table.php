<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255)->comment('标题-中文');
            $table->text('title_en')->comment('标题-英文');
            $table->text('name')->comment('名称-中文');
            $table->text('name_en')->comment('名称-英文');
            $table->text('content')->comment('内容-中文');
            $table->text('content_en')->comment('内容-英文');
            $table->text('file')->comment('图片/视频路径');
            $table->tinyInteger('is_video')->default(0)->comment("是否是视频（1-是 0-不是-默认）");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genes');
    }
}
