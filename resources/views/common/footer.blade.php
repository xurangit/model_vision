<section data-fixheight=""
         class="qfy-row-2-5aec7054bd876386281 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
         id=""
         style='margin-bottom:0;border-radius:0px;color:#ffffff;'>
    <style class="row_class qfy_style_class">
        @media only screen and (min-width: 991px) {
            section.section.qfy-row-2-5aec7054bd876386281 {
                padding-left: 0;
                padding-right: 0;
                padding-top: 20px;
                padding-bottom: 0;
                margin-top: 89px;
            }

            section.section.qfy-row-2-5aec7054bd876386281 > .container {
                max-width: 1500px;
                margin: 0 auto;
            }
        }

        @media only screen and (max-width: 992px) {
            .bit-html section.section.qfy-row-2-5aec7054bd876386281 {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 20px;
                padding-bottom: 0;
                margin-top: 0;
                min-height: 0;
            }
        }

    </style>
    <!--<div class="section-background-overlay background-overlay grid-overlay-0 " style="background-color: #252525;"></div>-->
    <div class="section-background-overlay background-overlay grid-overlay-0 "
         style="background-color: #212225;"></div>

    <div class="container" style="max-width: 1660px;">
        <div class="row qfe_row">
            <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                 data-duration="" data-delay=""
                 class=" qfy-column-2-5aec7054bea2f293948 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                 data-dw="1/1" data-fixheight="">
                <div style=";position:relative;" class="column_inner ">
                    <div class=" background-overlay grid-overlay-"
                         style="background-color:transparent;width:100%;"></div>
                    <div class="column_containter"
                         style="z-index:3;position:relative;">
                        <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                             css_animation_delay="0"
                             qfyuuid="qfy_column_text_g1r1f" data-anitime='1'
                             class="qfy-element qfy-text qfy-text-11444 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_left-to-right "
                             style="position: relative;line-height:1.5em;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                            <div class="qfe_wrapper">
                                <div style="position: relative;">
                                    <div class="col-lg-4 col-md-4"  style="min-width: 286px;float: left;text-align: center;margin-bottom: 15px;max-width: 286px;">
                                        <img src="{{asset('/home/images/footer_logo.png')}}"
                                             alt="">
                                    </div>
                                    <div class="col-lg-4 col-md-4"  style="max-width: 500px;float: left;margin-bottom: 15px">
                                        <div>
                                            <span>联系我们</span>
                                            <span>Contact Us</span>
                                        </div>
                                        <span>
                                            @if($lang == 'en')
                                                Addr
                                                @else
                                                地址
                                            @endif：</span><span>{{$company['address']}}</span></br>
                                        <span>
                                            @if($lang == 'en')
                                                Phone
                                            @else
                                                电话
                                            @endif：</span><span>{{$company['tel']}}</span></br>
                                        <span>
                                            @if($lang == 'en')
                                                Email
                                            @else
                                                邮箱
                                            @endif：</span><span>{{$company['email']}}</span>
                                    </div>
                                    <!--                                                                  <div style="max-width: 286px;float: left;">
                                                                                                          <img src="images/footer_logo.png"
                                                                                                               alt="">
                                                                                                      </div>-->
                                    <div  class="col-lg-4 col-md-4"  style="max-width: 286px;float: left;margin-bottom: 15px">
                                        <ul class="qr_code">
                                            <li style="display: inline-block">
                                                <img src="/uploads/{{$company['app_code']}}" width="108px" height="108px" alt="">
                                                <div class="span" style="text-align: center">
                                                    @if($lang == 'en')
                                                        App
                                                    @else
                                                        App下载
                                                    @endif
                                                </div>
                                            </li>
                                            <li style="display: inline-block;margin-left: 35px">
                                                <img src="/uploads/{{$company['wechat_code']}}" width="108px" height="108px" alt="">
                                                <div class="span" style="text-align: center">
                                                    @if($lang == 'en')
                                                        WeChat
                                                    @else
                                                        微信公众号
                                                    @endif</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--<div>666666666</div>-->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 备案 -->
    <div class="container" style="text-align: center;margin-top: 25px">
        <div style="margin-bottom: 60px">
            <span class=" col-xs-12 " >
                @if($lang == 'en')
                    Copyright@ {{$company['name_en']}}
                    @else
                    版权所有@ {{$company['name']}}
                @endif
            </span>
            @if($company['icp'] != '')
            <span class=" col-xs-12 " >
                    @if($lang == 'en')
                    ICP
                @else
                    备案编号
                @endif：{{$company['icp']}}
            </span>
            @endif
        </div>
    </div>
    <!--                                <div class="container" style="margin-top: 15px">
                                        <div class="row qfe_row">
                                            <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                 data-duration="" data-delay=""
                                                 class=" qfy-column-2-5aec7054bea2f293948 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                                 data-dw="1/1" data-fixheight="">
                                                <div style=";position:relative;" class="column_inner ">
                                                    <div class=" background-overlay grid-overlay-"
                                                         style="background-color:transparent;width:100%;"></div>
                                                    <div class="column_containter"
                                                         style="z-index:3;position:relative;text-align: center">
                                                        版权所有杭州傅氏文化创意有限公司@备案/许可证编号为:浙CP备16023481浙公网安备33010802006444号
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>-->

</section>