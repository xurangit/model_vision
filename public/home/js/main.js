//悬浮效果
(function($){$.fn.floatAd=function(l){var m={customhtml:"",close:1,closeHTML:"",speed:30,id_class:"",x:"0",y:"0"};var n=false;var l=$.extend(m,l);var o="<div id='qfy_float_ad' class='"+l.id_class+"' style='position:absolute;left:0px;top:0px;z-index:1000000;cleat:both;'>";o+=l.customhtml;if(l.close=="1"){if(l.closeHTML==""){o+="<div id='qfy_close_f_ad' class='"+l.id_class+"' style='position:absolute;width:30px;height:16px;top:-18px;right:0px;cursor:pointer;float:right;font-size:14px'>关闭</div></div>"}else{o+="<div id='qfy_close_f_ad' class='"+l.id_class+"' >"+l.closeHTML+"</div></div>"}}$('body').append(o);function qfy_ad_init(){var x=l.x,y=l.y+$(window).scrollTop();var g=true,yin=true;var h=1;var i=10;var j=$("#qfy_float_ad."+l.id_class);var k=function(){var L=0,T=$(window).scrollTop();var a=j.width();var b=j.height();var c=$(window).width();var d=$(window).height()+$(window).scrollTop();x=x+h*(g?1:-1);if(x<L){g=true;x=L}if(x>c-a-1){g=false;x=c-a-1}y=y+h*(yin?1:-1);if(y>d-b-10){yin=false;y=d-b-10}if(y<T){yin=true;y=T}var e=x;var f=y;j.css({'top':f,'left':e})};n=setInterval(k,l.speed);$('#qfy_float_ad.'+l.id_class).mouseover(function(){if(n){clearInterval(n)}});$('#qfy_float_ad.'+l.id_class).mouseout(function(){n=setInterval(k,l.speed)})}qfy_ad_init();$('#qfy_close_f_ad.'+l.id_class).click(function(){$('#qfy_float_ad.'+l.id_class+',#QFY_overlay.'+l.id_class).remove();clearInterval(n)})}})(jQuery);

//背景图片自适应
//https://css-tricks.com/almanac/properties/o/object-fit/
(function($){function coverFillSwitch(container,img,invert){if(!container||!img){return false}var imgHeight=img.naturalHeight||img.videoHeight;var imgWidth=img.naturalWidth||img.videoWidth;var containerRatio=container.offsetWidth/container.offsetHeight;var imgRatio=imgWidth/imgHeight;var ratioComparison=false;if(imgRatio>=containerRatio){ratioComparison=true}if(invert){ratioComparison=!ratioComparison}if(ratioComparison){img.style.height="100%";img.style.width="auto"}else{img.style.height="auto";img.style.width="100%"}}function objectFitResize(){var i,img,container;var imgsCover=document.getElementsByClassName("section-background-video");for(i=0;i<imgsCover.length;i++){img=imgsCover[i];container=img.parentElement;if(container.classList.contains("background-media")){coverFillSwitch(container,img)}}}function applyStandardProperties(container,img){var containerStyle=window.getComputedStyle(container);if(containerStyle.overflow!=="hidden"){container.style.overflow="hidden"}if(containerStyle.position!=="relative"&&containerStyle.position!=="absolute"&&containerStyle.position!=="fixed"){container.style.position="relative"}img.style.position="absolute";img.style.top="50%";img.style.left="50%";img.style.transform="translate(-50%,-50%)"}function objectFitInt(){var imgs=document.getElementsByClassName("section-background-video");for(var i=0;i<imgs.length;i++){var type="cover";var img=imgs[i];var container=img.parentElement;switch(type){case"container":break;case"cover":coverFillSwitch(container,img);applyStandardProperties(container,img);break;case"contain":coverFillSwitch(container,img,true);applyStandardProperties(container,img);break;case"fill":img.style.height="100%";img.style.width="100%";applyStandardProperties(container,img);break;case"none":img.style.height="auto";img.style.width="auto";applyStandardProperties(container,img);break;case"scale-down":img.style.maxHeight="100%";img.style.maxWidth="100%";img.style.height="auto";img.style.width="auto";applyStandardProperties(container,img);break;default:break}}}var resizeTimeout;function resizeThrottler(){if(!resizeTimeout){resizeTimeout=setTimeout(function(){resizeTimeout=null;objectFitResize()},66)}}if("objectFit" in document.documentElement.style===false&&top==self){window.addEventListener("load",objectFitInt,false);window.addEventListener("resize",resizeThrottler,false)}})(jQuery);
window.onload = function() {
    if(!document.hasOwnProperty("ontouchstart")) {
    	 jQuery("html").addClass("no-touch");
    }
};
function resetSectionHeight() {
	var h = jQuery(window).height();
	var body_w = jQuery("body").width();
	if (body_w > 760) {
		if (jQuery(".section.minperheight100:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight100:not(.fixheight)").css("min-height", h + "px");
		}
		if (jQuery(".section.minperheight90:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight90:not(.fixheight)").css("min-height", 0.9 * h + "px");
		}
		if (jQuery(".section.minperheight80:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight80:not(.fixheight)").css("min-height", 0.8 * h + "px");
		}
		if (jQuery(".section.minperheight70:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight70:not(.fixheight)").css("min-height", 0.7 * h + "px");
		}
		if (jQuery(".section.minperheight60:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight60:not(.fixheight)").css("min-height", 0.6 * h + "px");
		}
		if (jQuery(".section.minperheight50:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight50:not(.fixheight)").css("min-height", 0.5 * h + "px");
		}
		if (jQuery(".section.minperheight40:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight40:not(.fixheight)").css("min-height", 0.4 * h + "px");
		}
		if (jQuery(".section.minperheight30:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight30:not(.fixheight)").css("min-height", 0.3 * h + "px");
		}
		if (jQuery(".section.minperheight20:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight20:not(.fixheight)").css("min-height", 0.2 * h + "px");
		}
		if (jQuery(".section.minperheight15:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight15:not(.fixheight)").css("min-height", 0.15 * h + "px");
		}
		if (jQuery(".section.minperheight10:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight10:not(.fixheight)").css("min-height", 0.1 * h + "px");
		}
		if (jQuery(".section.minperheight5:not(.fixheight)").length > 0) {
			jQuery(".section.minperheight5:not(.fixheight)").css("min-height", 0.05 * h + "px");
		}
		jQuery(".section.fixheight").each(function() {
			var fixheight = jQuery(this).attr("data-fixheight");
			var w = jQuery(this).width();
			if (w > 0) {
				var h = w * fixheight;
				jQuery(this).height(h);
				jQuery(this).css("min-height", h + "px");
			}
		});
		jQuery(".qfy-column-inner.fixheight").each(function() {
			var fixheight = jQuery(this).attr("data-fixheight");
			var w = jQuery(this).width();
			if (w > 0) {
				var h = w * fixheight;
				if (jQuery(this).closest(".section.fixheight").length > 0) {
					h = jQuery(this).closest(".section.fixheight").height();
				}
				jQuery(this).height(h);
				jQuery(this).css("min-height", h + "px");
				jQuery(this).find(">.column_inner").css("min-height", h + "px");
			}
		})
	} else {
		if (jQuery(".section.minperheight100").length > 0) {
			jQuery(".section.minperheight100").css("min-height", h + "px");
		}
		if (jQuery(".section.minperheight90").length > 0) {
			jQuery(".section.minperheight90").css("min-height", 0.9 * h + "px");
		}
		if (jQuery(".section.minperheight80").length > 0) {
			jQuery(".section.minperheight80").css("min-height", 0.8 * h + "px");
		}
		if (jQuery(".section.minperheight70").length > 0) {
			jQuery(".section.minperheight70").css("min-height", 0.7 * h + "px");
		}
		if (jQuery(".section.minperheight60").length > 0) {
			jQuery(".section.minperheight60").css("min-height", 0.6 * h + "px");
		}
		if (jQuery(".section.minperheight50").length > 0) {
			jQuery(".section.minperheight50").css("min-height", 0.5 * h + "px");
		}
		if (jQuery(".section.minperheight40").length > 0) {
			jQuery(".section.minperheight40").css("min-height", 0.4 * h + "px");
		}
		if (jQuery(".section.minperheight30").length > 0) {
			jQuery(".section.minperheight30").css("min-height", 0.3 * h + "px");
		}
		if (jQuery(".section.minperheight20").length > 0) {
			jQuery(".section.minperheight20").css("min-height", 0.2 * h + "px");
		}
		if (jQuery(".section.minperheight15").length > 0) {
			jQuery(".section.minperheight15").css("min-height", 0.15 * h + "px");
		}
		if (jQuery(".section.minperheight10").length > 0) {
			jQuery(".section.minperheight10").css("min-height", 0.1 * h + "px");
		}
		if (jQuery(".section.minperheight5").length > 0) {
			jQuery(".section.minperheight5").css("min-height", 0.05 * h + "px");
		}
	}
}

var last_size_mobile = false;
var size_mobile = false;
function resizeDefaultObjSize(){
	if(dtGlobals.isMobile==false){
		 if(jQuery("body").width()<768){
			 size_mobile = "mobile";
			 if(!last_size_mobile) last_size_mobile ="mobile";
		 }else{
			 size_mobile = "pc";
			 if(!last_size_mobile) last_size_mobile ="pc";
		 }
		 if(size_mobile!=last_size_mobile){
			 last_size_mobile = size_mobile;
			 jQuery(".qfy-element").each(function(){
					var m_padding = jQuery(this).attr("m-padding");
					var p_padding = jQuery(this).attr("p-padding");
					if(size_mobile=="mobile"){
						if(m_padding ){
							jQuery(this).css("padding",m_padding);
						}
					}else{
						if(p_padding ){
							jQuery(this).css("padding",p_padding);
						}
					}

			 });
			
		 }
	}
	
	
	if(jQuery(".qfe_map_wraper iframe").length>0){
			jQuery(".qfe_map_wraper iframe").each(function(){
				var oh = jQuery(this).parent().attr("style");
				if(typeof(oh) =="undefined"){
					var width = jQuery(this).width();
					if(width>0){
						jQuery(this).parent().height((width*2/4)+"px");
					}
				}
			})
		}
		if(jQuery(".vc_bit_raw_video").length>0){
			jQuery(".vc_bit_raw_video").each(function(){
				var oh = jQuery(this).attr("style");
				if(typeof(oh) =="undefined"){
					var width = jQuery(this).width();
					if(width>0){
						jQuery(this).height((width*2/4)+"px");
					}  
				}
			})
		}
		resetSectionHeight();
		column_init_align();
}
function resize_royalSlider_gallery_new(org_obj){
	jQuery(".qfy-tabcontent .royalSlider_gallery_new").each(function(){
		var obj = jQuery(this);
		setTimeout(function(){
			var h1 = obj.find(".rsOverflow").height();
			var h2 = obj.find(".rsSlide.rsActiveSlide").height();
			if(h2>0 && h1!=h2){
				obj.find(".rsOverflow").height(h2);
			}
		},200);
	})
	
}
function searchForm(obj){
	if(typeof(obj)=="undefined"){
		obj = jQuery('.site_tooler .searchform .submit,.search_contain .searchform .submit');
	}
	
	/*--search*/
	obj.unbind().on('click', function(e) {
		e.preventDefault();
		jQuery(this).siblings('input.searchsubmit').click();
		return false;
	});

}
function qfy_setCookie(c_name, value, exdays) {
	  var exdate = new Date();
	  exdate.setDate(exdate.getDate() + exdays);
	  var c_value = encodeURIComponent(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
	  document.cookie = c_name + "=" + c_value;
	}
function mobile_menu_fix(){
	var top_scrTop = 0,top_scrDir = 0,top_scrUp = false,top_scrDown = false,top_isMoved = false;
	var top_threshold = jQuery("#dl-menu").offset().top + jQuery("#dl-menu").height();
	
	jQuery(window).on("scroll", function() {
		var top_tempCSS = {},top_tempScrTop = jQuery(window).scrollTop();

		top_scrDir = top_tempScrTop - top_scrTop;
		
		if (top_tempScrTop > top_threshold && top_isMoved === false) {
			top_isMoved = true;
			jQuery("#dl-menu").addClass("positionFixed");
		}
		else if (top_tempScrTop <= top_threshold && top_isMoved === true) {
			top_isMoved = false;
			jQuery("#dl-menu").removeClass("positionFixed");
		};
		top_scrTop = jQuery(window).scrollTop();
		
	});
}
function mobile_menu_fix_2(){
	var top_scrTop = 0,top_scrDir = 0,top_scrUp = false,top_scrDown = false,top_isMoved = false;
	var top_threshold = jQuery(".dl-menu-fixedheader").height();

	jQuery(window).on("scroll", function() {
		var top_tempCSS = {},top_tempScrTop = jQuery(window).scrollTop();

		top_scrDir = top_tempScrTop - top_scrTop;
		
		if (top_tempScrTop > top_threshold && top_isMoved === false) {
			top_isMoved = true;
			jQuery(".dl-menu-fixedheader").css("position","fixed");
			jQuery("body").addClass("fixedheadering");
		}
		else if (top_tempScrTop <= top_threshold && top_isMoved === true) {
			top_isMoved = false;
			jQuery(".dl-menu-fixedheader").css("position","relative");
			jQuery("body").removeClass("fixedheadering");
		};
		top_scrTop = jQuery(window).scrollTop();
		
	});
}
function _image_popup_flexslider(startAt){
	jQuery(".image_popup .qfe_flexslider").flexslider({
	       animation: "slide",
	       slideshow: false,
	       slideshowSpeed: 10000,
	       sliderSpeed: 800,
	       controlNav: 1,
	       directionNav: 1,
	       smoothHeight: true,
		   startAt:startAt,
		   start: function(){
			   var localvideo =  jQuery(".image_popup .flex-active-slide video.localvideo");
			   if(localvideo.length>0){
				   if(localvideo.get(0).currentTime==0){
					   jQuery(".image_popup .flex-active-slide video.localvideo").get(0).play();
				   }
			   }
			  var videoiframe =  jQuery(".image_popup .flex-active-slide iframe.media-cloud-iframe");
			  if(videoiframe.length>0 && !videoiframe.attr("src") ){
				  videoiframe.attr("src",videoiframe.attr("data-src"));
			  }
		   },
		   before: function(){
			   //var localvideo =  jQuery(".image_popup .flex-active-slide video.localvideo");
			   //if(localvideo.length>0){
					 //jQuery(".image_popup .flex-active-slide video.localvideo").get(0).stop();
			  // }
		   },  
		   after: function(){
			   var localvideo =  jQuery(".image_popup .flex-active-slide video.localvideo");
			   if(localvideo.length>0){
				   if(localvideo.get(0).currentTime==0){
					   jQuery(".image_popup .flex-active-slide video.localvideo").get(0).play();
				   }
			   }
			   var videoiframe =  jQuery(".image_popup .flex-active-slide iframe.media-cloud-iframe");
			   if(videoiframe.length>0 && !videoiframe.attr("src") ){
				  videoiframe.attr("src",videoiframe.attr("data-src"));
			   }
		   },
	     });
}
function initmouseover(){
	
	jQuery(".mouseHover").live({
	  mouseenter: function() {
		jQuery(this).addClass("hover");
		
	  },
	  mouseleave: function() {
		jQuery(this).removeClass("hover");
	  }
	});
	jQuery("a.bitButton").live({
		  mouseenter: function() {
			  	var delay = jQuery(this).attr("delay");
				var str = "";
				if(delay &&delay!="0"){
					str = "all "+delay+" linear";
				}
			  	var texthovercolor=jQuery(this).attr("texthovercolor");
				if(texthovercolor){
					jQuery(this).find(".iconText").css("color",texthovercolor);
					jQuery(this).find(".iconText").css("transition",str);
				}
				var iconhovercolor=jQuery(this).attr("iconhovercolor");
				if(iconhovercolor){
					jQuery(this).find("i.glyphicon").css("color",iconhovercolor);
					jQuery(this).find("i.glyphicon").css("transition",str);
				}
				var backgroundhovercolor=jQuery(this).attr("backgroundhovercolor");
				if(backgroundhovercolor){
					jQuery(this).css("background",backgroundhovercolor);
					jQuery(this).css("transition",str);
					jQuery(this).removeAttr('onmouseover');
					jQuery(this).removeAttr('onmouseout');
				}
				var borderhovercolor=jQuery(this).attr("borderhovercolor");
				if(borderhovercolor){
					jQuery(this).css("border","1px solid "+borderhovercolor);
					jQuery(this).css("transition",str);
				}
		  },
		  mouseleave: function() {
			   jQuery(this).css("transition","");
			  	var textcolor=jQuery(this).attr("textcolor");
				if(textcolor){
					jQuery(this).find(".iconText").css("color",textcolor);
				}
				var iconcolor=jQuery(this).attr("iconcolor");
				if(iconcolor){
					jQuery(this).find("i.glyphicon").css("color",iconcolor);
				}
				var backgroundcolor=jQuery(this).attr("backgroundcolor");
				if(backgroundcolor){
					jQuery(this).css("background",backgroundcolor);
				}
				var bordercolor=jQuery(this).attr("bordercolor");
				if(bordercolor){
					jQuery(this).css("border-color",bordercolor);
				}
		  }
		});
	
	jQuery("a.bitIcon").live({
		  mouseenter: function() {
			  	var delay = jQuery(this).attr("delay");
				var str = "";
				if(delay &&delay!="0"){
					str = "all "+delay+" linear";
				}
			  	var texthovercolor=jQuery(this).attr("texthovercolor");
				if(texthovercolor){
					jQuery(this).find(".iconText").css("color",texthovercolor);
					jQuery(this).find(".iconText").css("transition",str);
				}
				var iconhovercolor=jQuery(this).attr("iconhovercolor");
				if(iconhovercolor){
					jQuery(this).find("i.glyphicon").css("color",iconhovercolor);
					jQuery(this).find("i.glyphicon").css("transition",str);
				}
				var backgroundhovercolor=jQuery(this).attr("backgroundhovercolor");
				if(backgroundhovercolor){
					jQuery(this).find("b").css("background",backgroundhovercolor);
					jQuery(this).find("b").css("transition",str);
					jQuery(this).find("b").removeAttr('onmouseover');
					jQuery(this).find("b").removeAttr('onmouseout');
				}
				var borderhovercolor=jQuery(this).attr("borderhovercolor");
				if(borderhovercolor){
					jQuery(this).find("b").css("border","1px solid "+borderhovercolor);
					jQuery(this).find("b").css("transition",str);
				}
		  },
		  mouseleave: function() {
			   jQuery(this).css("transition","");
			  	var textcolor=jQuery(this).attr("textcolor");
				if(textcolor){
					jQuery(this).find(".iconText").css("color",textcolor);
				}
				var iconcolor=jQuery(this).attr("iconcolor");
				if(iconcolor){
					jQuery(this).find("i.glyphicon").css("color",iconcolor);
				}
				var backgroundcolor=jQuery(this).attr("backgroundcolor");
				if(backgroundcolor){
					jQuery(this).find("b").css("background",backgroundcolor);
				}
				var bordercolor=jQuery(this).attr("bordercolor");
				if(bordercolor){
					jQuery(this).find("b").css("border-color",bordercolor);
				}
		  }
		});
	jQuery("a.qfy_popup").live({
		click: function(e) {
			 e.preventDefault();
			 e.stopPropagation();
			 var popupmodel =  jQuery(this).attr("data-popup-model");
			 var popupstyle =  jQuery(this).attr("data-popup-style");
			 var popupsize =  jQuery(this).attr("data-popup-size");
			 var bodywidth = jQuery("body").width();
			 var bodyheight = jQuery(window).height();

			 if( popupstyle=="0" || popupstyle=="1"   ){
				 if(popupsize=="0.8"){
					var width =  bodywidth*0.8;
					var height = bodyheight*0.8;
					var toppx = bodyheight*0.1;
					var leftpx = bodywidth*0.1;
				 }else if(popupsize=="0.6"){
					var width =  bodywidth*0.6;
					var height = bodyheight*0.6;
					var toppx = bodyheight*0.2;
					var leftpx = bodywidth*0.2;
				 }else if(popupsize=="0.4"){
					 var width =  bodywidth*0.4;
					 var height = bodyheight*0.4;
					 var toppx = bodyheight*0.3;
				    var leftpx = bodywidth*0.3;
				 }
			 }else  if(  popupstyle=="2"){
				 if(popupsize=="0.8"){
					var width = bodywidth*0.5;
					var height = bodyheight*0.5;
					var toppx = bodyheight*0.25;
					var leftpx = bodywidth*0.25;
				 }else if(popupsize=="0.6"){
					var width =  bodywidth*0.3;
					var height = bodyheight*0.5;
					var toppx = bodyheight*0.25;
					var leftpx = bodywidth*0.35;
				 }else if(popupsize=="0.4"){
					 var width = bodywidth*0.2;
					 var height =bodyheight*0.5;
					 var toppx = bodyheight*0.25;
				    var leftpx = bodywidth*0.4;
				 }
			 }
			 var defaultpadding="padding:40px;";
			 if(width<480){
				 width = 320;
				 leftpx = (bodywidth-320)*0.5;
				 if(popupstyle=="1") popupstyle="2";
				 if(popupstyle!="2"){
					 height = 250;
					 toppx = (bodyheight-250)*0.5;
				 }
				 defaultpadding = "padding:10px;";
			 }
			 var  allmessage = "";
			 var startAt = 0;
			 if(popupmodel=="0"){
				 var default_img = jQuery(this).attr("data-href");
				 var title = jQuery(this).attr("data-ptitle");
				 var subtitle =  jQuery(this).attr("data-subtitle");
				 var desc = jQuery(this).attr("data-desc");
				 var isvideo =   jQuery(this).closest(".qfy_item_post").find("video.qfyvideo").length;
				 var isyunvideo =   jQuery(this).closest(".qfy_item_post").find(".video_play >iframe").length;
				 var video_html = "";
				 var videoclass =  "";
				 if(isvideo){
					 videoclass = "video";
					 var video_html = jQuery(this).closest(".qfy_item_post").find("video.qfyvideo").prop("outerHTML");
					 video_html = jQuery(video_html).attr("controls","controls").attr("class","localvideo simple").attr("style","width:100%;height:100%;background:#000;").prop("outerHTML");
					 
				 }else if(isyunvideo){
					 videoclass = "video";
					 var video_html = jQuery(this).closest(".qfy_item_post").find(".video_play >iframe").prop("outerHTML");
					 var videoheight = height;
					 if(popupstyle=="2"){
						 videoheight = 0.6*height;
					 }
					 video_html = jQuery(video_html).attr("data-height",videoheight).attr("data-autoplay","true").attr("src", jQuery(video_html).attr("data-src")).prop("outerHTML");
				 }
				 if(default_img || video_html){
					 var message = "";
					 if( popupstyle=="0" ){
						 message = '<div class="pop_image pop_image1 '+videoclass+'" style="width:'+width+'px;word-wrap: break-word;height:'+height+'px;background-image:url('+default_img+');background-size:cover;background-position:center center;background-repeat:no-repeat;">'+video_html+'<div  class="content_inner"  style="box-sizing:border-box;position:absolute;bottom:0;width:100%;background:rgba(0,0,0,0.6);padding:20px 15px;text-align:left;"><div class="head"  style="color:#fff;font-size:16px;padding-bottom:10px;">'+title+'</div><div class="content" style="color:#ccc;font-size:14px;">'+desc+'</div></div></div>';
					 } else if( popupstyle=="1"  ){
						 message = '<div class="pop_image pop_image2 '+videoclass+'" style="width:'+width+'px;word-wrap: break-word;height:'+height+'px;"><div style="background-image:url('+default_img+');background-size:cover;background-position:center center;background-repeat:no-repeat;width:60%;height:100%;float:left;">'+video_html+'</div><div class="content_inner"  style="box-sizing:border-box;float:left;width:40%;height:100%;background:#fff;padding:20px 15px;text-align:left;"><div class="head"  style="color:#333;font-size:16px;padding-bottom:10px;">'+title+'</div><div class="subhead"  style="color:#999;font-size:14px;padding-bottom:10px;">'+subtitle+'</div><div class="content overflowy" style="color:#666;font-size:14px;overflow-y: auto;;">'+desc+'</div></div> </div>';
				 	 }	else if( popupstyle=="2"  ){
						 message = '<div class="pop_image pop_image3 '+videoclass+'" style="width:'+width+'px;word-wrap: break-word;height:'+height+'px;"><div style="background-image:url('+default_img+');background-size:cover;background-position:center center;background-repeat:no-repeat;width:100%;height:60%;">'+video_html+'</div><div class="content_inner" style="box-sizing:border-box;float:left;width:100%;height:40%;background:#fff;'+defaultpadding+'text-align:left;"><div class="head"  style="color:#333;font-size:16px;padding-bottom:10px;">'+title+'</div><div class="subhead"  style="color:#999;font-size:14px;padding-bottom:10px;">'+subtitle+'</div><div class="content overflowy" style="color:#666;font-size:14px;overflow-y: auto;;">'+desc+'</div></div>  </div>';
					 }
					 allmessage = '<div class="image_popup" style="position:relative;">'+message+'<div class="block-close" style="position: absolute;top: 2px;right: 9px;color: rgb(204, 204, 204);cursor: pointer;">✕</div></div>';
				 }	 

			 }else{
				 var p = jQuery(this).closest(".qfy-element");
				 var $thispost =jQuery(this).closest(".qfy_item_post");
				
				 var message = "";
				 p.find(".qfy_item_post:visible").each(function(i){
					 if(jQuery(this)[0]===$thispost[0]){
						 startAt = i;
					 }
					 $this = jQuery(this).find("a.qfy_popup:first");
					 var default_img =$this.attr("data-href");
					 var title = $this.attr("data-ptitle");
					 var subtitle = $this.attr("data-subtitle");
					 var desc =$this.attr("data-desc");
					 var video_html = "";
					 var isvideo =   jQuery(this).find("video.qfyvideo").length;
					 var isyunvideo =   jQuery(this).closest(".qfy_item_post").find(".video_play >iframe").length;
					 var videoclass =  "";
					 if(isvideo){
						 var video_html = jQuery(this).find("video.qfyvideo").prop("outerHTML");
						 video_html =jQuery(video_html).attr("controls","controls").attr("class","localvideo").attr("style","width:100%;height:100%;background:#000;").prop("outerHTML");
						 videoclass = "video";
					 }else if(isyunvideo){
						 var video_html = jQuery(this).closest(".qfy_item_post").find(".video_play >iframe").prop("outerHTML");
						 videoclass = "video";
						 var videoheight = height;
						 if(popupstyle=="2"){
							 videoheight = 0.6*height;
						 }
						 video_html = jQuery(video_html).attr("data-height",videoheight).attr("data-autoplay","true").prop("outerHTML");
						
					 }
					 if(default_img || video_html){
						 if( popupstyle=="0" ){
							 message += '<li><div class="pop_image pop_image1 '+videoclass+'" style="position:relative;word-wrap: break-word;width:'+width+'px;height:'+height+'px;background-image:url('+default_img+');background-size:cover;background-position:center center;background-repeat:no-repeat;">'+video_html+'<div  class="content_inner"  style="box-sizing:border-box;position:absolute;bottom:0;width:100%;background:rgba(0,0,0,0.6);padding:20px 15px;text-align:left;"><div class="head"  style="color:#fff;font-size:16px;padding-bottom:10px;">'+title+'</div><div class="content" style="color:#ccc;font-size:14px;">'+desc+'</div></div></div></li>';
						 } else if( popupstyle=="1"  ){
							 message += '<li><div class="pop_image pop_image2 '+videoclass+'" style="position:relative;word-wrap: break-word;width:'+width+'px;height:'+height+'px;"><div style="background-image:url('+default_img+');background-size:cover;background-position:center center;background-repeat:no-repeat;width:60%;height:100%;float:left;">'+video_html+'</div><div class="content_inner"  style="box-sizing:border-box;float:left;width:40%;height:100%;background:#fff;padding:20px 15px;text-align:left;"><div class="head"  style="color:#333;font-size:16px;padding-bottom:10px;">'+title+'</div><div class="subhead"  style="color:#999;font-size:14px;padding-bottom:10px;">'+subtitle+'</div><div class="content overflowy" style="color:#666;font-size:14px;overflow-y: auto;;">'+desc+'</div></div> </div></li>';
					 	 }	else if( popupstyle=="2"  ){
							 message += '<li><div class="pop_image pop_image3 '+videoclass+'" style="position:relative;word-wrap: break-word;width:'+width+'px;height:'+height+'px;"><div style="background-image:url('+default_img+');background-size:cover;background-position:center center;background-repeat:no-repeat;width:100%;height:60%;">'+video_html+'</div><div class="content_inner" style="box-sizing:border-box;float:left;width:100%;height:40%;background:#fff;padding:40px;text-align:left;"><div class="head"  style="color:#333;font-size:16px;padding-bottom:10px;">'+title+'</div><div class="subhead"  style="color:#999;font-size:14px;padding-bottom:10px;">'+subtitle+'</div><div class="content overflowy" style="color:#666;font-size:14px;overflow-y: auto;;">'+desc+'</div></div>  </div></li>';
						 }
					 }
				 });
				 allmessage = '<div class="image_popup" style="position:relative;"><div  class="qfe_flexslider flexslider_slide flexslider" data-interval="30" data-flex_fx="slide" data-bottom_nav="1" data-direction="1"><ul class="slides">'+message+'</ul> </div><div class="block-close" style="position: absolute;top: 2px;right: 9px;color: rgb(204, 204, 204);cursor: pointer;">✕</div></div>';
			 }
			if(allmessage){	
					jQuery.blockUI({
						onOverlayClick: jQuery.unblockUI,
					    overlayCSS:{ 
					        backgroundColor: '#000', 
					        opacity:         0.8, 
					        cursor:          'pointer',
					        "z-index":"9400",
					    }, 
						css: {"top":toppx+"px","left":leftpx,width:width+"px",height:height+"px",cursor: 'pointer',"border":"0","z-index":"9401"},
						message:allmessage
					});
					jQuery(".localvideo.simple").attr("autoplay","autoplay");
					jQuery('.block-close').css('cursor', 'pointer').unbind().click(function(){
						 jQuery.unblockUI();
					});
					if(jQuery(".pop_image2 .content.overflowy").length>0){
						var h = jQuery(".pop_image2").height();
						var head = jQuery(".pop_image2 .head").height()+jQuery(".pop_image2 .subhead").height();
						jQuery(".pop_image2 .content.overflowy").height(h-head-60);
					}else if(jQuery(".pop_image3 .content.overflowy").length>0){
						var h = jQuery(".content_inner").height();
						var head = jQuery(".pop_image3 .head").height()+jQuery(".pop_image3 .subhead").height();
						jQuery(".pop_image3 .content.overflowy").height(h-head);
					}
				
					if(	jQuery(".image_popup .qfe_flexslider").length>0){
						 if(typeof jQuery.fn.flexslider=="undefined"){
							 jQuery.getScript("/qfy-content/plugins/qfy_editor/assets/lib/flexslider/jquery.flexslider-min.js").done(function() {
								 jQuery('head').append('<link href="/qfy-content/plugins/qfy_editor/assets/lib/flexslider/flexslider.css" rel="stylesheet" type="text/css" />');
								 _image_popup_flexslider(startAt);
							 })
						 }else{
							 _image_popup_flexslider(startAt);
						 }
					}
				}
		}
	});
	
	jQuery("a.qfy_thickbox").live({
		  click: function(e) {
			  e.preventDefault();
			  if( top.jQuery(".qfy_gallerys").length>0){
				  return false;
			  }
			  var default_img = jQuery(this).attr("href");
			  var thumbPath = jQuery(this).attr("thumbPath");
			  if(!thumbPath) thumbPath = default_img;
			  var i = 0;
			  var qfy_imglists= Array();
			  jQuery("a.qfy_thickbox").each(function(){
				var href = jQuery(this).attr("href");
				var thumbPath = jQuery(this).attr("thumbPath");
				if(!thumbPath) 	thumbPath = href;
				
				if(href!=default_img && href!= undefined){
					 qfy_imglists[i] = href+"|^|"+thumbPath;
					 i++;
				}
			  })
			 
			  top.jQuery("body").append('<div class="qfy_gallerys" style="position:fixed;top:0;left:0;width:100%;height:0;z-index:20000;"><iframe src="/FeiEditor/bitSite/gallerys?bgcolor='+encodeURIComponent(dtGlobals.gallery_bgcolor)+'&tfamily='+encodeURIComponent(dtGlobals.gallery_tfamily)+'&dfamily='+encodeURIComponent(dtGlobals.gallery_dfamily)+'&blankclose='+encodeURIComponent(dtGlobals.gallery_blankclose)+'&showthumbs='+dtGlobals.gallery_showthumbs+'&style='+dtGlobals.gallery_style+'&autoplay='+dtGlobals.gallery_autoplay+'&playspeed='+dtGlobals.gallery_playspeed+'&imagesize='+dtGlobals.gallery_imagesize+'&stopbutton='+dtGlobals.gallery_stopbutton+'&thumbsposition='+dtGlobals.gallery_thumbsposition+'&tsize='+dtGlobals.gallery_tsize+'&tcolor='+encodeURIComponent(dtGlobals.gallery_tcolor)+'&dsize='+dtGlobals.gallery_dsize+'&dcolor='+encodeURIComponent(dtGlobals.gallery_dcolor)+'&default_img='+default_img+'&thumbPath='+thumbPath+'&time='+Math.random()+'" width="100%" height="100%" border=0 style="border:0;" /></div>')
			  top.jQuery(".qfy_gallerys").animate({height:"100%"});
			  return false;
		  }
		});
	
	var body_width = window.screen.width; 
	var body_height =window.screen.height; 
	
	if(!jQuery("body").hasClass("compose-mode") && top==self){
		jQuery("a").each(function(){
			var href = jQuery(this).attr("href");
			if(href) href = href.toLowerCase();
			if(href && href.indexOf("/api/video-server/play.php")>-1){
				var turl =  href.split("?");
				var media_id = 0;
				var se =0;
				var nonce = 0;
				var width="800";
				var height="600";
				var turl2 = turl[1].split("&");
				for(i=0;i<turl2.length;i++){
					var t3 = turl2[i].split("=");
					if(t3[0]=="media_id"){
						media_id = t3[1];
					}else if(t3[0]=="se"){
						se = t3[1];
					}else if(t3[0]=="width" && t3[1]!=""){
						width = t3[1];
					}else if(t3[0]=="height"&& t3[1]!=""){
						height = t3[1];
					}
				}
				if(typeof jQuery.blockUI=="undefined"){
					jQuery.getScript("/qfy-content/plugins/bitcommerce/assets/js/jquery-blockui/jquery.blockUI.min.js");
				}
				
				if(media_id){
					jQuery(this).unbind().bind("click",function(e){
						e.stopPropagation();
						e.preventDefault();
						//获取播放权限
						var url = "/api/video-server/play.php?se="+se+"&media_id="+media_id;
						var bodywidth = jQuery("body").width();
						var bodyheight = jQuery(window).height();
						if(bodywidth>width){
							top.jQuery.blockUI({css: {"top":"15%","left":"50%","margin-left":"-"+(width/2)+"px",width:width,height:height},message: "<iframe class='videoframe' style='width:100%;height:100%;display:block;border:0;overflow:hidden;' data-height='"+height+"' data-controlBarVisibility='hover' data-autoplay='true' src='"+url+"' ></iframe><div class='block-close' style='position:absolute;top:-26px;right:-3px;color:#fff;'>✕</div><div class='block-move ' title='按住拖动位置' style='position:absolute;top: 0;left: 0;color:#fff;cursor:move;background: transparent;width: 100%;height: 80px;'></div>"});
						}else{
							var h =  Math.round(bodywidth*3/4);
							var toppx = (bodyheight-h)/2;
							top.jQuery.blockUI({css: {"top":toppx+"px","left":"0",width:bodywidth+"px",height:h+"px"},message: "<iframe class='videoframe'  style='width:100%;height:100%;display:block;border:0;overflow:hidden;' data-height='"+ h+"' data-controlBarVisibility='hover' data-autoplay='true' src='"+url+"' ></iframe><div class='block-close' style='position:absolute;top:-26px;right:7px;color:#fff;'>✕</div>"});
						}
						top.jQuery('.block-close').css('cursor', 'pointer').unbind().click(function(){
							 top.jQuery.unblockUI();
						});
						top.jQuery('.blockUI.blockMsg').unbind().draggable({handle: ".block-move"});
						top.jQuery('body,.blockOverlay').css('cursor', 'auto');
						
						return false;
					});
				}
			}else if(href && href.indexOf("iframe.php?video_mp4_local=")>-1 && href.indexOf("size=")>-1 && href.indexOf("&auto=")>-1){
				jQuery(this).addClass("thickbox");
				var auto = true;
				var full = true;
				var width = body_width;
				var height = body_height;
				var color = "#000";
				var opacity = "";
				var id = 0;
				var t1 = href.split("&");
				for(i=0;i<t1.length;i++){
					var t2 = t1[i].split("=");
					if(t2[0]=="size"){
						if(t2[1]=="big"){
							width = width*0.8;
							height = height*0.6;
							full =false;
						}else if(t2[1]=="middle"){
							width = width*0.6;
							height = height*0.4;
							full =false;
						}else if(t2[1]=="small"){
							width = width*0.4;
							height = width*0.8;
							full =false;
						}else if(t2[1]=="less"){
							width = width*0.3;
							height = height*0.2;
							full =false;
						}else if(t2[1]=="full"){
							width = width;
							height = height*0.7;
							full =true;
						}
					}else if(t2[0]=="auto"){
						if(t2[1]=="1"){
							auto = true;
						}else{
							auto= false;
						}
					}else if(t2[0]=="color"){
						color = decodeURIComponent(t2[1]);
					}else if(t2[0]=="opacity"){
						opacity = t2[1];
					}
				}
				var newhref = href+"&video_volume=50&video_fullscreen="+full+"&video_auto="+auto+"&ispopup=1&video_theme=maccaco&video_bgcolor=%23000&KeepThis=true&TB_iframe=true&height="+height+"&width="+width;
			
				jQuery(this).attr("href",newhref).unbind().bind("click",function(e){
			
					setTimeout(function(){
						jQuery("#TB_overlay:visible").css("background-color",color).css("opacity",opacity);
						jQuery("#TB_window:visible iframe").css("background-color",color);
						if(full){
							jQuery("#TB_window:visible").addClass("full");
						}
					},100);
					setTimeout(function(){
						jQuery("#TB_overlay:visible").css("background-color",color).css("opacity",opacity);
						jQuery("#TB_window:visible iframe").css("background-color",color);
						if(full){
							jQuery("#TB_window:visible").addClass("full");
						}
					},400);
					setTimeout(function(){
						jQuery("#TB_overlay:visible").css("background-color",color).css("opacity",opacity);
						jQuery("#TB_window:visible iframe").css("background-color",color);
						if(full){
							jQuery("#TB_window:visible").addClass("full");
						}
					},800);
				   
				})
			}
			
		})
	}

	
}
function qfy_custom_select(){
	jQuery(".qfy_custom_select").each(function(){
		var fun = jQuery(this).attr("data-fun");
		var ids = '['+jQuery(this).attr("data-ids")+']';
		var urls = jQuery(this).attr("data-urls");
		if(ids!=""){
			jQuery(this).cxSelect({
				  selects: eval(ids),
				  required: true,
				  url: eval(urls),
				});	
		}
	})
}
function thebackground(qfy_bg_images,time) {
	if(screen.width<760){
		return false;
	}
	var tmp_qfy_bg_images = qfy_bg_images.split("|^^|");
	var str ="";
	var bodyColor = jQuery("body").css("background-color");
	var bodyrepeat = jQuery("body").css("background-repeat");
	var bodyattachment = jQuery("body").css("background-attachment");
	var bodyposition = jQuery("body").css("background-position");
	var bodysize = jQuery("body").css("background-size");

	
	for(i=0;i<tmp_qfy_bg_images.length;i++){
		var img = tmp_qfy_bg_images[i];
		if(i==0){
			str += '<div style="position: absolute;opacity: 1;height:100%;width:100%;background:url('+img+')"  class="show"></div>';
		}else{
			str += '<div style="position: absolute;opacity: 0;height:100%;width:100%;background:url('+img+')"  ></div>';
		}
	}
	jQuery("body").prepend('<div class="qfy_body_background" style="width:100%;height:100%;left: 0;;position: fixed;top: 0;z-index: -1;"> '+str+'</div>');
	jQuery("body .qfy_body_background > div").css("background-color",bodyColor).css("background-repeat",bodyrepeat).css("background-attachment",bodyattachment).css("background-position",bodyposition).css("background-size",bodysize);
	jQuery('div.qfy_body_background div').css({opacity: 0.0});
	jQuery('div.qfy_body_background div:first').css({opacity: 1.0});
	
	setInterval(function(){
		var current = (jQuery('div.qfy_body_background div.show')? jQuery('div.qfy_body_background div.show') : jQuery('div.qfy_body_background div:first'));
		if ( current.length == 0 ) current = jQuery('div.qfy_body_background div:first');
		var next = ((current.next().length) ? ((current.next().hasClass('show')) ? jQuery('div.qfy_body_background div:first') :current.next()) : jQuery('div.qfy_body_background div:first'));
		next.css({opacity: 0.0})
		.addClass('show')
		.animate({opacity: 1.0}, 1000);
		current.animate({opacity: 0.0}, 1000)
		.removeClass('show');
	},time*1000);

	
}
function vc_element_init(){
	if(dtGlobals.isMobile==false && jQuery("body").width()<768){
		jQuery(".qfy-element").each(function(){
			var m_padding = jQuery(this).attr("m-padding");
			if(m_padding ){
				jQuery(this).css("padding",m_padding);
			}
		});
	}
}

function right_nav_bar(){
	if(jQuery("#right_nav_bar_scroll").length>0){
		jQuery("#right_nav_bar_scroll").waypoint({
			handler: function(direction) {
				if(direction=="down"){
					jQuery(".right_nav_bar").show();
				}else{
					jQuery(".right_nav_bar").hide();
				}
			},
			offset: "0",
		})
	}

	var bgcolor = jQuery(".right_nav_bar").attr("data-bgcolor");
	var hovercolor = jQuery(".right_nav_bar").attr("data-hovercolor");
	var data_pop= jQuery(".right_nav_bar").attr("data-pop");
	jQuery(".right_nav_bar .icons").each(function(){
		var title = jQuery(this).attr("data-desc");
		var $this = jQuery(this);
		var imageurl = jQuery(this).attr("data-file");
		if(imageurl){
			
			var tiptitle="<img src='"+imageurl+"'  style='width:150px;height:150px;'/><div style='width:150px;text-align:center;overflow:hidden;margin-top:5px;'>"+title+"</div>";
			if(jQuery(this).find(".title").html()=="" || jQuery(this).find(".title").length==0){
				var data = { tipJoint: "right", fixed:true,offset:[-50,0],style: data_pop };
				jQuery("<img />").attr("src", imageurl).load(function(){
					setTimeout(function(){new Opentip( $this, tiptitle, data);},1500);
				})
				
			}
			
		}
	}).mouseenter(function(){
		if(jQuery(this).find(".title").html()!="" && jQuery(this).find(".title").length>0){
			var width = jQuery(this).find(".title").css("width");
			width = width.replace("px","");
			jQuery(this).animate({width:(width*1+64)+"px"},200);
		}
		var tmpcolor = jQuery(this).attr("data-hovercolor");
		if(!tmpcolor) tmpcolor = hovercolor;

		jQuery(this).css({background:tmpcolor});
	}).mouseleave(function(){
		if(jQuery(this).find(".title").html()!="" && jQuery(this).find(".title").length>0){
			jQuery(this).animate({width:"54px"},200);
		}
		var tmpcolor = jQuery(this).attr("data-color");
		if(!tmpcolor) tmpcolor = bgcolor;
		
		jQuery(this).css({background:tmpcolor});
	});
	jQuery(".right_nav_bar .totop-icon").click(function(e){
		e.preventDefault();
		jQuery("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	})
}


function dropdownmenu_event(obj){
	
	 var $this = jQuery(obj);
	 var p = $this.closest(".dropdown_container");
	 if(p.find(".list-content").hasClass("active")){
		 p.find(".list-content").removeClass("active");
		 p.closest("section").removeClass("overflowauto");
		 p.closest(".qfy-column-inner").removeClass("overflowauto");
	 }else{
		 p.find(".list-content").addClass("active");
		 p.closest("section").addClass("overflowauto");
		 p.closest(".qfy-column-inner").addClass("overflowauto");
	 } 
	
}
function column_init_align(){
	
	var w = jQuery("body").width();
	jQuery('.qfy-column-inner.column_middle').each(function(){
		
		jQuery(this).css("margin-top",0).css("margin-bottom",0);
		var section = jQuery(this).closest("section.section");
		var heigth = jQuery(this).height();
		var t1 = section.css("padding-top").replace("px","");
		var t2 = section.css("padding-bottom").replace("px","");
		var pheight = section.height();
		var padding = 0;
		if(pheight<heigth ) pheight = heigth;
		if(t1>0 || t2>0){
			if( (pheight*1-heigth) < (t1-t2)){
				if(pheight*1==heigth){
					padding = (t2*1-t1*1)/2;
				}else{
					padding = (t2*1-t1*1-heigth)/2;
				}
			}else if( (pheight*1-heigth) < (t2-t1)){
				padding = t2*1-t1*1+(heigth-pheight)/2;
			}else{
				padding = (t2*1-t1*1)/2;
			}
		}
	
		if(jQuery(this).siblings(".qfy-column-inner").length>0){
			if(w>760){
				jQuery(this).css("margin-top",((pheight-heigth)/2 + padding)+"px");
			}
		}else{
			jQuery(this).css("margin-top",((pheight-heigth)/2+ padding)+"px");
		}
		
	})
	jQuery('.qfy-column-inner.column_bottom').each(function(){
		var section = jQuery(this).closest("section.section");
		jQuery(this).css("margin-top",0).css("margin-bottom",0);
		var heigth = jQuery(this).height();
		var t1 = section.css("padding-top").replace("px","");
		var t2 = section.css("padding-bottom").replace("px","");
		var pheight = section.height();
		if(pheight<heigth) pheight = heigth;

		if(section.find(".qfy-column-inner").length>1){
			if(w>760){
				jQuery(this).css("margin-top",(pheight-heigth)+"px");
			}
		}else{
			jQuery(this).css("margin-top",(pheight-heigth)+"px");
		}
		
	})
	
}
function vc_royalSlider_gallery_init(){
	if(  jQuery('.royalSlider_gallery_new').length==0 ){
		return;
	}
	
	 if(typeof jQuery.fn.royalSlider=="undefined"){
		 jQuery.getScript("/FeiEditor/bitSite/js/jquery.royalslider.min.js").done(function() {
			 _vc_royalSlider_gallery_init();
		 })
	 }else{
		 _vc_royalSlider_gallery_init();
	 }
	
}
function _vc_royalSlider_gallery_init(){
	var w = jQuery("body").width();
	var is_image_ok =true;
	jQuery('.royalSlider_gallery_new:not(.played) img').each(function(){
		if(!jQuery(this).prop('complete')){
			is_image_ok = false;
		}
		
	})
	if(!is_image_ok) {
	  window.setTimeout(function(){
		  _vc_royalSlider_gallery_init();
	  }, 500);
	  return;
   }
   royalSlider_gallery_new();
	
}
function royalSlider_gallery_new(obj){
	
	if(typeof(obj)== "undefined"){
		obj = jQuery('.royalSlider_gallery_new:not(".played")');
	}
	var w = jQuery("body").width();
	obj.each(function(){
				
				if(w<760 && !jQuery(this).hasClass("tabroya")){
					return;
				}

				var id =  jQuery(this).parent().attr("id");
				
				var $this = this;
				var istolast = false;
				var count = 0;
				if(top!=self &&  parent.vc){
					if(jQuery($this).hasClass("played") || jQuery($this).find(".rsContainer").length>0){
						 if(parent.vc.ShortcodesBuilder.isAddSlider){
							 istolast = true;
							 parent.vc.ShortcodesBuilder.isAddSlider = false;
						 }
						 var slider = jQuery($this).data('royalSlider');
						 if(slider)
							slider.destroy();
						 jQuery($this).html("");
						 var id = jQuery(this).closest(".vc-container").attr("data-model-id");
						 var model =  parent.vc.shortcodes.get(id);
						 var old_view = model.view;

						 var models =  parent.vc.shortcodes.where({parent_id: model.get('id')});
						
						parent._.each(models, function(childmodel) {
							if(childmodel.view.$el){
								childmodel.view.$el.appendTo(model.view.content());
								count++;
							}
						  }, this);	
						model.view.updated();
					
					}
				}
				jQuery(this).addClass("played");
				var imageScaleMode = jQuery(this).attr("imageScaleMode")?jQuery(this).attr("imageScaleMode"):"fit-if-smaller";
				var slidesOrientation = jQuery(this).attr("slidesOrientation")?jQuery(this).attr("slidesOrientation"):"horizontal";
				//var autoScaleSlider = jQuery(this).attr("autoScaleSlider")==""?true:false;
				var arrowsNavAutoHide = jQuery(this).attr("arrowsNavAutoHide")=="true"?true:false;
				//var showfullscreen = jQuery(this).attr("showfullscreen")=="true"?true:false;
				//var g_width = jQuery(this).attr("g_width")?jQuery(this).attr("g_width"):"";
				//var g_height = jQuery(this).attr("g_height")?jQuery(this).attr("g_height"):"";
				var transitionSpeed = jQuery(this).attr("transitionSpeed");
				var loop = jQuery(this).attr("g_loop")=="true"?true:false;
				var visiblenearby = jQuery(this).attr("visiblenearby")=="true"?true:false;
				var autoPlay  = jQuery(this).attr("auto_Play")=="true"?true:false;
				var arrowsNav = jQuery(this).attr("arrowsNav")=="true"?true:false;
				
				var controlNavigation = jQuery(this).attr("controlNavigation");
				if(controlNavigation=="thumbnails") controlNavigation="thumbnails";
				else if(controlNavigation=="none") controlNavigation="none";
				else controlNavigation = "bullets";
				var thumbnails_orientation = jQuery(this).attr("thumbnails_orientation")?jQuery(this).attr("thumbnails_orientation"):"horizontal";
				var disabledclick = jQuery(this).attr("disabledclick")=="true"?false:true;
				
				var transitionSpeed  = jQuery(this).attr("transitionSpeed");
				if(!transitionSpeed) transitionSpeed = 600;
				var transitionType  = jQuery(this).attr("transitionType");
				if(!transitionType) transitionType = "move";
				if(transitionType=="fade") transitionSpeed = Math.ceil(transitionSpeed/500);

				var delay  = jQuery(this).attr("delay");
				if(!delay) delay = 3000;
				var autoHeight = true;
				if(jQuery(this).attr("data-minheight") && jQuery(this).attr("data-minheight")>0){
					jQuery(this).find("section.section").css("height",jQuery(this).attr("data-minheight")+"px");
				}
				if(jQuery(this).children().length==0) return;
				jQuery(this).royalSlider({
					    fullscreen: {
						  enabled: false,
						  nativeFS: false
						},
						controlNavigation: controlNavigation,
						slidesOrientation:slidesOrientation,
						autoScaleSlider: false, 
						autoScaleSliderWidth: 300,     
						autoScaleSliderHeight: 150,
						loop: loop,
						transitionSpeed:transitionSpeed,
						transitionType:transitionType,
						imageScaleMode: imageScaleMode,
						navigateByClick: disabledclick,
						numImagesToPreload:10,
						arrowsNav:arrowsNav,
						arrowsNavAutoHide: arrowsNavAutoHide,
						arrowsNavHideOnTouch: true,
						keyboardNavEnabled: top==self?true:false,
						sliderDrag: w<760?true:false,
						fadeinLoadedSlide: true,
						globalCaption: false,
						globalCaptionInside: false,
						addActiveClass:true,
						autoHeight:autoHeight,
						thumbs: {
						  orientation :thumbnails_orientation, 
						  appendSpan: true,
						  firstMargin: true,
						  paddingBottom: 4
						},
						visibleNearby: {
							enabled: visiblenearby,
							centerArea: 0.7,
							center: true,
							breakpoint: 650,
							breakpointCenterArea: 0.64,
							navigateByCenterClick: true
						},
						autoPlay: {
							enabled: autoPlay,
							pauseOnHover: true,
							delay:delay
						}
					  });	
				var slider = jQuery(this).data('royalSlider');
				if(istolast && count>0){
					
					slider.goTo(count-1);
					
				}
				var $this = jQuery(this);
				var p = $this.closest(".qfy-tabcontent")
			
				if(p.length>0){
					slider.ev.on('rsAfterSlideChange', function() {
							// mouse/touch drag end
							p.find(".tabcontent-header-menu li.item button").removeClass("active");
							var index = p.find(".rsBullets .rsNavItem.rsNavSelected").index();
							p.find(".tabcontent-header-menu li.item:eq("+index+") button").addClass("active");
							p.find(".rsActiveSlide .qfe_start_animation").each(function(i){
								var $this = this;
								jQuery($this).removeClass("qfe_start_animation");
								setTimeout(function(){qfe_animate_fun($this);},50);
								
							})
							
							
					});
				}
				
				})
				
		
}
function accordioncontent(){
	 jQuery('.qfy-accordioncontent:not(.changed)').each(function(){
		 jQuery(this).addClass("changed").find('.a_content').each(function(){
			 var p = jQuery(this).parent();
			 jQuery(this).find('> section').each(function(index){
				 var curr = p.find(".a_header h4:eq("+index+")");
				 var text = curr.html();
				 if(text){
					jQuery(this).before('<h4 class="panel-title" >'+text+'</h4>');
					jQuery(this).prev().find(".line").remove();
				 }
				
				 if(curr.find(".line").length>0){
					 var line = "<h5 class='qfydownline' style='position:relative;z-index:5;margin:0;padding:0;opacity:1;'>"+curr.find(".line").clone().prop("outerHTML")+"</h5>";
					 jQuery(this).after(line);
				 }
			 })
		 })
	 })

	 
}
jQuery(window).load(function() {
	if(top==self){
		resizeDefaultObjSize();
	}
});
//处理锚点
var hash = window.location.hash;
_menu_link_event(hash);

jQuery(document).ready(function($) {
	//滚动...
	if($("#phantom").css("display")=="block"){
		var floatMenuH = $("#phantom").height();
	}else{
		var floatMenuH = 0;
	}
	var urlHash = "#" + window.location.href.split("#")[1];
	if($(".one-page-row").length && $(".one-page-row div[data-anchor^='#']").length ){
		if(urlHash!= "#undefined"){
			$("html, body").animate({
				scrollTop: $(".one-page-row div[data-anchor='" + urlHash + "']").offset().top - floatMenuH + 1
			}, 600, function(){
				$("body").removeClass("is-scroll");
			});
		}
	}else{
		if($(urlHash).length > 0){
			$("body").addClass("is-scroll");
			setTimeout(function(){
				$("html, body").animate({
					scrollTop:  $(urlHash).offset().top
				}, 600, function(){
					jQuery("#parallax-nav a[href='"+urlHash+"']").closest("li").addClass("active");
					$("body").removeClass("is-scroll");
					parallax_scroll_fun();
				});
			},500);
			
		}else{
			parallax_scroll_fun();
		}
	}
	//滚动...end
	//...check body
	vc_element_init();
	//侧边栏
	right_nav_bar();
	//底部菜单
	jQuery(".footerbar-menu .menu-item-has-children>a").click(function(e){
		e.stopPropagation();
		e.preventDefault();
		var submenu = jQuery(this).next(".sub-menu");
		if(submenu.css("display")=="none"){
			submenu.css("display","block");
		}else{
			submenu.css("display","none");
		}
	})
	//背景多个图片切换
	if(typeof(qfy_bg_images)!= "undefined"){
		thebackground(qfy_bg_images,qfy_bg_images_int);
	}
	//图片加载完再执行一下resize
	if(jQuery(".preloadimg:not('.loaded')").length>0){
		setTimeout(function(){
				jQuery(".preloadimg:not('.loaded')").each(function(){
					var newurl = jQuery(this).attr("org-src");
					jQuery(this).attr("src",newurl).addClass("loaded").load(function(){
						resizeDefaultObjSize();
					});
				});
		},300);
	}
	
	//多级选择
	qfy_custom_select();
	

	if(top==self){
		$(".qfy-text a[url-flagtarget='_blank']").attr("target","_blank");
		$(".qfy-text a[url-flagtarget='']").removeAttr("target");
	}
	if(top!=self){
		  function closeOpenPanel(){
			  if(top.jQuery(".boxy-wrapper:visible").length>0){
					top.jQuery(".boxy-wrapper:visible .buttonClass1").click();
		    	}
				if(parent.jQuery("#qfbody-content>.panel:visible").length>0){
					parent.jQuery("#qfbody-content>.panel:visible .vc-close").click();
		    	}
				if(top.jQuery(".bitPopIframe:visible").length>0){
					top.jQuery(".bitPopIframe:visible .btn-default").click();
		    	}
		  }
		  var ctrlDown = false;
		  var escDown = false;
		  var ctrlKey = 17, vKey = 86, cKey = 67,escKey=27,zkey=90,ykey=89;
		  $(document).keydown(function(e){
	  			if (e.keyCode == escKey){
	  				closeOpenPanel();
	  				 top.jQuery.unblockUI();
	  			}
	  			if(e.ctrlKey && (e.keyCode== zkey|| e.keyCode== ykey)){
	  				top.restorePage();
	  			}
		  })
		  $(top.document).keydown(function(e){
			  			if (e.keyCode == escKey){
			  				closeOpenPanel();
			  			}
			  			if(e.ctrlKey  && (e.keyCode== zkey|| e.keyCode== ykey)){
			  				top.restorePage();
			  			}
		 })
		  $(parent.document).keydown(function(e){
			  			if (e.keyCode == escKey){
			  				closeOpenPanel();
			  			}
		 })
	  }else{
		  $(document).keydown(function(e){
	  			if (e.keyCode == "27"){
	  				 jQuery.unblockUI();
	  			}
		  })
		  
		  
	  }
	$(".dropdown-toggle").unbind("click").bind("click",function(){
		 var obj =jQuery(this).parent().find(".dropdown-menu");
		 if(obj.css("display") == "none"){
			obj.show();
		 }else{
			obj.hide();
		 }
	})
	initmouseover();
	resizeDefaultObjSize();
	toolTip();
	weiBoAndWeiXinToolTip();
	searchForm();
	/*--Append element </i> to preloader*/
	//$(".tp-loader, .ls-defaultskin .ls-loading-indicator").not(".loading-label").append('<svg class="fa-spinner" viewBox="0 0 48 48" ><path d="M23.98,0.04c-13.055,0-23.673,10.434-23.973,23.417C0.284,12.128,8.898,3.038,19.484,3.038c10.76,0,19.484,9.395,19.484,20.982c0,2.483,2.013,4.497,4.496,4.497c2.482,0,4.496-2.014,4.496-4.497C47.96,10.776,37.224,0.04,23.98,0.04z M23.98,48c13.055,0,23.673-10.434,23.972-23.417c-0.276,11.328-8.89,20.42-19.476,20.42	c-10.76,0-19.484-9.396-19.484-20.983c0-2.482-2.014-4.496-4.497-4.496C2.014,19.524,0,21.537,0,24.02C0,37.264,10.736,48,23.98,48z"/></svg>');

	/*--Set variable for floating menu*/
	if (dtGlobals.isMobile && !dtGlobals.isiPad) smartMenu = false;

	/*--old ie remove csstransforms3d*/
	//if ($.browser.msie) $("html").removeClass("csstransforms3d");

	/*--Detect iphone phone*/
	if(dtGlobals.isiPhone){
		$("body").addClass("is-iphone");
	};
	
	/* !Custom touch events */
	/* !(we need to add swipe events here) */

	/* Custom touch events:end */
	if(top==self){
		if($(".bitMainTopSider .vc-no-content-helper").length==0 && $(".bitMainTopSider").length>0  && $(".bitMainTopSider").height()==0){
			$(".bitMainTopSider").parent().parent().remove();
			//$(".bitMainTopSider-wrapper").css("padding-top",0);
		}
	}



	
	/* !-overlap for webkit*/
	if ( !$.browser.webkit || dtGlobals.isMobile ){
		$("body").addClass("not-webkit").removeClass("is-webkit");
	}else{
		$("body").removeClass("not-webkit").addClass("is-webkit");
		//$(".overlap #main").prepend("<div class='main-gradient'></div>");
	};


	/*overlap for webkit:end*/
/*Misc:end*/

/* !-jQuery extensions */

	/* !- Check if element exists */
	$.fn.exists = function() {
		if ($(this).length > 0) {
			return true;
		} else {
			return false;
		}
	}

	/* !- Check if element is loaded */
	$.fn.loaded = function(callback, jointCallback, ensureCallback){
		var len	= this.length;
		if (len > 0) {
			return this.each(function() {
				var	el		= this,
					$el		= $(el),
					blank	= "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";

				$el.on("load.dt", function(event) {
					$(this).off("load.dt");
					if (typeof callback == "function") {
						callback.call(this);
					}
					if (--len <= 0 && (typeof jointCallback == "function")){
						jointCallback.call(this);
					}
				});

				if (!el.complete || el.complete === undefined) {
					el.src = el.src;
				} else {
					$el.trigger("load.dt")
				}
			});
		} else if (ensureCallback) {
			if (typeof jointCallback == "function") {
				jointCallback.call(this);
			}
			return this;
		}
	};

/* jQuery extensions: end */

/* !-Main navigation */
/* We need to fine-tune timings and do something about the usage of jQuery "animate" function */ 



if(dtGlobals.isWindowsPhone){
	$("body").addClass("windows-phone");
}

$(".mini-nav select").change(function() {
	window.location.href = $(this).val();
});


dtGlobals.isHovering = false;

mainmenu_event();

/* !-Navigation widget */
var customTimeoutShow
$(".custom-nav > li > a").click(function(e){
	$menuItem = $(this).parent();
	if ($menuItem.hasClass("has-children")) e.preventDefault();
	
	
		if ($(this).attr("class") != "active"){
				$(".custom-nav > li > ul").stop(true, true).slideUp(400);
				$(this).next().stop(true, true).slideDown(500);
				$(".custom-nav > li > a").removeClass("active");
				$(this).addClass('active');
		}else{
				$(this).next().stop(true, true).slideUp(500);
				$(this).removeClass("active");
		}

		$menuItem.siblings().removeClass("act");
		$menuItem.addClass("act");
});
$(".custom-nav > li > ul").each(function(){
	clearTimeout(customTimeoutShow);
	$this = $(this);
	$thisChildren = $this.find("li");
	if($thisChildren.hasClass("act")){
		$this.prev().addClass("active");
		$this.parent().siblings().removeClass("act");
		$this.parent().addClass("act");
		$(this).slideDown(500);
	}
});

/* Navigation widget: end */

/*!-SLIDERS*/

	/* !-Metro slider*/
	var mtResizeTimeout;

	$(window).on("resize", function() {
		resizeDefaultObjSize();
		
		clearTimeout(mtResizeTimeout);
		mtResizeTimeout = setTimeout(function() {
			$(window).trigger( "metroresize" );
		}, 200);
		try{
			if( parent.jQuery("#mobile_iframe_preivew").length==1){
				parent.setMobileDocumentInit();
			}
			
		}catch(e){
	
		}
		if(jQuery("#header.logo-left-right #navigation ul.ab-center").length>0){
			var v1 = jQuery("#header #branding").width();
			var v2 = jQuery("#header .assistive-info").width();
			jQuery("#header #navigation #main-nav").css("text-align","center").css("left",(v2-v1)/2+"px");
		
		}
	});
	try{
		if( parent.jQuery("#mobile_iframe_preivew").length==1){
			setTimeout(function(){
				parent.setMobileDocumentInit();
			},1000);
			
		}
	}catch(e){
		//跨域异常下，document.referrer
		
		var lang = getQueryString("lang");
		var BodyIsFt=getCookie(JF_cn)
		if(lang=="zh-cn"){
			BodyIsFt = 0;
			setCookie(JF_cn,"",-1);
		}
		if(lang=="zh" || BodyIsFt=="1"){
			setTimeout("StranBody()",100);
		}
		
	}
	
	if(jQuery(".zh_tw_lang").length>0){
		StranBody(jQuery(".zh_tw_lang")[0]);
		var href = jQuery(".selected_template_a").attr("href");
		var nhref = Traditionalized(href);
		jQuery(".selected_template_a").attr("href",nhref);
	}
	//翻译简繁体
	tranlanguage(jQuery("html"));



	/*!Scroll to Top*/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			$('.scroll-top').removeClass('off').addClass('on');
		}
		else {
			$('.scroll-top').removeClass('on').addClass('off');
		}
	});
	$(".scroll-top").click(function(e) {
		e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});
	/*Scroll to Top:end*/


	/*Shopping cart top bar:end*/

	/* !- Skills */
	$.fn.animateSkills = function() {
		$(".skill-value", this).each(function () {
			var $this = $(this),
				$this_data = $this.data("width");

			$this.css({
				width: $this_data + '%'
			});
		});
	};

	// !- Animation "onScroll" loop
	function doAnimation() {
		
		if(dtGlobals.isMobile){
			$(".skills").animateSkills();
		}
		if($("html").hasClass("old-ie")){
			$(".skills").animateSkills();
		};
	};
	// !- Fire animation
	doAnimation();
	/* Skills:end */
	// Create the dropdown base 12.02.14
	$("<select />").prependTo(".mini-nav .menu-select");

	// Create default option "Select"
	$("<option />", {
		"selected"  :  "selected",
		"value"     :  "",
		"text"      :  "———"
	}).appendTo(".mini-nav .menu-select select");

	// Populate dropdown with menu items
	$(".mini-nav").each(function() {
		var elPar = $(this),
			thisSelect = elPar.find("select");
		$("a", elPar).each(function() {
			var el = $(this);
			$("<option />", {
				"value"   : el.attr("href"),
				"text"    : el.text(),
				"data-level": el.attr("data-level")
			}).appendTo(thisSelect);
		});
	});
	

	$(".mini-nav select").change(function() {
		window.location = $(this).find("option:selected").val();
	});
	$(".mini-nav select option").each(function(){
		var $this = $(this),
			winHref = window.location.href;
		 if($this.attr('value') == winHref){
			$this.attr('selected','selected');
		};
	})
	/*!-Appearance for custom select*/
	$('.bitcommerce-ordering-div select').each(function(){
		$(this).customSelect();
	});
	$(".menu-select select, .mini-nav .customSelect1, .vc_pie_chart .vc_pie_wrapper").css("visibility", "visible");

	$(".mini-nav option").each(function(){
		var $this	= $(this),
			text	= $this.text(),
			prefix	= "";

		switch ( parseInt($this.attr("data-level"))) {
			case 1:
				prefix = "";
			break;
			case 2:
				prefix = "— ";
			break;
			case 3:
				prefix = "—— ";
			break;
			case 4:
				prefix = "——— ";
			break;
			case 5:
				prefix = "———— ";
			break;
		}
		$this.text(prefix+text);
	});


/* !Onepage template */
	$(".anchor-link").each(function(){
		var $_this = $(this),
			selector 	= $_this.attr("href");
			var this_offset, that_offset, offset_diff;
			var base_speed  = 600,
				speed       = base_speed;
			if($(selector).length > 0){
				var this_offset = $_this.offset(),
					that_offset = $(selector).offset(),
					offset_diff = Math.abs(that_offset.top - this_offset.top),
					speed       = (offset_diff * base_speed) / 1000;
			}
		$(this).on('click',function(e){
			$("body").addClass("is-scroll");
			if($("#phantom").css("display")=="block"){
				var floatMenuH = $("#phantom").height();
			}else{
				var floatMenuH = 0;
			}
			var $_this = $(this),
				selector = $_this.attr("href");
			if(selector == "#"){
				$("html, body").animate({
					scrollTop: 0
				}, speed, function(){
					$("body").removeClass("is-scroll");
				});
			}else{
				if($(".one-page-row").length && $(".one-page-row div[data-anchor^='#']").length ){
					$("html, body").animate({
						scrollTop: $(".one-page-row div[data-anchor='" + selector + "']").offset().top - floatMenuH + 1
					}, speed, function(){
						$("body").removeClass("is-scroll");
					});
				}else{
					if($(selector).length > 0){
						$("html, body").animate({
							scrollTop:  $(selector).offset().top - floatMenuH + 1
						}, speed, function(){
							$("body").removeClass("is-scroll");
						});
					}
				}
			}
			return false;
			
		});
	});
	jQuery(window).load(function(){
		if(jQuery('#load').length){
			jQuery('#load').fadeOut().remove();
		}
	});

	if($(".one-page-row").length){

		function onePageAct (){
			var active_row = $(".one-page-row div:in-viewport[data-anchor^='#']").attr("data-anchor");
			if($('.one-page-row .menu-item a[href='+ active_row +']').length ){
				$('.one-page-row .menu-item a').parent("li").removeClass('act');
				$('.one-page-row .menu-item a[href='+ active_row +']').parent("li").addClass('act');
			}
			if(active_row == undefined && $('.one-page-row .menu-item a[href="#"]').length){
				$('.one-page-row .menu-item a[href="#"]').parent("li").addClass('act');
			}
		};
		onePageAct();
	}

	$("#main-nav .menu-item a,.dl-menu li a").not(".dl-menu li.dl-back a[href^='#']").each(function(){
			var $_this = $(this),selector = $_this.attr("href");
		
		if(selector && selector.indexOf("#")>-1 && selector!="#"){
			//if(selector.indexOf("#")!=0){
				selector = jQuery.trim(selector);
				var tmpselecter = selector.split("#");
				selector = "#"+tmpselecter[tmpselecter.length-1];
			//}	
			
			if(selector!="#" &&  selector.indexOf("=")<0 && $(selector).length>0){
				$(this).on('click',function(e){
					var $_this = $(this),selector = $_this.attr("href");
					
					//手机下菜单是#，子菜单无法点击
					var mobilemenu =$_this.closest("#dl-menu").find("#mobile-menu").length;
					if( mobilemenu>0 && $_this.parent().hasClass("has-children")){
						var $submenu = $_this.parent().find( 'ul.dl-submenu:first' );
						if($submenu.length>0){
							var xx=event.pageX;
							var width = $submenu.width();
							var isclick = width-xx>35;
							if(!isclick) return;
						}
					}
					
					if($("body >.dl-menuwrapper.floatmenu").length>0){
						$("#dl-menu #mobile-menu .phone-text").click();
					}
	
					$("body").addClass("is-scroll");
					

					if($("#phantom").css("display")=="block"){
						var floatMenuH = $("#phantom").height();
					}else{
						var floatMenuH = 0;
					}
					
					//if(selector.indexOf("#")!=0){
						var tmpselecter = selector.split("#");
						selector = "#"+tmpselecter[tmpselecter.length-1];
					//}	
					$_this.closest("#dl-menu").find(".wf-phone-hidden.phone-text").click();
					var base_speed  = 600,
						speed       = base_speed;
					if(selector.indexOf("=")<0 &&  $(selector).length > 0){
						var this_offset = $_this.offset(),
							that_offset = $(selector).offset(),
							offset_diff = Math.abs(that_offset.top - this_offset.top),
							speed       = (offset_diff * base_speed) / 1000;
					}

					$(".one-page-row .menu-item a").parent("li").removeClass("act");
					if($(".one-page-row").length>0){
						$_this.parent("li").addClass("act");
					}
					if(selector == "#" && ($(".one-page-row").length > 0)){
						$("html, body").animate({
							scrollTop: 0
						}, speed, function(){
							$("body").removeClass("is-scroll");
							$_this.parent().siblings(".onepage").removeClass("act onepage");
						});
					}else{
						if($(".one-page-row").length && $(".one-page-row div[data-anchor^='#']").length ){
							$("html, body").animate({
								scrollTop: $(".one-page-row div[data-anchor='" + selector + "']").offset().top - floatMenuH + 1
							}, speed, function(){
								$("body").removeClass("is-scroll");
							});
						}else{
							if(selector.indexOf("=")<0 &&  $(selector).length > 0){
								$("html, body").animate({
									scrollTop:  $(selector).offset().top - floatMenuH + 1
								}, speed, function(){
									$("body").removeClass("is-scroll");
								
									$_this.parent().siblings(".onepage").removeClass("act onepage");
									$_this.parent().addClass("act onepage");
									_menu_link_event(selector);
									
								});
							}
						}
					}
					return false;
				});
		
			}
		
		}

	});
	$("ul.menu a,.tabcontent-inner>ul a").each(function(){
		var $_this = $(this),selector = $_this.attr("href");
		if(selector && selector.indexOf("#")>-1 && selector!="#"){
		
			selector = jQuery.trim(selector);
			var tmpselecter = selector.split("#");
			selector = "#"+tmpselecter[tmpselecter.length-1];
			if(selector!="#" &&  selector.indexOf("=")<0 && $(selector).length>0){
				$(this).on('click',function(e){
					$("body").addClass("is-scroll");
					if($("#phantom").css("display")=="block"){
						var floatMenuH = $("#phantom").height();
					}else{
						var floatMenuH = 0;
					}
					var base_speed  = 600,speed       = base_speed;
					var this_offset = $_this.offset(),that_offset = $(selector).offset(),offset_diff = Math.abs(that_offset.top - this_offset.top),speed       = (offset_diff * base_speed) / 1000;
		
					$("html, body").animate({
						scrollTop:  $(selector).offset().top - floatMenuH + 1
					}, speed, function(){
						$("body").removeClass("is-scroll");
						_menu_link_event(selector);
					});
					
				});
			}
		}
	});
	$(".logo-box a[href^='#'], #branding a[href^='#'], #branding-bottom a[href^='#']").on('click',function(e){
		$("body").addClass("is-scroll");
		if($("#phantom").css("display")=="block"){
			var floatMenuH = $("#phantom").height();
		}else{
			var floatMenuH = 0;
		}
		var $_this = $(this),
			selector 	= $_this.attr("href");

		var base_speed  = 600,
			speed       = base_speed;
		if($(selector).length > 0){
			var this_offset = $_this.offset(),
				that_offset = $(selector).offset(),
				offset_diff = Math.abs(that_offset.top - this_offset.top),
				speed       = (offset_diff * base_speed) / 1000;
		}
		if(selector == "#"){
			$("html, body").animate({
				scrollTop: 0
			}, speed, function(){
				$("body").removeClass("is-scroll");
			});
		}else{
			if($(".one-page-row").length && $(".one-page-row div[data-anchor^='#']").length ){
				$("html, body").animate({
					scrollTop: $(".one-page-row div[data-anchor='" + selector + "']").offset().top - floatMenuH + 1
				}, speed, function(){
					$("body").removeClass("is-scroll");
				});
			}else{
				if($(selector).length > 0){
					$("html, body").animate({
						scrollTop:  $(selector).offset().top - floatMenuH + 1
					}, speed, function(){
						$("body").removeClass("is-scroll");
					});
				}
			}
		}
		return false;
	});
	if($(".one-page-row").length){
		$(window).scroll(function () {
			var currentNode = null;
			if(!$("body").hasClass("is-scroll")){
				$('.one-page-row div[data-anchor^="#"]').each(function(){
					var activeSection = $(this),
						currentId = $(this).attr('data-anchor');
					if($(window).scrollTop() >= ($(".one-page-row div[data-anchor='" + currentId + "']").offset().top - 100)){
						currentNode = currentId;
					}
				});
				$('.menu-item a').parent("li").removeClass('act');
				$('.menu-item a[href="'+currentNode+'"]').parent("li").addClass('act');
				if($('.menu-item a[href="#"]').length && currentNode == null){
					$('.menu-item a[href="#"]').parent("li").addClass('act');
				}
			}
		});
	}
/* Onepage template:end */
	 floatmenu_create();
	var waypoint_menu_hash = {}
	jQuery(".mainmenu >.menu-item > a").each(function(){
	
		var $this = this;
		var href =jQuery(this).attr("href");
		if(href.indexOf("#")>-1 &&href!="#" ){
			var href_id = href.split("#");
			if(href_id.length>1){
				href_id = jQuery.trim(href_id[1]);
			}
			if(href_id && href_id.indexOf("=")<0 &&  jQuery("section#"+href_id).length>0){
				waypoint_menu_hash[href_id] = $this;
			}
		}
	})
	$.each(waypoint_menu_hash, function(hash, obj) {
		jQuery("section#"+hash).waypoint({
			handler: function(direction) {
				jQuery(".mainmenu .act").removeClass("act onepage");
				jQuery(".mainmenu  a").each(function(){
					var href =jQuery(this).attr("href");
					if(href && href.indexOf("#")>-1 &&href.indexOf(hash)>-1){
						currli = jQuery(this).parent();
						currli.addClass("act onepage");
					}	
				})
				jQuery(".widget_nav_menu ul.menu .current-menu-item,.qfy-listmenuvertical ul.menu .current-menu-item,.widget_nav_menuhorizontal ul.menu .current-menu-item").removeClass("current-menu-item current-menu-ancestor");
				jQuery(".widget_nav_menu ul.menu a,.qfy-listmenuvertical ul.menu a,.widget_nav_menuhorizontal ul.menu a,.tabcontent-inner>ul a").each(function(){
					var href =jQuery(this).attr("href");
					if(href && href.indexOf("#")>-1 &&href.indexOf(hash)>-1){
						currli = jQuery(this).parent();
						currli.addClass("current-menu-item");
					}	
					
				})
				jQuery(".tabcontent-inner>ul .active").removeClass("active");
				jQuery(".tabcontent-inner>ul  a").each(function(){
					var href =jQuery(this).attr("href");
					if(href && href.indexOf("#")>-1 &&href.indexOf(hash)>-1){
						jQuery(this).addClass("active");
					}	
				})
				_menu_link_event(hash);
				
			},
			offset: "20px",
		})
	});
	
//ready  in
	
	
//ready end	
});

function floatmenu_create(){
	
	if(jQuery("#header.logo-left-right #navigation ul.ab-center").length>0){
		var v1 = jQuery("#header #branding").width();
		var v2 = jQuery("#header .assistive-info .top-bar-right").width();
		jQuery("#header #navigation #main-nav").css("text-align","center").css("left",(v2-v1)/2+"px");
	}
	var $ = jQuery;
	if($("#header.newrightmenu,#header.newleftmenu").length>0){
		if($("#page.bodyheader40,#page.bodyheader0").length>0){
			$("#header").css("position","fixed").css("top","0");
			return;
		}
	}

	

	/*!Floating menu*/
	if (smartMenu && $("#header").length>0 ) {
		var controlDiv = "";
		var mouseHtml = "";
		try{
			if(self!=top && !top.jQuery("body").hasClass("caterole")){
				var pcontroldiv = '<div class="controls-column bitPcontrols wf-mobile-hidden" style="top:50%;margin-top:-8px;"><div class="controls-out-tl"><div class="parent parent-vc_row active "><a class="control-btn"><span class="vc-btn-content vo-btn-nomove" style="padding:0 5px 0 10px !important;  line-height: 18px !important;font-size:12px !important;color:#fff;" onclick="top.qfFloatMenuSetting()">设置浮动头部和LOGO</span></a></div></div></div>';

				mouseHtml = 'onmouseover="document.getElementById(\'phantomFloatMenu\').style.display = \'block\';floatmenucontrols_mouseenter();" onmouseout="document.getElementById(\'phantomFloatMenu\').style.display = \'none\';floatmenucontrols_mouseout();"  ';
				controlDiv = '<div id="phantomFloatMenu"  style="line-height: 0px; opacity: 1; visibility: visible; transition: auto 0s ease 0s; display: none;" class="controls-element vc-controls bit-controls-element">'+pcontroldiv+'<div class="controls-cc"><a class="control-btn vc-element-name"><span class="vc-btn-content vo-btn-nomove">浮动菜单和LOGO</span></a><a class="control-btn control-btn-edit " onclick="top.qfFloatMenuSetting()" title="设置 浮动菜单样式"><span class="vc-btn-content"><span class="icon"></span></span></a></div></div>';
			}
		}catch(e){
			
		
		}
		
		var scrTop = 0,
			scrDir = 0,
			scrUp = false,
			scrDown = false,
			/*$header = $("#main-nav"),*/
			$header = $("#main-nav"),
			$headerSlide = $("#main-nav-slide .main-nav-slide-inner"),
			f_logoheight =  $("#main-nav").attr("data-lh"),
			f_maxwidth =  $("#main-nav").attr("data-mw"),
			f_fh =  $("#main-nav").attr("data-fh"),
			logoURL = $("#branding a").attr("href"),
			desc =  $("#bit-floatlogoText").html(),
			$parent = $header.parent(),
			$headerSlideParent = $headerSlide.parent(),
			$phantom = $('<div id="phantom" '+mouseHtml+' >'+controlDiv+'<div class="ph-wrap"><div class="ph-wrap-content"><div class="ph-wrap-inner"><div class="logo-box" ></div><div class="menu-box" ></div><div class="menu-info-box"  ></div></div></div></div></div>').appendTo("body"),
			$menuBox = $phantom.find(".menu-box"),
			$headerH = $header.height(),
			isMoved = false,
			breakPoint = 0,
			threshold = $("#header").offset().top + $("#header").height(),
			doScroll = false;
		var headerobj = $("#header>*");
		var header_height = jQuery("#header").height();
		
		
		if ($("#qfadminbar:visible").exists()) { $phantom.css("top", "28px"); };
		if ($("#page").hasClass("boxed")) { $phantom.find(".ph-wrap").addClass("boxed"); $phantom.addClass("boxed");}
		$phantom.find(".ph-wrap").addClass("with-logo");
		if (dtGlobals.logoURL && dtGlobals.logoEnabled) {
			//if(logoURL == undefined){
				if(dtGlobals.logoURL){
					var valign = jQuery(".floatlogoText").css("vertical-align");
					if(!valign) valign="middle";
					var img_str = "";
					if(logoURL){
						img_str = '<a href="'+logoURL+'"><img src="'+dtGlobals.logoURL+'" height="'+dtGlobals.logoH+'" width="'+dtGlobals.logoW+'"></a>';
					}else{
						img_str = '<img src="'+dtGlobals.logoURL+'" height="'+dtGlobals.logoH+'" width="'+dtGlobals.logoW+'">';
					}
					$phantom.find(".logo-box").html('<div style="height:48px;vertical-align: '+valign+';display: table-cell;" >'+img_str+'</div>');
				}
			//}else{
			//	$phantom.find(".logo-box").html('<img src="'+dtGlobals.logoURL+'" height="'+dtGlobals.logoH+'" width="'+dtGlobals.logoW+'"></a>');
			//}
		}else{
			$phantom.find(".ph-wrap").addClass("with-logo-no");
		}
		if(desc){
			$phantom.find(".logo-box").append(desc);
			$phantom.find(".with-logo-no .logo-box").css("display","table-cell");
		}
		if(f_logoheight >=10 &&f_logoheight<=50 ){
			$("#phantom .logo-box img").css("max-height",f_logoheight+"px");
		}
		if(f_fh==1 ){
			$("#phantom").addClass("min");
		}
		if(f_maxwidth>0 ){
			$("#phantom .ph-wrap .ph-wrap-content").css("max-width",f_maxwidth+"px");
		}
		var info_w = $("#phantom .menu-info-box").width();
		if(jQuery("#header.bit-logo-menu").length>0 && !$("#phantom").hasClass("bit-logo-menu")){
			var old_img_str = $header.find("#branding .logo img:first").attr("src");
			$header.find("#branding .logo img:first").css("width","auto");
			$("#phantom").addClass("bit-logo-menu").find(".logo-box").remove();
			$("#phantom .menu-box").css("padding-left",info_w+"px");
		}

		$(".one-page-row .logo-box a").on('click',function(e){
			$("body").addClass("is-scroll");
			if($("#phantom").css("display")=="block"){
				var floatMenuH = $("#phantom").height();
			}else{
				var floatMenuH = 0;
			}
			var $_this = $(this),
				selector 	= $_this.attr("href");

			var base_speed  = 600,
				speed       = base_speed;
			if($(selector).length > 0){
				var this_offset = $_this.offset(),
					that_offset = $(selector).offset(),
					offset_diff = Math.abs(that_offset.top - this_offset.top),
					speed       = (offset_diff * base_speed) / 1000;
			}
			if(selector == "#"){
				$("html, body").animate({
					scrollTop: 0
				}, speed, function(){
					$("body").removeClass("is-scroll");
				});
			}else{
				if($(".one-page-row").length && $(".one-page-row div[data-anchor^='#']").length ){
					$("html, body").animate({
						scrollTop: $(".one-page-row div[data-anchor='" + selector + "']").offset().top - floatMenuH + 1
					}, speed, function(){
						$("body").removeClass("is-scroll");
					});
				}else{
					if($(selector).length > 0){
						$("html, body").animate({
							scrollTop:  $(selector).offset().top - floatMenuH + 1
						}, speed, function(){
							$("body").removeClass("is-scroll");
						});
					}
				}
			}
			return false;
		});

		$(window).on("debouncedresize", function() {
			$headerH = $header.height();
		});
		var top_threshold = $("#dl-menu").offset().top + $("#dl-menu").height();
		
		$(window).on("scroll", function() {
			
			var tempCSS = {},tempScrTop = $(window).scrollTop();
			scrDir = tempScrTop - scrTop;
			if (tempScrTop > threshold && isMoved === false) {
				if( !dtGlobals.isHovering ) {
					var s="0.5s";
					if($header.attr("data-sp")==0) s="0.4s";
					else if($header.attr("data-sp")==1) s="1s";
					else if($header.attr("data-sp")==2) s="2s";
					if($header.attr("data-st")=="1"){
						$phantom.css("animation-duration",s).addClass("showed_tb");
					}else{
						$phantom.css({"transition":"opacity "+s,"opacity": 1, "visibility": "visible"});
					}
					if($("#phantom").hasClass("bit-logo-menu")){
						if (dtGlobals.logoURL && dtGlobals.logoEnabled) {
							$header.find("#branding .logo img:first").attr("src",dtGlobals.logoURL);
						}
					}
					
					 if(jQuery("#header.newrightmenu,#header.newleftmenu").length>0){
						 if( jQuery("#dl-menu:visible").length==0 && (jQuery("body").height() -threshold - tempScrTop >jQuery(window).height() ) ){
							 	jQuery("#header").addClass("position-ab-fixed");
						 }
						 $phantom.find(".ph-wrap").remove();
					}else{
						$parent.css("min-height",$header.height()+"px");
						$header.appendTo($menuBox);
						$headerSlide.prependTo($phantom.find(".menu-info-box"));
						$header.removeClass($header.data('bit-menu')).addClass($header.data('bit-float-menu')).removeClass("bit-menu-default").addClass("bit-menu-float");
					}
					var infoclass = $headerSlide.attr("data-class");
					$phantom.find(".menu-info-box").addClass(infoclass);
			
					isMoved = true;
					//fancy-rollovers wf-mobile-hidden underline-hover bit-menu-float
					
					
					if($phantom.find("#main-nav").hasClass("position-ab-center")){
						var div1 = $phantom.find(".logo-box").width()+$phantom.find(".logo-box").css("padding-left").replace("px","")*1+$phantom.find(".logo-box").css("padding-right").replace("px","")*1;
						var div2 = $phantom.find(".menu-info-box").width();
						$phantom.find(".menu-box").css("position","relative").css("left",(div2-div1)/2+"px");
					}
				}
			}
			else if (tempScrTop <= threshold && isMoved === true) {
				//act 
				jQuery("#phantom .mainmenu >.menu-item.onepage").removeClass("onepage");
				if($("#phantom").hasClass("bit-logo-menu")){
					if(old_img_str)
						$header.find("#branding .logo img:first").attr("src",old_img_str);
				}
				 if(jQuery("#header.newrightmenu,#header.newleftmenu").length>0){
					 jQuery("#header").removeClass("position-ab-fixed");
				}else{
					$header.appendTo($parent);
					$parent.css("min-height","auto");
					$header.removeClass($header.data('bit-float-menu')).addClass($header.data('bit-menu')).addClass("bit-menu-default").removeClass("bit-menu-float");
				}
				
				if($(".logo-classic-centered, .logo-center").length){
					if($(".mini-search").length ){
						$header.insertBefore(".mini-search");
					}
				}
				if($header.attr("data-st")=="1"){
					$phantom.removeClass("showed_tb");
				}else{
					$phantom.css({"opacity": 0, "visibility": "hidden"});
				}
				
				
				isMoved = false;
				
			};
			scrTop = $(window).scrollTop();
			
		});
	}
/*Floating menu:end*/
}
function _menu_link_event(hash){
	$ = jQuery;

	//处理主菜单
	var ishash = false;
	if(hash.indexOf("#")==0  &&  hash!="#"&& jQuery(hash).length>0 ){
		$(".mainmenu").each(function(){
			$(this).find("a").each(function(){
				var href = jQuery(this).attr("href");
				var start = href.indexOf(hash);
				if(href.substr(start)==hash && ishash==false){
					ishash = true;
					var ul =  jQuery(this).closest(".mainmenu");
					ul.find(".act").removeClass("act");
					var currli = jQuery(this).closest("li");
					 currli.addClass("act");
					 currli.parents("li").addClass("act");
				}
			})
		})
		
	}else{
		$(".mainmenu .sub-nav .act").each(function(){
			var href = $(this).find(">a").attr("href");
			if(href.indexOf("#")>-1){
				$(this).removeClass("act");
			}
		})
		
	}

	//处理其他菜单
	
	if(hash.indexOf("#")==0  &&  hash!="#"&& jQuery(hash).length>0 ){
		$(".widget_nav_menu ul.menu,.qfy-listmenuvertical ul.menu,.widget_nav_menuhorizontal ul.menu").each(function(){
			var ismenuhash = false;
			$(this).find("a").each(function(){
				var href = jQuery(this).attr("href");
				var start = href.indexOf(hash);
				if(href.substr(start)==hash && ismenuhash==false){
					ismenuhash = true;
					var ul =  jQuery(this).closest("ul.menu");
					ul.find(".current-menu-item").removeClass("current-menu-item current-menu-ancestor");
					var currli = jQuery(this).closest("li");
					 currli.addClass("current-menu-item");
					 currli.parents("li").addClass("current-menu-item");
				}
			})
		})

	}else{
		$(".widget_nav_menu ul.menu,.qfy-listmenuvertical ul.menu,.widget_nav_menuhorizontal ul.menu").each(function(){
			$(this).find(">li.current-menu-item").each(function(i){
				if(i>0){
					$(this).removeClass("current-menu-item current-menu-ancestor");
					$(this).find(".current-menu-item").removeClass("current-menu-item current-menu-ancestor");
				}
			});
		})
	}
	
	if(hash.indexOf("#")==0  &&  hash!="#"&& jQuery(hash).length>0 ){
		$(".tabcontent-inner>ul").each(function(){
			var ismenuhash = false;
			$(this).find("a").each(function(){
				var href = $(this).attr("href");
				if(href){
					var start = href.indexOf(hash);
					if(href.substr(start)==hash && ismenuhash==false){
						ismenuhash = true;
						var ul =  jQuery(this).closest(".tabcontent-inner");
						ul.find(".active").removeClass("active");
						$(this).addClass("active");
					}
				}
			})
		})
	}
	
	
	
	
}
function mainmenu_event(){
	$ = jQuery;
	//custom menu
	
	$("#mobile-menu").removeAttr("style").wrap("<div id='dl-menu' class='dl-menuwrapper wf-mobile-visible main-mobile-menu' style='display:none' />");
		if($("#mobile-menu").hasClass("dropTopStyle")){
				$("#mobile-menu").removeClass("glyphicon glyphicon-icon-align-justify").append('<span class="mobile_icon glyphicon glyphicon-icon-angle-down" ></span>');
				var menu_html = $("#dl-menu:not(.dl-qfymobile-menu)").html();
				$("#dl-menu:not(.dl-qfymobile-menu)").remove();
				$("body").prepend("<div id='dl-menu' class='dl-menuwrapper wf-mobile-visible dropTopStyle_containter' >"+menu_html+"</div>");
				menu_html = null;
				if($("#mobile-menu").hasClass("left")){
					$("#dl-menu:not(.dl-qfymobile-menu)").addClass("left");
				}else if($("#mobile-menu").hasClass("right")){
					$("#dl-menu:not(.dl-qfymobile-menu)").addClass("right");
				}else{
					$("#dl-menu:not(.dl-qfymobile-menu)").addClass("center");
				}

				var padding = $("#mobile-menu").attr("data-padding");
				if(padding){
					$("#dl-menu:not(.dl-qfymobile-menu)").css("padding-left",padding+"px").css("padding-right",padding+"px");
				}
				if($("#mobile-menu").hasClass("positionFixed")){
					mobile_menu_fix();
				}
				
			}else if($("#mobile-menu").hasClass("dropCenterStyle")){
				$("#mobile-menu").removeClass("glyphicon glyphicon-icon-align-justify").append('<span class="mobile_icon glyphicon glyphicon-icon-angle-down" ></span>');
				var menu_html = $("#dl-menu:not(.dl-qfymobile-menu)").html();
				$("#dl-menu:not(.dl-qfymobile-menu)").remove();
				$("#header").after("<div id='dl-menu' class='dl-menuwrapper wf-mobile-visible dropCenterStyle_containter' >"+menu_html+"</div>");
				menu_html = null;
				if($("#mobile-menu").hasClass("left")){
					$("#dl-menu:not(.dl-qfymobile-menu)").addClass("left");
				}else if($("#mobile-menu").hasClass("right")){
					$("#dl-menu:not(.dl-qfymobile-menu)").addClass("right");
				}else{
					$("#dl-menu:not(.dl-qfymobile-menu)").addClass("center");
				}	
				var padding = $("#mobile-menu").attr("data-padding");
				if(padding){
					$("#dl-menu:not(.dl-qfymobile-menu)").css("padding-left",padding+"px").css("padding-right",padding+"px");
				}
				if($("#mobile-menu").hasClass("positionFixed")){
					mobile_menu_fix();
				}

				
			}else if($("#header").hasClass("wf-mobile-hidden")){
				var menu_html = $("#dl-menu:not(.dl-qfymobile-menu)").html();
				$("#dl-menu:not(.dl-qfymobile-menu)").remove();
				$("#header").after("<div id='dl-menu' class='dl-menuwrapper wf-mobile-visible mobiledefault_containter' style='text-align:center;margin:0px auto;' >"+menu_html+"</div>");
				if($("#mobile-menu").hasClass("positionFixed")){
					mobile_menu_fix();
				}

			}else{
				//default
				if($("#mobile-menu").hasClass("positionFixed")){
					$("#dl-menu:not(.dl-qfymobile-menu)").addClass("mobiledefault_containter");
					mobile_menu_fix();
				}

		}
	
	$(".underline-hover > li > a > span").not(".underline-hover > li > a > span.mega-icon").append("<i class='underline'></i>");
	$("#main-nav .menu-nav > li > a > span").append("<i class='underline'></i>");

	var $mainNav = $("#main-nav,.mini-nav");
	
	


	
	var	$mobileNav = $mainNav.clone();
	var	$mobileTopNav = $(".mini-nav").clone();
	var backCap = $("#mobile-menu > .menu-back").html();

	$mobileNav
		.attr("id", "")
		.attr("class", "dl-menu")
		.find(".sub-nav")
			.addClass("dl-submenu")
			.removeClass("sub-nav")
			.prepend("<li class='menu-item dl-back'><a href='#'><span>"+backCap+"</a></li>");
	

	$mobileNav.appendTo("#dl-menu:not(.dl-qfymobile-menu)").wrap("<div class='dl-container' />");


	$("body").removeClass("mobilefloatmenu");
	$("body >.dl-menu-film,body >.dl-menu-fixedheader").remove();
	if($("#mobile-menu").hasClass("floatmenu") ||$("#mobile-menu").hasClass("fullfloatmenu") || $("#mobile-menu").hasClass("leftbtnmenu")){
		$("body").addClass("mobilefloatmenu");
		var menu_content = $("#dl-menu:not(.dl-qfymobile-menu) .dl-container").prop("outerHTML");
		$("#dl-menu:not(.dl-qfymobile-menu) .dl-container").remove();
		$("body").prepend("<div  class='dl-menuwrapper  wf-mobile-visible floatmenu floatwarpper' >"+menu_content+"</div>");
		var menu_html = $("#dl-menu:not(.dl-qfymobile-menu) #mobile-menu").prop("outerHTML");
		$("#dl-menu:not(.dl-qfymobile-menu) #mobile-menu").remove();
		$("body #page").prepend("<div id='dl-menu' class='dl-menuwrapper wf-mobile-visible floatmenu' >"+menu_html+"</div>");
		
		if($("#mobile-menu").hasClass("fullfloatmenu")){
			$(".dl-menuwrapper.floatmenu").addClass("fullfloatmenu");
		}else	if($("#mobile-menu").hasClass("leftbtnmenu")){
			$("body").addClass("mobileleftbtnmenu");
			$(".dl-menuwrapper.floatmenu").addClass("leftbtnmenu").find(".dl-container").prepend( jQuery(".menu_displaynone").html() ).append( jQuery(".menu_displaynone_footer").html()  );
			if($("body >.dl-menu-film").length==0){
				$("body").prepend("<div class='dl-menu-film wf-mobile-visible'></div>");
			}
			if($("body >.dl-menu-fixedheader").length==0 && $(".leftbtnmenu #mobile-menu.displaynone").length==0){
				$("body").prepend("<div class='dl-menu-fixedheader wf-mobile-visible'>"+ jQuery(".menu_displaynone_header").html()+"</div>");
			}
			if(top!=self && jQuery("body").hasClass("compose-mode")){
				 jQuery("body").find(".qfyheadercontent [bitDataAction='site_widget_container'] .site_tooler").each(
						function() {
							parent.widgetHover(this,  jQuery("body"));
						})
				 jQuery("body").find(".qfyheadercontent [bitDataAction='site_fix_container']").each(function() {
					 parent.widgetFixEvent(this);
				})
			}
			jQuery("body").addClass("moble_menufixed");
			mobile_menu_fix_2();
			if ( jQuery(window).scrollTop()> jQuery(".dl-menu-fixedheader").height()) {
				jQuery(".dl-menu-fixedheader").css("position","fixed");
				jQuery("body").addClass("fixedheadering");
			}else{
				jQuery(".dl-menu-fixedheader").css("position","relative");
				jQuery("body").removeClass("fixedheadering");
			}
		}
		if($("#mobile-menu").hasClass("left")){
			$("#dl-menu:not(.dl-qfymobile-menu)").addClass("left");
			$(".floatwarpper").addClass("left");
		}else if($("#mobile-menu").hasClass("right")){
			$("#dl-menu:not(.dl-qfymobile-menu)").addClass("right");
			$(".floatwarpper").addClass("right");
		}else{
			$("#dl-menu:not(.dl-qfymobile-menu)").addClass("center");
			$(".floatwarpper").addClass("center");
		}
		var padding = $("#mobile-menu").attr("data-padding");
		if(padding){
			$(".floatwarpper").css("padding-left",padding+"px").css("padding-right",padding+"px");
		}
		var element_right = $("#mobile-menu").attr("data-right");
		if(element_right){
			$("#dl-menu:not(.dl-qfymobile-menu)").css("right",element_right+"px");
		}
		var element_top = $("#mobile-menu").attr("data-top");
		if(element_top){
			$("#dl-menu:not(.dl-qfymobile-menu)").css("top",element_top+"px");
		}

	}
	
	if (!$("html").hasClass("old-ie")) $( "#dl-menu:not(.dl-qfymobile-menu)" ).dlmenu();
	
	var timeouthidden = new Array();
	$(".qfy-sub-div.MenuAnimIn_js", $mainNav).parent().each(function(i) {
		var $this = jQuery(this);
		
		
		jQuery(this).on("mouseenter", function(e) {
			
			var obj = jQuery(this).find(".qfy-sub-div:first");
			if(timeouthidden[i]){
				clearTimeout(timeouthidden[i]);
			}
			var h = obj.children().height();
			var t = obj.attr("data-time");
			if(!obj.hasClass("ing") && !obj.hasClass("ed")){
				obj.stop().addClass("ing").removeClass("ending").css({"visibility":"visible","height": 0}).animate({"height": h}, t*1000,function(){
					jQuery(this).removeClass("ing").addClass("ed").css({"visibility":"visible"});
				});
			}

		}).on("mouseleave", function(e) {
			
			var obj = jQuery(this).find(".qfy-sub-div:first");
			if(timeouthidden[i]){
				clearTimeout(timeouthidden[i]);
			}
			timeouthidden[i] = setTimeout(function(){
				if(obj.hasClass("ending") || obj.hasClass("ing") ) return;
				obj.addClass("ending").removeClass("ed").animate({"height": 0}, 500,function(){jQuery(this).removeClass("ending ed").css({"visibility":"hidden","height": 0});});
			},300);
		});
	});

	$(".sub-nav", $mainNav).parent().each(function() {
	var $this = $(this);
	if(dtGlobals.isMobile || dtGlobals.isWindowsPhone){
		$this.find("> a").on("click tap", function(e) {
			if (!$(this).hasClass("dt-clicked")) {
				e.preventDefault();
				$mainNav.find(".dt-clicked").removeClass("dt-clicked");
				$(this).addClass("dt-clicked");
			} else {
				e.stopPropagation();
			}

		});
	};

	var menuTimeoutShow,
		menuTimeoutHide;

	if($this.hasClass("dt-mega-menu")){
		
		$this.on("mouseenter tap", function(e) {
			if(e.type == "tap") e.stopPropagation();

			var $this = jQuery(this);
			$this.addClass("dt-hovered");

			dtGlobals.isHovering = true;


			var $_this = jQuery(this),
				$_this_h = $this.height();

			var $_this_ofs_top = $this.position().top;
				$this.find("> .sub-nav").css({
					top: $_this_ofs_top+$_this_h
				});

			
			if($this.hasClass("mega-auto-width")){
				var $_this = $(this),
					$_this_sub = $_this.find(" > .sub-nav > li"),
					coll_width = $("#main .wf-wrap").width()/5,
					$_this_par_width = $_this.parent().width(),
					$_this_parents_ofs = $_this.offset().left - $this.parents("#header .wf-table, .ph-wrap-inner, .logo-center #navigation, .logo-classic #navigation, .logo-classic-centered #navigation").offset().left;

				$_this.find(" > .sub-nav").css({
					left: $_this_parents_ofs,
					"marginLeft": -($_this.find(" > .sub-nav").width()/2 - $_this.width()/2)
				});
			}
			if($this.is(':first-child') && $this.hasClass("mega-auto-width")){
				$this.find(" > .sub-nav").css({
					left: $_this.offset().left - $this.parents("#header .wf-table, .ph-wrap-inner, .logo-center #navigation, .logo-classic #navigation, .logo-classic-centered #navigation").offset().left,
					"marginLeft": 0
				});
			}else if($this.is(':last-child') && $this.hasClass("mega-auto-width")){
				$this.find(" > .sub-nav").css({
					left: "auto",
					right: $this.parents("#header .wf-table, .ph-wrap-inner, .logo-center #navigation, .logo-classic #navigation, .logo-classic-centered #navigation").width() - ( $this.position().left + $this.width() ),
					"marginLeft": 0
				});
			};

			if ($("#page").width() - ($this.children("ul").offset().left - $("#page").offset().left) - $this.children("ul").width() < 0) {
				$this.children("ul").addClass("right-overflow");
			};
			if($this.position().left < ($this.children("ul").width()/2)) {
				$this.children("ul").addClass("left-overflow");
			}

			clearTimeout(menuTimeoutShow);
			clearTimeout(menuTimeoutHide);

			menuTimeoutShow = setTimeout(function() {
				if($this.hasClass("dt-hovered")){
					$this.find("ul").stop().css("visibility", "visible").animate({
						"opacity": 1
					}, 150);
				}
			}, 100);
		});

		$this.on("mouseleave", function(e) {
			var $this = $(this);
			$this.removeClass("dt-hovered");

			dtGlobals.isHovering = false;
			clearTimeout(menuTimeoutShow);
			clearTimeout(menuTimeoutHide);

			menuTimeoutHide = setTimeout(function() {
				if(!$this.hasClass("dt-hovered")){
					$this.children("ul").stop().animate({
						"opacity": 0
					}, 150, function() {
						$(this).css("visibility", "hidden");

						$(this).find("ul").stop().css("visibility", "hidden").animate({
							"opacity": 0
						}, 10);
					});
					
					setTimeout(function() {
						if(!$this.hasClass("dt-hovered")){
							$this.children("ul").removeClass("right-overflow");
							$this.children("ul").removeClass("left-overflow");
						}
					}, 400);
					
				}
			}, 150);

			$this.find("> a").removeClass("dt-clicked");
		});
	}else{
	
		$this.on("mouseenter tap", function(e) {
			if(e.type == "tap") e.stopPropagation();
			
			var $this = jQuery(this);
			$this.addClass("dt-hovered");
			if ($("#page").width() - ($this.children("ul").offset().left - $("#page").offset().left) - $this.children("ul").width() < 0) {
			//if ($("#page").width() - ($this.children("ul").offset().left - $("#page").offset().left) - 240 < 0) {
				$this.children("ul").addClass("right-overflow");
			}
			dtGlobals.isHovering = true;
			clearTimeout(menuTimeoutShow);
			clearTimeout(menuTimeoutHide);
			
			if($this.find(".qfy-sub-div").length==0 && $this.closest(".qfyheaderul").length==0){
				menuTimeoutShow = setTimeout(function() {
					if($this.hasClass("dt-hovered")){
						if($this.closest("#main-nav").attr("data-sliderdown")){
							var h = $this.children('ul').height();
							$this.children('ul').stop().css({"visibility":"visible","opacity":"1","overflow":"hidden","max-height":"0"}).animate({
								"max-height": h
							}, 300,function(){
								jQuery(this).css({"overflow":"inherit"})
							});
						}else if($this.closest("#main-nav").hasClass("sub-sliderup")){
							$this.children('ul').stop().css("opacity", "1").css("visibility", "visible").css("margin-top", "0");
						}else{
							$this.children('ul').stop().css("visibility", "visible").animate({
								"opacity": 1
							}, 150);
						}
						
						
					}
				}, 100);
			}
		});

		$this.on("mouseleave", function(e) {
			var $this = jQuery(this);
			$this.removeClass("dt-hovered");

			dtGlobals.isHovering = false;
			clearTimeout(menuTimeoutShow);
			clearTimeout(menuTimeoutHide);
			if($this.find(".qfy-sub-div").length==0 && $this.closest(".qfyheaderul").length==0 ){
				menuTimeoutHide = setTimeout(function() {
					if(!$this.hasClass("dt-hovered")){
						
						if($this.closest("#main-nav").attr("data-sliderdown")){
							$this.children("ul").stop().removeAttr("style");
						}else if($this.closest("#main-nav").hasClass("sub-sliderup")){
							$this.children("ul").stop().removeAttr("style");
						}else{
							$this.children("ul").stop().animate({
								"opacity": 0
							}, 150, function() {
								$(this).css("visibility", "hidden");
							});
							
						}
						
						setTimeout(function() {
							if(!$this.hasClass("dt-hovered")){
								$this.children("ul").removeClass("right-overflow");
							}
						}, 400);
					}
				}, 150);
			}

			$this.find("> a").removeClass("dt-clicked");
		});
	};

});

/* Main navigation: end */

}
function floatmenucontrols_mouseenter(){
	 if(typeof(parent.floatmenucontrols_mouseenter)=="function"){
			parent.floatmenucontrols_mouseenter();
	 }
}
function floatmenucontrols_mouseout(){
	 if(typeof(parent.floatmenucontrols_mouseout)=="function"){
			parent.floatmenucontrols_mouseout();
	 }
}
function base64_decode(str)
{
    var base64DecodeChars = [
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
            -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
            -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1
        ];
    var c1, c2, c3, c4;
    var i, j, len, r, l, out;
 
    len = str.length;
    if (len % 4 != 0) {
        return '';
    }
    if (/[^ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789\+\/\=]/.test(str)) {
        return '';
    }
    if (str.charAt(len - 2) == '=') {
        r = 1;
    }
    else if (str.charAt(len - 1) == '=') {
        r = 2;
    }
    else {
        r = 0;
    }
    l = len;
    if (r > 0) {
        l -= 4;
    }
    l = (l >> 2) * 3 + r;
    out = new Array(l);
 
    i = j = 0;
    while (i < len) {
        // c1
        c1 = base64DecodeChars[str.charCodeAt(i++)];
        if (c1 == -1) break;
 
        // c2
        c2 = base64DecodeChars[str.charCodeAt(i++)];
        if (c2 == -1) break;
 
        out[j++] = String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4));
 
        // c3
        c3 = base64DecodeChars[str.charCodeAt(i++)];
        if (c3 == -1) break;
 
        out[j++] = String.fromCharCode(((c2 & 0x0f) << 4) | ((c3 & 0x3c) >> 2));
 
        // c4
        c4 = base64DecodeChars[str.charCodeAt(i++)];
        if (c4 == -1) break;
 
        out[j++] = String.fromCharCode(((c3 & 0x03) << 6) | c4);
    }
    return toUTF16(out.join(''));
}
function toUTF8(str)
{
    if (str.match(/^[\x00-\x7f]*$/) != null) {
        return str.toString();
    }
    var out, i, j, len, c, c2;
    out = [];
    len = str.length;
    for (i = 0, j = 0; i < len; i++, j++) {
        c = str.charCodeAt(i);
        if (c <= 0x7f) {
            out[j] = str.charAt(i);
        }
        else if (c <= 0x7ff) {
            out[j] = String.fromCharCode(0xc0 | (c >>> 6),
                                         0x80 | (c & 0x3f));
        }
        else if (c < 0xd800 || c > 0xdfff) {
            out[j] = String.fromCharCode(0xe0 | (c >>> 12),
                                         0x80 | ((c >>> 6) & 0x3f),
                                         0x80 | (c & 0x3f));
        }
        else {
            if (++i < len) {
                c2 = str.charCodeAt(i);
                if (c <= 0xdbff && 0xdc00 <= c2 && c2 <= 0xdfff) {
                    c = ((c & 0x03ff) << 10 | (c2 & 0x03ff)) + 0x010000;
                    if (0x010000 <= c && c <= 0x10ffff) {
                        out[j] = String.fromCharCode(0xf0 | ((c >>> 18) & 0x3f),
                                                     0x80 | ((c >>> 12) & 0x3f),
                                                     0x80 | ((c >>> 6) & 0x3f),
                                                     0x80 | (c & 0x3f));
                    }
                    else {
                       out[j] = '?';
                    }
                }
                else {
                    i--;
                    out[j] = '?';
                }
            }
            else {
                i--;
                out[j] = '?';
            }
        }
    }
    return out.join('');
}
function toUTF16(str)
{
    if ((str.match(/^[\x00-\x7f]*$/) != null) ||
        (str.match(/^[\x00-\xff]*$/) == null)) {
        return str.toString();
    }
    var out, i, j, len, c, c2, c3, c4, s;
 
    out = [];
    len = str.length;
    i = j = 0;
    while (i < len) {
        c = str.charCodeAt(i++);
        switch (c >> 4) {
            case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
            // 0xxx xxxx
            out[j++] = str.charAt(i - 1);
            break;
            case 12: case 13:
            // 110x xxxx   10xx xxxx
            c2 = str.charCodeAt(i++);
            out[j++] = String.fromCharCode(((c  & 0x1f) << 6) |
                                            (c2 & 0x3f));
            break;
            case 14:
            // 1110 xxxx  10xx xxxx  10xx xxxx
            c2 = str.charCodeAt(i++);
            c3 = str.charCodeAt(i++);
            out[j++] = String.fromCharCode(((c  & 0x0f) << 12) |
                                           ((c2 & 0x3f) <<  6) |
                                            (c3 & 0x3f));
            break;
            case 15:
            switch (c & 0xf) {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                // 1111 0xxx  10xx xxxx  10xx xxxx  10xx xxxx
                c2 = str.charCodeAt(i++);
                c3 = str.charCodeAt(i++);
                c4 = str.charCodeAt(i++);
                s = ((c  & 0x07) << 18) |
                    ((c2 & 0x3f) << 12) |
                    ((c3 & 0x3f) <<  6) |
                     (c4 & 0x3f) - 0x10000;
                if (0 <= s && s <= 0xfffff) {
                    out[j++] = String.fromCharCode(((s >>> 10) & 0x03ff) | 0xd800,
                                                  (s         & 0x03ff) | 0xdc00);
                }
                else {
                    out[j++] = '?';
                }
                break;
                case 8: case 9: case 10: case 11:
                // 1111 10xx  10xx xxxx  10xx xxxx  10xx xxxx  10xx xxxx
                i+=4;
                out[j++] = '?';
                break;
                case 12: case 13:
                // 1111 110x  10xx xxxx  10xx xxxx  10xx xxxx  10xx xxxx  10xx xxxx
                i+=5;
                out[j++] = '?';
                break;
            }
        }
    }
    return out.join('');
}
function weiBoAndWeiXinToolTip(){
	//ie8不支持
	if(jQuery("html#ie8").length==1){
		return false;
	}
	 Opentip.styles.stemsDemo = {
        stem: true,
        containInViewport: false,
        borderWidth: 2,
        borderColor: "#a7c1c5",
        background: "#EFF7F0"
      };
	 var objs = jQuery(".soc-ico .weixin");
	objs.each(function(){
		var obj = jQuery(this);
		var name = obj.attr("data-href");
		obj.attr("title", '');
		var title="<img src='/FeiEditor/public_api/getImageFromWebApi/weixin/"+name+"'  width='160'/><div>请扫二维码关注微信</div>";
		var data = { tipJoint: "bottom",style: "dark" };
		new Opentip(obj, title, data);
	})
	var objs = jQuery(".soc-ico .weibo");
	objs.each(function(){
		var obj = jQuery(this);
		var name = obj.attr("data-href");
		obj.attr("title", '');
		var title="<img src='/FeiEditor/public_api/getImageFromWebApi/weibo/"+name+"' width='160' /><div>请扫二维码关注微博</div>";
		var data = { tipJoint: "bottom",style: "dark" };
		new Opentip(obj, title, data);
	})
	
	
}
function toolTip(objs){
	//ie8不支持
	if(jQuery("html#ie8").length==1){
		return false;
	}
	 if(typeof(objs)=="undefined"){
		objs = jQuery("[data-tooltip='on']");
	 }
	 Opentip.styles.stemsDemo = {
        stem: true,
        containInViewport: false,
        borderWidth: 2,
        borderColor: "#a7c1c5",
        background: "#EFF7F0"
      };
	objs.each(function(){
		var obj = jQuery(this);
		var title = jQuery(this).attr("data-original-title");
		if(!title) title="";
		var image = jQuery(this).attr("data-original-image");
		var imagewidth = jQuery(this).attr("data-original-imagewidth");
		var imageheight = jQuery(this).attr("data-original-imageheight");
		if(image){
			if(!imagewidth)	imagewidth=150;
			if(!imageheight) imageheight=150;
			title = "<div>"+title+"</div>"+"<img src='"+image+"'  style='width:"+imagewidth+"px;height:"+imageheight+"px'/>";
		}
		var cate =  jQuery(this).attr("data-tooltip-cate");
		var data ="";
		if(cate==1){
			data = { tipJoint: "bottom"};
		}else if(cate==2){
			data = { tipJoint: "bottom",style: "alert"};
		}else if(cate==3){
			data = { tipJoint: "bottom",style: "dark" };
		}else if(cate==4){
			data = { tipJoint: "bottom",style: "glass" };
		}else if(cate==5){
			data = { tipJoint: "bottom right",style: "dark"};
		}else if(cate==6){
			data = { tipJoint: "bottom left",style: "dark" };
		}else if(cate==7){
			data = { tipJoint: "top right",style: "dark" };
		}else if(cate==8){
			data = { style: "stemsDemo", tipJoint: "bottom right", stem: "bottom right", stemLength: 10, stemBase: 30 };
		}else if(cate==9){
			data = { style: "stemsDemo", tipJoint: "bottom left", stem: "bottom left", stemLength: 10, stemBase: 30 };
		}else if(cate==10){
			data = { borderWidth: 5, stemLength: 18, stemBase: 20, style: "glass", target: true, tipJoint: "top", borderColor: "#317CC5" };
		}else if(cate==11){
			data = { borderWidth: 5, stemLength: 18, stemBase: 20, style: "glass", target: true, tipJoint: "bottom", borderColor: "#317CC5" };
		}else if(cate==12){
			data = { borderWidth: 5, stemLength: 18, stemBase: 20, style: "glass", target: true, tipJoint: "bottom left", borderColor: "#317CC5" }
		}
		if(data!=""){
			new Opentip(obj, title, data);
		}
	})
}
function toVisit(obj){
	var p  = jQuery(obj).closest(".qfy_item_post");
	
	var post_id = p.attr("data-postid");
	var pt = p.find('[data-title="true"]');
	var title = "";
	if(pt.length>0){
		var title =jQuery.trim(pt.text());
	}
	jConfirm("确认是否离开当前页面，访问页面【"+title+"】？",function(){
				top.$('#pageUrl').val(post_id).change();
	});

}
function toEditor(obj,e){
	if(e){
		 e.preventDefault();
		 e.stopPropagation();
	}
	parent.toEditor(obj);
}
function toCopy(obj){
	var p  = jQuery(obj).closest(".qfy_item_post");
	var id=p.closest(".vc-element").attr("data-model-id");


	var post_id = p.attr("data-postid");
	parent.bitSettingsEdit(post_id, "复制一个页面", "copyPage","COPY");
}
function toDelete(obj){
	parent.toDelete(obj);
}
function toEditProduct(obj){
	parent.toEditProduct(obj);
	return false;
}
function toRedirectProduct(obj){
	parent.toRedirectProduct(obj);
	return false;
}
function toDeleteCate(obj){
	parent.toDeleteCate(obj);
}
function pageNav(paged){
	var obj =top.jQuery("#site-content-iframe");
	var url  =obj.attr("src");
	if(url.indexOf("paged")>-1){
		var tmp = url.split("&paged");
		url = tmp[0]+"&paged="+paged;
	}else{
		if(url.indexOf("?")>-1){
			url = url+"&paged="+paged;
		}else{
			url = url+"?paged="+paged;
		}
	}
	obj.attr("src",url);
}
function pageCate(page_id,cate_id){
	var obj = top.jQuery("#site-content-iframe");
	var url  =obj.attr("src");
	if(cate_id==0) cate_id="";
	if(url.indexOf("post_id")>-1){
		var tmp = url.split("&post_id");
		url = tmp[0]+"&post_id="+page_id+"&categories="+cate_id;
	}else{
		if(url.indexOf("?")>-1){
			url = url+"&post_id="+page_id+"&categories="+cate_id;
		}else{
			url = url+"?post_id="+page_id+"&categories="+cate_id;
		}
		
	}
	
	jConfirm("确认是否需要打开该分类列表页面？",function(){
		obj.attr("src",url);
	});

	return false;
}
function searchResult(p){
	
	var action = jQuery(p).attr("action");
	var search = jQuery(p).find("[name='search']").val();
	var page_id = jQuery(p).find("[name='page_id']").val();
	var searchtype = jQuery(p).find("[name='searchtype']").val();
	if(search=="") return false;
	var obj = top.jQuery("#site-content-iframe");
	if(obj.length>0){
		var url  = obj.attr("src");
		if(url.indexOf("post_id")>-1){
			var tmp = url.split("&post_id");
			url = tmp[0]+"&post_id="+page_id+"&search="+encodeURIComponent(search)+"&searchtype="+searchtype;
		}else{
			if(url.indexOf("?")>-1){
				url = url+"&post_id="+page_id+"&search="+encodeURIComponent(search)+"&searchtype="+searchtype;
			}else{
				url = url+"?post_id="+page_id+"&search="+encodeURIComponent(search)+"&searchtype="+searchtype;
			}
		}
		obj.attr("src",url);
		return false;
	}
}

var clicksmsnum = 0;
function toverify(){
	smsWarning("");
	var p = jQuery("[name='sms_phone']").val();
	var image_code = jQuery("[name='sms_image']").val();
	var hidden_rand = jQuery("[name='hidden_rand']").val();
	if(jQuery.trim(p)=="" || p==null ){
		smsWarning("手机号码不能为空");
		return ;
	}
	if(jQuery.trim(image_code)=="" || image_code==null){
		smsWarning("图形验证码不能为空");
		return ;
	}
	var parentDIv = jQuery("[name='sms_code']").parents(".verifyDiv");
	var str ='<a class="bit-button-waiting countVerify" style="width:100%;height:30px;margin-top:5px; line-height:30px;font-size:14px;color:#898989;background:#e6e6e6;border-color:#d9d9d9;">30</a>';
	parentDIv.find(".bitButtonVerify").after(str);
	countVerify();
	jQuery.post("/admin/admin-ajax.php",{"action":"toverifySms","phone":jQuery.trim(p),"image_code":jQuery.trim(image_code),"hidden_rand":hidden_rand},
		function(data){
			if(data=="success"){
				smsWarning("短信已经成功发送，请查看您的手机。您可能在1-5分钟之后收到短信",1);
				clicksmsnum++;
				if(clicksmsnum>1 && (p.indexOf("+")<0||p.indexOf("+86")>-1) ){
					jQuery(".bitButtonVerify").attr("onclick","tocallbutton()");
					jQuery(".bitButtonVerify .iconText").html("收不到短信?");
				}
			}else if(data=="empty"){
				jQuery(".bit-button-waiting.countVerify").remove();
				smsWarning("手机号码不能为空");
			}else if(data=="errorphone"){
				jQuery(".bit-button-waiting.countVerify").remove();
				smsWarning("手机号码格式错误(国际号码示例：例如美国+112345678901)");
			}else if(data=="image_empty"){
				jQuery(".bit-button-waiting.countVerify").remove();
				smsWarning("图形验证码不能为空");
			}else if(data=="image_error"||data=="image_notmatch"){
				jQuery(".bit-button-waiting.countVerify").remove();
				smsWarning("图形验证码填写错误");
			}else if(data=="loginout"||data=="0"){
				smsWarning("会话已经过期，请重新登录");
			}else if(data=="exist"){
				smsWarning("短信已经发送，请查看您的手机,30秒之内只能获取一次验证码");
				clicksmsnum++;
				if(clicksmsnum>1 && (p.indexOf("+")<0||p.indexOf("+86")>-1)){
					jQuery(".bitButtonVerify").attr("onclick","tocallbutton()");
					jQuery(".bitButtonVerify .iconText").html("收不到短信?");
				}
			}else if(data=="existUserPhone"){
				jQuery(".bit-button-waiting.countVerify").remove();
				smsWarning("手机号码已经绑定了帐号,一个手机号码只能绑定一个帐号哦");
			}else if(data=="limitPhone"){
				smsWarning("1个小时内对一个号码不能发送超过10条短信");
			}else if(data=="limitIp"){
				smsWarning("1个小时内一个客户IP端不能发送超过10条短信");
			}else if(data=="failed"){
				jQuery(".bit-button-waiting.countVerify").remove();
				smsWarning("短信发送失败，请30秒后再试一次");
				clicksmsnum++;
				if(clicksmsnum>1 && (p.indexOf("+")<0||p.indexOf("+86")>-1)){
					jQuery(".bitButtonVerify").attr("onclick","tocallbutton()");
					jQuery(".bitButtonVerify .iconText").html("收不到短信?");
				}
			}else{
				smsWarning("短信发送异常,请30秒后再试一次");
				clicksmsnum++;
				if(clicksmsnum>1 && (p.indexOf("+")<0||p.indexOf("+86")>-1) ){
					jQuery(".bitButtonVerify").attr("onclick","tocallbutton()");
					jQuery(".bitButtonVerify .iconText").html("收不到短信?");
				}
			}

	})
}
function tocallbutton(){
	smsWarning("");
	if(confirm("确认是否通过呼叫你的手机来获取语言验证码？")){

		var p = jQuery("[name='sms_phone']").val();
		var image_code = jQuery("[name='sms_image']").val();
		var hidden_rand = jQuery("[name='hidden_rand']").val();
		if(jQuery.trim(p)=="" || p==null ){
			smsWarning("手机号码不能为空");
			return ;
		}
		if(jQuery.trim(image_code)=="" || image_code==null){
			smsWarning("图形验证码不能为空");
			return ;
		}
		var parentDIv = jQuery("[name='sms_code']").parents(".verifyDiv");
		var str ='<a class="bit-button-waiting countVerify" style="width:100%;height:30px;margin-top:5px; line-height:30px;font-size:14px;color:#898989;background:#e6e6e6;border-color:#d9d9d9;">30</a>';
		parentDIv.find(".bitButtonVerify").after(str);
		countVerify();
		jQuery.post("/admin/admin-ajax.php",{"action":"toverifyCall","phone":jQuery.trim(p),"image_code":jQuery.trim(image_code),"hidden_rand":hidden_rand},
			function(data){
				if(data=="success"){
					smsWarning("我们正在尝试呼叫你的手机，请注意接听语言提示",1);
				}else if(data=="empty"){
					jQuery(".bit-button-waiting.countVerify").remove();
					smsWarning("手机号码不能为空");
				}else if(data=="errorphone"){
					jQuery(".bit-button-waiting.countVerify").remove();
					smsWarning("手机号码格式错误");
				}else if(data=="image_empty"){
					jQuery(".bit-button-waiting.countVerify").remove();
					smsWarning("图形验证码不能为空");
				}else if(data=="image_error"||data=="image_notmatch"){
					jQuery(".bit-button-waiting.countVerify").remove();
					smsWarning("图形验证码填写错误");
				}else if(data=="loginout"||data=="0"){
					smsWarning("会话已经过期，请重新登录");
				}else if(data=="existUserPhone"){
					jQuery(".bit-button-waiting.countVerify").remove();
					smsWarning("手机号码已经绑定了帐号,一个手机号码只能绑定一个帐号哦");
				}else if(data=="limitPhone"){
					smsWarning("1个小时内对一个号码不能发送超过10条短信");
				}else if(data=="limitIp"){
					smsWarning("1个小时内一个客户IP端不能发送超过10条短信");
				}else if(data=="failed"){
					smsWarning("拨打电话失败,请30秒后再试一次");
				}else{
					smsWarning("拨打电话失败,请30秒后再试一次");
				}

		})
	}
}
function countVerify(){
	if(jQuery(".bit-button-waiting.countVerify").length==1){
		var countVerifyNum = jQuery(".bit-button-waiting.countVerify").text();
		if(countVerifyNum*1<2){
			jQuery(".bit-button-waiting.countVerify").remove();
		}
		setTimeout(function(){
			countVerifyNum = countVerifyNum*1 -1;
			jQuery(".bit-button-waiting.countVerify").text(countVerifyNum);
			countVerify();
		},1000);
	
	}
	
}
function countVerifyCall(){
	if(jQuery(".tocallbutton").css("opacity")!="1"){
		var countVerifyNum = jQuery(".tocallbutton").attr("data-second");
		if(countVerifyNum*1<2){
			jQuery(".tocallbutton").css("opacity","1").attr("title","").attr("data-second",0).css("cursor","pointer");
		}
		setTimeout(function(){
			countVerifyNum = countVerifyNum*1 -1;
			jQuery(".tocallbutton").attr("title","等待"+countVerifyNum+"s后可以重拨").attr("data-second",countVerifyNum);
			countVerifyCall();
		},1000);
	
	}
	
}
function toSubmitPhoneSms(){
	smsWarning("");
	var p = jQuery("[name='sms_phone']").val();
	if(jQuery.trim(p)=="" || p==null){
		smsWarning("手机号码不能为空");
		return ;
	}
	var c = jQuery("[name='sms_code']").val();
	if(jQuery.trim(c)=="" || c==null){
		smsWarning("手机验证码不能为空");
		return ;
	}
	var image_code = jQuery("[name='sms_image']").val();
	if(jQuery.trim(image_code)=="" || image_code==null){
		smsWarning("图形验证码不能为空");
		return ;
	}
	var hidden_rand = jQuery("[name='hidden_rand']").val();
	var parentDIv = jQuery("[name='sms_code']").parents(".verifyDiv");
	if(parentDIv.find(".bitButtonSubmitDiv .bit-button-waiting").length==1){
		return ;
	}
	var str ='<a class="bit-button-waiting" style="background:none;border:0;position:relative;"><img style=" height: 20px;margin-left: 10px;position: relative;top: 5px;" src="/qfy-content/plugins/qfyuser/skins/elegant/img/loading.gif"></a>';
	parentDIv.find(".bitButtonSubmit").after(str);
	parentDIv.find(".bitButtonSubmit").css("opacity","0.65").css("cursor","default");
	jQuery.post("/admin/admin-ajax.php",{"action":"successToVerify","phone":jQuery.trim(p),"code":jQuery.trim(c),"image_code":jQuery.trim(image_code),hidden_rand:hidden_rand},
		function(data){
			parentDIv.find(".bit-button-waiting").remove();
			parentDIv.find(".bitButtonSubmit").css("opacity","1").css("cursor","pointer");
			if(data=="success"){
				
				if(!jQuery("body").hasClass("only_show_sms_content")){
					smsWarning("手机验证成功，系统正在自动跳转到网站管理页面",1);
					if(jQuery("#redirect_sms_url").val()==""){
						location.href="/";
					}else{
						location.href=jQuery("#redirect_sms_url").val();
					}
				}else{
					smsWarning("手机验证成功，请关闭弹框，继续下面的操作",1);
				}
			}else if(data=="loginout"||data=="0"){
				smsWarning("会话已经过期，请重新登录");
			}else if(data=="None"){
				smsWarning("验证失败，请认真填写相关信息");
			}else if(data=="image_empty"){
				smsWarning("图形验证码不能为空");
			}else if(data=="image_error"||data=="image_notmatch"){
				smsWarning("图形验证码填写错误");
			}else if(data=="fail"){
				smsWarning("验证失败，一个手机验证码只能使用一次，请重新获取");
			}else if(data=="expire"){
				smsWarning("验证失败，手机验证码已经过期，请重新获取");
			}else if(data=="nomatch"){
				smsWarning("验证失败，验证码发送手机与当前绑定手机不符");
			}else if(data=="nomatchcode"){
				smsWarning("验证失败，手机验证码错误");
			}else {
				smsWarning("验证失败,未知异常，请尝试重新验证");
			}
	})

}
function smsWarning(content,flag){
	var parentDIv = jQuery("[name='sms_code']").parents(".verifyDiv");
	parentDIv.find(".sms-warning").html("");
	parentDIv.find(".btn-warning").html("");
	if(content==""){
		return false;
	}
	if(flag=="1"){
		parentDIv.find(".sms-warning").html("<span style='color:green;'>"+content+"...</span>");
	}else {
		parentDIv.find(".btn-warning").html(content);
	}
}

function cart_button_warning(){
	jAlert("编辑情况下，无法使用购物车功能。如果修改商城默认页面，你可以从左上角选择功能页面进行编辑");
}
function toorderview(id){
	id = id.replace("#","");
	var obj = top.jQuery("#site-content-iframe");
	var page_id = top.jQuery("#pageUrl").val();
	var  url=obj.attr("src");	
	if(url.indexOf("post_id")>-1){
		var tmp = url.split("&post_id");
		url = tmp[0]+"&post_id="+page_id+"&view-order="+id;
	}else{
		if(url.indexOf("?")>-1){
			url = url+"&post_id="+page_id+"&view-order="+id;
		}else{
			url = url+"?post_id="+page_id+"&view-order="+id;
		}
	}
	

	obj.attr("src",url);
}

function toorderpage(page_id){
	
	var obj = top.jQuery("#site-content-iframe");
	var  url=obj.attr("src");	
	if(url.indexOf("post_id")>-1){
		var tmp = url.split("&post_id");
		url = tmp[0]+"&post_id="+page_id;
	}else{
		if(url.indexOf("?")>-1){
			url = url+"&post_id="+page_id;
		}else{
			url = url+"?post_id="+page_id;
		}
	}
	obj.attr("src",url);
}

function checkCommentForm(obj){
	if(top!=self){
		top.jAlert("编辑状态下不能提交，请在预览模式下使用！");
		return false;
	}
	var form = jQuery(obj);
	var text1 = form.attr("data-text_7")?form.attr("data-text_7"):"请认真填写评价内容，字数不得少于10个字。";
	var text2 = form.attr("data-text_9")?form.attr("data-text_9"):"请填写必填项目。";
	var text3 = form.attr("data-text_10")?form.attr("data-text_10"):"请输入有效的地址邮件。";
	var text4 = form.attr("data-text_11")?form.attr("data-text_11"):"提交成功";
	var text5 = form.attr("data-button_texting")?form.attr("data-button_texting"):"提交中";


	var comment = jQuery.trim(form.find("#comment").val());
	if(comment.length <10 ){
		if(top!=self){top.jAlert(text1)}else{ alert(text1)};
		return false;
	}
	if(form.find("#author[aria-required='true']").length>0){
		var author = jQuery.trim(form.find("#author[aria-required='true']").val());
		if(author=="" ){
			if(top!=self){top.jAlert(text2)}else{ alert(text2)};
			return false;
		}
		var reg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/; 
		var email = jQuery.trim(form.find("#email[aria-required='true']").val());
		if(email=="" ){
			if(top!=self){top.jAlert(text2)}else{ alert(text2)};
			return false;
		}else if(!reg.test(email)){
			if(top!=self){top.jAlert(text3)}else{ alert(text3)};
			return false;
		}
	}
	var url  = form.attr("action");
	var old_submit_text = form.find("#submit").val();
	if(form.find("#submit").val()==text5+"..."){
		return false;
	}
	form.find("#comment_parent").val(0);
	form.find("#hide_content").remove();
	form.find("#submit").val(text5+"...");
	var p = form.closest(".qfy-comments");
	var pp = p.parent();
	if(form.find("[name='type']").val()=="nomarl"){
		if(form.find("#short_atts").length==0){
			form.prepend("<input type='hidden' name='short_atts'  id='short_atts' value='"+jQuery(obj).closest(".qfy-comments").attr("data-atts")+"' />");
		}else{
			form.find("#short_atts").val(jQuery(obj).closest(".qfy-comments").attr("data-atts"));
		}
	}
	jQuery.ajax({
        type: "POST",
        url:url,
        data:form.serialize(),
        error: function(request) {
        	if(top!=self){top.jAlert("连接异常。")}else{alert("连接异常");};
        	form.find("#submit").val(old_submit_text);
        },
        success: function(data) {
            if(data.indexOf("success")>-1){
				if(form.closest(".qfy-comments").length>0){
					form.find("#submit").val(text4);
					var tmp = data.split('|<result>|');
					p.replaceWith(tmp[1]);
					pp.find(".commentlist").show();
					var success_htm = '<div style="text-align:center;" ><img class="qfy_pop_checkedimg" src="/FeiEditor/images/bitcms/check.png" style="margin-top:20px;margin-bottom:20px;" /><div style="padding-bottom:20px;">'+text4+'</div></div>';
					qfy_popinfo_fun(success_htm,2);
				}else{
					var success_htm = '<div style="text-align:center;" ><img class="qfy_pop_checkedimg" src="/FeiEditor/images/bitcms/check.png" style="margin-top:20px;margin-bottom:20px;" /><div style="padding-bottom:20px;">'+text4+'</div></div>';
					qfy_popinfo_fun(success_htm,2);
					setTimeout(function(){
						location.reload();
					},3000)
				}
            }else{
				if( form.attr("data-text_12")){
					if(data=="对不起，你已经提交过评论啦。该商品或页面你只能评价一次！"){
						data = form.attr("data-text_12");
					}
				}else if( form.attr("data-text_13")){
					if(data=="禁止回复，超过条数限制..."){
						data = form.attr("data-text_13");
					}
				}
            	if(top!=self){top.jAlert(data)}else{alert(data);};
            	form.find("#submit").val(old_submit_text);
            }
            
        }
    });
	return false;
}

function response_comment(obj){
	var p = jQuery(obj).parent();
	jQuery(obj).closest(".comment").find(".response_comment"+p.attr("data-id")).show();
	p.find(".response_comment_span").hide();
	p.find(".response_comment_tosave").show();
	p.find(".response_comment_tocancel").show();
}
function response_comment_edit(obj){
	var p = jQuery(obj).closest(".comment-body");
	var v = jQuery.trim(p.find(">.content").html());
	var id = p.attr("data-id");
	var status =  p.attr("data-status");
	var t1 = jQuery(obj).attr("data-text_4")?jQuery(obj).attr("data-text_4"):"已审核";
	var t2 = jQuery(obj).attr("data-text_5")?jQuery(obj).attr("data-text_5"):"待审";
	var t3 = jQuery(obj).attr("data-text_6")?jQuery(obj).attr("data-text_6"):"垃圾评论";
	var t4 = jQuery(obj).attr("data-text_2")?jQuery(obj).attr("data-text_2"):"关闭";
	var t5 = jQuery(obj).attr("data-button_text")?jQuery(obj).attr("data-button_text"):"保存";

	p.find(">.content").hide();
	p.find(".reply.commentmetadata").hide();
	if(p.find(".edit_comment").length==0){
		var str ='<div class="edit_comment commentmetadata edit_comment'+id+'"><div style="border: 2px solid rgb(204, 212, 217);"><textarea name="comment"  style="color: rgb(64, 64, 64);font-size:14px;width:100%;border: 0 none; overflow-x: hidden;overflow-y: auto;resize: none; height: 80px;line-height: 20px;" id="editcontent'+id+'" cols="58" rows="10" tabindex="4">'+v+'</textarea><div style="background-color: rgb(246, 248, 249); text-align: right;"><label class="approved" style="font-weight:400px;font-size:12px;color: rgb(0, 128, 0);margin-right:5px;cursor:pointer;" ><input type="radio" value="1" name="comment_status" >'+t1+'</label><label class="waiting" style="font-weight:400px;font-size:12px;color: rgb(255, 165, 0);margin-right:5px;cursor:pointer;" ><input type="radio" value="0" name="comment_status">'+t2+'</label><label class="spam" style="font-weight:400px;font-size:12px; color: rgb(255, 0, 0);margin-right:5px;cursor:pointer;" ><input type="radio" value="spam" name="comment_status">'+t3+'</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="submit" style="border: 1px solid rgb(94, 144, 165); padding: 5px 10px; background-color: rgb(94, 144, 165); color: rgb(255, 255, 255);" class="response_comment_toedit" id="submit" tabindex="5" value="'+t5+'" onclick="response_comment_toedit(this)">&nbsp;&nbsp;<input type="button" onclick="response_comment_tocancel3(this)" name="cancel" style="border:1px solid #ccc;padding:5px 10px;background-color:#ccc;color:#fff;" id="cancel" tabindex="5" value="'+t4+'"></div></div></div>';
		p.append(str);
	}else{
		p.find(".edit_comment").show();
	}

	p.find(".edit_comment"+id+" [name='comment_status'][value="+status+"]").attr("checked",true); 
}
function response_comment_tocancel(obj){
	var p = jQuery(obj).parent();
	jQuery(obj).closest(".comment").find(".response_comment"+p.attr("data-id")).hide();
    p.find(".response_comment_span").show();
    p.find(".response_comment_tosave").hide();
	p.find(".response_comment_tocancel").hide();
}
function response_comment_tocancel2(obj){
	var p = jQuery(obj).closest(".comment").find(".reply.commentmetadata");
	jQuery(obj).closest(".comment").find(".response_comment"+p.attr("data-id")).hide();
    p.find(".response_comment_span").show();
    p.find(".response_comment_tosave").hide();
	p.find(".response_comment_tocancel").hide();
}
function response_comment_tocancel3(obj){
	var p = jQuery(obj).closest(".comment-body");
	p.find(">.content").show();
	p.find(".edit_comment.commentmetadata").hide();
	p.find(".reply.commentmetadata").show();
}
function response_comment_toedit(obj){
	var	p =jQuery(obj).closest(".comment-body");
	var pp = jQuery(obj).closest(".qfy-comments").parent();
	var id = p.attr("data-id");
	var comment = p.find("#editcontent"+id).val();
	var dp = p.attr("data-parent");

	if(comment.length <10 ){
		if(top!=self){top.jAlert("请认真填写评价内容，字数不得少于10个字。")}else{ alert("请认真填写评价内容，字数不得少于10个字。")};
		return false;
	}
	var form =jQuery(obj).closest(".qfy-comments").find("form#commentform");
	var url  =form.attr("action");
	form.find("#comment_parent").val(dp);
	if(form.find("#comment_approved").length==0){
		form.append('<input type="hidden" value="0" id="comment_approved" name="comment_approved">');
	}
	form.find("#comment_approved").val(p.find(".edit_comment"+id+" [name='comment_status']:checked").val());
	if(form.find("#hide_content").length==0){
		form.prepend("<input type='hidden' name='hide_content'  id='hide_content' value='"+comment+"' />");
	}else{
		form.find("#hide_content").val(comment);
	}
	if(form.find("#comment_ID").length==0){
		form.prepend("<input type='hidden' name='comment_ID'  id='comment_ID' value='"+id+"' />");
	}else{
		form.find("#comment_ID").val(id);
	}
	if(p.find(".response_comment_toedit").val()!=p.attr("data-btntext")){
		return false;
	}
	if(form.find("#short_atts").length==0){
		form.prepend("<input type='hidden' name='short_atts'  id='short_atts' value='"+jQuery(obj).closest(".qfy-comments").attr("data-atts")+"' />");
	}else{
		form.find("#short_atts").val(jQuery(obj).closest(".qfy-comments").attr("data-atts"));
	}
	p.find(".response_comment_toedit").val(p.attr("data-btntexting")+"...");
	//....
	jQuery.ajax({
        type: "POST",
        url:url,
        data:form.serialize(),
        error: function(request) {
        	if(top!=self){top.jAlert("连接异常。")}else{alert("连接异常");};
        	p.find(".response_comment_toedit").val(p.attr("data-btntext"));
			form.find("#comment_ID").remove();
			form.find("#comment_approved").remove();
        },
        success: function(data) {
            if(data.indexOf("success")>-1){
				p.find(".response_comment_toedit").val(p.attr("data-btntext"));
				var tmp = data.split('|<result>|');
				form.closest(".qfy-comments").replaceWith(tmp[1]);
				pp.find(".commentlist").show();
				
            }else{
				p.find(".response_comment_toedit").val(p.attr("data-btntext"));
				form.find("#comment_ID").remove();
				form.find("#comment_approved").remove();
            	if(top!=self){top.jAlert(data)}else{alert(data);};
				
            }
        }
    });
}
function commentSort(obj,order){
	var p = jQuery(obj).closest(".qfy-comments");
	var pp = p.parent();
	var form =p.find("form#commentform");
	var comment_post_ID = form.find("#comment_post_ID").val();
	
	var url  = form.attr("action");
	
	var newsest_text = p.find("#commentmenu.nav-menu-left li:first a").html();
	var oldest_text = p.find("#commentmenu.nav-menu-left li:last a").html();
	p.css("opacity","0.5");
	jQuery.post(url,{action:"search",comment_post_ID:comment_post_ID,reverse_top_level:order,short_atts:p.attr("data-atts")},function(data){
		if(data.indexOf("success")>-1){
			var tmp = data.split('|<result>|');
			p.replaceWith(tmp[1]);
			pp.find(".commentlist").show();
			if(order==0){
				p.find(".comment-nav-left b").html(newsest_text);
			}else{
				p.find(".comment-nav-left b").html(oldest_text);
			}
		}
		p.css("opacity","1");
	})
}
function next_comment_page(obj,pageid){
	var p = jQuery(obj).closest(".qfy-comments");
	var form =p.find("form#commentform");
	var comment_post_ID = form.find("#comment_post_ID").val();
	var url  = form.attr("action");
	p.css("opacity","0.5");
	jQuery.post(url+"?cpage="+pageid,{action:"search",comment_post_ID:comment_post_ID,short_atts:p.attr("data-atts")},function(data){
		if(data.indexOf("success")>-1){
			var tmp = data.split('|<result>|');			
			var t = jQuery(tmp[1]).find(".commentlist").html();
			var load = jQuery(tmp[1]).find(".loadmore_inner").html();
			p.find(".commentlist").append(t);
			if(load){
				p.find(".loadmore_inner").html(load);
			}else{
				p.find(".loadmore_inner").remove();
			}
			p.css("opacity","1");
			
		}
	})

}
function removecomment(obj,id){
	if(confirm(jQuery(obj).attr("data-text"))){
		var p = jQuery(obj).closest(".qfy-comments");
		var pp = p.parent();
		var form =p.find("form#commentform");
		var comment_post_ID = form.find("#comment_post_ID").val();
		var url  = form.attr("action");
		p.find("#div-comment-"+id).css("opacity","0.5");
		jQuery.post(url,{action:"trash",comment_id:id,comment_post_ID:comment_post_ID,short_atts:p.attr("data-atts")},function(data){
			if(data.indexOf("success")>-1){
				var tmp = data.split('|<result>|');
				p.replaceWith(tmp[1]);
				pp.find(".commentlist").show();
			}else{
				p.find("#div-comment-"+id).css("opacity","1");
				alert(data);
			}
		})
	}
}
function response_comment_tosave(obj){
	//商品和普通评论，不一样的结构
	var p = jQuery(obj).parent();
	var pp = jQuery(obj).closest(".qfy-comments").parent();
	var id = p.attr("data-id");
	var product = true;
	var form = jQuery(obj).closest("#tab-reviews").find("form#commentform");
	if(!id){ 
		//普通
		p =jQuery(obj).closest(".reply.commentmetadata");
		id = p.attr("data-id");
		product = false;
		form = jQuery(obj).closest(".qfy-comments").find("form#commentform");
		if(form.find("#short_atts").length==0){
			form.prepend("<input type='hidden' name='short_atts'  id='short_atts' value='"+jQuery(obj).closest(".qfy-comments").attr("data-atts")+"' />");
		}else{
			form.find("#short_atts").val(jQuery(obj).closest(".qfy-comments").attr("data-atts"));
		}
		var old_submit_text = p.find(".response_comment_tosave").val();
	}
	if(p.find("#replycontent"+id).length>0){
		var comment = p.find("#replycontent"+id).val();
	}else{
		var comment = jQuery("#replycontent"+id).val();
	}
	if(comment.length <10 ){
		if(top!=self){top.jAlert("请认真填写评价内容，字数不得少于10个字。")}else{ alert("请认真填写评价内容，字数不得少于10个字。")};
		return false;
	}
	
	var url  = form.attr("action");
	form.find("#comment_parent").val(id);
	if(form.find("#hide_content").length==0){
		form.prepend("<input type='hidden' name='hide_content'  id='hide_content' value='"+comment+"' />");
	}else{
		form.find("#hide_content").val(comment);
	}
	
	
	if(p.find(".response_comment_tosave").html()=="提交中..." || p.find(".response_comment_tosave").val()=="提交中..."){
		return false;
	}
	if(product){
		p.find(".response_comment_tosave").html("提交中...");
	}else{
		 p.find(".response_comment_tosave").val("提交中..");
	}
	p.find(".response_comment_tocancel").hide();
	//....
	jQuery.ajax({
        type: "POST",
        url:url,
        data:form.serialize(),
        error: function(request) {
        	if(top!=self){top.jAlert("连接异常。")}else{alert("连接异常");};
        	form.find(".response_comment"+p.attr("data-id")).hide();
        	p.find(".response_comment_span").show();
			if(product){
        		p.find(".response_comment_tosave").hide().html("保存");
			}else{
				p.find(".response_comment_tosave").hide().val(old_submit_text);
			}
        },
        success: function(data) {
        	
            if(data.indexOf("success")>-1){
				if(form.closest(".qfy-comments").length>0){
					p.find(".response_comment_tosave").val("提交成功");
					var tmp = data.split('|<result>|');
					form.closest(".qfy-comments").replaceWith(tmp[1]);
					pp.find(".commentlist").show();
				}else{
					if(top!=self){top.jAlert("提交成功。")}else{alert("提交成功,");};
            		location.reload();
				}
            }else{
            	if(top!=self){top.jAlert(data)}else{alert(data);};
                form.find(".response_comment"+p.attr("data-id")).hide();
            	p.find(".response_comment_span").show();
				if(product){
            		p.find(".response_comment_tosave").hide().html("保存");
				}else{
					p.find(".response_comment_tosave").val(old_submit_text);
				}
            }
        }
    });
}






function jConfirm(msg,yes,no,title){
	if(top!=self){
		top.jConfirm(msg,yes,no,title);
	}
}

function jAlert(msg,title){
	if(top==self){
		alert(msg);
	}else{
		top.jAlert(msg,title);
	}
}

function setCookie(cname, cvalue,exdays)		//cookies设置
{
	var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(Name)			//cookies读取
{
	var search = Name + "="
	if(document.cookie.length > 0) 
	{
		offset = document.cookie.indexOf(search)
		if(offset != -1) 
		{
			offset += search.length
			end = document.cookie.indexOf(";", offset)
			if(end == -1) end = document.cookie.length
			return unescape(document.cookie.substring(offset, end))
		 }
	else return ""
	  }
}
function getQueryString(name) {  
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");  
    var r = location.search.substr(1).match(reg);  
    if (r != null) return unescape(decodeURI(r[2])); return null;  
}  
function tobigimage(obj){
	var bigimage = jQuery(obj).parents(".qfy_showbox").find(".bigImage");
	var height = bigimage.find("img").height();
	var src = jQuery(obj).find("img").attr("src");
	jQuery(obj).parents(".qfy_imgList").find("img").css("border","0");
	jQuery(obj).find("img").css("border","2px solid #f63");
	src = src.replace("-150x150","");
	bigimage.find("img").attr("src",src).height(height);
}
function slideLine(box,stf,delay,speed,h){
	
	if(jQuery("#"+box+" div").length ==0){
		return false;
	}

	var slideBox = document.getElementById(box);
	var delay = delay||1000,speed = speed||20,h = h||20;
	var tid = null,pause = false;
	var s = function(){tid=setInterval(slide, speed);}
	var slide = function(){
	if(pause) return;
	slideBox.scrollTop += 1;
	if(slideBox.scrollTop%h == 0){
		clearInterval(tid);
		slideBox.appendChild(slideBox.getElementsByTagName(stf)[0]);
		slideBox.scrollTop = 0;
		setTimeout(s, delay);
		}
	}
	slideBox.onmouseover=function(){pause=true;}
	slideBox.onmouseout=function(){pause=false;}
	setTimeout(s, delay);
}

function vc_gallery_relat(){
	jQuery(".qfe_gallery.rela_other .vc-carousel .qfy_image_vc_item img").each(function(){
		var img = jQuery(this);
		var src = img.attr("src");
		img.css("cursor","pointer");
		src = src.replace(/-[\d]*x[\d]*/g,"");
		img.unbind("click").bind("click",function(){
			jQuery(".qfe_gallery.rela_other .slides").each(function(){
				var obj = jQuery(this);
				var index = 0;
				obj.find("img").each(function(){
					var toimg = jQuery(this);
					var tosrc = toimg.attr("src");
					tosrc = tosrc.replace(/-[\d]*x[\d]*/g,"");
					var p = toimg.closest("li");
					
					if(!p.hasClass("clone")){
						index++;
					}
					
					if(!p.hasClass("clone") && src == tosrc){
						p.siblings().removeClass("flex-active-slide");
						
						p.addClass("flex-active-slide");
						var width = p.width();
						
						p.parent().css("-webkit-transition-duration","0.6s");
						p.parent().css("transition-duration","0.6s");
						p.parent().css("-webkit-transform",'translate3d(-'+(index*width)+'px, 0px, 0px)');
						p.parent().css("transform",'translate3d(-'+(index*width)+'px, 0px, 0px)');
						
						return;
					}
				})
			})	
						
		})
	});
}
function qfbookformSubmit(obj){
	if(top==self){
		var p = jQuery(obj).closest(".QFBOOKSearchsimpleformdiv");
		var url = p.attr("action");
		var t1 = p.find("[name='QFBOOKSearch-check-in-view']").val();
		var t2 = p.find("[name='QFBOOKSearch-check-out-view']").val();
		if(url.indexOf("?")>-1){
			location.href = url+"&QFBOOKSearch-check-in-view="+t1+"&QFBOOKSearch-check-out-view="+t2;
		}else{
			location.href = url+"?QFBOOKSearch-check-in-view="+t1+"&QFBOOKSearch-check-out-view="+t2;
		}
		
	}
}
function entersearchqfylist(){
	 var event = window.event || arguments.callee.caller.arguments[0];
     if (event.keyCode == 13)
     {
    	 jQuery(".qfytemplateslist .itemli" ).removeClass("active");
    	 jQuery(".qfytemplateslist .itemli.all-0" ).addClass("active");
		 var tag = "all-0";
    	 searchqfylist(tag,1);
     }
}

function qfytemplate_hover(){
		jQuery('.qfy-template-content .qfy_template_lib a').hover(function() {
			var img = $(this).find('img');
			var imgh = img.height();
			var ph =  img.parent().height();
			if(imgh>ph && imgh>300){
				img.css("transform","translateY(-"+(imgh-ph)+"px)");
				img.css("transition","all 5.5s linear 0s");
			}
		}, function() {
			var img = $(this).find('img');
			img.css("transform","translateY(0px)");
			img.css("transition","all 2s linear 0s");
		});
	
}
function searchqfylist(tag,paged,flag){
	console.trace();
	if(top==self){
		var searchText = jQuery(".qfytemplateslist #form_search").val();
		if(jQuery('.qfytemplateslist .viewmoretemplate .loadingimage').length>0){
			return;
		}
		//jQuery('.qfytemplateslist .qfy-template-content').block({ message: '<img src="http://fast.qifeiye.com/FeiEditor/bitSite/images/preloader.gif" />',css: { backgroundColor: 'transparent',borderColor: 'transparent', margin:"0 0 0 -35px",left:"50%",width:'70px',height:'32px',padding:"8px 0 9px 0"},overlayCSS:{backgroundColor:"#F2F2F2",opacity:"0.9",} });
		jQuery('.qfytemplateslist .viewmoretemplate .viewmoretemplate_inner').hide();
		jQuery('.qfytemplateslist .viewmoretemplate').append("<div class='loadingimage' style='text-align:center;margin:50px auto;'><img style='vertical-align: middle;width:30px;' src='http://fast.qifeiye.com/FeiEditor/bitSite/images/preloader.gif' /><span style='color:#0088c2;margin-left:10px;'>正在自动加载更多模板...</span></div>");
		
		var url = jQuery(".qfytemplateslist").attr("data-url");
		jQuery.post("/admin/admin-ajax.php",{"action":"getqfytemplatelist","url":url,"paged":paged,"searchText":searchText,"tag":jQuery(".qfytemplateslist .itemli.active").attr("data-tag")},function(data){
			jQuery('.qfy_template_lib .viewmoretemplate').waypoint('destroy');
			jQuery('.qfytemplateslist .viewmoretemplate .viewmoretemplate_inner').show();
			jQuery('.qfytemplateslist .viewmoretemplate .loadingimage').remove();
			if(data){
				if(flag=="append"){
					jQuery('.qfytemplateslist .qfy-template-content-inner').append(data);
					jQuery('.qfytemplateslist .viewmoretemplate:first').remove();
				}else{
					jQuery('.qfytemplateslist .qfy-template-content-inner').html(data);
					
				}
				if(jQuery(".qfytemplateslist.zh_tw_lang").length>0){
					StranBody(jQuery(".qfytemplateslist.zh_tw_lang")[0]);
				}
				jQuery(".qfytemplateslist .qfypreloadimg").each(function(){
					var newurl = jQuery(this).attr("data-src");
					var $this = jQuery(this);
					jQuery('<img src="'+newurl+'">').load(function(){
						$this.attr("src",newurl);
					})
				})
				jQuery(".qfytemplateslist  .info").unbind().click(function(){
					var url = jQuery(this).attr("data-url");
					if(top==self){
						window.open(url);
					}
				})
				jQuery(".qfytemplateslist  .use").unbind().click(function(){
					var url = jQuery(this).attr("data-url");
					if(top==self){
						location.href = url;
					}
				})
				jQuery(".qfytemplateslist .viewmoretemplate_inner").unbind().click(function(){
					var tag = "all-0";
					if(jQuery(".qfytemplateslist .itemli.active").attr("data-tag")){
						tag = jQuery(".qfytemplateslist .itemli.active").attr("data-tag")
					}
					var paged = jQuery(this).attr("data-page");
					searchqfylist(tag,paged*1+1,'append');
				})
				qfytemplate_hover();
				
				jQuery(".qfy_template_lib .viewmoretemplate").unbind().waypoint({
					handler: function(direction) {
						if(jQuery(".qfy_template_lib .viewmoretemplate .viewmoretemplate_inner").length>0){
							jQuery(".qfy_template_lib .viewmoretemplate .viewmoretemplate_inner").click();
						}
					
					},
					triggerOnce: true,
					offset: "90%",
				})
			}
		});
	}
}
function parallax_scroll_fun(){
	var old_current_url = window.location.href;
	jQuery("#parallax-nav a").each(function(i){
		var $this = this;
		var obj = jQuery.attr(this, 'href');
		var orgcolor = jQuery("#parallax-nav ul").attr("data-orgcolor");
		if(jQuery("#parallax-nav.mouseEvent").length==0){
			jQuery(obj).waypoint({
				handler: function() {
					if(!jQuery($this).hasClass("active") && isqfyscrolling==false ){
					
						jQuery($this).parent().siblings().removeClass("active");
						jQuery($this).parent().addClass("active");
						jQuery("#parallax-nav li  a").removeAttr("style");
						if(top==self){
							  var current_url = window.location.href;
							  var tmpurl = current_url.split("#");
							  try {
						        	 window.history.pushState({
						                path: window.location.href
						            }, '', tmpurl[0]+jQuery($this).attr("href"));
						        } catch(e) {
						            console.error('history.pushState failed, maybe HTML5 is not supported?')
						        }
						  }
						
						
						if(jQuery("#parallax-nav").hasClass("color-weight-dark")){
							
							var bordercolor = jQuery("#parallax-nav li.active a").attr("data-color");
							if(!bordercolor){
								bordercolor = orgcolor;
							}
							jQuery("#parallax-nav li a").css("border-color",bordercolor).css("background-color","transparent").css("color",bordercolor);
							jQuery("#parallax-nav .active a").css("border-color",bordercolor).css("background-color",bordercolor).css("color",bordercolor);
							
						}else{
							var bordercolor = jQuery("#parallax-nav li.active a").attr("data-color");
							if(!bordercolor){
								bordercolor = orgcolor;
							}
							jQuery("#parallax-nav li a").css("border-color","transparent").css("background-color",bordercolor).css("color",bordercolor);
							jQuery("#parallax-nav li.active a").css("border-color",bordercolor).css("background-color","transparent").css("color",bordercolor);
						}
					}
				},
				/*horizontal:true,*/
				triggerOnce: false
			})
		}
		jQuery($this).parent().click(function(e) {
			e.preventDefault();
			qfyToscroll($this);
			return false;
		});
		
	})

	if(old_current_url.indexOf("#")<0){
		jQuery("#parallax-nav li:first:visible").click();
	}
	
	if(jQuery("#parallax-nav.mouseEvent").length>0){
		
		var qfyscrollFunc=function(e){ 
			if(e){
				 e.preventDefault();
				 e.stopPropagation();
			}
			if( (e.wheelDelta && e.wheelDelta>0 ) || (e.detail && e.detail<0 )){
				//上
				var thisobj = jQuery("#parallax-nav.mouseEvent .active").prev().find("a");
				if(thisobj.length>0){
					qfyToscroll(thisobj)
					return false;
				}
				
			}else{
				
				var thisobj = jQuery("#parallax-nav.mouseEvent .active").next().find("a");
				if(thisobj.length>0){
					qfyToscroll(thisobj);
					return false;
				}
				
			}
		}  
		if(document.addEventListener){ 
			document.addEventListener('DOMMouseScroll',qfyscrollFunc,false); 
		}//W3C 
		window.onmousewheel=document.onmousewheel=qfyscrollFunc;

	}
	
}

var isqfyscrolling =false;
function qfyToscroll(thisobj){
		if(isqfyscrolling){
			return;
		}
		isqfyscrolling = true;
		var obj = jQuery(thisobj).attr('href');
		jQuery(thisobj).parent().siblings().removeClass("active");
		jQuery(thisobj).parent().addClass("active");
		var orgcolor = jQuery(thisobj).closest("ul").attr("data-orgcolor");
		jQuery("#parallax-nav li  a").removeAttr("style");
		if(jQuery("#parallax-nav").hasClass("color-weight-dark")){
			if(orgcolor){
				jQuery("#parallax-nav li  a").css("border-color",orgcolor).css("color",orgcolor);
			}
			var bordercolor = jQuery("#parallax-nav li.active a").attr("data-color");
			if(!bordercolor){
				bordercolor = orgcolor;
			}
			jQuery("#parallax-nav li a").css("border-color",bordercolor).css("background-color","transparent").css("color",bordercolor);
			jQuery("#parallax-nav .active a").css("border-color",bordercolor).css("background-color",bordercolor).css("color",bordercolor);
			
		
		}else{
			if(orgcolor){
				jQuery("#parallax-nav li  a").css("background-color",orgcolor).css("color",orgcolor);
			}
			var bordercolor = jQuery("#parallax-nav li.active a").attr("data-color");
			if(!bordercolor){
				bordercolor = orgcolor;
			}
			jQuery("#parallax-nav li a").css("border-color","transparent").css("background-color",bordercolor).css("color",bordercolor);
			jQuery("#parallax-nav .active a").css("border-color",bordercolor).css("background-color","transparent").css("color",bordercolor);
		}

		var speed = 'normal';
		speed = 1000;
		if(jQuery("#parallax-nav").hasClass("speedquick")){
			speed = speed*0.75;
		}else if(jQuery("#parallax-nav").hasClass("speedslow")){
			speed = speed*1.5;
		}
		if(obj=="#"||obj=="#header"){
			jQuery("html, body").animate({ scrollTop: 0 }, speed,function(){isqfyscrolling=false;
				  if(top==self){
					  var current_url = window.location.href;
					  var tmpurl = current_url.split("#");
					  try {
				        	 window.history.pushState({
				                path: window.location.href
				            }, '', tmpurl[0]+obj);
				        } catch(e) {
				            console.error('history.pushState failed, maybe HTML5 is not supported?')
				        }
				  }
			});
		}else{
			if(jQuery( jQuery(thisobj).attr('href') ).length>0){
				jQuery("html, body").animate({ scrollTop: jQuery( jQuery(thisobj).attr('href') ).offset().top }, speed,'easeInOutExpo',function(){isqfyscrolling=false;
				   if(top==self){
					  var current_url = window.location.href;
					  var tmpurl = current_url.split("#");
					  var tmpobj = jQuery(thisobj).attr('href');
					  try {
				        	 window.history.pushState({
				                path: window.location.href
				            }, '', tmpurl[0]+tmpobj);
				        } catch(e) {
				            console.error('history.pushState failed, maybe HTML5 is not supported?')
				        }
				  }
				
				});
			}else{
				isqfyscrolling=false;
			}
		}
}
function checkAdvertising(){
	var uuid = dtGlobals.id;
	var href = 'http://www.qifeiye.com?tcode=freesite&uuid='+uuid;
	if(dtGlobals.qfymodel){ href="#";}

	var style = 'display:block !important;opacity: 1 !important;position:fixed !important;bottom:40px !important;left:0 !important;width:116px !important;height:25px !important;line-height:24px !important;background:#23282D !important;z-index:2147483647 !important;text-align:center;color:#fff !important;font-size:12px;border-top-right-radius:3px;border-bottom-right-radius:3px;';
	var content = 'BY 起飞页自助建站';
	if(jQuery(".qfy_advertising").length==0){
		var tmphtml= '<a class="wf-mobile-hidden qfy_advertising" target=_blank style="'+style+'"  rel="external nofollow" href="'+href+'" >'+content+'</a>';
		jQuery("#page").append(tmphtml);
	}
	
	var opacity = jQuery(".qfy_advertising").css("opacity");

	if(opacity!="1"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	if(jQuery(".qfy_advertising").css("position")!="fixed"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	if(jQuery(".qfy_advertising").css("bottom")!="40px"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	if(jQuery(".qfy_advertising").css("left")!="0px"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var bgcolor = jQuery(".qfy_advertising").css("background-color");
	if(bgcolor!="#000000" && bgcolor!='rgb(0, 0, 0)'){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var color = jQuery(".qfy_advertising").css("color");
	if(color!="#ffffff" &&  color!="rgb(255, 255, 255)"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var marginLeft = jQuery(".qfy_advertising").css("margin-left");
	if(marginLeft!="0px"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var marginTop = jQuery(".qfy_advertising").css("margin-top");
	if(marginTop!="0px"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var zindex = jQuery(".qfy_advertising").css("z-index");
	if(zindex!="2147483647"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var width = jQuery(".qfy_advertising").css("width");
	if(width!="116px"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var height = jQuery(".qfy_advertising").css("height");
	if(height!="20px"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var transform = jQuery(".qfy_advertising").css("transform");
	if(transform!="none"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var visible =  jQuery(".qfy_advertising").css("visibility");
	if(visible!="visible"){
		jQuery(".qfy_advertising").attr("style",style);
	}
	var display = jQuery(".qfy_advertising").css("display");
	if(display!="block"&& jQuery("body").width()>760){
		jQuery(".qfy_advertising").attr("style",style);
	}
	if(jQuery(".qfy_advertising").attr("href")!=href){
		jQuery(".qfy_advertising").attr("href",href);
	}
	if(jQuery(".qfy_advertising").html()!=content){
		jQuery(".qfy_advertising").html(content);
	}
	//检查预览下是否在iframe
	if(top!=self ){		
		try{
			//不同host
			if(top.window.location.host!=window.location.host){
				location.href = "http://www.qifeiye.com";
			}
		}catch(e){
			//跨域啦
			location.href = "http://www.qifeiye.com";
		}
	}
	
	
	
	jQuery("#footer_band").hide();
	
}
function clickscode(obj){
	jQuery(obj).attr('src','/api/scode.php'+'?'+Math.random());
}
function nav_pagemore(obj){
	if(jQuery(obj).hasClass("vc")){
		jAlert("编辑情况下，无法点击，请在预览情况下使用！");
		return;
	}
	if(jQuery(obj).find(".loading").length>0){
		return ;
	}
	if(jQuery(obj).hasClass("notmore")){
		return ;
	}
	var type=0;
	if(jQuery(obj).parent().hasClass("bitcommerce-pagination")){
		var p = jQuery(obj).closest(".bitcommerce").parent();
		var id = p.attr("qfyuuid");
		var luClass = "[qfyuuid='"+id+"'] ul.products";
		var childClass = "ul.products";
	}else if(jQuery(obj).closest(".qfy_images_list").length>0){
		var p = jQuery(obj).closest(".qfy_images_list");
		var id = p.attr("qfyuuid");
		var luClass = "[qfyuuid='"+id+"'] .qfe_images_list";
		var childClass = ".qfe_images_list";
		if(jQuery(luClass).find(".more.mypages").length>0){
			type = 1;
		}
	}else if(jQuery(obj).closest(".advanced_list").length>0){
		var p = jQuery(obj).closest(".advanced_list");
		var id = p.attr("qfyuuid");
		var luClass = "[qfyuuid='"+id+"'] .qfe_wrapper ";
		var childClass = ".qfe_wrapper";
		type = 0;
		if(p.find(".list-style7").length>0){
			type = 1;
		}
	}else{
		var p = jQuery(obj).closest(".qfe_teaser_grid");
		var id = p.attr("qfyuuid");
		var luClass = "[qfyuuid='"+id+"'].qfe_teaser_grid.qfe_content_element ul.qfe_thumbnails";
		var childClass = "ul.qfe_thumbnails";
	}
	
	if(p.find("div.qfy_list_loading").length>0){
		return ;
	}
	var list_id = p.find(".qfe_teaser_grid.qfe_content_element").attr("id");
	
	p.find(".qfe_teaser_grid.qfe_content_element .teaser_grid_container").addClass("noanimale");
	p.find(luClass+" li").css("transform","none").css("top","auto").css("position","relative");
	var url = jQuery(obj).attr("data-url");
	var loadtext = jQuery(obj).attr("data-loadtext");
	var old_html = jQuery(obj).find("a").html();
	jQuery(obj).hide();
	jQuery(obj).after('<div class="qfy_list_loading"  style="margin:0 auto;text-align:center;"><img src="/FeiEditor/bitSite/images/spinner.gif"><span style="color:#808080;font-size:12px;position: relative; top: -3px;">&nbsp;'+loadtext+'</span></div>');
	jQuery.get(url,function(data){
		if(list_id){
			var nowlistobj =jQuery(data).find("#"+list_id+" ul.qfe_thumbnails");
		}else{
			var nowlistobj =jQuery(data).find(luClass);
		}
		
		var pagemore = nowlistobj.closest(".qfy-element").find(".more.mypages");

		var style = nowlistobj.find(" >li").attr("style");
		p.find(childClass).append(nowlistobj.html());
		
		
		p.find(childClass).height("auto");
		p.find(childClass+" li").attr("style",style).css("transform","none").css("top","auto").css("position","relative");
		jQuery(obj).show();
		//jQuery("html, body").animate({ scrollTop:  jQuery(obj).offset().top - jQuery(window).height()/2 }, "fast");
		p.find("div.qfy_list_loading").remove();
		if(type=="1"){
			jQuery(obj).remove();
		}

		if(pagemore.length>0){
			jQuery(obj).attr("data-url",pagemore.attr("data-url"));
			jQuery(obj).attr("style",pagemore.attr("style"));
		}else{
			jQuery(obj).remove();
		}
		
		if(p.find(".categories_filter").length>0){
			p.find(".categories_filter a[data-filter='*']").click();
		}
		
		if(p.attr("data-open")=="1")
			changelistlinkfun(p);
		p.find('div.divmiddle').each(function(){
			var pimage = jQuery(this).closest(".images");
			if(pimage.find("img").length>0){
				var $this = this;
				jQuery('<img src="'+pimage.find("img").attr("src")+'">').load(function(){
						var h = jQuery($this).height();
						if(h>0){
							jQuery($this).css("margin-top","-"+(h/2)+"px");
						}
				})
			
			}else{
				var h = jQuery(this).height();
				if(h>0){
					jQuery(this).css("margin-top","-"+(h/2)+"px");
				}
			}

			
		})
		vc_js_init();
		tranlanguage(p);
	})
}
function bindqfylist(obj,curr){
	jQuery("body").addClass("doing");
	var c = jQuery(obj).closest(".qfy-listcatecontrols");
	
	jQuery(".qfy-element.qfe_teaser_grid.qfe_content_element,.qfy-element.qfyproductlist").css("outline","10px solid #FF6600").css("cursor","cursor").css("opacity","0.6");
	jQuery(".qfy-element.qfe_teaser_grid.qfe_content_element,.qfy-element.qfyproductlist").unbind("click.bindqfylist").bind("click.bindqfylist",function(e){
		var item = $(e.currentTarget);
		
		jQuery("body").removeClass("doing");
		var list =   item.closest(".qfy-element.qfe_teaser_grid.qfe_content_element,.qfy-element.qfyproductlist");
		var val = list.attr("qfyuuid");
		var post_type =  list.attr("data-post");
		var post_cate =  list.attr("data-cate");

		//能添加uuid，则添加uuid
		if(list.parent().attr("data-model-id") && post_type!="page" && (!val || val==0 || val=="") ){
			var listmodel_id = list.parent().attr("data-model-id");
			var listmodel =  parent.vc.shortcodes.get(listmodel_id);
			var listparams = listmodel.get('params');
			listparams.qfyuuid = "qfy_posts_grid_"+parent.vc.ShortcodesBuilder.randomString();
			listmodel.save({params: listparams}, {silent: true});
			val = listparams.qfyuuid;
			list.attr("qfyuuid",val);
		}
		var pageid=0;
		if(top.jQuery("#pageUrl").length>0){
			pageid = top.jQuery("#pageUrl").val();
		}

		if(val && val!=0 && val!="" && post_type!="page"){
			top.jAlert("你已经成功绑定了列表");
			if(c.parent().attr("data-model-id")){

				var model_id = c.parent().attr("data-model-id");
				var model =  parent.vc.shortcodes.get(model_id);
				var params = model.get('params');
				params.cate_to_listuuid = val;
				params.cate_type = post_type;
				
				if(post_cate!=""){
					if(post_type=="products"){
						params.cate_products_selected_id = post_cate;
					}else if(post_type=="product"){
						params.cate_product_selected_id = post_cate;
					}else if(post_type=="image"){
						params.cate_image_selected_id = post_cate;
					}else if(post_type=="video"){
						params.cate_video_selected_id = post_cate;
					}else if(post_type=="yunvideo"){
						params.cate_yunvideo_selected_id = post_cate;
					}else{
						params.cate_post_selected_id = post_cate;
					}
				}
				if(curr && curr.length>0){ 
					curr.find("[name='cate_to_listuuid']").val(val);
					curr.find("[name='cate_type']").val(post_type).change();
					if(post_cate!=""){
						if(post_type=="products"){
							curr.find("[name='cate_products_selected_id']").val(post_cate)
						}else if(post_type=="product"){
							curr.find("[name='cate_product_selected_id']").val(post_cate)
						}else if(post_type=="image"){
							curr.find("[name='cate_image_selected_id']").val(post_cate)
						}else if(post_type=="video"){
							curr.find("[name='cate_video_selected_id']").val(post_cate)
						}else if(post_type=="yunvideo"){
							curr.find("[name='cate_yunvideo_selected_id']").val(post_cate)
						}else{
							curr.find("[name='cate_post_selected_id']").val(post_cate)
						}
					}
				}
				
				model.save({params: params});
				parent.ajaxBeforeLoading(0,model.view.$el);
				parent.vc.ShortcodesBuilder.update(model);
				parent.updatePage(model, null);
			}else if(c.closest(".site_tooler").length>0){
				var wid = c.closest(".site_tooler").attr("id");
				jQuery.post("/FeiEditor/siteEdit/updatePostlistOneparam" ,{"wid":wid,"bind_pageid":pageid,"cate_to_listuuid":val,"cate_type":post_type,"post_type_selected":post_cate}, function(rs){
						var p =  jQuery(obj).closest('.site_tooler');
						parent.loaderDivUI(p);
						parent.updateWidgetViewEvent(wid,p);
				});
			}
			jQuery(".qfy-element.qfe_teaser_grid.qfe_content_element,.qfy-element.qfyproductlist").css("outline","0").css("cursor","normal").css("opacity","1");
			jQuery(".qfy-element.qfe_teaser_grid.qfe_content_element,.qfy-element.qfyproductlist").unbind("click.bindqfylist");
		}else if(post_type=="page" ){
			top.jAlert("这个列表类型不正确，只能绑定资讯和产品列表");
		}else{
			top.jAlert("这个列表还没有设置自己的UUID，你可以在设置中添加该列表唯一的UUID");
		}
		
	})
}

function nav_pagecate_confirm(obj){
	
	if(jQuery(obj).find(".loading").length>0){
		return ;
	}
	
	var li = jQuery(obj).closest("li");
	var li_id = li.attr("data-id");
	var c = jQuery(obj).closest(".qfy-listcatecontrols");
	var type = c.attr("type");
	var id = c.attr("to_qfyuuid");
	if(!id ||!type){
		jAlert("没有找到对应的列表");
		return ;
	}

	if(jQuery(obj).closest(".dl-menuwrapper.dropCenterStyle_containter").length>0 && !li.hasClass("has-children") ){
		jQuery(obj).closest(".dl-menuwrapper.dropCenterStyle_containter").find("a.dropCenterStyle").click();
	}
	
	c.find(".current-cat").removeClass("current-cat current-menu-item");
	li.addClass("current-cat current-menu-item");
	if(c.attr("data-child")=="1"){
		c.find(".list-content .children").hide();
		li.parents("li,ul").show();
		if(li.hasClass("cat-parent") && li.closest(".list-content").length>0 ) li.find(">.children").show();
	}
	if(c.find(".item_a").length>0){
		c.find(".item_a").removeClass("active");
		li.find(".item_a").addClass("active");
	}
	var p = jQuery("[qfyuuid='"+id+"'].qfe_teaser_grid.qfe_content_element").parent();
	var model_id = p.attr("data-model-id");
	if(model_id && parent){
		parent.ajaxBeforeLoading(0,p);
		var model =  parent.vc.shortcodes.get(model_id);
		var params = model.get('params');
		if(type)
			params.post_type = type;
		if(type=="products"){
			var pro_old = params.pro_categories;
			params.pro_categories = li_id;
		}else if(type=="product"){
			var product_old = params.product_categories;
			params.product_categories = li_id;
		}else{
			var post_old = params.post_categories;
			params.post_categories = li_id;
		}
		if(params.parse_url)
			var parse_url_old =  params.parse_url;
		if(params.parse_search)
			var parse_search_old =  params.parse_search;
		params.parse_url ="0";
		params.parse_search ="0";

		model.save({params: params});
        parent.vc.ShortcodesBuilder.update(model);
		if(type=="products"){
			params.pro_categories = pro_old;
		}else if(type=="product"){
			params.product_categories = product_old;
		}else{
			params.post_categories = post_old;
		}
		if(parse_url_old)
			params.parse_url = parse_url_old;
		if(parse_search_old)
			params.parse_search = parse_search_old;
		model.save({params: params});
	}

}
function nav_customsearch(obj){
	if(top.jQuery("#site-html #site-body").length>0){
		top.jAlert("请在预览下查看");
		return;
	}

	var li = jQuery(obj);
	var ul = jQuery(obj).closest("ul");
	var url = ul.attr("data-url");
	url = url.replace(/q_term=-?\d*&/g,"");

	url = url.replace(/q_term-key_\d*=-?cs-\d*-[^&]*&/g,"");
	url = url.replace(/q_term-key_\d*=-?pt-\d*-[^&]*&/g,"");
	url = url.replace(/q_term-key_\d*=-?[^&]*&/g,"");
	url = url.replace(/q_term-key_sub_\d*=-?[^&]*&/g,"");
	
	
	var p = ul.closest(".mainul");
	var data_key = li.attr("data-key");
	if(data_key && data_key.indexOf("sub")<0){
		p.find("[data-key=sub_"+data_key+"]").removeClass("active");
	}
	
	if(li.hasClass("active")){
		if(ul.closest("ul.subul").attr("data-morecheck")=="0" ){
			ul.find(">li").removeClass("active");
		}else{
			li.removeClass("active");
		}
	}else{
		if(ul.closest("ul.subul").attr("data-morecheck")=="0" || li.attr("data-id")=="-1"){
			ul.find(">li").removeClass("active");
		}
		li.addClass("active");
	}
	var out = [];
	var i = 0;
	p.find("ul.subul").each(function(){
		var key = jQuery(this).attr("data-key");
		var morecheck = jQuery(this).attr("data-morecheck");
		
		if(jQuery(this).find("li.active").length>0){
			var ids = new Array();
			var len = jQuery(this).find("li.active").length;
			var n = 0;
			jQuery(this).find("li.active").each(function(){
				if(len>1  ){
					if( jQuery(this).attr("data-id")!="-1"){
						ids[n]= jQuery(this).attr("data-id");
						n++;
					}
				}else{
					ids[n]= jQuery(this).attr("data-id");
					n++;
				}
			});
			out[i++]  = "q_term-key_"+key+"="+ids.join(",");
		}else{
			if(jQuery(this).attr("data-default")!="" && jQuery(this).attr("data-default")!="0"){
				//none,not default
				out[i++]  = "q_term-key_"+key+"=-2";
			}
		}
	})
	
	
	if(p.attr("data-ajax")=="1"){
		url = url+"&"+out.join("&");
		var touuid = p.attr("data-touuid");
		var posttype = p.attr("data-posttype");
		
		var id = ul.closest(".qfy_custom_search ").attr("id");
		var image_src = "//fast.qifeiye.com/qfy-content/plugins/qfy_form/admin/images/loading.gif";
		var w = "";
		if(touuid!="-1"){
			var plist = jQuery(".qfy-element[qfyuuid='"+touuid+"']");
		}else{
			if(posttype=="product"){
				var plist = jQuery("#main .qfyproductlist:first");
			}else{
				var plist = jQuery("#main .qfe_teaser_grid:first");
			}
		}
		var pp = plist.parent();
		if(plist.attr("data-loading")){
			image_src = plist.attr("data-loading");
			w = plist.attr("data-loading-w");
		}
		
		plist.html('<div class="qfy_list_loading"  style="margin:50px auto;text-align:center;"><img  style="width:'+w+'px"  src="'+image_src+'"></div>');
		

		jQuery.post(url,function(data){
			if(touuid!="-1"){
				var list = jQuery(data).find(".qfy-element[qfyuuid='"+touuid+"']");

			}else{
				if(posttype=="product"){
					var list = jQuery(data).find("#main .qfyproductlist:first");
				}else{
					var list = jQuery(data).find("#main .qfe_teaser_grid:first");
				}
			}
			if(list.length>0){
				plist.replaceWith(list);
			}
			var search = jQuery(data).find("#"+id);
			if(search.length>0){
				jQuery("#"+id).replaceWith(search);
			}
		
			if(plist.attr("data-open")=="1"){
				changelistlinkfun(pp);
			}
			window.vc_js_init2();
			window.vc_js_init();
			if(plist.find(".noResult").length==0){
				 jQuery('[data-ride="vc-carousel"]').each(function(){
					qfy_carousel_fun(jQuery(this))
				 })
			 }
			tranlanguage(pp);
		})
	}else{
		location.href=url+"&"+out.join("&");
	}
	return;
}
function tranlanguage(htm){

	if(jQuery("body.qfy_slw_tzh").length==1){
		StranBody(htm[0]);
		
	}else if(jQuery("body.qfy_slw_szh").length==1){
		StranBody(htm[0],0);
		
	}
	
}
function nav_pagecate(obj,flag){
	
	if(jQuery(obj).find(".loading").length>0){
		return ;
	}
	var type=0;
	var li = jQuery(obj).closest("li");
	var c = jQuery(obj).closest(".qfy-listcatecontrols");
	var id = c.attr("to_qfyuuid");
	var p = jQuery("[qfyuuid='"+id+"']");
	if(p.length==0 && jQuery(obj).attr("data-bindurl")){
		location.href = jQuery(obj).attr("data-bindurl");
		return;
	}else if(flag=="direct"){
		location.href = jQuery(obj).attr("data-url");
		return;
	}


	var luClass = "[qfyuuid='"+id+"'].qfe_teaser_grid.qfe_content_element ul.qfe_thumbnails";
	var childClass = "ul.qfe_thumbnails";
	if(p.find("div.qfy_list_loading").length>0){
		return ;
	}
	
	if(jQuery(obj).closest(".dl-menuwrapper.dropCenterStyle_containter").length>0 && !li.hasClass("has-children")){
		jQuery(obj).closest(".dl-menuwrapper.dropCenterStyle_containter").find("a.dropCenterStyle").click();
	}
	
	p.find(".qfe_teaser_grid.qfe_content_element .teaser_grid_container").addClass("noanimale");
	p.find(luClass+" li").css("transform","none").css("top","auto").css("position","relative");
	var url = jQuery(obj).attr("data-url");
	
	
	c.find(".current-cat").removeClass("current-cat current-menu-item");
	li.addClass("current-cat current-menu-item");
	if(c.attr("data-child")=="1"){
		c.find(".list-content .children").hide();
		li.parents("li,ul").show();
		if(li.hasClass("cat-parent") && li.closest(".list-content").length>0) li.find(">.children").show();
	}
	if(c.find(".item_a").length>0){
		c.find(".item_a").removeClass("active");
		li.find(".item_a").addClass("active");
	}
	var image_src = "//fast.qifeiye.com/qfy-content/plugins/qfy_form/admin/images/loading.gif";
	var w = "";
	if(p.attr("data-loading")){
		image_src = p.attr("data-loading");
		w = p.attr("data-loading-w");
	}

	var text = jQuery.trim(jQuery(obj).text());
	jQuery(obj).closest(".dl-menuwrapper").find(".phone-text").html(text);
	
	p.html('<div class="qfy_list_loading"  style="margin:50px auto;text-align:center;"><img style="width:'+w+'px" src="'+image_src+'"></div>');
	jQuery.get(url,function(data){	
		var nowlistobj =jQuery(data).find("[qfyuuid='"+id+"']");
		if(nowlistobj.length>0){
			p.replaceWith(nowlistobj);	
			window.vc_js_init2();
			window.vc_js_init();
			 if(p.find(".noResult").length==0){
				 jQuery('[data-ride="vc-carousel"]').each(function(){
					qfy_carousel_fun(jQuery(this))
				 })
			 }
			 tranlanguage(p);
		}
		if(nowlistobj.attr("data-open")=="1")
			changelistlinkfun(nowlistobj);
	})
}
var noticeTimeoutShow = false;
function qfy_notice_event(){
	jQuery(".qfy-element.qfy_notice .notice_warp").each(function(){
		notice_pre_event(this);
	})
	
	jQuery("a[href^='qfy_notice']").unbind().click(function(e){
		 e.preventDefault();
		 e.stopPropagation();
		 var id = jQuery(this).attr("href");
		 if(jQuery("#"+id).length>0){
			 notice_pre_event("#"+id+" .notice_warp","preview");
		 }
	})
}
function notice_pre_event(thisobj,flag){
	
	var tooler = jQuery(thisobj).closest(".site_tooler");
	if( tooler.length>0 && tooler.css("display")=="none" ){
		return;
	}
	var event = jQuery(thisobj).attr("data-event");
	var event_delay = jQuery(thisobj).attr("data-event_delay");
	var showtimes = jQuery(thisobj).attr("data-showtimes");
	var closetime = jQuery(thisobj).attr("data-closetime");
	var id = jQuery(thisobj).parent().attr("id");
	var contenttype = jQuery(thisobj).attr("data-contenttype");
	var post_id = jQuery(thisobj).attr("data-post_id");
	var contenttype = jQuery(thisobj).attr("data-contenttype");
	var torun = true;
	var num = 0;
	
	if(flag=="preview"){
		torun = true;
		//closetime = 0;
		event = 0;
	}else{
		if(id &&　showtimes !=3){
			var num = getCookie(id);
			if(showtimes=="0" && num>0){
				torun = false;
			}else if(showtimes=="1" && num>1){
				torun = false;
			}else if(showtimes=="2" && num>2){
				torun = false;
			}else if(showtimes=="4"){
				var is_confirm = getCookie(id+"_confirm");
				if(is_confirm>0){
					torun = false;
				}
			}
		}
	}
	if(torun){
		if(num) num = num+1;
		else num = 1;
		if(flag!="preview"){
			setCookie(id,num,"365");
		}
		if(contenttype == "2"){
			//ajax
			if(!post_id || post_id==""){
				content = "对应的组件页面不存在(ID:0)。请重新指定一个组件页面。";
				notice_event(thisobj,content);
				return;
			}else{
				jQuery.post("/admin/admin-ajax.php",{"action":"get_noticepostcontent_from_id","post_id":post_id},function(data){
					if(data.indexOf("Error:")!==0){
						if(event=="0" || (event=="1" && event_delay==0)){
							notice_event(thisobj,data);
						}else if(event=="1"){
							noticeTimeoutShow = setTimeout(function(){notice_event(thisobj,data);},event_delay*1000);
						}
						if(closetime>0){
							setTimeout(function(){
								if(noticeTimeoutShow)
									clearTimeout(noticeTimeoutShow);
								//close it
								$('#qfy_float_ad.'+id+',#QFY_overlay.'+id+',#QFY_window.'+id).remove();
							},closetime*1000);
							
						}
					}else {
						content = "对应的组件页面不存在(ID:"+post_id+")。请重新指定一个组件页面。";
						notice_event(thisobj,content);
						return;
					}
				});
			}
			
		}else{
			if(event=="0" || (event=="1" && event_delay==0)){
				notice_event(thisobj);
			}else if(event=="1"){
				var $this = thisobj;
				noticeTimeoutShow = setTimeout(function(){notice_event(thisobj);},event_delay*1000);
			}
			if(closetime>0){
				setTimeout(function(){
					if(noticeTimeoutShow)
						clearTimeout(noticeTimeoutShow);
					//close it
					//$('#qfy_float_ad.'+id+',#QFY_overlay.'+id+',#QFY_window.'+id).remove();
					notice_close_fun("#"+id+" .notice_warp");
				},closetime*1000);
				
			}
			
			
		}

		
	}
}
function notice_close_fun(thisobj){
	var obj = jQuery(thisobj); 
	var id_class = jQuery(thisobj).parent().attr("id");
	var animale = obj.attr("data-animale");
	var close_animale = obj.attr("data-close_animale");
	if(close_animale=="1"){
		jQuery("#QFY_window."+id_class+",#QFY_overlay."+id_class).remove();
	}else{
		if(animale=="0"){
			jQuery("#QFY_window."+id_class).stop(true).animate({opacity:"0"},'fast',function(){
				jQuery("#QFY_window."+id_class+",#QFY_overlay."+id_class).remove();
			});
		}else if(animale=="1"){
			jQuery("#QFY_window."+id_class).stop(true).animate({top:"-100%",opacity:"0"},'fast',function(){
				jQuery("#QFY_window."+id_class+",#QFY_overlay."+id_class).remove();
			})
		}else if(animale=="2"){
			jQuery("#QFY_window."+id_class).stop(true).animate({top:"100%",opacity:"0"},'fast',function(){
				jQuery("#QFY_window."+id_class+",#QFY_overlay."+id_class).remove();
			})
		}else if(animale=="3"){
			jQuery("#QFY_window."+id_class).stop(true).animate({left:"-100%",opacity:"0"},'fast',function(){
				jQuery("#QFY_window."+id_class+",#QFY_overlay."+id_class).remove();
			})
		}else if(animale=="4"){
			jQuery("#QFY_window."+id_class).stop(true).animate({left:"100%",opacity:"0"},'fast',function(){
				jQuery("#QFY_window."+id_class+",#QFY_overlay."+id_class).remove();
			})
		}
	}

	
}
function notice_event(thisobj,precontent){
	var id_class = jQuery(thisobj).parent().attr("id");
	jQuery("#QFY_window."+id_class+",#QFY_overlay."+id_class+",#qfy_float_ad."+id_class).remove();
	jQuery("body").removeAttr("style");
	
	var obj = jQuery(thisobj); 
	var device = obj.attr("data-device");
	if(device=="1" && jQuery(window).width()<760){
		return;
	}else if(device=="2" && jQuery(window).width()>760){
		return;
	}
	var type = obj.attr("data-type");
	var align = obj.attr("data-align");
	var screenpadding = obj.attr("data-screenpadding")?obj.attr("data-screenpadding"):0;
	var animale = obj.attr("data-animale");
	var animale_time = obj.attr("data-animale_time");
	if(animale_time<=0 || !animale_time) animale_time =1;
	var qfy_width = obj.attr("data-width")?obj.attr("data-width"):600;
	var qfy_height = obj.attr("data-height")?obj.attr("data-height"):400;
	if(jQuery(window).width()<760){
		qfy_width = obj.attr("data-mobilewidth")?obj.attr("data-mobilewidth"):300;
		qfy_height = obj.attr("data-mobileheight")?obj.attr("data-mobileheight"):300;
	}
	
	var allow_close = obj.attr("data-allow_close");
	var btn_align = obj.attr("data-btn_align");
	var z_index = obj.attr("data-z_index");
	var btn_position = obj.attr("data-btn_position");
	var btn_position_x = obj.attr("data-btn_position_x");
	if(qfy_width.indexOf("%")>-1){
		qfy_width = qfy_width.replace("%","");
		if(qfy_width>100) qfy_width =100;
		if(qfy_width<10) qfy_width =10;
		qfy_width = jQuery(window).width()*qfy_width/100;
	}
	if(qfy_height.indexOf("%")>-1){
		qfy_height = qfy_height.replace("%","");
		if(qfy_height>100) qfy_height =100;
		if(qfy_height<10) qfy_height =10;
		qfy_height = jQuery(window).height()*qfy_height/100;
	}
	
	var content = obj.html();
	//content = content.replace("display:none;","").replace("display:none;","").replace("display:none;","").replace("display:none;","");
	
	if(precontent){
		//ajax
		content = content.replace('<div class="notice_loading"></div>',precontent);
	}
	var style = "opacity:0;";

	var marginleft = parseInt((qfy_width / 2),10);
	var margintop = parseInt((qfy_height / 2),10);
	var totop = "50%";
	var defaultleft = "50%";
	var toleft = "50%";
	if(type=="0" || type=="1"){
		if(align=="0"){
			if(animale=="1"){
				style += "top:0;";
			}else if(animale=="2"){
				style += "top:100%;";
			}else{
				style += "top:50%;";
			}
			
			if(animale=="3"){
				defaultleft = "0";
			}else if(animale=="4"){
				defaultleft = "100%";
			}
			style += "margin-top:-"+margintop+"px;margin-left:-"+marginleft+"px; position: fixed; left: "+defaultleft+";z-index:10003";
			
			totop = "50%";
			toleft = "50%";
			
		}else if(align=="1"){
			if(animale=="1"){
				style += "top:-50%;";
			}else if(animale=="2"){
				style += "top:50%;";
			}else{
				style += "top:0;";
			}
			
			if(animale=="3"){
				defaultleft = "0";
			}else if(animale=="4"){
				defaultleft = "100%";
			}
			style += "margin-top:"+screenpadding+"px;margin-left:-"+marginleft+"px; position: fixed; left: "+defaultleft+";z-index:10003";
			
			totop = "0%";
			toleft = "50%";
		}else if(align=="2"){
			if(animale=="1"){
				//style += "top:50%;";
				style += "top:"+(jQuery(window).height() - 2*qfy_height)+"px;";
			}else if(animale=="2"){
				//style += "top:100%;";
				style += "top:"+jQuery(window).height()+"px;";
			}else {
				style += "bottom:0;";
			} 
			
			if(animale=="3"){
				defaultleft = "0";
			}else if(animale=="4"){
				defaultleft = "100%";
			}
			style += "margin-bottom:"+screenpadding+"px;margin-left:-"+marginleft+"px; position: fixed; left: "+defaultleft+";z-index:10003";
			
			totop = jQuery(window).height() - qfy_height;
			toleft = "50%";
		}else if(align=="3"){
			 if(animale=="1"){
				style += "top:0;";
			}else if(animale=="2"){
				style += "top:100%;";
			}else {
				style += "top:50%;";
			}
			
			if(animale=="3"){
				defaultleft = -qfy_width;
			}else if(animale=="4"){
				defaultleft = qfy_width;
			}else{
				defaultleft = screenpadding;
			}
			style += "margin-top:-"+margintop+"px; position: fixed; left: "+defaultleft+"px;z-index:10003";
			
			totop = "50%";
			toleft = screenpadding;
		}else if(align=="4"){
			
			if(animale=="1"){
				style += "top:0;";
			}else if(animale=="2"){
				style += "top:100%;";
			}else {
				style += "top:50%;";
			}
			
			if(animale=="3"){
				defaultleft = (jQuery(window).width() - 2*qfy_width)+"px";
			}else if(animale=="4"){
				defaultleft = jQuery(window).width()+"px";
			}else{
				defaultleft = "auto;right:0";
			}
			
			style += "margin-top:-"+margintop+"px;position: fixed; margin-right: "+screenpadding+"px;left:"+defaultleft+";z-index:10003";
				
			
			totop = "50%";
			toleft = jQuery(window).width() - qfy_width;
		}else if(align=="5"){
			var defaultleft ="0";
			var screeny = obj.attr("data-screeny");
			var screenx = obj.attr("data-screenx");
			if(animale=="1"){
				style += "top:-50%;";
			}else if(animale=="2"){
				style += "top:50%;";
			}else{
				style += "top:0;";
			}
			
			if(animale=="3"){
				defaultleft = "-"+qfy_width+"px";
			}else if(animale=="4"){
				defaultleft = qfy_width+"px";
			}
			style += "margin-top:"+screeny+"px;margin-left:"+screenx+"px; position: fixed; left: "+defaultleft+";z-index:10003";
			
			totop = "0%";
			toleft = "0%";
		}else if(align=="6"){
			var defaultleft = (jQuery(window).width() - qfy_width)+"px";
			var screeny = obj.attr("data-screeny");
			var screenx = obj.attr("data-screenx");
			if(animale=="1"){
				style += "top:-50%;";
			}else if(animale=="2"){
				style += "top:50%;";
			}else{
				style += "top:0;";
			}
			
			if(animale=="3"){
				defaultleft = (jQuery(window).width() - 2*qfy_width)+"px";
			}else if(animale=="4"){
				defaultleft = jQuery(window).width()+"px";
			}
			style += "margin-top:"+screeny+"px;margin-left:-"+screenx+"px; position: fixed; left: "+defaultleft+";z-index:10003";
			
			totop = "0%";
			toleft = jQuery(window).width() - qfy_width;
			
		}else if(align=="7"){
			var defaultleft = 0;
			var screeny = obj.attr("data-screeny");
			var screenx = obj.attr("data-screenx");
			if(animale=="1"){
				style += "top:"+(jQuery(window).height() - 2*qfy_height)+"px;";
			}else if(animale=="2"){
				style += "top:"+jQuery(window).height()+"px;";
			}else {
				style += "bottom:0;";
			} 
			
			if(animale=="3"){
				defaultleft = "-"+qfy_width+"px";
			}else if(animale=="4"){
				defaultleft = qfy_width+"px";
			}
			style += "margin-bottom:"+screeny+"px;margin-left:"+screenx+"px; position: fixed; left: "+defaultleft+";z-index:10003";
			
			totop = jQuery(window).height() - qfy_height;
			toleft = "0%";
			
		}else if(align=="8"){
			var defaultleft = (jQuery(window).width() - qfy_width)+"px";
			var screeny = obj.attr("data-screeny");
			var screenx = obj.attr("data-screenx");
			if(animale=="1"){
				style += "top:"+(jQuery(window).height() - 2*qfy_height)+"px;";
			}else if(animale=="2"){
				style += "top:"+jQuery(window).height()+"px;";
			}else {
				style += "bottom:0;";
			} 
			
			if(animale=="3"){
				defaultleft = (jQuery(window).width() - 2*qfy_width)+"px";
			}else if(animale=="4"){
				defaultleft = jQuery(window).width()+"px";
			}
			style += "margin-bottom:"+screeny+"px;margin-left:-"+screenx+"px; position: fixed; left: "+defaultleft+";z-index:10003";
			
			totop = jQuery(window).height() - qfy_height;
			toleft = jQuery(window).width() - qfy_width;
			
		}
		
		var qfy_width_tmp = qfy_width;
		var qfy_height_tmp = qfy_height;
		
		if(jQuery("#QFY_window."+id_class).length==0){
			if(obj.attr("data-width")=="100%"){
				qfy_width_tmp = "100%";
			}else{
				qfy_width_tmp = qfy_width+"px";
			}
			if(obj.attr("data-height")=="100%"){
				qfy_height_tmp = "100%";
			}else{
				qfy_height_tmp = qfy_height+"px";
			}
			jQuery("body").append("<div id='QFY_window' class='"+id_class+"' style='width:"+qfy_width_tmp+";height:"+qfy_height_tmp+";"+style+"'><div id='QFY_content' style='height:"+qfy_height_tmp+"' >"+content+"</div></div>");
			
		}
		if(jQuery("#TB_overlay."+id_class).length==0 && type=="0"){
			var optcolor = obj.attr("data-optcolor")?obj.attr("data-optcolor"):"#000000";
			var opt= obj.attr("data-opt")?obj.attr("data-opt"):"0.7";
			jQuery("body").append("<div id='QFY_overlay' class='"+id_class+"' style='height: 100%;left: 0;position: fixed;top: 0;width: 100%;z-index: 100;background-color: "+optcolor+";opacity: "+opt+";z-index:10001;'></div>");
		}
	
		if(animale=="0"){
			jQuery("#QFY_window."+id_class).stop(true).animate({opacity:"1"},animale_time*1000,function(){
					if(obj.attr("data-width")=="100%"){
						jQuery("#QFY_window."+id_class).css({"left":"0","margin-left":"0"});
					}
				});
		}else if(animale=="1" || animale=="2"){
			if(align=="2"){
				jQuery("#QFY_window."+id_class).stop(true).animate({top:totop,opacity:"1"},animale_time*1000,function(){
					jQuery("#QFY_window."+id_class).css({"bottom":"0","top":"auto"});
					if(obj.attr("data-width")=="100%"){
						jQuery("#QFY_window."+id_class).css({"left":"0","margin-left":"0"});
					}
				});
			}else{
				jQuery("#QFY_window."+id_class).stop(true).animate({top:totop,opacity:"1"},animale_time*1000,function(){
					if(obj.attr("data-width")=="100%"){
						jQuery("#QFY_window."+id_class).css({"left":"0","margin-left":"0"});
					}
				});
			}
		}else if(animale=="3"||animale=="4"){
			if(align=="4"){
				jQuery("#QFY_window."+id_class).stop(true).animate({left:toleft,opacity:"1"},animale_time*1000,function(){
					jQuery("#QFY_window."+id_class).css({"right":"0","left":"auto"});
					if(obj.attr("data-width")=="100%"){
						jQuery("#QFY_window."+id_class).css({"left":"0","margin-left":"0"});
					}
				});
			}else{
				jQuery("#QFY_window."+id_class).stop(true).animate({left:toleft,opacity:"1"},animale_time*1000,function(){
					if(obj.attr("data-width")=="100%"){
						jQuery("#QFY_window."+id_class).css({"left":"0","margin-left":"0"});
					}
				});
			}
		}
	}else if(type=="3"){

		if(jQuery("#qfy_float_ad."+id_class).length==0){
			if(allow_close=="0" && jQuery("#qfy_float_ad."+id_class+" #QFY_close").length==0){
				if(jQuery(thisobj).find(".notice_close").length>0){
					var notice_close = jQuery(thisobj).find(".notice_close").prop("outerHTML");
					notice_close = notice_close.replace("display:none;","");
					if(btn_align==0){
						closeHTML = "<div id='QFY_close'  style='position:absolute;z-index:5;right:0px;top:0;cursor:pointer;'>"+notice_close+"</div>";
					}else if(btn_align==1){
						closeHTML = "<div id='QFY_close' style='position:absolute;z-index:5;left:48%;top:0;cursor:pointer;' >"+notice_close+"</div>";
					}else if(btn_align==2){
						closeHTML = "<div id='QFY_close' style='position:absolute;z-index:5;left:0;top:0;cursor:pointer;' >"+notice_close+"</div>";
					}
					
				}
			}
			
			var x=(jQuery(window).width()-qfy_width)/2,y=(jQuery(window).height()-qfy_height)/2;
			if(align=="1"){
				y=0;
			}else if(align=="2"){
				y=jQuery(window).height()-qfy_height;
			}else if(align=="3"){
				x=0;
			}else if(align=="4"){
				x=jQuery(window).width()-qfy_width;
			}else if(align=="5"){
				x=0;y=0;
			}else if(align=="6"){
				y=0;
				x=jQuery(window).width()-qfy_width;
			}else if(align=="7"){
				y=jQuery(window).height()-qfy_height;
				x=0;
			}else if(align=="8"){
				y=jQuery(window).height()-qfy_height;
				x=jQuery(window).width()-qfy_width;
			}
		   jQuery("body").floatAd({
	         customhtml: "<div id='QFY_content' style='height:"+qfy_height+"px;'>"+content+"</div>",
	         speed:animale_time*10,
	         id_class:id_class,
	         closeHTML:closeHTML,
	         x:x,
	         y:y
	       });
		   if(obj.attr("data-width")=="100%"){
			   jQuery("#qfy_float_ad."+id_class).width("100%").css("margin-left","1px").height(qfy_height);
		   }else{
			   jQuery("#qfy_float_ad."+id_class).width(qfy_width).height(qfy_height);
		   }
		   jQuery("#qfy_float_ad."+id_class+" #QFY_close").css("top",btn_position+"px");
			var close_left = jQuery("#qfy_float_ad."+id_class+" #QFY_close").css("left");
			if(close_left=="auto"){
				close_left = qfy_width - jQuery("."+id_class+" #QFY_close").width();
			}else{
				close_left = close_left.replace("px","");
			}

			jQuery("#qfy_float_ad."+id_class+" #QFY_close").css("right","auto").css("left",(close_left*1+btn_position_x*1)+"px");
			
		    jQuery("#QFY_window."+id_class).remove();
		}
	}
	
	
	if(jQuery("#QFY_window."+id_class).length==1){
		jQuery("#QFY_window."+id_class).css("z-index",z_index);
		if(allow_close=="0" && jQuery("#QFY_window."+id_class+" #QFY_close").length==0){
			if(jQuery(thisobj).find("#QFY_close").length==0){
				var notice_close = jQuery(thisobj).find(".notice_close").prop("outerHTML");
				notice_close = notice_close.replace("display:none;","").replace("display: none;","");
				if(btn_align==0){
					jQuery("#QFY_window."+id_class).append("<div id='QFY_close'  style='position:absolute;z-index:5;right:0px;top:0;cursor:pointer;'>"+notice_close+"</div>");
				}else if(btn_align==1){
					jQuery("#QFY_window."+id_class).append("<div id='QFY_close' style='position:absolute;z-index:5;left:48%;top:0;cursor:pointer;' >"+notice_close+"</div>");
				}else if(btn_align==2){
					jQuery("#QFY_window."+id_class).append("<div id='QFY_close' style='position:absolute;z-index:5;left:0;top:0;cursor:pointer;' >"+notice_close+"</div>");
				}
			
				
				jQuery("#QFY_window."+id_class+" #QFY_close").css("top",btn_position+"px");
				var close_left = jQuery("#QFY_window."+id_class+" #QFY_close").css("left");
							
				if(close_left=="auto"){
					close_left = qfy_width - jQuery("."+id_class+" #QFY_close").width();
				}else{
					close_left = close_left.replace("px","");
				}
				
				jQuery("#QFY_window."+id_class+" #QFY_close").css("right","auto").css("left",(close_left*1+btn_position_x*1)+"px");
					
				jQuery("#QFY_window."+id_class+" #QFY_close").click(function(){
					var title = jQuery("#QFY_window."+id_class+" .notice_content").attr("data-allow_warninfo");
					if(title){
						title = base64_decode(title);
						alert(title);
					}
					var allow_forceclose = jQuery("#QFY_window."+id_class+" .notice_content").attr("data-allow_forceclose");
					if(allow_forceclose!="1"){
						//..........
						notice_close_fun(thisobj);
					}
				})
			}
		}
		
		
	}
	
	var bgcolor = obj.attr("data-bgcolor");
	var borderwidth = obj.attr("data-borderwidth");
	var bordercolor = obj.attr("data-bordercolor");
	var shadowwidth = obj.attr("data-shadowwidth");

	var shadowcolor = obj.attr("data-shadowcolor");
	var radius = obj.attr("data-radius");
	
	
	jQuery("#QFY_window."+id_class+" #QFY_content .notice_close,#qfy_float_ad."+id_class+" #QFY_content .notice_close").remove();
	
	if(bgcolor){
		jQuery("#QFY_window."+id_class+"  #QFY_content,#qfy_float_ad."+id_class+" #QFY_content").css("background",bgcolor);
	}else{
		jQuery("#QFY_window."+id_class+"  #QFY_content,#qfy_float_ad."+id_class+" #QFY_content").css("background","#fff");
	}
	if(borderwidth){
		//jQuery("#QFY_window."+id_class+"  #QFY_content,#qfy_float_ad."+id_class+" #QFY_content").css("border",borderwidth+"px solid "+bordercolor);
	}
	
	if(shadowwidth>0 ){
		jQuery("#QFY_window."+id_class+" #QFY_content,#qfy_float_ad."+id_class+" #QFY_content").css("box-shadow","0px 0px "+shadowwidth+"px  "+shadowcolor);
	}
	if(radius>0){
		jQuery("#QFY_window."+id_class+" #QFY_content,#qfy_float_ad."+id_class+" #QFY_content").css("border-radius",radius+"px");
	}
	
	
	if(jQuery("."+id_class+" .notice_header").length>0){
		var f_h = jQuery("."+id_class+" .notice_header").outerHeight();
	}else{
		var f_h = 0;
	}
	if(jQuery("."+id_class+" .notice_footer").length>0){
		var f_f = jQuery("."+id_class+" .notice_footer").outerHeight();
	}else{
		var f_f = 0;
	}
	
	var h = jQuery("."+id_class+" #QFY_content").height() - f_f  - f_h;
	jQuery("."+id_class+" .notice_content").height(h);
	
	jQuery("#QFY_window .editorview,#qfy_float_ad .editorview").remove();
	if( obj.attr("data-opt_hide")=="1" && jQuery("#QFY_overlay."+id_class).length>0){
		jQuery("#QFY_overlay."+id_class).unbind().click(function(){
			var title = jQuery("#QFY_window."+id_class+" .notice_content").attr("data-allow_warninfo");
			if(title){
				title = base64_decode(title);
				alert(title);
			}
			var allow_forceclose = jQuery("#QFY_window."+id_class+" .notice_content").attr("data-allow_forceclose");
			if(allow_forceclose!="1"){
				
				//..........
				notice_close_fun(thisobj);
			}
		})
	}
	
	jQuery("."+id_class+" .notice_header,."+id_class+" .notice_footer,."+id_class+" .notice_content").show();
	if(precontent){
		window.vc_js_init2();
		window.vc_js_init();
	}
	setTimeout(function(){ searchForm(); },100);
}
function gototab(obj){
	var p = jQuery(obj).closest(".qfy-tabcontent");
	p.find(".tabcontent-header-menu li.item button").removeClass("active");
	jQuery(obj).find("button").addClass("active");
	var index = p.find(".tabcontent-header-menu li.item").index(jQuery(obj));
	p.find(".rsBullets .rsNavItem:eq("+index+")").click();
}
function backlistbtn(obj){
	var $this = jQuery(obj).closest(".qfe_teaser_grid.qfe_content_element");
	var h = $this.find(".list_hidden_btn");
	h.siblings().show();
	if(h.siblings(".mypages").length>0){
		var mstyle =h.siblings(".mypages").attr("style");
		if(mstyle) mstyle = mstyle.replace("display: block;","");
		h.siblings(".mypages").attr("style",mstyle);
	}
	h.hide();
	vc_teaserGrid();
	if(curr_scrollbar) jQuery(window).scrollTop(curr_scrollbar);
}
function qfy_btn_primry_notice(obj){
	var p = jQuery(obj).closest("#QFY_window,#qfy_float_ad");
	if(p.length==1){
		var title = p.find(".notice_content").attr("data-allow1_warninfo");
		if(title){
			title = base64_decode(title);
			alert(title);
		}
		var allow_forceclose = p.find(".notice_content").attr("data-allow1_forceclose");
		if(allow_forceclose!="1"){
			if(p.attr("id")=="qfy_float_ad"){
				p.remove();
				var id_class = p.attr("class");
				jQuery("#QFY_overlay."+id_class).remove();
			}else{
				var id_class = p.attr("class");
				notice_close_fun("#"+id_class+" .notice_warp");
			}
			setCookie(id_class+"_confirm",1,"365");
		}
	}
}
function qfy_btn_default_notice(obj){
	var p = jQuery(obj).closest("#QFY_window,#qfy_float_ad");
	if(p.length==1){
		var title = p.find(".notice_content").attr("data-allow2_warninfo");
		if(title){
			title = base64_decode(title);
			alert(title);
		}
		var allow_forceclose = p.find(".notice_content").attr("data-allow2_forceclose");
		if(allow_forceclose!="1"){
			if(p.attr("id")=="qfy_float_ad"){
				p.remove();
				var id_class = p.attr("class");
				jQuery("#QFY_overlay."+id_class).remove();
			}else{
				var id_class = p.attr("class");
				notice_close_fun("#"+id_class+" .notice_warp");
			}
		}
	}
	
}
var curr_scrollbar = 0;
function changelistlinkfun($this){
	
	$this.find("a.link_title:not(.a_file),a.link_image:not(.a_file),a.vc_read_more:not(.a_file),a.item_link,a.item_a_link").click(function(event) {
		    curr_scrollbar =  jQuery(window).scrollTop();
			var link = jQuery(this).attr("href");
			var h = $this.find(".list_hidden_btn");
			var slider = h.attr("data-slider");
			h.find(".backbtn").hide();
			h.siblings().hide();
			h.show();
			var image_src = "//fast.qifeiye.com/qfy-content/plugins/qfy_form/admin/images/loading.gif";
			var w = "";
			if(h.closest(".qfy-element").attr("data-loading")){
				image_src = h.closest(".qfy-element").attr("data-loading");
				w = h.closest(".qfy-element").attr("data-loading-w");
			}
			
			h.find(".list_hiddencontent").html('<div class="qfy_list_loading"  style="margin:0 auto;padding:100px 0;text-align:center;"><img style="width:'+w+'px" src="'+image_src+'"></div>');
			var pt = h.closest(".qfy-element").offset().top;
			if(curr_scrollbar > pt){
				jQuery("html").animate({ scrollTop: pt }, "fast");
			}
	
			jQuery.get(link,function(data){
				var htm =jQuery(data).find(".content-wrapper").html();
				if(!htm) htm ="";	    	
				if(slider=="1"){
					var htm_prev = "";
					var htm_next = "";
					if(jQuery(data).find(".bitMainTopSider").length>0){
						htm_prev = "<div class='bmts' style='position:relative;width:100%'>"+jQuery(data).find(".bitMainTopSider").html()+"</div>";
					}
					if(jQuery(data).find(".bitMainBottomSider").length>0){
						htm_next = "<div class='bmbs'  style='position:relative;width:100%'>"+jQuery(data).find(".bitMainBottomSider").html()+"</div>";
					}
					htm = htm_prev+htm+htm_next;
				}
				h.find(".list_hiddencontent").html(htm);
				h.find(".backbtn").show();
				 window.vc_js_init2();
				 window.vc_js_init();
				 qfy_notice_event();
				 tranlanguage(h);
			});
			
			return false;//阻止链接跳转
	 });
}
var qfy_template_waypoint;
jQuery(document).ready(function($) {
	jQuery(".mobile_variation_warp").closest("section").css("z-index","100");
	jQuery("#dl-menu .qfy-sub-div").remove();
	jQuery(".qfy-sub-div").each(function(){
		if(jQuery(this).attr("data-full")=="1"){
			var bodywidth = jQuery("body").width();
			var offsetleft = jQuery(this).closest("li").offset().left;
			jQuery(this).css("width","100vw").css("margin-left","-"+offsetleft+"px");
		}
		
	})
	if(jQuery(".qfy-listcatecontrols[data-child='1'],.qfy-listmenuvertical[data-child='1']").length>0){
		jQuery(".qfy-listcatecontrols[data-child='1'],.qfy-listmenuvertical[data-child='1']").each(function(){
			var cur = jQuery(this).find(".list-content .current-cat");
			jQuery(this).find(".list-content .children").hide();
			cur.parents("li,ul").show();
			if(cur.hasClass("cat-parent") && cur.closest(".list-content").length>0 ) cur.find(">.children").show();
		})
	
	}
	if(jQuery(".qfy_template_lib .viewmoretemplate").length>0){
		jQuery(".qfy_template_lib .viewmoretemplate").waypoint({
			handler: function(direction) {
				if(jQuery(".qfy_template_lib .viewmoretemplate .viewmoretemplate_inner").length>0){
					jQuery(".qfy_template_lib .viewmoretemplate .viewmoretemplate_inner").click();
				}
				
			},
			triggerOnce: true,
			offset: "bottom-in-view",
		})
		
	}

	if(top==self){
		jQuery(".qfe_teaser_grid.qfe_content_element[data-open='1']").each(function(){
			var $this =  jQuery(this);
			changelistlinkfun($this);
		})
		qfy_notice_event();
	}
	
	if(jQuery("body.free_public").length>0){
		checkAdvertising();
		setTimeout("checkAdvertising()",3000);
		setTimeout("checkAdvertising()",6000);
		setTimeout("checkAdvertising()",10000);
		setTimeout("checkAdvertising()",20000);
		setTimeout("checkAdvertising()",30000);
		setTimeout("checkAdvertising()",100000);
	}
	
	jQuery(".qfytemplateslist .qfypreloadimg").each(function(){
		var newurl = jQuery(this).attr("data-src");
		var $this = jQuery(this);
		jQuery('<img src="'+newurl+'">').load(function(){
			$this.attr("src",newurl);
		})
	})
	jQuery(".qfy_scroll_box:not(.load)").each(function(){
		jQuery(this).addClass("load");
		var box = jQuery(this).attr("id");
		var delay = jQuery(this).attr("data-delay");
		var speed = jQuery(this).attr("data-speed");
		var h = jQuery(this).attr("data-h");
		slideLine(box,"div",delay,speed,h);
	});
	
	vc_gallery_relat();
	//特殊处理一些网站
	if(jQuery(".re_second_user_class").length>0 && jQuery.trim(jQuery(".re_second_user_span").text())==""){
		jQuery(".re_second_user_class").hide();
	}
	
	if(jQuery(".addon-custom-datepicker" ).length>0 ){
		jQuery(".addon-custom-datepicker" ).datepicker({
						dateFormat: "yy-mm-dd",
						numberOfMonths: 1,
		});
	}
	if(jQuery(".addon-custom-datetimepicker" ).length>0 ){
		jQuery(".addon-custom-datetimepicker" ).datetimepicker({
					dateFormat: "yy-mm-dd",
					numberOfMonths: 1,
					showTime: true,
					constrainInput: false
		});
	}
	if(jQuery(".qfytemplateslist").length>0 ){
		if(jQuery(".qfytemplateslist .citem > a.active").length>0){
			jQuery(".qfytemplateslist .citem > a.active").closest(".citem").show();
		}
		jQuery(".qfytemplateslist  .info").click(function(){
			var url = jQuery(this).attr("data-url");
			if(top==self){
				window.open(url);
			}
		})
		jQuery(".qfytemplateslist  .use").click(function(){
			var url = jQuery(this).attr("data-url");
			if(top==self){
				location.href = url;
			}
		})
		
		jQuery(".qfytemplateslist .mypages a").removeAttr("href").click(function(){
			var tag = "all-0";
			if(jQuery(".qfytemplateslist .itemli.active").attr("data-tag")){
				tag = jQuery(".qfytemplateslist .itemli.active").attr("data-tag")
			}
			var paged = jQuery(this).attr("paged");
			searchqfylist(tag,paged);
		})
		
		jQuery(".qfytemplateslist .itemli" ).click(function(){
			jQuery(".qfytemplateslist .itemli" ).removeClass("active");
			jQuery(".qfytemplateslist .citem").hide();
			if(jQuery(this).parent().hasClass("citem")){
				jQuery(this).parent().show();
			}else{
				jQuery(this).parent().find(".citem").show();
			}
			jQuery(this).addClass("active");
		
			var tag = "all-0";
			if(jQuery(this).attr("data-tag")){
				tag = jQuery(this).attr("data-tag");
			}
			searchqfylist(tag,1);
		})
		jQuery(".qfytemplateslist .viewmoretemplate_inner").click(function(){
					var tag = "all-0";
					if(jQuery(".qfytemplateslist .itemli.active").attr("data-tag")){
						tag = jQuery(".qfytemplateslist .itemli.active").attr("data-tag")
					}
					var paged = jQuery(this).attr("data-page");
					searchqfylist(tag,paged*1+1,'append');
		});
		qfytemplate_hover();
	}	

	var current_page_id = jQuery('body').attr('data-pid');
	var current_page_key = jQuery('body').attr('data-pkey');
	
	/*if(current_page_id != '' && current_page_id != 'undefined' && current_page_key != '' && current_page_key != 'undefined'){
	    jQuery.ajax({
	        url: '/FeiEditor/traffic/log',
	        type: 'post',
	        async: true,
	        data: {
	        	st_pid: current_page_id,
	        	st_pkey: current_page_key,
	        }
	    });
	}*/
	setTimeout(function(){
		try{
			if(top!=self && !jQuery("body").hasClass("compose-mode")){
				if(top.jQuery('#pageUrl').length>0 && parent.jQuery("#vc-post-id").val()!=dtGlobals.curr_id){
					if(parent.jQuery("#vc-post-id").val()){
						top.jQuery('#pageUrl').val(dtGlobals.curr_id).change();
						return;
					}else if(jQuery("#vc-post-id").length==0){
						top.jAlert("页面过期，正尝试自动刷新！");
						setTimeout(function(){top.location.reload();},1000);
						return;
					}
				}
			}
		}catch(e){
			
		}
		
	},1000);
	
	//ready end
})


function qfy_popinfo_fun(htm,timetoclose){
	jQuery(".qfy_popinfo").remove();
	if(htm){
		var msg = '<div class="qfy_popinfo" ><div class="md-content">'+htm+'</div></div>';
		jQuery("body").append(msg);
		setTimeout(function(){ jQuery(".qfy_popinfo").addClass("qfy_show"); },500);
		if(timetoclose){
			setTimeout(function(){ jQuery(".qfy_popinfo").removeClass("qfy_show"); },timetoclose*1000);
		}
	}
}
function mobilecart_click(id){
	if( jQuery("#product-"+id+" form .single_add_to_cart_button").length>0 ){
		jQuery("#product-"+id+" form .single_add_to_cart_button").click();
	}else if(jQuery("#product-"+id+" .custom_btn_link .single_add_to_cart_button").length>0 ){
		jQuery("#product-"+id+" .custom_btn_link .single_add_to_cart_button").click();
	}
}
function mobilecart(){
	jQuery(".mobile_variation_warp").addClass("show");
	jQuery("#bottom-bar,#footer,#bitBanner,section.section,.right_nav_bar,#phantom").addClass("lowIndex");
	jQuery(".mobile_variation_warp").closest("section").removeClass("lowIndex").addClass("topIndex");
	jQuery(".mobile_variation_warp").closest(".bit_main_content").addClass("topIndex");
	jQuery(".mobile-mask").addClass("show");
	jQuery(".product_mobilecart").hide();
	jQuery('body').addClass("doing");
	var  height = jQuery(window).height();
	var isnotmove = false;
	if(jQuery("section.section.topIndex").length>0  && jQuery("section.section.topIndex").height()<height*0.75){
		isnotmove = true;
	}else{
		jQuery(".mobile_variation_warp").css("padding-bottom","0");
	}
	jQuery('html, body').on('touchmove', function(e){ 
		if(isnotmove){
			e.preventDefault(); 
		}
	});
	if(!isnotmove){
		jQuery("section.section.topIndex").nextAll("section.section").addClass("tmp_displaynone");
		jQuery("#bottom-bar,#footer").addClass("tmp_displaynone");
	}
	
}
function mobileunmask(){
	jQuery(".mobile-mask").removeClass("show");
	jQuery(".mobile_variation_warp").removeClass("show");
	jQuery(".mobile_variation_warp").closest("section").removeClass("topIndex");
	jQuery(".mobile_variation_warp").closest(".bit_main_content").removeClass("topIndex");
	jQuery("#bottom-bar,#footer,#bitBanner,section.section,.right_nav_bar,#phantom").removeClass("lowIndex");
	jQuery(".product_mobilecart").show();
	jQuery('body').removeClass("doing");
	jQuery("section.section,#bottom-bar,#footer").removeClass("tmp_displaynone");
}
function changeUserPhonenumber(obj){
	var form = jQuery(obj).closest("form");
	if(form.find(".qfyuser-loading:visible").length>0){
		return;
	}
	var url = "/admin/admin-ajax.php";
	var element = form.find(".phone_number");
	form.find(".qfyuser-warning").remove();
	form.find(".qfyuser-loading").show().addClass('inline');
	jQuery.ajax({
        type: "POST",
        url:url,
		dataType: "JSON",
        data:form.serialize(),
        error: function(request) {
			form.find(".qfyuser-loading").hide().removeClass('inline');
        	if (element.parent().find('.qfyuser-warning').length==0 ) {
				element.addClass('warning').removeClass('ok');
				element.after('<div class="qfyuser-warning"><i class="qfyuser-icon-caret-up"></i>提交失败</div>');
				element.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
			} else {
				element.parent().find('.qfyuser-warning').html('<i class="qfyuser-icon-caret-up"></i>提交失败' );
				element.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
			}
        },
        success: function(data) {
			form.find(".qfyuser-loading").hide().removeClass('inline');
        	if(data.status=="error"){
				var element1 = form.find("."+data.key);
				if (element1.parent().find('.qfyuser-warning').length==0 ) {
					element1.addClass('warning').removeClass('ok');
					element1.after('<div class="qfyuser-warning"><i class="qfyuser-icon-caret-up"></i>'+data.msg+'</div>');
					element1.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
				} else {
					element1.parent().find('.qfyuser-warning').html('<i class="qfyuser-icon-caret-up"></i>'+data.msg );
					element1.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
				}
			}else{
				location.reload();
			
			}
        }
    });
}
function send_user_sms(key,flag,obj){
	var text1 = jQuery(obj).attr("data-sms_gtext2");
	var text2 = jQuery(obj).attr("data-sms_gtext3");
	if(jQuery(obj).hasClass("begining")) return;
	jQuery(obj).val("60").addClass("begining");
	var p = jQuery(obj).closest("form");
	if(key!=""){
		var phone = p.find("#phone_number-"+key);
	}else{
		var phone = p.find(".phone_number");
	}
	jQuery.post("/admin/admin-ajax.php",{"action":"tosendsms","phone":phone.val()},function(data){
		
		if(data!="success"){
			jQuery(obj).val(text1);
			var element = phone;
			if (element.parent().find('.qfyuser-warning').length==0 ) {
				element.addClass('warning').removeClass('ok');
				element.after('<div class="qfyuser-warning"><i class="qfyuser-icon-caret-up"></i>' + data + '</div>');
				element.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
			} else {
				element.parent().find('.qfyuser-warning').html('<i class="qfyuser-icon-caret-up"></i>' + data );
				element.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
			}
		}else{
			jQuery(obj).val(text2);
			var element = phone;
			element.parent().find('.qfyuser-warning').remove();
			setTimeout(function(){jQuery(obj).val(text1);},10000);
		}
		
	})
	
	setTimeout(function(){
		tominuto1(obj);
		
	},1000);
}
function tominuto1(obj){
	var text1 = jQuery(obj).attr("data-sms_gtext2");
	var text2 = jQuery(obj).attr("data-sms_gtext3");
	if(text1 && text2){
		if(jQuery(obj).val()>0 && jQuery(obj).val()!=text1 && jQuery(obj).val()!=text2){
			jQuery(obj).val(jQuery(obj).val()-1);
		}else{
			jQuery(obj).removeClass("begining");
			return;
		}
	}else{

		if(jQuery(obj).val()>0 && jQuery(obj).val()!="发送" && jQuery(obj).val()!="获取" && jQuery(obj).val()!="发送成功"){
			jQuery(obj).val(jQuery(obj).val()-1);
		}else{
			jQuery(obj).removeClass("begining");
			return;
		}
	}
	setTimeout(function(){
		tominuto1(obj);
	},1000);
}
function qfy_phone_change(obj,flag){
	var p = jQuery(obj).closest(".phonediv");
	if(flag=="1"){
		p.find(".old_phone_div").hide();
		p.find(".password_div,.new_phone_div,.yanzheng_div,.qfyuser-submit").show();
	}else if(flag=="2" || flag=="3"){
		p.find(".old_phone_div,.password_div").hide();
		p.find(".new_phone_div,.yanzheng_div,.qfyuser-submit").show();
		v = p.find(".old_phone_div input").val();
		if(p){
			p.find(".new_phone_div .phone_number").val(v);
		}
	}else if(flag=="4"){
		p.find(".new_phone_div,.password_div,.qfyuser-submit,.yanzheng_div").hide();
		p.find(".old_phone_div").show();
	}
}
function weixin_auto_redirect(){
	var last_weixin_check = getCookie("last_weixin_check");
	if(last_weixin_check){
		return;
	}
	//大概15分钟失效
	setCookie("last_weixin_check",1,0.01);
	jQuery.post("/admin/admin-ajax.php",{action:"weixin_auto_redirect"},function(data){
		if(data.indexOf("http")==0){
			location.href=data;
		}
	});
	
}


function openheaderbtn(obj){
	if(jQuery("#page,#phantom").hasClass("menuopen")){
		jQuery("#page,#phantom").removeClass("menuopen");
	}else{
		jQuery("#page,#phantom").addClass("menuopen");
	}
}
function before_quick_search(obj){
	var search =jQuery(obj).find("[name='search']").val();
	if(search==""){
		return false;
	}
}
function quick_search(obj){
	var v = jQuery(obj).val();
	var pageid = jQuery(obj).attr("data-pageid");
	var title = jQuery(obj).attr("data-title")? jQuery(obj).attr("data-title"):"搜索";
	var placeholder = jQuery(obj).attr("data-placeholder")?jQuery(obj).attr("data-placeholder"):"请输入关键词";
	var btn1 = jQuery(obj).attr("data-btn1")?jQuery(obj).attr("data-btn1"):"确认";
	var btn2 = jQuery(obj).attr("data-btn2")?jQuery(obj).attr("data-btn2"):"取消";
	var  posttype =  jQuery(obj).attr("data-posttype");
	var postcate = jQuery(obj).attr("data-postcate");
	var bodywidth = jQuery("body").width();
	var bodyheight = jQuery(window).height();
	var message="<div><form action='/'  onsubmit='return before_quick_search(this)'><input type='hidden'  name='searchtype' value='"+posttype+"' /><input type='hidden'  name='searchcate' value='"+postcate+"' /><input type='hidden' name='page_id' value='"+pageid+"'><div style='box-sizing: border-box;padding: 26px 24px 5px 24px;font-size: 18px;font-weight: 500;line-height: 24px;text-align: left;'>"+title+"</div>";
	message +='<div  style="height: 80px;box-sizing: border-box;padding: 0 24px;overflow-y: auto;font-size: 15px;line-height: 1.5;color: rgba(0,0,0,.7);"><input style="border-width: 1px;border-style: solid;padding: 15px;background: #f2f2f2;width: 100%;text-indent: 6px;box-sizing: border-box;margin-top: 5px;border-radius: 0;outline:0;border-color:#ececec;" placeholder="'+placeholder+'" type="search" name="search"></div>';
	message +='<div class="pop_search_btn_div" style="padding: 0 20px;text-align: right;box-sizing: border-box;"><button type="submit" style="min-width: 64px;display: inline-block;height: 36px;padding: 0 16px;text-align: center;text-decoration: none;vertical-align: middle;border-radius: 2px;background:transparent;border:0;font-size:14px;">'+btn1+'</button><button type="button" onclick="unblockUI()" style="font-size:14px;min-width: 64px;display: inline-block;height: 36px;padding: 0 16px;text-align: center;text-decoration: none;vertical-align: middle;border-radius: 2px;background:transparent;border:0;">'+btn2+'</button></div></form></div>';
	if(bodywidth<768){
		var w = bodywidth - 40;
		jQuery.blockUI({onOverlayClick: top.jQuery.unblockUI,css: {"cursor":"auto","top":"30%","left":"50%","margin-left":"-"+(w/2)+"px",width:w+"px",height:"180px"},message:message});
	}else{
		jQuery.blockUI({onOverlayClick: top.jQuery.unblockUI,css: {"cursor":"auto","top":"30%","left":"50%","margin-left":"-360px",width:"720px",height:"180px"},message:message});
	}
	jQuery('body').css('cursor', 'auto');

}
function unblockUI(){
	jQuery.unblockUI();
}
function beforeOnclick(e,msg){
	if(confirm(msg)){
		
	}else{
		 e.preventDefault();
		 e.stopPropagation();
		 return false;
	}
}
function login_button_click(id,link){
	if(self!=top && typeof parent.jAlert =="function"){
		jAlert("在编辑模式下，不允许使用该功能，请到预览模式下使用。");
		return false;
	}
	var back = location.href;
	try{if(location.href.indexOf('qfy-login.php')>0) back = document.loginform.redirect_to.value;}catch(e){back = '/';}
	location.href=(link?link:'/')+'?connect='+id+'&action=login&qfy_nocache=true&back='+escape(back);
}
function play_qfy_video(obj){
	var p = jQuery(obj).closest(".video_play");
	var iframe = p.find("iframe");
	var src =  iframe.attr("data-src");
	var ispop = iframe.attr("data-ispopup");
	if(jQuery("body").width()<768){
		ispop = 0;
    }
	if(ispop=="1"){
    	var pop_width=iframe.attr("data-pop_width");
    	var pop_height=iframe.attr("data-pop_height");
    	var cover=iframe.attr("data-cover");
    	var controlBarVisibility=iframe.attr("data-controlbarvisibility");
    	var volume=iframe.attr("data-volume");
    	var bodywidth = jQuery("body").width();
    	var bodyheight = jQuery(window).height();
    	if(pop_width>bodywidth){
    		pop_width = bodywidth;
    	}
    	if(pop_height>bodyheight){
    		pop_height = bodyheight;
    	}
    	//onOverlayClick: top.jQuery.unblockUI,
		top.jQuery.blockUI({css: {"top":"50%","left":"50%","margin-top":"-"+(pop_height/2)+"px","margin-left":"-"+(pop_width/2)+"px",width:pop_width+"px",height:pop_height+"px"},message: "<iframe style='width:100%;height:100%;display:block;border:0;overflow:hidden;' data-height='"+pop_height+"' data-cover='"+cover+"' data-controlbarvisibility='"+controlBarVisibility+"' data-volume='"+volume+"' data-autoplay='true' src='"+src+"' ></iframe><div class='block-close' style='position:absolute;top:-26px;right:-3px;color:#fff;'>✕</div><div class='block-move ' title='按住拖动位置' style='position:absolute;top: 0;left: 0;color:#fff;cursor:move;background: transparent;width: 100%;height: 80px;'></div>"});
		top.jQuery('.block-close').css('cursor', 'pointer').unbind().click(function(){
			 top.jQuery.unblockUI();
		});
		top.jQuery('.blockUI.blockMsg').unbind().draggable({handle: ".block-move"});
		top.jQuery('body,.blockOverlay').css('cursor', 'auto');
	}else{
		jQuery(obj).removeClass("toplay").addClass("playing").attr("src","http://fast.qifeiye.com/qfy-content/plugins/qfy_editor/projekktor/themes/maccaco/buffering.gif");
		iframe.attr("data-autoplay","true").attr("src",src);
	}
}

function init_usermange_detail(){
	jQuery(".usermanage_container").each(function(){
		var hash = location.hash;
		var curr_action = "";
		if(hash.indexOf("#usermange")>-1){
			hash = hash.replace("#usermange-","");
			var tmp = hash.split("-");
			var obj = jQuery(this);
			if(jQuery(this).find(".usermenu."+tmp[0]).length>0){
				obj = jQuery(this).find(".usermenu."+tmp[0]);
			}
			open_user_detail(tmp[1],obj,tmp[0],tmp[2],tmp[4],tmp[3]);
		}else{

			if(jQuery(".usermanage_container .usermenu.detail").length>0){
				open_user_detail("detail",jQuery(this).find(".usermenu.detail"),"user");
			}else{
				open_user_detail("detail",jQuery(this),"user");
			}
		}
	})
	
}
function open_user_detail_confirm(status,obj,curr_action,sider,$id,$paged,msg){
	if(confirm(msg)){
		open_user_detail(status,obj,curr_action,sider,$id,$paged)
	}
}
function open_user_detail(status,obj,curr_action,sider,$id,$paged){
		var p = jQuery(obj).closest(".usermanage_container");
		if(p.hasClass("doing")){
			return;
		}
		if( jQuery(obj).hasClass("usermenu")){
			p.find(".active.usermenu").removeClass("active");
			 jQuery(obj).addClass("active");
		}
		p.addClass("doing");
		var width = jQuery("body").width();
		var isleft = false;
		var action = "bit_get_user_order_details";
		if(curr_action=="user"){
			action = "bit_get_user_details";
		}

		if(sider=="order" && sider=="left"){
			isleft = true;
			jQuery(obj).find("i").attr("data-tmpclass",jQuery(obj).find("i").attr("class") ).attr("class","fa fa-spinner fa-spin fa-3x fa-fw");
		}else if(sider=="right"){
			if(obj && jQuery(obj).length>0){
				jQuery(obj).siblings().removeClass("active");
				jQuery(obj).addClass("active");
			}
			p.find(".bit-order-list:visible").prepend('<div class="user_loading" style="z-index:11;position:absolute;height:100%;width:100%;padding-top:200px; background: rgba(255,255,255,0.2);text-align: center;max-width: 100%;"><img src="http://fast.qifeiye.com/qfy-content/plugins/qfy_form/admin/images/loading.gif" /></div>');
		}else{
			if(width<992){
				isleft = true;
			}
			if(obj && jQuery(obj).length>0){
				jQuery(obj).siblings().removeClass("active");
				jQuery(obj).addClass("active");
			}
			
			p.find(".usermanage_content .bit-order-list").prepend('<div class="user_loading" style="z-index:11;position:absolute;height:100%;width:100%;padding-top:200px;background: rgba(255,255,255,0.2);text-align: center;max-width: 100%;"><img src="http://fast.qifeiye.com/qfy-content/plugins/qfy_form/admin/images/loading.gif" /></div>');
		}
		if(top==self){
			if(!sider) sider="";
			if(!$paged) $paged="0";
			if(!$id) $id="0";
			location.hash = "#usermange-"+curr_action+"-"+status+"-"+sider+"-"+$paged+"-"+$id;
		}
		var shortcode = p.attr("data-shortcode");
		jQuery.post("/admin/admin-ajax.php",{"action":action,"curr_action":curr_action,"status":status,"id":$id,"paged":$paged,"shortcode":shortcode},function(data){
			p.removeClass("doing");
			if(data==0){
				data="<div class='empty' style='text-align:center;'><i class='fa-dropbox fa' style='font-size:150px;color:#EDEDED;padding:20px;'></i></div>"; 
			}
				
			if(p.find(".usermanage_leftslider .bit-order-list").length>0){
				p.find(".usermanage_leftslider .bit-order-list").html(data);
			}else{
				p.find(".usermanage_rightslider .bit-order-list").html(data);
			}
		
			p.find(".usermanage_content .mypages.pagenav a,.usermanage_content .bitcommerce-pagination .page-numbers").removeAttr("href").click(function(){
				var $paged = jQuery(this).attr("paged");
				if($paged) open_user_detail(status,obj,curr_action,sider,$id,$paged);
				return;
			})

			if(isleft){
				jQuery(obj).find("i").attr("class",jQuery(obj).find("i").attr("data-tmpclass") );
				var content = p.find(".usermanage_content .bit-order-list").prop("outerHTML");
				p.find(".usermanage_leftslider .usermanage_page").addClass("page-from-center-to-left");
				p.find(".usermanage_leftslider").append('<div class="usermanage_page page-from-right-to-center" style="position:absolute;top:0;width:100%;">'+content+'</div>');
				setTimeout(function(){
					p.find(".usermanage_leftslider .usermanage_page.page-from-center-to-left").hide();
					p.find(".usermanage_leftslider .usermanage_page.page-from-right-to-center").attr("style","");
				},500);
			}
			jQuery('.qfy_datatable_event:not(.loaded)').each(function(){
				$this = jQuery(this);
				if(typeof jQuery.fn.DataTable=="undefined"){
					jQuery.getScript("/FeiEditor/bitSite/js/dataTables/jquery.dataTables.js").done(function() {
						 qfy_dataTable_event($this);
					 })
				}else{
					qfy_dataTable_event($this);
				}
			})
			if( jQuery('[data-toggle="distpicker"]').length>0 ){
				jQuery('[data-toggle="distpicker"]').distpicker();
			}
			
			if(curr_action=="user"){
				/* reinitialise */
				qfyuser_responsive();
				qfyuser_chosen();
				qfyuser_fluid_videos();
				qfyuser_ajax_picupload();
				if(typeof(qfyuser_media_manager)=='function')
				{	
					qfyuser_media_manager();
				}
				jQuery('.qfyuser form').each(function(){
				qfyuser_collapse( jQuery(this) );
				});
				qfyuser_overlay_center('.qfyuser-overlay-inner');
			}else if(curr_action=="billing_address"  || curr_action=="address"){
				chang_city_init();
				p.find(".qfyuser-submit .qfyuser-button").click(function(){
					p.find('input[type=submit],input[type=button]').attr('disabled','disabled');
					p.find('img.qfyuser-loading').show().addClass('inline');
					jQuery(this).closest("form").ajaxSubmit({
						success: function (result)
						{
							p.find('input[type=submit],input[type=button]').removeAttr('disabled');
							p.find('img.qfyuser-loading').hide().removeClass('inline');
							qfyuser_overlay_confirmation("信息保存成功！");
						}
					});
				})
				
			}

		});
		
	
}
function chang_city_init(){
	// wc_country_select_params is required to continue, ensure the object exists
	if (typeof wc_country_select_params === "undefined")
		return false;

	/* State/Country select boxes */
	var states_json = wc_country_select_params.countries.replace(/&quot;/g, '"');
	var states = $.parseJSON( states_json );

	$('select.country_to_state, input.country_to_state').change(function(){
		var country = $(this).val();

		var $statebox = $(this).closest('div').find('#billing_state, #shipping_state, #calc_shipping_state');
		var $parent = $statebox.parent();

		var input_name = $statebox.attr('name');
		var input_id = $statebox.attr('id');
		var value = $statebox.val();
		var placeholder = $statebox.attr('placeholder');

		if (states[country]) {
			if (states[country].length == 0) {

				$statebox.parent().hide().find('.chosen-container').remove();
				$statebox.replaceWith('<input type="hidden" class="hidden" name="' + input_name + '" id="' + input_id + '" value="" placeholder="' + placeholder + '" />');

				$('body').trigger('country_to_state_changed', [country, $(this).closest('div')]);

			} else {

				var options = '';
				var state = states[country];
				for(var index in state) {
					options = options + '<option value="' + index + '">' + state[index] + '</option>';
				}
				$statebox.parent().show();
				if ($statebox.is('input')) {
					// Change for select
					$statebox.replaceWith('<select name="' + input_name + '" id="' + input_id + '" class="state_select" placeholder="' + placeholder + '"></select>');
					$statebox = $(this).closest('div').find('#billing_state, #shipping_state, #calc_shipping_state');
				}
				$statebox.html( '<option value="">' + wc_country_select_params.i18n_select_state_text + '</option>' + options);

				$statebox.val(value);

				$('body').trigger('country_to_state_changed', [country, $(this).closest('div')]);

			}
		} else {
			if ($statebox.is('select')) {

				$parent.show().find('.chosen-container').remove();
				$statebox.replaceWith('<input type="text" class="input-text" name="' + input_name + '" id="' + input_id + '" placeholder="' + placeholder + '" />');

				$('body').trigger('country_to_state_changed', [country, $(this).closest('div')]);

			} else if ($statebox.is('.hidden')) {

				$parent.show().find('.chosen-container').remove();
				$statebox.replaceWith('<input type="text" class="input-text" name="' + input_name + '" id="' + input_id + '" placeholder="' + placeholder + '" />');

				$('body').trigger('country_to_state_changed', [country, $(this).closest('div')]);

			}
		}

		$('body').trigger('country_to_state_changing', [country, $(this).closest('div')]);

	}).change();
	
}
function open_user_back(){
	jQuery(".usermanage_leftslider .page-from-right-to-center").removeClass("page-from-right-to-center").addClass("page-from-center-to-right");
	jQuery(".usermanage_leftslider .page-from-center-to-left").removeClass("page-from-center-to-left").css({"position":"absolute","top":"0","display":"block","width":"100%","z-index":"1"}).addClass("page-from-left-to-center");
	setTimeout(function(){
		jQuery(".usermanage_leftslider .usermanage_page.page-from-left-to-center").removeClass("page-from-left-to-center").attr("style","");
		jQuery(".usermanage_leftslider .usermanage_page.page-from-center-to-right").remove();
	},500);
}


function qfy_secode_check(obj){
	var form = jQuery(obj).closest("form");	
	var code = form.find("[name='secode']").val();
	var imagecode = form.find("[name='secode_check']").val();
	form.find(".qfyuser-warning,.qfyuser-success").remove();

	form.find(".qfyuser-loading").show().addClass('inline');
	jQuery.ajax({
			type: "POST",
	        url:"/admin/admin-ajax.php",
			dataType: "JSON",
			data:{"action":"secode_check","code":code,"imagecode":imagecode},
			error: function(request) {
				form.find(".qfyuser-loading").hide().removeClass('inline');
				console.log("Connent error");
			},
			success:function(data){
				
				form.find(".imageCodeClass").click();
				form.find(".qfyuser-loading").hide().removeClass('inline');
				if(data.status && data.key=="secodeinfo"){
					form.find("[name='secode_check']").val("");
					form.find("#secodeinfo").html("<div class='qfyuser-success' style='color:green;'>"+data.msg+"</div>");
					var bz_size = form.find("#secodeinfo").attr("data-bz_size");
					if(bz_size) form.find("#secodeinfo .comment_content").css("font-size",bz_size+"px");
					var bz_color = form.find("#secodeinfo").attr("data-bz_color");
					if(bz_color) form.find("#secodeinfo .comment_content").css("color",bz_color);
					var bzdate_color = form.find("#secodeinfo").attr("data-bzdate_color");
					if(bzdate_color) form.find("#secodeinfo .comment_date").css("color",bzdate_color);
					var bzdate_size = form.find("#secodeinfo").attr("data-bzdate_size");
					if(bzdate_size) form.find("#secodeinfo .comment_date").css("font-size",bzdate_size+"px");
					
					
				}else if(data.status && data.status=="error"){
						if(data.key!="secode_check"){
							form.find("[name='secode_check']").val("");
						}
						var element1 = form.find("."+data.key);
						if (element1.parent().find('.qfyuser-warning').length==0 ) {
							element1.addClass('warning').removeClass('ok');
							element1.after('<div class="qfyuser-warning"><i class="qfyuser-icon-caret-up"></i>'+data.msg+'</div>');
							element1.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
						} else {
							element1.parent().find('.qfyuser-warning').html('<i class="qfyuser-icon-caret-up"></i>'+data.msg );
							element1.parent().find('.qfyuser-warning').css({'top' : '0px', 'opacity' : '1'});
						}
				}else if(data.status && data.status=="success"){
					form.find("[name='secode_check']").val("");
					form.find("#secodeinfo").html("<div class='qfyuser-success' style='color:green;'>"+data.msg+"</div>");
				}
			}
	});
}

