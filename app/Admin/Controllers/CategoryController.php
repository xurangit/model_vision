<?php

namespace App\Admin\Controllers;

use App\Models\Category;

use App\Models\Link;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use function foo\func;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('分类管理');
            $content->description('分类列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('分类管理');
            $content->description('修改分类');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('分类管理');
            $content->description('创建分类');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Category::class, function (Grid $grid) {
            $grid->id('ID');
            $grid->title('背景标题');
            $grid->name('中文名称');
            $grid->name_en('英文名称');
            $grid->link("链接地址")->display(function ($id){
                $info = Link::find($id);
                return "【<span style='font-weight: bold;'>".$info->name."</span>】";
            });
//            $grid->column("link.name",'链接地址');//or
            $grid->picture('背景图片')->display(function ($picture) {
                return "<image class='images' style='height:30px;' src='".env('APP_URL').'/uploads/'.$picture."'>
                    <script>
                        $('.images').mouseover(function() {
                            $(this).attr('style','height:400px;position:absolute;z-index:1;');
                        });
                        $('.images').mouseout(function() {
                            $(this).attr('style','height:30px;');
                        });
                    </script>
                ";
            });

            $grid->sort('排序')->sortable();

            $states = [
                'on'  => ['value' => 1, 'text' => '打开', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '关闭', 'color' => 'danger'],
            ];
            $grid->is_show('是否显示')->switch($states);

            $grid->created_at();
            $grid->updated_at();

            $grid->model()->orderBy('sort','asc');

            $grid->disableCreateButton();
            $grid->disableRowSelector();
            $grid->disableFilter();
            $grid->disableExport();
            $grid->actions(function ($actions) {
                $actions->disableDelete();//禁用删除按钮
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Category::class, function (Form $form) {

            $form->display('id', 'ID');

            //选择链接
            $links = DB::table('link')->get();
            foreach ($links as $key => $value) {
                $link[$value->id] = $value->name;
            }
            $form->select( 'link','链接地址')->options($link)->rules('required',['required' => '链接地址为必填项'])->help('选择点击分类进入的页面');

            $form->text('title', '背景标题')->rules(function ($form){
                if (!$id = $form->model()->id) {
                    return 'required|unique:categories';
                }
            },['required' => '分类标题为必填项','unique'=>'分类标题必须唯一']);
            $form->text('name', '分类名称（中文）')->rules(function ($form){
                if (!$id = $form->model()->id) {
                    return 'required|unique:categories';
                }
            },['required' => '分类名称（中文）为必填项','unique'=>'分类名称（中文）必须唯一']);
            $form->text('name_en', '分类名称（英文）')->rules(function ($form){
                if (!$id = $form->model()->id) {
                    return 'required|unique:categories';
                }
            },['required' => '分类名称（英文）为必填项','unique'=>'分类名称（英文）必须唯一']);
            $form->image('picture','背景图片')->rules('required',['required' => '背景图片为必填项']);
//            $form->radio('is_show','是否显示')->options(['1' => '显示', '0' => '隐藏'])->default(1);
//            $grid->is_show('是否显示')->switch($states);
            $states = [
                'on'  => ['value' => 1, 'text' => '打开', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '关闭', 'color' => 'danger'],
            ];
            $form->switch('is_show','是否显示')->states($states)->default(1);
            $form->text('sort','排序')->default(10)->help('手动填写排序，默认排序是数字从小到大排列（数字相同按照ID排序）');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
