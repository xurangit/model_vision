<?php

namespace App\Http\Middleware;

use App\Models\Company;
use Closure;
use Illuminate\Support\Facades\DB;

class Web
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $info = Company::first()->toArray();
        $request->merge($info);
        //访问数据添加
        $ip       = $request->ip();
        $today    = self::today();
        $isExists = DB::table("web_visit")
            ->where('ip', $ip)
            ->whereBetween('time', $today)
            ->get();
        if ($isExists->isEmpty()) {
            //获取ip的所在地
            $data    = $this->getCity($ip);
            $country = $data['country'];
            if ($country == "台湾" || $country == "香港" || $country == "澳门" || $data['province'] == "台湾") {
                $country = "中国";
            }
            DB::table("web_visit")
                ->insert([
                    'ip'       => $ip,
                    'country'  => $country,
                    'province' => $data['province'],
                    'city'     => $data['city'],
                    'isp'      => $data['isp'],
                    'time'     => time(),
                ]);
        }
        return $next($request);
    }

    /**
     * 返回今日开始和结束的时间戳
     *
     * @return array
     */
    public static function today()
    {
        return [
            mktime(0, 0, 0, date('m'), date('d'), date('Y')),
            mktime(23, 59, 59, date('m'), date('d'), date('Y'))
        ];
    }

    /**
     * 获取 IP  地理位置
     * 淘宝IP接口
     * @Return: array
     */
    function getCity($ip = '')
    {
        if ($ip == '') {
            $url  = "http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json";
            $info = json_decode(file_get_contents($url), true);
            $data = [
                'country'  => $info['country'],
                'province' => $info['province'],
                'city'     => $info['city'],
                'isp'      => $info['isp'],
            ];
        } else {
            $url = "http://ip.taobao.com/service/getIpInfo.php?ip=" . $ip;
            $ip  = json_decode(file_get_contents($url));
            if ((string)$ip->code == '1') {
                return false;
            }
            $info = (array)$ip->data;
            $data = [
                'country'  => $info['country'],
                'province' => $info['region'],
                'city'     => $info['city'],
                'isp'      => $info['isp'],
            ];
        }
        return $data;
    }
}
