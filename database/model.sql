/*
Navicat MySQL Data Transfer

Source Server         : dnmp
Source Server Version : 80013
Source Host           : localhost:3306
Source Database       : model

Target Server Type    : MYSQL
Target Server Version : 80013
File Encoding         : 65001

Date: 2019-07-18 18:01:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_menu
-- ----------------------------
INSERT INTO `admin_menu` VALUES ('1', '0', '1', '控制台', 'fa-bar-chart', '/', null, null, '2019-07-17 18:44:00');
INSERT INTO `admin_menu` VALUES ('2', '0', '2', '后台管理', 'fa-tasks', null, null, null, '2019-07-17 18:44:59');
INSERT INTO `admin_menu` VALUES ('3', '2', '3', '用户', 'fa-users', 'auth/users', null, null, '2019-07-17 18:44:48');
INSERT INTO `admin_menu` VALUES ('4', '2', '4', '角色', 'fa-user', 'auth/roles', null, null, '2019-07-17 18:45:15');
INSERT INTO `admin_menu` VALUES ('5', '2', '5', '权限', 'fa-ban', 'auth/permissions', null, null, '2019-07-17 18:45:24');
INSERT INTO `admin_menu` VALUES ('6', '2', '6', '菜单', 'fa-bars', 'auth/menu', null, null, '2019-07-17 18:44:37');
INSERT INTO `admin_menu` VALUES ('7', '2', '7', '操作日志', 'fa-history', 'auth/logs', null, null, '2019-07-17 18:45:33');
INSERT INTO `admin_menu` VALUES ('8', '0', '12', '分类管理', 'fa-bars', null, null, '2019-07-17 18:46:52', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('9', '8', '13', '分类列表', 'fa-cubes', 'category', null, '2019-07-17 18:47:54', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('10', '8', '14', '添加分类', 'fa-anchor', 'category/create', null, '2019-07-18 10:14:00', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('11', '0', '9', '公司信息', 'fa-bars', 'company_basic', null, '2019-07-18 10:40:00', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('12', '0', '10', '公司配置', 'fa-balance-scale', 'company', null, '2019-07-18 10:57:02', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('13', '0', '18', '超模课程', 'fa-amazon', null, null, '2019-07-18 11:02:03', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('14', '13', '19', '课程列表', 'fa-bars', 'course', null, '2019-07-18 11:02:16', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('15', '13', '20', '添加课程', 'fa-plus-square', 'course/create', null, '2019-07-18 11:02:51', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('16', '0', '8', '首页', 'fa-anchor', 'first', null, '2019-07-18 11:06:21', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('17', '0', '11', '超模基因', 'fa-android', 'gene', null, '2019-07-18 11:12:07', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('18', '0', '15', '故事管理', 'fa-angellist', 'story', null, '2019-07-18 11:14:46', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('19', '18', '16', '故事列表', 'fa-arrow-circle-left', 'story_list', null, '2019-07-18 11:15:06', '2019-07-18 16:24:54');
INSERT INTO `admin_menu` VALUES ('20', '18', '17', '添加故事', 'fa-apple', 'story_list/create', null, '2019-07-18 11:15:34', '2019-07-18 16:24:54');

-- ----------------------------
-- Table structure for admin_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_operation_log
-- ----------------------------
INSERT INTO `admin_operation_log` VALUES ('1', '1', 'admin', 'GET', '172.18.0.1', '[]', '2019-07-02 09:07:48', '2019-07-02 09:07:48');
INSERT INTO `admin_operation_log` VALUES ('2', '1', 'admin/auth/users', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:20:26', '2019-07-02 09:20:26');
INSERT INTO `admin_operation_log` VALUES ('3', '1', 'admin/auth/roles', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:20:29', '2019-07-02 09:20:29');
INSERT INTO `admin_operation_log` VALUES ('4', '1', 'admin/auth/users', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:20:31', '2019-07-02 09:20:31');
INSERT INTO `admin_operation_log` VALUES ('5', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:20:33', '2019-07-02 09:20:33');
INSERT INTO `admin_operation_log` VALUES ('6', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '[]', '2019-07-02 09:43:12', '2019-07-02 09:43:12');
INSERT INTO `admin_operation_log` VALUES ('7', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:43:18', '2019-07-02 09:43:18');
INSERT INTO `admin_operation_log` VALUES ('8', '1', 'admin', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:43:21', '2019-07-02 09:43:21');
INSERT INTO `admin_operation_log` VALUES ('9', '1', 'admin', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:43:23', '2019-07-02 09:43:23');
INSERT INTO `admin_operation_log` VALUES ('10', '1', 'admin', 'GET', '172.18.0.1', '[]', '2019-07-02 09:43:52', '2019-07-02 09:43:52');
INSERT INTO `admin_operation_log` VALUES ('11', '1', 'admin', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:01', '2019-07-02 09:46:01');
INSERT INTO `admin_operation_log` VALUES ('12', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:04', '2019-07-02 09:46:04');
INSERT INTO `admin_operation_log` VALUES ('13', '1', 'admin/auth/permissions/create', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:07', '2019-07-02 09:46:07');
INSERT INTO `admin_operation_log` VALUES ('14', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:09', '2019-07-02 09:46:09');
INSERT INTO `admin_operation_log` VALUES ('15', '1', 'admin/auth/permissions/1/edit', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:14', '2019-07-02 09:46:14');
INSERT INTO `admin_operation_log` VALUES ('16', '1', 'admin/auth/permissions/1', 'PUT', '172.18.0.1', '{\"slug\":\"*\",\"name\":\"All permission\",\"http_method\":[null],\"http_path\":\"*\",\"_token\":\"Vwxof56k0FFUrQm69NC1sbxyNlTMJMfrNPRHVnV8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/permissions\"}', '2019-07-02 09:46:16', '2019-07-02 09:46:16');
INSERT INTO `admin_operation_log` VALUES ('17', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '[]', '2019-07-02 09:46:16', '2019-07-02 09:46:16');
INSERT INTO `admin_operation_log` VALUES ('18', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '[]', '2019-07-02 09:46:20', '2019-07-02 09:46:20');
INSERT INTO `admin_operation_log` VALUES ('19', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:22', '2019-07-02 09:46:22');
INSERT INTO `admin_operation_log` VALUES ('20', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:24', '2019-07-02 09:46:24');
INSERT INTO `admin_operation_log` VALUES ('21', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:24', '2019-07-02 09:46:24');
INSERT INTO `admin_operation_log` VALUES ('22', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:24', '2019-07-02 09:46:24');
INSERT INTO `admin_operation_log` VALUES ('23', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:24', '2019-07-02 09:46:24');
INSERT INTO `admin_operation_log` VALUES ('24', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:24', '2019-07-02 09:46:24');
INSERT INTO `admin_operation_log` VALUES ('25', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:25', '2019-07-02 09:46:25');
INSERT INTO `admin_operation_log` VALUES ('26', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:25', '2019-07-02 09:46:25');
INSERT INTO `admin_operation_log` VALUES ('27', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:25', '2019-07-02 09:46:25');
INSERT INTO `admin_operation_log` VALUES ('28', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:29', '2019-07-02 09:46:29');
INSERT INTO `admin_operation_log` VALUES ('29', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:30', '2019-07-02 09:46:30');
INSERT INTO `admin_operation_log` VALUES ('30', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:32', '2019-07-02 09:46:32');
INSERT INTO `admin_operation_log` VALUES ('31', '1', 'admin/auth/permissions', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\",\"id\":\"3\"}', '2019-07-02 09:46:45', '2019-07-02 09:46:45');
INSERT INTO `admin_operation_log` VALUES ('32', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:53', '2019-07-02 09:46:53');
INSERT INTO `admin_operation_log` VALUES ('33', '1', 'admin/auth/menu/2/edit', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:46:59', '2019-07-02 09:46:59');
INSERT INTO `admin_operation_log` VALUES ('34', '1', 'admin/auth/roles', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:48:59', '2019-07-02 09:48:59');
INSERT INTO `admin_operation_log` VALUES ('35', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:49:01', '2019-07-02 09:49:01');
INSERT INTO `admin_operation_log` VALUES ('36', '1', 'admin/auth/menu/5/edit', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:49:05', '2019-07-02 09:49:05');
INSERT INTO `admin_operation_log` VALUES ('37', '1', 'admin/auth/menu/5', 'PUT', '172.18.0.1', '{\"parent_id\":\"2\",\"title\":\"Permission\",\"icon\":\"fab fa-alipay\",\"uri\":\"auth\\/permissions\",\"roles\":[null],\"permission\":null,\"_token\":\"Vwxof56k0FFUrQm69NC1sbxyNlTMJMfrNPRHVnV8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-02 09:49:09', '2019-07-02 09:49:09');
INSERT INTO `admin_operation_log` VALUES ('38', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '[]', '2019-07-02 09:49:09', '2019-07-02 09:49:09');
INSERT INTO `admin_operation_log` VALUES ('39', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '[]', '2019-07-02 09:49:12', '2019-07-02 09:49:12');
INSERT INTO `admin_operation_log` VALUES ('40', '1', 'admin/auth/menu/5/edit', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:49:18', '2019-07-02 09:49:18');
INSERT INTO `admin_operation_log` VALUES ('41', '1', 'admin/auth/menu/5', 'PUT', '172.18.0.1', '{\"parent_id\":\"2\",\"title\":\"Permission\",\"icon\":\"fa-alipay\",\"uri\":\"auth\\/permissions\",\"roles\":[null],\"permission\":null,\"_token\":\"Vwxof56k0FFUrQm69NC1sbxyNlTMJMfrNPRHVnV8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-02 09:49:31', '2019-07-02 09:49:31');
INSERT INTO `admin_operation_log` VALUES ('42', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '[]', '2019-07-02 09:49:32', '2019-07-02 09:49:32');
INSERT INTO `admin_operation_log` VALUES ('43', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:49:36', '2019-07-02 09:49:36');
INSERT INTO `admin_operation_log` VALUES ('44', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:49:37', '2019-07-02 09:49:37');
INSERT INTO `admin_operation_log` VALUES ('45', '1', 'admin/auth/menu/5/edit', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:49:38', '2019-07-02 09:49:38');
INSERT INTO `admin_operation_log` VALUES ('46', '1', 'admin/auth/menu/5', 'PUT', '172.18.0.1', '{\"parent_id\":\"2\",\"title\":\"Permission\",\"icon\":\"fa-ban\",\"uri\":\"auth\\/permissions\",\"roles\":[null],\"permission\":null,\"_token\":\"Vwxof56k0FFUrQm69NC1sbxyNlTMJMfrNPRHVnV8\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-02 09:49:59', '2019-07-02 09:49:59');
INSERT INTO `admin_operation_log` VALUES ('47', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '[]', '2019-07-02 09:49:59', '2019-07-02 09:49:59');
INSERT INTO `admin_operation_log` VALUES ('48', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-02 09:50:03', '2019-07-02 09:50:03');
INSERT INTO `admin_operation_log` VALUES ('49', '1', 'admin/auth/menu', 'GET', '172.18.0.1', '[]', '2019-07-02 09:50:05', '2019-07-02 09:50:05');
INSERT INTO `admin_operation_log` VALUES ('50', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-03 02:27:17', '2019-07-03 02:27:17');
INSERT INTO `admin_operation_log` VALUES ('51', '1', 'admin/auth/logout', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-03 02:27:22', '2019-07-03 02:27:22');
INSERT INTO `admin_operation_log` VALUES ('52', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-03 08:25:32', '2019-07-03 08:25:32');
INSERT INTO `admin_operation_log` VALUES ('53', '1', 'admin/auth/roles', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-03 08:26:18', '2019-07-03 08:26:18');
INSERT INTO `admin_operation_log` VALUES ('54', '1', 'admin/auth/roles/1/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-03 08:31:45', '2019-07-03 08:31:45');
INSERT INTO `admin_operation_log` VALUES ('55', '1', 'admin/auth/permissions', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-03 08:31:58', '2019-07-03 08:31:58');
INSERT INTO `admin_operation_log` VALUES ('56', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-17 18:31:52', '2019-07-17 18:31:52');
INSERT INTO `admin_operation_log` VALUES ('57', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-17 18:40:19', '2019-07-17 18:40:19');
INSERT INTO `admin_operation_log` VALUES ('58', '1', 'admin/auth/login', 'GET', '172.19.0.1', '[]', '2019-07-17 18:41:35', '2019-07-17 18:41:35');
INSERT INTO `admin_operation_log` VALUES ('59', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-17 18:41:35', '2019-07-17 18:41:35');
INSERT INTO `admin_operation_log` VALUES ('60', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-17 18:41:52', '2019-07-17 18:41:52');
INSERT INTO `admin_operation_log` VALUES ('61', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:42:05', '2019-07-17 18:42:05');
INSERT INTO `admin_operation_log` VALUES ('62', '1', 'admin/auth/roles', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:42:08', '2019-07-17 18:42:08');
INSERT INTO `admin_operation_log` VALUES ('63', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:42:10', '2019-07-17 18:42:10');
INSERT INTO `admin_operation_log` VALUES ('64', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:23', '2019-07-17 18:43:23');
INSERT INTO `admin_operation_log` VALUES ('65', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:23', '2019-07-17 18:43:23');
INSERT INTO `admin_operation_log` VALUES ('66', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:23', '2019-07-17 18:43:23');
INSERT INTO `admin_operation_log` VALUES ('67', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:24', '2019-07-17 18:43:24');
INSERT INTO `admin_operation_log` VALUES ('68', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:24', '2019-07-17 18:43:24');
INSERT INTO `admin_operation_log` VALUES ('69', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:24', '2019-07-17 18:43:24');
INSERT INTO `admin_operation_log` VALUES ('70', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:24', '2019-07-17 18:43:24');
INSERT INTO `admin_operation_log` VALUES ('71', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:41', '2019-07-17 18:43:41');
INSERT INTO `admin_operation_log` VALUES ('72', '1', 'admin/auth/menu/1/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:43:52', '2019-07-17 18:43:52');
INSERT INTO `admin_operation_log` VALUES ('73', '1', 'admin/auth/menu/1', 'PUT', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u63a7\\u5236\\u53f0\",\"icon\":\"fa-bar-chart\",\"uri\":\"\\/\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:44:00', '2019-07-17 18:44:00');
INSERT INTO `admin_operation_log` VALUES ('74', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:44:00', '2019-07-17 18:44:00');
INSERT INTO `admin_operation_log` VALUES ('75', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:44:03', '2019-07-17 18:44:03');
INSERT INTO `admin_operation_log` VALUES ('76', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:05', '2019-07-17 18:44:05');
INSERT INTO `admin_operation_log` VALUES ('77', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:08', '2019-07-17 18:44:08');
INSERT INTO `admin_operation_log` VALUES ('78', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:10', '2019-07-17 18:44:10');
INSERT INTO `admin_operation_log` VALUES ('79', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:15', '2019-07-17 18:44:15');
INSERT INTO `admin_operation_log` VALUES ('80', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:27', '2019-07-17 18:44:27');
INSERT INTO `admin_operation_log` VALUES ('81', '1', 'admin/auth/menu/6/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:31', '2019-07-17 18:44:31');
INSERT INTO `admin_operation_log` VALUES ('82', '1', 'admin/auth/menu/6', 'PUT', '172.19.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u83dc\\u5355\",\"icon\":\"fa-bars\",\"uri\":\"auth\\/menu\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:44:37', '2019-07-17 18:44:37');
INSERT INTO `admin_operation_log` VALUES ('83', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:44:37', '2019-07-17 18:44:37');
INSERT INTO `admin_operation_log` VALUES ('84', '1', 'admin/auth/menu/3/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:41', '2019-07-17 18:44:41');
INSERT INTO `admin_operation_log` VALUES ('85', '1', 'admin/auth/menu/3', 'PUT', '172.19.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u7528\\u6237\",\"icon\":\"fa-users\",\"uri\":\"auth\\/users\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:44:48', '2019-07-17 18:44:48');
INSERT INTO `admin_operation_log` VALUES ('86', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:44:48', '2019-07-17 18:44:48');
INSERT INTO `admin_operation_log` VALUES ('87', '1', 'admin/auth/menu/2/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:44:51', '2019-07-17 18:44:51');
INSERT INTO `admin_operation_log` VALUES ('88', '1', 'admin/auth/menu/2', 'PUT', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u540e\\u53f0\\u7ba1\\u7406\",\"icon\":\"fa-tasks\",\"uri\":null,\"roles\":[\"1\",null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:44:59', '2019-07-17 18:44:59');
INSERT INTO `admin_operation_log` VALUES ('89', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:44:59', '2019-07-17 18:44:59');
INSERT INTO `admin_operation_log` VALUES ('90', '1', 'admin/auth/menu/4/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:45:03', '2019-07-17 18:45:03');
INSERT INTO `admin_operation_log` VALUES ('91', '1', 'admin/auth/menu/4', 'PUT', '172.19.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u89d2\\u8272\",\"icon\":\"fa-user\",\"uri\":\"auth\\/roles\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:45:15', '2019-07-17 18:45:15');
INSERT INTO `admin_operation_log` VALUES ('92', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:45:15', '2019-07-17 18:45:15');
INSERT INTO `admin_operation_log` VALUES ('93', '1', 'admin/auth/menu/5/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:45:18', '2019-07-17 18:45:18');
INSERT INTO `admin_operation_log` VALUES ('94', '1', 'admin/auth/menu/5', 'PUT', '172.19.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u6743\\u9650\",\"icon\":\"fa-ban\",\"uri\":\"auth\\/permissions\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:45:24', '2019-07-17 18:45:24');
INSERT INTO `admin_operation_log` VALUES ('95', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:45:24', '2019-07-17 18:45:24');
INSERT INTO `admin_operation_log` VALUES ('96', '1', 'admin/auth/menu/7/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:45:27', '2019-07-17 18:45:27');
INSERT INTO `admin_operation_log` VALUES ('97', '1', 'admin/auth/menu/7', 'PUT', '172.19.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u64cd\\u4f5c\\u65e5\\u5fd7\",\"icon\":\"fa-history\",\"uri\":\"auth\\/logs\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:45:32', '2019-07-17 18:45:32');
INSERT INTO `admin_operation_log` VALUES ('98', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:45:33', '2019-07-17 18:45:33');
INSERT INTO `admin_operation_log` VALUES ('99', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:45:35', '2019-07-17 18:45:35');
INSERT INTO `admin_operation_log` VALUES ('100', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:46:28', '2019-07-17 18:46:28');
INSERT INTO `admin_operation_log` VALUES ('101', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5206\\u7c7b\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":\"admin\\/Category\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\"}', '2019-07-17 18:46:52', '2019-07-17 18:46:52');
INSERT INTO `admin_operation_log` VALUES ('102', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:46:52', '2019-07-17 18:46:52');
INSERT INTO `admin_operation_log` VALUES ('103', '1', 'admin/auth/menu/8/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:47:02', '2019-07-17 18:47:02');
INSERT INTO `admin_operation_log` VALUES ('104', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:47:05', '2019-07-17 18:47:05');
INSERT INTO `admin_operation_log` VALUES ('105', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:47:05', '2019-07-17 18:47:05');
INSERT INTO `admin_operation_log` VALUES ('106', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"8\",\"title\":\"\\u5206\\u7c7b\\u5217\\u8868\",\"icon\":\"fa-bars\",\"uri\":\"Category\\/index\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\"}', '2019-07-17 18:47:54', '2019-07-17 18:47:54');
INSERT INTO `admin_operation_log` VALUES ('107', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:47:54', '2019-07-17 18:47:54');
INSERT INTO `admin_operation_log` VALUES ('108', '1', 'admin/auth/menu/8/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:47:58', '2019-07-17 18:47:58');
INSERT INTO `admin_operation_log` VALUES ('109', '1', 'admin/auth/menu/8', 'PUT', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5206\\u7c7b\\u7ba1\\u7406\",\"icon\":\"fa-bars\",\"uri\":null,\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:48:02', '2019-07-17 18:48:02');
INSERT INTO `admin_operation_log` VALUES ('110', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:48:03', '2019-07-17 18:48:03');
INSERT INTO `admin_operation_log` VALUES ('111', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:48:05', '2019-07-17 18:48:05');
INSERT INTO `admin_operation_log` VALUES ('112', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:48:29', '2019-07-17 18:48:29');
INSERT INTO `admin_operation_log` VALUES ('113', '1', 'admin/auth/menu/9/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:48:31', '2019-07-17 18:48:31');
INSERT INTO `admin_operation_log` VALUES ('114', '1', 'admin/auth/menu/9', 'PUT', '172.19.0.1', '{\"parent_id\":\"8\",\"title\":\"\\u5206\\u7c7b\\u5217\\u8868\",\"icon\":\"fa-bars\",\"uri\":\"category\\/index\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:48:35', '2019-07-17 18:48:35');
INSERT INTO `admin_operation_log` VALUES ('115', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:48:36', '2019-07-17 18:48:36');
INSERT INTO `admin_operation_log` VALUES ('116', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:48:40', '2019-07-17 18:48:40');
INSERT INTO `admin_operation_log` VALUES ('117', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:48:43', '2019-07-17 18:48:43');
INSERT INTO `admin_operation_log` VALUES ('118', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:48:43', '2019-07-17 18:48:43');
INSERT INTO `admin_operation_log` VALUES ('119', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:49:03', '2019-07-17 18:49:03');
INSERT INTO `admin_operation_log` VALUES ('120', '1', 'admin/auth/menu/9/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 18:49:07', '2019-07-17 18:49:07');
INSERT INTO `admin_operation_log` VALUES ('121', '1', 'admin/auth/menu/9', 'PUT', '172.19.0.1', '{\"parent_id\":\"8\",\"title\":\"\\u5206\\u7c7b\\u5217\\u8868\",\"icon\":\"fa-cubes\",\"uri\":\"category\\/index\",\"roles\":[null],\"_token\":\"00ByyxZuaDzI25XNMLT3I3wrq7Wv8fJhAwQd3Gki\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-17 18:49:33', '2019-07-17 18:49:33');
INSERT INTO `admin_operation_log` VALUES ('122', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:49:34', '2019-07-17 18:49:34');
INSERT INTO `admin_operation_log` VALUES ('123', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-17 18:51:38', '2019-07-17 18:51:38');
INSERT INTO `admin_operation_log` VALUES ('124', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-17 19:13:19', '2019-07-17 19:13:19');
INSERT INTO `admin_operation_log` VALUES ('125', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:06:28', '2019-07-18 10:06:28');
INSERT INTO `admin_operation_log` VALUES ('126', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:06:47', '2019-07-18 10:06:47');
INSERT INTO `admin_operation_log` VALUES ('127', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:06:47', '2019-07-18 10:06:47');
INSERT INTO `admin_operation_log` VALUES ('128', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:09:44', '2019-07-18 10:09:44');
INSERT INTO `admin_operation_log` VALUES ('129', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:09:44', '2019-07-18 10:09:44');
INSERT INTO `admin_operation_log` VALUES ('130', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:10:34', '2019-07-18 10:10:34');
INSERT INTO `admin_operation_log` VALUES ('131', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:10:34', '2019-07-18 10:10:34');
INSERT INTO `admin_operation_log` VALUES ('132', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:10:36', '2019-07-18 10:10:36');
INSERT INTO `admin_operation_log` VALUES ('133', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:10:37', '2019-07-18 10:10:37');
INSERT INTO `admin_operation_log` VALUES ('134', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:10:40', '2019-07-18 10:10:40');
INSERT INTO `admin_operation_log` VALUES ('135', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:10:43', '2019-07-18 10:10:43');
INSERT INTO `admin_operation_log` VALUES ('136', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:10:44', '2019-07-18 10:10:44');
INSERT INTO `admin_operation_log` VALUES ('137', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:11:26', '2019-07-18 10:11:26');
INSERT INTO `admin_operation_log` VALUES ('138', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:11:27', '2019-07-18 10:11:27');
INSERT INTO `admin_operation_log` VALUES ('139', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:12:51', '2019-07-18 10:12:51');
INSERT INTO `admin_operation_log` VALUES ('140', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:12:51', '2019-07-18 10:12:51');
INSERT INTO `admin_operation_log` VALUES ('141', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:13:33', '2019-07-18 10:13:33');
INSERT INTO `admin_operation_log` VALUES ('142', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"8\",\"title\":\"\\u6dfb\\u52a0\\u5206\\u7c7b\",\"icon\":\"fa-anchor\",\"uri\":\"category\\/create\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 10:14:00', '2019-07-18 10:14:00');
INSERT INTO `admin_operation_log` VALUES ('143', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:14:00', '2019-07-18 10:14:00');
INSERT INTO `admin_operation_log` VALUES ('144', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:14:04', '2019-07-18 10:14:04');
INSERT INTO `admin_operation_log` VALUES ('145', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:14:07', '2019-07-18 10:14:07');
INSERT INTO `admin_operation_log` VALUES ('146', '1', 'admin/category', 'POST', '172.19.0.1', '{\"link\":\"1\",\"title\":\"\\u5206\\u7c7b\\u5217\\u8868\",\"name\":\"\\u591a\\u8868\\u5173\\u8054\\u67e5\\u8be2\",\"name_en\":\"asdadadadasdasd\",\"is_show\":\"on\",\"sort\":\"10\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 10:14:40', '2019-07-18 10:14:40');
INSERT INTO `admin_operation_log` VALUES ('147', '1', 'admin/category/create', 'GET', '172.19.0.1', '[]', '2019-07-18 10:14:41', '2019-07-18 10:14:41');
INSERT INTO `admin_operation_log` VALUES ('148', '1', 'admin/category', 'POST', '172.19.0.1', '{\"link\":\"1\",\"title\":\"\\u5206\\u7c7b\\u5217\\u8868\",\"name\":\"\\u591a\\u8868\\u5173\\u8054\\u67e5\\u8be2\",\"name_en\":\"asdadadadasdasd\",\"is_show\":\"on\",\"sort\":\"10\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 10:15:39', '2019-07-18 10:15:39');
INSERT INTO `admin_operation_log` VALUES ('149', '1', 'admin/category/create', 'GET', '172.19.0.1', '[]', '2019-07-18 10:15:39', '2019-07-18 10:15:39');
INSERT INTO `admin_operation_log` VALUES ('150', '1', 'admin/category', 'POST', '172.19.0.1', '{\"link\":\"1\",\"title\":\"\\u5206\\u7c7b\\u5217\\u8868\",\"name\":\"\\u591a\\u8868\\u5173\\u8054\\u67e5\\u8be2\",\"name_en\":\"asdadadadasdasd\",\"is_show\":\"on\",\"sort\":\"10\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 10:15:54', '2019-07-18 10:15:54');
INSERT INTO `admin_operation_log` VALUES ('151', '1', 'admin/category', 'GET', '172.19.0.1', '[]', '2019-07-18 10:15:54', '2019-07-18 10:15:54');
INSERT INTO `admin_operation_log` VALUES ('152', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:16:04', '2019-07-18 10:16:04');
INSERT INTO `admin_operation_log` VALUES ('153', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:17:15', '2019-07-18 10:17:15');
INSERT INTO `admin_operation_log` VALUES ('154', '1', 'admin/category/create', 'GET', '172.19.0.1', '[]', '2019-07-18 10:17:15', '2019-07-18 10:17:15');
INSERT INTO `admin_operation_log` VALUES ('155', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:18:05', '2019-07-18 10:18:05');
INSERT INTO `admin_operation_log` VALUES ('156', '1', 'admin/category/create', 'GET', '172.19.0.1', '[]', '2019-07-18 10:18:05', '2019-07-18 10:18:05');
INSERT INTO `admin_operation_log` VALUES ('157', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:18:44', '2019-07-18 10:18:44');
INSERT INTO `admin_operation_log` VALUES ('158', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:18:47', '2019-07-18 10:18:47');
INSERT INTO `admin_operation_log` VALUES ('159', '1', 'admin/category/create', 'GET', '172.19.0.1', '[]', '2019-07-18 10:18:47', '2019-07-18 10:18:47');
INSERT INTO `admin_operation_log` VALUES ('160', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:18:50', '2019-07-18 10:18:50');
INSERT INTO `admin_operation_log` VALUES ('161', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:18:54', '2019-07-18 10:18:54');
INSERT INTO `admin_operation_log` VALUES ('162', '1', 'admin/category/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:18:54', '2019-07-18 10:18:54');
INSERT INTO `admin_operation_log` VALUES ('163', '1', 'admin/category/create', 'GET', '172.19.0.1', '[]', '2019-07-18 10:18:54', '2019-07-18 10:18:54');
INSERT INTO `admin_operation_log` VALUES ('164', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:20:00', '2019-07-18 10:20:00');
INSERT INTO `admin_operation_log` VALUES ('165', '1', 'admin/auth/menu/9/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:20:14', '2019-07-18 10:20:14');
INSERT INTO `admin_operation_log` VALUES ('166', '1', 'admin/auth/menu/9', 'PUT', '172.19.0.1', '{\"parent_id\":\"8\",\"title\":\"\\u5206\\u7c7b\\u5217\\u8868\",\"icon\":\"fa-cubes\",\"uri\":\"category\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 10:20:21', '2019-07-18 10:20:21');
INSERT INTO `admin_operation_log` VALUES ('167', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:20:21', '2019-07-18 10:20:21');
INSERT INTO `admin_operation_log` VALUES ('168', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:20:30', '2019-07-18 10:20:30');
INSERT INTO `admin_operation_log` VALUES ('169', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:20:34', '2019-07-18 10:20:34');
INSERT INTO `admin_operation_log` VALUES ('170', '1', 'admin/category', 'GET', '172.19.0.1', '[]', '2019-07-18 10:23:28', '2019-07-18 10:23:28');
INSERT INTO `admin_operation_log` VALUES ('171', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:24:16', '2019-07-18 10:24:16');
INSERT INTO `admin_operation_log` VALUES ('172', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:24:25', '2019-07-18 10:24:25');
INSERT INTO `admin_operation_log` VALUES ('173', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:24:50', '2019-07-18 10:24:50');
INSERT INTO `admin_operation_log` VALUES ('174', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:24:52', '2019-07-18 10:24:52');
INSERT INTO `admin_operation_log` VALUES ('175', '1', 'admin/category/7/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:24:55', '2019-07-18 10:24:55');
INSERT INTO `admin_operation_log` VALUES ('176', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:25:17', '2019-07-18 10:25:17');
INSERT INTO `admin_operation_log` VALUES ('177', '1', 'admin/category', 'GET', '172.19.0.1', '[]', '2019-07-18 10:26:12', '2019-07-18 10:26:12');
INSERT INTO `admin_operation_log` VALUES ('178', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:23', '2019-07-18 10:29:23');
INSERT INTO `admin_operation_log` VALUES ('179', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:23', '2019-07-18 10:29:23');
INSERT INTO `admin_operation_log` VALUES ('180', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:23', '2019-07-18 10:29:23');
INSERT INTO `admin_operation_log` VALUES ('181', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:23', '2019-07-18 10:29:23');
INSERT INTO `admin_operation_log` VALUES ('182', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:24', '2019-07-18 10:29:24');
INSERT INTO `admin_operation_log` VALUES ('183', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:24', '2019-07-18 10:29:24');
INSERT INTO `admin_operation_log` VALUES ('184', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:25', '2019-07-18 10:29:25');
INSERT INTO `admin_operation_log` VALUES ('185', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:25', '2019-07-18 10:29:25');
INSERT INTO `admin_operation_log` VALUES ('186', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:25', '2019-07-18 10:29:25');
INSERT INTO `admin_operation_log` VALUES ('187', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:25', '2019-07-18 10:29:25');
INSERT INTO `admin_operation_log` VALUES ('188', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:26', '2019-07-18 10:29:26');
INSERT INTO `admin_operation_log` VALUES ('189', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:26', '2019-07-18 10:29:26');
INSERT INTO `admin_operation_log` VALUES ('190', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:28', '2019-07-18 10:29:28');
INSERT INTO `admin_operation_log` VALUES ('191', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:30', '2019-07-18 10:29:30');
INSERT INTO `admin_operation_log` VALUES ('192', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:31', '2019-07-18 10:29:31');
INSERT INTO `admin_operation_log` VALUES ('193', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:29:32', '2019-07-18 10:29:32');
INSERT INTO `admin_operation_log` VALUES ('194', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:36:02', '2019-07-18 10:36:02');
INSERT INTO `admin_operation_log` VALUES ('195', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:36:09', '2019-07-18 10:36:09');
INSERT INTO `admin_operation_log` VALUES ('196', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:36:10', '2019-07-18 10:36:10');
INSERT INTO `admin_operation_log` VALUES ('197', '1', 'admin/category/7/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:36:15', '2019-07-18 10:36:15');
INSERT INTO `admin_operation_log` VALUES ('198', '1', 'admin/category/7', 'PUT', '172.19.0.1', '{\"link\":\"1\",\"title\":\"Domingo Morissette Jr.11\",\"name\":\"Prof. Pearl Thiel DVM\",\"name_en\":\"Ollie Huels\",\"is_show\":\"off\",\"sort\":\"3\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/category\"}', '2019-07-18 10:36:20', '2019-07-18 10:36:20');
INSERT INTO `admin_operation_log` VALUES ('199', '1', 'admin/category/7/edit', 'GET', '172.19.0.1', '[]', '2019-07-18 10:36:21', '2019-07-18 10:36:21');
INSERT INTO `admin_operation_log` VALUES ('200', '1', 'admin/category/7', 'PUT', '172.19.0.1', '{\"link\":\"1\",\"title\":\"Domingo Morissette Jr.11\",\"name\":\"Prof. Pearl Thiel DVM\",\"name_en\":\"Ollie Huels\",\"is_show\":\"off\",\"sort\":\"3\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\"}', '2019-07-18 10:36:28', '2019-07-18 10:36:28');
INSERT INTO `admin_operation_log` VALUES ('201', '1', 'admin/category', 'GET', '172.19.0.1', '[]', '2019-07-18 10:36:29', '2019-07-18 10:36:29');
INSERT INTO `admin_operation_log` VALUES ('202', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:36:48', '2019-07-18 10:36:48');
INSERT INTO `admin_operation_log` VALUES ('203', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u516c\\u53f8\\u4fe1\\u606f\",\"icon\":\"fa-bars\",\"uri\":\"company_basic\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 10:39:59', '2019-07-18 10:39:59');
INSERT INTO `admin_operation_log` VALUES ('204', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:40:00', '2019-07-18 10:40:00');
INSERT INTO `admin_operation_log` VALUES ('205', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:40:03', '2019-07-18 10:40:03');
INSERT INTO `admin_operation_log` VALUES ('206', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:40:04', '2019-07-18 10:40:04');
INSERT INTO `admin_operation_log` VALUES ('207', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:40:05', '2019-07-18 10:40:05');
INSERT INTO `admin_operation_log` VALUES ('208', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:40:18', '2019-07-18 10:40:18');
INSERT INTO `admin_operation_log` VALUES ('209', '1', 'admin/auth/menu/11/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:40:21', '2019-07-18 10:40:21');
INSERT INTO `admin_operation_log` VALUES ('210', '1', 'admin/auth/menu/11', 'PUT', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u516c\\u53f8\\u4fe1\\u606f\",\"icon\":\"fa-bars\",\"uri\":\"CompanyBasic\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 10:40:29', '2019-07-18 10:40:29');
INSERT INTO `admin_operation_log` VALUES ('211', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:40:29', '2019-07-18 10:40:29');
INSERT INTO `admin_operation_log` VALUES ('212', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:40:32', '2019-07-18 10:40:32');
INSERT INTO `admin_operation_log` VALUES ('213', '1', 'admin/auth/menu/11/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:41:08', '2019-07-18 10:41:08');
INSERT INTO `admin_operation_log` VALUES ('214', '1', 'admin/auth/menu/11', 'PUT', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u516c\\u53f8\\u4fe1\\u606f\",\"icon\":\"fa-bars\",\"uri\":\"companyBasic\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 10:41:13', '2019-07-18 10:41:13');
INSERT INTO `admin_operation_log` VALUES ('215', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:41:13', '2019-07-18 10:41:13');
INSERT INTO `admin_operation_log` VALUES ('216', '1', 'admin/company_basic', 'GET', '172.19.0.1', '[]', '2019-07-18 10:41:23', '2019-07-18 10:41:23');
INSERT INTO `admin_operation_log` VALUES ('217', '1', 'admin/company_basic', 'GET', '172.19.0.1', '[]', '2019-07-18 10:44:59', '2019-07-18 10:44:59');
INSERT INTO `admin_operation_log` VALUES ('218', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:45:07', '2019-07-18 10:45:07');
INSERT INTO `admin_operation_log` VALUES ('219', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:45:10', '2019-07-18 10:45:10');
INSERT INTO `admin_operation_log` VALUES ('220', '1', 'admin/auth/menu/11/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:45:12', '2019-07-18 10:45:12');
INSERT INTO `admin_operation_log` VALUES ('221', '1', 'admin/auth/menu/11', 'PUT', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u516c\\u53f8\\u4fe1\\u606f\",\"icon\":\"fa-bars\",\"uri\":\"company_basic\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 10:45:18', '2019-07-18 10:45:18');
INSERT INTO `admin_operation_log` VALUES ('222', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:45:18', '2019-07-18 10:45:18');
INSERT INTO `admin_operation_log` VALUES ('223', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:45:25', '2019-07-18 10:45:25');
INSERT INTO `admin_operation_log` VALUES ('224', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:45:27', '2019-07-18 10:45:27');
INSERT INTO `admin_operation_log` VALUES ('225', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:45:27', '2019-07-18 10:45:27');
INSERT INTO `admin_operation_log` VALUES ('226', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:45:56', '2019-07-18 10:45:56');
INSERT INTO `admin_operation_log` VALUES ('227', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:46:10', '2019-07-18 10:46:10');
INSERT INTO `admin_operation_log` VALUES ('228', '1', 'admin/company_basic', 'GET', '172.19.0.1', '[]', '2019-07-18 10:46:14', '2019-07-18 10:46:14');
INSERT INTO `admin_operation_log` VALUES ('229', '1', 'admin/company_basic/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u8bad\\u7ec3\\u8425\",\"name\":\"\\u5085\\u6b63\\u521a\\u8d85\\u6a21\\u8bad\\u7ec3\\u8425\",\"name_en\":\"FuZhengGang\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\"}', '2019-07-18 10:46:48', '2019-07-18 10:46:48');
INSERT INTO `admin_operation_log` VALUES ('230', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 10:46:49', '2019-07-18 10:46:49');
INSERT INTO `admin_operation_log` VALUES ('231', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:46:52', '2019-07-18 10:46:52');
INSERT INTO `admin_operation_log` VALUES ('232', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:47:01', '2019-07-18 10:47:01');
INSERT INTO `admin_operation_log` VALUES ('233', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:47:06', '2019-07-18 10:47:06');
INSERT INTO `admin_operation_log` VALUES ('234', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:47:07', '2019-07-18 10:47:07');
INSERT INTO `admin_operation_log` VALUES ('235', '1', 'admin/category', 'GET', '172.19.0.1', '[]', '2019-07-18 10:47:11', '2019-07-18 10:47:11');
INSERT INTO `admin_operation_log` VALUES ('236', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:47:52', '2019-07-18 10:47:52');
INSERT INTO `admin_operation_log` VALUES ('237', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:50:15', '2019-07-18 10:50:15');
INSERT INTO `admin_operation_log` VALUES ('238', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:50:18', '2019-07-18 10:50:18');
INSERT INTO `admin_operation_log` VALUES ('239', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:50:23', '2019-07-18 10:50:23');
INSERT INTO `admin_operation_log` VALUES ('240', '1', 'admin/auth/permissions', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:52:28', '2019-07-18 10:52:28');
INSERT INTO `admin_operation_log` VALUES ('241', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:52:28', '2019-07-18 10:52:28');
INSERT INTO `admin_operation_log` VALUES ('242', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:53:27', '2019-07-18 10:53:27');
INSERT INTO `admin_operation_log` VALUES ('243', '1', 'admin/auth/users', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:53:35', '2019-07-18 10:53:35');
INSERT INTO `admin_operation_log` VALUES ('244', '1', 'admin/auth/roles', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:53:40', '2019-07-18 10:53:40');
INSERT INTO `admin_operation_log` VALUES ('245', '1', 'admin/auth/permissions', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:53:42', '2019-07-18 10:53:42');
INSERT INTO `admin_operation_log` VALUES ('246', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:53:43', '2019-07-18 10:53:43');
INSERT INTO `admin_operation_log` VALUES ('247', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:54:48', '2019-07-18 10:54:48');
INSERT INTO `admin_operation_log` VALUES ('248', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:54:48', '2019-07-18 10:54:48');
INSERT INTO `admin_operation_log` VALUES ('249', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:54:48', '2019-07-18 10:54:48');
INSERT INTO `admin_operation_log` VALUES ('250', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:54:49', '2019-07-18 10:54:49');
INSERT INTO `admin_operation_log` VALUES ('251', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:55:45', '2019-07-18 10:55:45');
INSERT INTO `admin_operation_log` VALUES ('252', '1', 'admin/category/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:55:45', '2019-07-18 10:55:45');
INSERT INTO `admin_operation_log` VALUES ('253', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:55:48', '2019-07-18 10:55:48');
INSERT INTO `admin_operation_log` VALUES ('254', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:55:55', '2019-07-18 10:55:55');
INSERT INTO `admin_operation_log` VALUES ('255', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:55:57', '2019-07-18 10:55:57');
INSERT INTO `admin_operation_log` VALUES ('256', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:56:30', '2019-07-18 10:56:30');
INSERT INTO `admin_operation_log` VALUES ('257', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:56:40', '2019-07-18 10:56:40');
INSERT INTO `admin_operation_log` VALUES ('258', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u516c\\u53f8\\u914d\\u7f6e\",\"icon\":\"fa-balance-scale\",\"uri\":\"company\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 10:57:02', '2019-07-18 10:57:02');
INSERT INTO `admin_operation_log` VALUES ('259', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:57:02', '2019-07-18 10:57:02');
INSERT INTO `admin_operation_log` VALUES ('260', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:57:05', '2019-07-18 10:57:05');
INSERT INTO `admin_operation_log` VALUES ('261', '1', 'admin/company', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:57:07', '2019-07-18 10:57:07');
INSERT INTO `admin_operation_log` VALUES ('262', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 10:57:07', '2019-07-18 10:57:07');
INSERT INTO `admin_operation_log` VALUES ('263', '1', 'admin/company', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 10:57:35', '2019-07-18 10:57:35');
INSERT INTO `admin_operation_log` VALUES ('264', '1', 'admin/company/update', 'PUT', '172.19.0.1', '{\"name\":\"\\u5085\\u6b63\\u521a\\u8d85\\u6a21\\u8bad\\u7ec3\\u8425\",\"name_en\":\"FuZhengGang\",\"tel\":\"0571116231\",\"address\":\"\\u6d59\\u6c5f\\u7701\\u676d\\u5dde\\u5e02\\u6c5f\\u5e72\\u533a\\u4e0b\\u6c99\\u8857\\u9053\",\"phone\":\"18811112222\",\"fax\":\"1\",\"email\":\"nlxuran@163.com\",\"code\":\"341100\",\"icp\":\"Z-15246325872\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 10:58:47', '2019-07-18 10:58:47');
INSERT INTO `admin_operation_log` VALUES ('265', '1', 'admin/company', 'GET', '172.19.0.1', '[]', '2019-07-18 10:58:48', '2019-07-18 10:58:48');
INSERT INTO `admin_operation_log` VALUES ('266', '1', 'admin/company/update', 'PUT', '172.19.0.1', '{\"name\":\"\\u5085\\u6b63\\u521a\\u8d85\\u6a21\\u8bad\\u7ec3\\u8425\",\"name_en\":\"FuZhengGang\",\"tel\":\"0571116231\",\"address\":\"\\u6d59\\u6c5f\\u7701\\u676d\\u5dde\\u5e02\\u6c5f\\u5e72\\u533a\\u4e0b\\u6c99\\u8857\\u9053\",\"phone\":\"18811112222\",\"fax\":\"1\",\"email\":\"nlxuran@163.com\",\"code\":\"341100\",\"icp\":\"Z-15246325872\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\"}', '2019-07-18 10:59:48', '2019-07-18 10:59:48');
INSERT INTO `admin_operation_log` VALUES ('267', '1', 'admin/company', 'GET', '172.19.0.1', '[]', '2019-07-18 10:59:49', '2019-07-18 10:59:49');
INSERT INTO `admin_operation_log` VALUES ('268', '1', 'admin/company/update', 'PUT', '172.19.0.1', '{\"name\":\"\\u5085\\u6b63\\u521a\\u8d85\\u6a21\\u8bad\\u7ec3\\u8425\",\"name_en\":\"FuZhengGang\",\"tel\":\"0571116231\",\"address\":\"\\u6d59\\u6c5f\\u7701\\u676d\\u5dde\\u5e02\\u6c5f\\u5e72\\u533a\\u4e0b\\u6c99\\u8857\\u9053\",\"phone\":\"18811112222\",\"fax\":\"1\",\"email\":\"nlxuran@163.com\",\"code\":\"341100\",\"icp\":\"Z-15246325872\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\"}', '2019-07-18 11:00:18', '2019-07-18 11:00:18');
INSERT INTO `admin_operation_log` VALUES ('269', '1', 'admin/company', 'GET', '172.19.0.1', '[]', '2019-07-18 11:00:18', '2019-07-18 11:00:18');
INSERT INTO `admin_operation_log` VALUES ('270', '1', 'admin/company/update', 'PUT', '172.19.0.1', '{\"name\":\"\\u5085\\u6b63\\u521a\\u8d85\\u6a21\\u8bad\\u7ec3\\u8425\",\"name_en\":\"FuZhengGang\",\"tel\":\"0571116231\",\"address\":\"\\u6d59\\u6c5f\\u7701\\u676d\\u5dde\\u5e02\\u6c5f\\u5e72\\u533a\\u4e0b\\u6c99\\u8857\\u9053\",\"phone\":\"18811112222\",\"fax\":\"1\",\"email\":\"nlxuran@163.com\",\"code\":\"341100\",\"icp\":\"Z-15246325872\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\"}', '2019-07-18 11:00:30', '2019-07-18 11:00:30');
INSERT INTO `admin_operation_log` VALUES ('271', '1', 'admin/company', 'GET', '172.19.0.1', '[]', '2019-07-18 11:00:30', '2019-07-18 11:00:30');
INSERT INTO `admin_operation_log` VALUES ('272', '1', 'admin/company', 'GET', '172.19.0.1', '[]', '2019-07-18 11:00:34', '2019-07-18 11:00:34');
INSERT INTO `admin_operation_log` VALUES ('273', '1', 'admin/company', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:00:42', '2019-07-18 11:00:42');
INSERT INTO `admin_operation_log` VALUES ('274', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:00:43', '2019-07-18 11:00:43');
INSERT INTO `admin_operation_log` VALUES ('275', '1', 'admin/company', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:00:44', '2019-07-18 11:00:44');
INSERT INTO `admin_operation_log` VALUES ('276', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:01:26', '2019-07-18 11:01:26');
INSERT INTO `admin_operation_log` VALUES ('277', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u8d85\\u6a21\\u8bfe\\u7a0b\",\"icon\":\"fa-amazon\",\"uri\":null,\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:02:03', '2019-07-18 11:02:03');
INSERT INTO `admin_operation_log` VALUES ('278', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:02:03', '2019-07-18 11:02:03');
INSERT INTO `admin_operation_log` VALUES ('279', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u8bfe\\u7a0b\\u5217\\u8868\",\"icon\":\"fa-bars\",\"uri\":\"course\\/index\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:02:16', '2019-07-18 11:02:16');
INSERT INTO `admin_operation_log` VALUES ('280', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:02:16', '2019-07-18 11:02:16');
INSERT INTO `admin_operation_log` VALUES ('281', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u6dfb\\u52a0\\u8bfe\\u7a0b\",\"icon\":\"fa-plus-square\",\"uri\":\"course\\/create\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:02:50', '2019-07-18 11:02:50');
INSERT INTO `admin_operation_log` VALUES ('282', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:02:51', '2019-07-18 11:02:51');
INSERT INTO `admin_operation_log` VALUES ('283', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:02:53', '2019-07-18 11:02:53');
INSERT INTO `admin_operation_log` VALUES ('284', '1', 'admin/course/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:02:55', '2019-07-18 11:02:55');
INSERT INTO `admin_operation_log` VALUES ('285', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:02:56', '2019-07-18 11:02:56');
INSERT INTO `admin_operation_log` VALUES ('286', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:03:05', '2019-07-18 11:03:05');
INSERT INTO `admin_operation_log` VALUES ('287', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:03:12', '2019-07-18 11:03:12');
INSERT INTO `admin_operation_log` VALUES ('288', '1', 'admin/course', 'POST', '172.19.0.1', '{\"name\":\"\\u5085\\u6b63\\u521a\\u8d85\\u6a21\\u8bad\\u7ec3\\u8425-\\u745c\\u4f3d\",\"name_en\":\"YUJIA\",\"introduce\":\"11111111111\",\"introduce_en\":\"aaaaaaaaaaaaaaaa\",\"is_show\":\"on\",\"sort\":\"10\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:03:39', '2019-07-18 11:03:39');
INSERT INTO `admin_operation_log` VALUES ('289', '1', 'admin/course', 'GET', '172.19.0.1', '[]', '2019-07-18 11:03:39', '2019-07-18 11:03:39');
INSERT INTO `admin_operation_log` VALUES ('290', '1', 'admin/course/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:03:42', '2019-07-18 11:03:42');
INSERT INTO `admin_operation_log` VALUES ('291', '1', 'admin/course', 'GET', '172.19.0.1', '[]', '2019-07-18 11:03:42', '2019-07-18 11:03:42');
INSERT INTO `admin_operation_log` VALUES ('292', '1', 'admin/course', 'GET', '172.19.0.1', '[]', '2019-07-18 11:03:51', '2019-07-18 11:03:51');
INSERT INTO `admin_operation_log` VALUES ('293', '1', 'admin/course/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:03:55', '2019-07-18 11:03:55');
INSERT INTO `admin_operation_log` VALUES ('294', '1', 'admin/course', 'GET', '172.19.0.1', '[]', '2019-07-18 11:03:55', '2019-07-18 11:03:55');
INSERT INTO `admin_operation_log` VALUES ('295', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:03:58', '2019-07-18 11:03:58');
INSERT INTO `admin_operation_log` VALUES ('296', '1', 'admin/course/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:00', '2019-07-18 11:04:00');
INSERT INTO `admin_operation_log` VALUES ('297', '1', 'admin/course/create', 'GET', '172.19.0.1', '[]', '2019-07-18 11:04:00', '2019-07-18 11:04:00');
INSERT INTO `admin_operation_log` VALUES ('298', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:01', '2019-07-18 11:04:01');
INSERT INTO `admin_operation_log` VALUES ('299', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:03', '2019-07-18 11:04:03');
INSERT INTO `admin_operation_log` VALUES ('300', '1', 'admin/course/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:10', '2019-07-18 11:04:10');
INSERT INTO `admin_operation_log` VALUES ('301', '1', 'admin/course', 'GET', '172.19.0.1', '[]', '2019-07-18 11:04:10', '2019-07-18 11:04:10');
INSERT INTO `admin_operation_log` VALUES ('302', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:23', '2019-07-18 11:04:23');
INSERT INTO `admin_operation_log` VALUES ('303', '1', 'admin/auth/menu/14/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:25', '2019-07-18 11:04:25');
INSERT INTO `admin_operation_log` VALUES ('304', '1', 'admin/auth/menu/14', 'PUT', '172.19.0.1', '{\"parent_id\":\"13\",\"title\":\"\\u8bfe\\u7a0b\\u5217\\u8868\",\"icon\":\"fa-bars\",\"uri\":\"course\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 11:04:29', '2019-07-18 11:04:29');
INSERT INTO `admin_operation_log` VALUES ('305', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:04:30', '2019-07-18 11:04:30');
INSERT INTO `admin_operation_log` VALUES ('306', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:32', '2019-07-18 11:04:32');
INSERT INTO `admin_operation_log` VALUES ('307', '1', 'admin/course/index', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:33', '2019-07-18 11:04:33');
INSERT INTO `admin_operation_log` VALUES ('308', '1', 'admin/course/create', 'GET', '172.19.0.1', '[]', '2019-07-18 11:04:33', '2019-07-18 11:04:33');
INSERT INTO `admin_operation_log` VALUES ('309', '1', 'admin/course/create', 'GET', '172.19.0.1', '[]', '2019-07-18 11:04:36', '2019-07-18 11:04:36');
INSERT INTO `admin_operation_log` VALUES ('310', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:38', '2019-07-18 11:04:38');
INSERT INTO `admin_operation_log` VALUES ('311', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:41', '2019-07-18 11:04:41');
INSERT INTO `admin_operation_log` VALUES ('312', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:43', '2019-07-18 11:04:43');
INSERT INTO `admin_operation_log` VALUES ('313', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:46', '2019-07-18 11:04:46');
INSERT INTO `admin_operation_log` VALUES ('314', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:04:51', '2019-07-18 11:04:51');
INSERT INTO `admin_operation_log` VALUES ('315', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:05:57', '2019-07-18 11:05:57');
INSERT INTO `admin_operation_log` VALUES ('316', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:06:04', '2019-07-18 11:06:04');
INSERT INTO `admin_operation_log` VALUES ('317', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u9996\\u9875\",\"icon\":\"fa-anchor\",\"uri\":\"first\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:06:21', '2019-07-18 11:06:21');
INSERT INTO `admin_operation_log` VALUES ('318', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:06:21', '2019-07-18 11:06:21');
INSERT INTO `admin_operation_log` VALUES ('319', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:06:25', '2019-07-18 11:06:25');
INSERT INTO `admin_operation_log` VALUES ('320', '1', 'admin/first', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:06:26', '2019-07-18 11:06:26');
INSERT INTO `admin_operation_log` VALUES ('321', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:06:26', '2019-07-18 11:06:26');
INSERT INTO `admin_operation_log` VALUES ('322', '1', 'admin/first', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:07:09', '2019-07-18 11:07:09');
INSERT INTO `admin_operation_log` VALUES ('323', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"off\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 11:07:35', '2019-07-18 11:07:35');
INSERT INTO `admin_operation_log` VALUES ('324', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:07:35', '2019-07-18 11:07:35');
INSERT INTO `admin_operation_log` VALUES ('325', '1', 'admin/first', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:07:38', '2019-07-18 11:07:38');
INSERT INTO `admin_operation_log` VALUES ('326', '1', 'admin/first', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:07:42', '2019-07-18 11:07:42');
INSERT INTO `admin_operation_log` VALUES ('327', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:07:47', '2019-07-18 11:07:47');
INSERT INTO `admin_operation_log` VALUES ('328', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:08:02', '2019-07-18 11:08:02');
INSERT INTO `admin_operation_log` VALUES ('329', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"on\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\"}', '2019-07-18 11:08:11', '2019-07-18 11:08:11');
INSERT INTO `admin_operation_log` VALUES ('330', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:08:11', '2019-07-18 11:08:11');
INSERT INTO `admin_operation_log` VALUES ('331', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:08:14', '2019-07-18 11:08:14');
INSERT INTO `admin_operation_log` VALUES ('332', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"off\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\"}', '2019-07-18 11:08:20', '2019-07-18 11:08:20');
INSERT INTO `admin_operation_log` VALUES ('333', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:08:20', '2019-07-18 11:08:20');
INSERT INTO `admin_operation_log` VALUES ('334', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:08:23', '2019-07-18 11:08:23');
INSERT INTO `admin_operation_log` VALUES ('335', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u8d85\\u6a21\\u57fa\\u56e0\",\"icon\":\"fa-android\",\"uri\":\"genes\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:12:06', '2019-07-18 11:12:06');
INSERT INTO `admin_operation_log` VALUES ('336', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:12:07', '2019-07-18 11:12:07');
INSERT INTO `admin_operation_log` VALUES ('337', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:12:08', '2019-07-18 11:12:08');
INSERT INTO `admin_operation_log` VALUES ('338', '1', 'admin/auth/menu/17/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:12:32', '2019-07-18 11:12:32');
INSERT INTO `admin_operation_log` VALUES ('339', '1', 'admin/auth/menu/17', 'PUT', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u8d85\\u6a21\\u57fa\\u56e0\",\"icon\":\"fa-android\",\"uri\":\"gene\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 11:12:36', '2019-07-18 11:12:36');
INSERT INTO `admin_operation_log` VALUES ('340', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:12:37', '2019-07-18 11:12:37');
INSERT INTO `admin_operation_log` VALUES ('341', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:12:38', '2019-07-18 11:12:38');
INSERT INTO `admin_operation_log` VALUES ('342', '1', 'admin/gene', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:12:40', '2019-07-18 11:12:40');
INSERT INTO `admin_operation_log` VALUES ('343', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:12:40', '2019-07-18 11:12:40');
INSERT INTO `admin_operation_log` VALUES ('344', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:12:59', '2019-07-18 11:12:59');
INSERT INTO `admin_operation_log` VALUES ('345', '1', 'admin/gene', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:13:04', '2019-07-18 11:13:04');
INSERT INTO `admin_operation_log` VALUES ('346', '1', 'admin/gene/update', 'PUT', '172.19.0.1', '{\"is_video\":\"off\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/first\"}', '2019-07-18 11:13:16', '2019-07-18 11:13:16');
INSERT INTO `admin_operation_log` VALUES ('347', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 11:13:17', '2019-07-18 11:13:17');
INSERT INTO `admin_operation_log` VALUES ('348', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"off\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/gene\"}', '2019-07-18 11:13:41', '2019-07-18 11:13:41');
INSERT INTO `admin_operation_log` VALUES ('349', '1', 'admin/gene', 'GET', '172.19.0.1', '[]', '2019-07-18 11:13:42', '2019-07-18 11:13:42');
INSERT INTO `admin_operation_log` VALUES ('350', '1', 'admin/gene', 'GET', '172.19.0.1', '[]', '2019-07-18 11:13:44', '2019-07-18 11:13:44');
INSERT INTO `admin_operation_log` VALUES ('351', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:14:03', '2019-07-18 11:14:03');
INSERT INTO `admin_operation_log` VALUES ('352', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:14:04', '2019-07-18 11:14:04');
INSERT INTO `admin_operation_log` VALUES ('353', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u6545\\u4e8b\\u7ba1\\u7406\",\"icon\":\"fa-angellist\",\"uri\":\"story\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:14:46', '2019-07-18 11:14:46');
INSERT INTO `admin_operation_log` VALUES ('354', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:14:46', '2019-07-18 11:14:46');
INSERT INTO `admin_operation_log` VALUES ('355', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"18\",\"title\":\"\\u6545\\u4e8b\\u5217\\u8868\",\"icon\":\"fa-arrow-circle-left\",\"uri\":\"story\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:15:06', '2019-07-18 11:15:06');
INSERT INTO `admin_operation_log` VALUES ('356', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:15:06', '2019-07-18 11:15:06');
INSERT INTO `admin_operation_log` VALUES ('357', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"parent_id\":\"18\",\"title\":\"\\u6dfb\\u52a0\\u6545\\u4e8b\",\"icon\":\"fa-apple\",\"uri\":\"story\\/create\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\"}', '2019-07-18 11:15:34', '2019-07-18 11:15:34');
INSERT INTO `admin_operation_log` VALUES ('358', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:15:34', '2019-07-18 11:15:34');
INSERT INTO `admin_operation_log` VALUES ('359', '1', 'admin/course', 'GET', '172.19.0.1', '[]', '2019-07-18 11:15:37', '2019-07-18 11:15:37');
INSERT INTO `admin_operation_log` VALUES ('360', '1', 'admin/gene', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:16:58', '2019-07-18 11:16:58');
INSERT INTO `admin_operation_log` VALUES ('361', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:19:25', '2019-07-18 11:19:25');
INSERT INTO `admin_operation_log` VALUES ('362', '1', 'admin/auth/menu/19/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:19:38', '2019-07-18 11:19:38');
INSERT INTO `admin_operation_log` VALUES ('363', '1', 'admin/auth/menu/19', 'PUT', '172.19.0.1', '{\"parent_id\":\"18\",\"title\":\"\\u6545\\u4e8b\\u5217\\u8868\",\"icon\":\"fa-arrow-circle-left\",\"uri\":\"story_list\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 11:19:48', '2019-07-18 11:19:48');
INSERT INTO `admin_operation_log` VALUES ('364', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:19:48', '2019-07-18 11:19:48');
INSERT INTO `admin_operation_log` VALUES ('365', '1', 'admin/auth/menu/20/edit', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:19:51', '2019-07-18 11:19:51');
INSERT INTO `admin_operation_log` VALUES ('366', '1', 'admin/auth/menu/20', 'PUT', '172.19.0.1', '{\"parent_id\":\"18\",\"title\":\"\\u6dfb\\u52a0\\u6545\\u4e8b\",\"icon\":\"fa-apple\",\"uri\":\"story_list\\/create\",\"roles\":[null],\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/auth\\/menu\"}', '2019-07-18 11:19:55', '2019-07-18 11:19:55');
INSERT INTO `admin_operation_log` VALUES ('367', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 11:19:55', '2019-07-18 11:19:55');
INSERT INTO `admin_operation_log` VALUES ('368', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:20:17', '2019-07-18 11:20:17');
INSERT INTO `admin_operation_log` VALUES ('369', '1', 'admin/story_list', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:20:21', '2019-07-18 11:20:21');
INSERT INTO `admin_operation_log` VALUES ('370', '1', 'admin/story_list/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:24:11', '2019-07-18 11:24:11');
INSERT INTO `admin_operation_log` VALUES ('371', '1', 'admin/story_list', 'POST', '172.19.0.1', '{\"introduce\":\"\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\\u5085\\u6b63\\u521a\",\"introduce_en\":\"FuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGang\",\"is_video\":\"off\",\"is_show\":\"on\",\"sort\":\"10\",\"_token\":\"M4OF0Bydm5Imed3uAcFLkHBV1SKv0ReWuiKiR63s\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/story_list\"}', '2019-07-18 11:24:39', '2019-07-18 11:24:39');
INSERT INTO `admin_operation_log` VALUES ('372', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:24:40', '2019-07-18 11:24:40');
INSERT INTO `admin_operation_log` VALUES ('373', '1', 'admin/story_list', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:24:43', '2019-07-18 11:24:43');
INSERT INTO `admin_operation_log` VALUES ('374', '1', 'admin/gene', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:24:45', '2019-07-18 11:24:45');
INSERT INTO `admin_operation_log` VALUES ('375', '1', 'admin/story_list', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:24:47', '2019-07-18 11:24:47');
INSERT INTO `admin_operation_log` VALUES ('376', '1', 'admin/story_list/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:24:48', '2019-07-18 11:24:48');
INSERT INTO `admin_operation_log` VALUES ('377', '1', 'admin/story_list', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:24:49', '2019-07-18 11:24:49');
INSERT INTO `admin_operation_log` VALUES ('378', '1', 'admin/story_list', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:24:56', '2019-07-18 11:24:56');
INSERT INTO `admin_operation_log` VALUES ('379', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:25:17', '2019-07-18 11:25:17');
INSERT INTO `admin_operation_log` VALUES ('380', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:25:20', '2019-07-18 11:25:20');
INSERT INTO `admin_operation_log` VALUES ('381', '1', 'admin/company_basic', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:41:23', '2019-07-18 11:41:23');
INSERT INTO `admin_operation_log` VALUES ('382', '1', 'admin/company', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:41:24', '2019-07-18 11:41:24');
INSERT INTO `admin_operation_log` VALUES ('383', '1', 'admin/first', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:41:25', '2019-07-18 11:41:25');
INSERT INTO `admin_operation_log` VALUES ('384', '1', 'admin/gene', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:41:26', '2019-07-18 11:41:26');
INSERT INTO `admin_operation_log` VALUES ('385', '1', 'admin/company', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:41:30', '2019-07-18 11:41:30');
INSERT INTO `admin_operation_log` VALUES ('386', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:41:32', '2019-07-18 11:41:32');
INSERT INTO `admin_operation_log` VALUES ('387', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:41:35', '2019-07-18 11:41:35');
INSERT INTO `admin_operation_log` VALUES ('388', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:44:10', '2019-07-18 11:44:10');
INSERT INTO `admin_operation_log` VALUES ('389', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:44:14', '2019-07-18 11:44:14');
INSERT INTO `admin_operation_log` VALUES ('390', '1', 'admin/gene', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:44:16', '2019-07-18 11:44:16');
INSERT INTO `admin_operation_log` VALUES ('391', '1', 'admin/story_list', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:44:18', '2019-07-18 11:44:18');
INSERT INTO `admin_operation_log` VALUES ('392', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:44:29', '2019-07-18 11:44:29');
INSERT INTO `admin_operation_log` VALUES ('393', '1', 'admin/course/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:44:32', '2019-07-18 11:44:32');
INSERT INTO `admin_operation_log` VALUES ('394', '1', 'admin/course', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:45:55', '2019-07-18 11:45:55');
INSERT INTO `admin_operation_log` VALUES ('395', '1', 'admin/gene', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:46:00', '2019-07-18 11:46:00');
INSERT INTO `admin_operation_log` VALUES ('396', '1', 'admin/story_list', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:46:02', '2019-07-18 11:46:02');
INSERT INTO `admin_operation_log` VALUES ('397', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:47:14', '2019-07-18 11:47:14');
INSERT INTO `admin_operation_log` VALUES ('398', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:49:03', '2019-07-18 11:49:03');
INSERT INTO `admin_operation_log` VALUES ('399', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:49:58', '2019-07-18 11:49:58');
INSERT INTO `admin_operation_log` VALUES ('400', '1', 'admin/story_list', 'GET', '172.19.0.1', '[]', '2019-07-18 11:57:02', '2019-07-18 11:57:02');
INSERT INTO `admin_operation_log` VALUES ('401', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 11:57:37', '2019-07-18 11:57:37');
INSERT INTO `admin_operation_log` VALUES ('402', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 14:59:24', '2019-07-18 14:59:24');
INSERT INTO `admin_operation_log` VALUES ('403', '1', 'admin/category', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:28', '2019-07-18 14:59:28');
INSERT INTO `admin_operation_log` VALUES ('404', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:34', '2019-07-18 14:59:34');
INSERT INTO `admin_operation_log` VALUES ('405', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:36', '2019-07-18 14:59:36');
INSERT INTO `admin_operation_log` VALUES ('406', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:39', '2019-07-18 14:59:39');
INSERT INTO `admin_operation_log` VALUES ('407', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:39', '2019-07-18 14:59:39');
INSERT INTO `admin_operation_log` VALUES ('408', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:39', '2019-07-18 14:59:39');
INSERT INTO `admin_operation_log` VALUES ('409', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:39', '2019-07-18 14:59:39');
INSERT INTO `admin_operation_log` VALUES ('410', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 14:59:39', '2019-07-18 14:59:39');
INSERT INTO `admin_operation_log` VALUES ('411', '1', 'admin/story_list/create', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 15:04:40', '2019-07-18 15:04:40');
INSERT INTO `admin_operation_log` VALUES ('412', '1', 'admin/first', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 15:04:48', '2019-07-18 15:04:48');
INSERT INTO `admin_operation_log` VALUES ('413', '1', 'admin/first/update', 'GET', '172.19.0.1', '[]', '2019-07-18 15:05:58', '2019-07-18 15:05:58');
INSERT INTO `admin_operation_log` VALUES ('414', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:06:13', '2019-07-18 15:06:13');
INSERT INTO `admin_operation_log` VALUES ('415', '1', 'admin/first/update', 'GET', '172.19.0.1', '[]', '2019-07-18 15:07:30', '2019-07-18 15:07:30');
INSERT INTO `admin_operation_log` VALUES ('416', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:07:34', '2019-07-18 15:07:34');
INSERT INTO `admin_operation_log` VALUES ('417', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a22\",\"is_video\":\"off\",\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/laravel55.cc\\/admin\\/first\\/update\"}', '2019-07-18 15:07:40', '2019-07-18 15:07:40');
INSERT INTO `admin_operation_log` VALUES ('418', '1', 'admin/first/update', 'GET', '172.19.0.1', '[]', '2019-07-18 15:07:40', '2019-07-18 15:07:40');
INSERT INTO `admin_operation_log` VALUES ('419', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:07:40', '2019-07-18 15:07:40');
INSERT INTO `admin_operation_log` VALUES ('420', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"on\",\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_method\":\"PUT\"}', '2019-07-18 15:07:57', '2019-07-18 15:07:57');
INSERT INTO `admin_operation_log` VALUES ('421', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:07:57', '2019-07-18 15:07:57');
INSERT INTO `admin_operation_log` VALUES ('422', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a11\",\"is_video\":\"on\",\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_method\":\"PUT\"}', '2019-07-18 15:08:00', '2019-07-18 15:08:00');
INSERT INTO `admin_operation_log` VALUES ('423', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:08:01', '2019-07-18 15:08:01');
INSERT INTO `admin_operation_log` VALUES ('424', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"off\",\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_method\":\"PUT\"}', '2019-07-18 15:08:07', '2019-07-18 15:08:07');
INSERT INTO `admin_operation_log` VALUES ('425', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:08:07', '2019-07-18 15:08:07');
INSERT INTO `admin_operation_log` VALUES ('426', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"off\",\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_method\":\"PUT\"}', '2019-07-18 15:08:08', '2019-07-18 15:08:08');
INSERT INTO `admin_operation_log` VALUES ('427', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:08:09', '2019-07-18 15:08:09');
INSERT INTO `admin_operation_log` VALUES ('428', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"off\",\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_method\":\"PUT\"}', '2019-07-18 15:08:10', '2019-07-18 15:08:10');
INSERT INTO `admin_operation_log` VALUES ('429', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:08:11', '2019-07-18 15:08:11');
INSERT INTO `admin_operation_log` VALUES ('430', '1', 'admin/first/update', 'PUT', '172.19.0.1', '{\"title\":\"\\u5085\\u6b63\\u521a\",\"is_video\":\"off\",\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_method\":\"PUT\"}', '2019-07-18 15:08:11', '2019-07-18 15:08:11');
INSERT INTO `admin_operation_log` VALUES ('431', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 15:08:11', '2019-07-18 15:08:11');
INSERT INTO `admin_operation_log` VALUES ('432', '1', 'admin', 'GET', '172.19.0.1', '[]', '2019-07-18 15:08:51', '2019-07-18 15:08:51');
INSERT INTO `admin_operation_log` VALUES ('433', '1', 'admin', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 15:40:48', '2019-07-18 15:40:48');
INSERT INTO `admin_operation_log` VALUES ('434', '1', 'admin/first/update', 'GET', '172.19.0.1', '[]', '2019-07-18 16:23:47', '2019-07-18 16:23:47');
INSERT INTO `admin_operation_log` VALUES ('435', '1', 'admin/first', 'GET', '172.19.0.1', '[]', '2019-07-18 16:23:51', '2019-07-18 16:23:51');
INSERT INTO `admin_operation_log` VALUES ('436', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 16:24:11', '2019-07-18 16:24:11');
INSERT INTO `admin_operation_log` VALUES ('437', '1', 'admin/auth/menu', 'POST', '172.19.0.1', '{\"_token\":\"hYZY1zh51aYet6lBO8nGlJl20UvvQxaCgCEwFpPF\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]},{\\\"id\\\":16},{\\\"id\\\":11},{\\\"id\\\":12},{\\\"id\\\":17},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10}]},{\\\"id\\\":18,\\\"children\\\":[{\\\"id\\\":19},{\\\"id\\\":20}]},{\\\"id\\\":13,\\\"children\\\":[{\\\"id\\\":14},{\\\"id\\\":15}]}]\"}', '2019-07-18 16:24:54', '2019-07-18 16:24:54');
INSERT INTO `admin_operation_log` VALUES ('438', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 16:24:55', '2019-07-18 16:24:55');
INSERT INTO `admin_operation_log` VALUES ('439', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '[]', '2019-07-18 16:24:58', '2019-07-18 16:24:58');
INSERT INTO `admin_operation_log` VALUES ('440', '1', 'admin/auth/menu', 'GET', '172.19.0.1', '{\"_pjax\":\"#pjax-container\"}', '2019-07-18 16:26:40', '2019-07-18 16:26:40');

-- ----------------------------
-- Table structure for admin_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`),
  UNIQUE KEY `admin_permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_permissions
-- ----------------------------
INSERT INTO `admin_permissions` VALUES ('1', 'All permission', '*', '', '*', null, null);
INSERT INTO `admin_permissions` VALUES ('2', 'Dashboard', 'dashboard', 'GET', '/', null, null);
INSERT INTO `admin_permissions` VALUES ('3', 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', null, null);
INSERT INTO `admin_permissions` VALUES ('4', 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', null, null);
INSERT INTO `admin_permissions` VALUES ('5', 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', null, null);

-- ----------------------------
-- Table structure for admin_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_role_menu
-- ----------------------------
INSERT INTO `admin_role_menu` VALUES ('1', '2', null, null);

-- ----------------------------
-- Table structure for admin_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_role_permissions
-- ----------------------------
INSERT INTO `admin_role_permissions` VALUES ('1', '1', null, null);

-- ----------------------------
-- Table structure for admin_role_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_role_users
-- ----------------------------
INSERT INTO `admin_role_users` VALUES ('1', '1', null, null);

-- ----------------------------
-- Table structure for admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`),
  UNIQUE KEY `admin_roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_roles
-- ----------------------------
INSERT INTO `admin_roles` VALUES ('1', 'Administrator', 'administrator', '2019-07-02 08:50:24', '2019-07-02 08:50:24');

-- ----------------------------
-- Table structure for admin_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for admin_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of admin_users
-- ----------------------------
INSERT INTO `admin_users` VALUES ('1', 'admin', '$2y$10$47p4pLqKIAWM1mzCXQ.u1.7S/j5sdeJo6bA.JWPFHbglLKBIkwy16', 'Admin', null, null, '2019-07-17 18:30:04', '2019-07-17 18:30:07');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类的标题',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类的名称',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类的名称-英文',
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类图片',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类的父id',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示 1-显示 0-隐藏',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '字体图标',
  `link` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`),
  UNIQUE KEY `categories_name_unique` (`name`),
  UNIQUE KEY `categories_name_en_unique` (`name_en`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Dr. Katelynn Metz DDS', 'Ulices Collier', 'Ms. Amy Schmitt MD', '', '0', '44', '0', 'SmZvUZIN5M', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('2', 'Dr. Lucinda Adams V', 'Genoveva O\'Keefe', 'Prof. Omari Grant', '', '0', '30', '0', '3j1Pn86soF', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('3', 'Elsa Howell', 'Ms. Eloise Boyer DDS', 'Prof. Mathew Oberbrunner', '', '0', '41', '1', 'NBkgvD4sbp', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('4', 'Zula Stokes V', 'Lilyan Weissnat III', 'Prof. Obie Hintz', '', '0', '37', '1', 'dzT9Y9amZ3', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('5', 'Dr. Ronaldo Keeling', 'Stacy Jacobson', 'Desiree O\'Conner', '', '0', '23', '0', 'a42S9o7EkK', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('6', 'Mr. Marcus Pollich II', 'Margret Mayer', 'Jarrett Lueilwitz PhD', '', '0', '44', '1', 'kNqNuspICO', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('7', 'Domingo Morissette Jr.11', 'Prof. Pearl Thiel DVM', 'Ollie Huels', 'images/b799e2fef6af1eabbb5ddcc622441e50.png', '0', '3', '0', 'itv9LP8mmE', '1', '2019-07-15 14:13:41', '2019-07-18 10:36:29');
INSERT INTO `categories` VALUES ('8', 'Mrs. Imogene Schiller', 'Dorian Hammes', 'Lydia Will', '', '0', '34', '1', '3n4W2IbgDd', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('9', 'Camryn Oberbrunner', 'Fritz Kerluke', 'Royce Parisian', '', '0', '16', '0', 'OXmdzPyV9o', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('10', 'Dr. Gerard Hermiston', 'Reuben Marquardt', 'Lucienne Will', '', '0', '31', '0', 'uTo3UUPKEw', '1', '2019-07-15 14:13:41', '2019-07-15 14:13:41');
INSERT INTO `categories` VALUES ('11', '分类列表', '多表关联查询', 'asdadadadasdasd', 'images/edffa8adbfb74ad3d8a538c9928de101.png', '0', '10', '1', null, '1', '2019-07-18 10:15:54', '2019-07-18 10:15:54');

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公司名称-中文',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公司名称-英文',
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '电话',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机',
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '传真',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮箱',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `icp` char(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '邮编',
  `app_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'app二维码',
  `wechat_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '微信公众号二维码',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES ('1', '傅正刚超模训练营', 'FuZhengGang', '0571116231', '18811112222', '1', 'nlxuran@163.com', '浙江省杭州市江干区下沙街道', 'Z-15246325872', '341100', 'images/8892eca8cf9c36f51c0e994029180481.png', 'images/7b419ad4fa1a5ea3b8d0f861dd40e2c4.png', '2019-07-18 10:57:26', '2019-07-18 11:00:30');

-- ----------------------------
-- Table structure for company_basic
-- ----------------------------
DROP TABLE IF EXISTS `company_basic`;
CREATE TABLE `company_basic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题（中文）',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称（中文）',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '名称（英文）',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of company_basic
-- ----------------------------
INSERT INTO `company_basic` VALUES ('1', '训练营', '傅正刚超模训练营', 'FuZhengGang', 'files/aebc21d966cbb106530976d8c61841f7.png', '2019-07-18 10:46:49', '2019-07-18 10:46:49');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '课程名-中文',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '课程名-英文',
  `introduce` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介-中文',
  `introduce_en` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介-英文',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '照片',
  `sort` int(11) NOT NULL DEFAULT '10' COMMENT '排序',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示（1-显示-默认 0-不显示）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '傅正刚超模训练营-瑜伽', 'YUJIA', '11111111111', 'aaaaaaaaaaaaaaaa', 'images/3410fb806f9a6fab89810b047c211231.png', '10', '1', '2019-07-18 11:03:39', '2019-07-18 11:03:39');

-- ----------------------------
-- Table structure for genes
-- ----------------------------
DROP TABLE IF EXISTS `genes`;
CREATE TABLE `genes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题-中文',
  `title_en` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题-英文',
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称-中文',
  `name_en` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称-英文',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容-中文',
  `content_en` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容-英文',
  `file` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片/视频路径',
  `is_video` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是视频（1-是 0-不是-默认）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of genes
-- ----------------------------
INSERT INTO `genes` VALUES ('1', '1', '1', '1', '1', '1', '1', 'files/43fb4ee95a0d65e4d70d48f750afd6b0.png', '0', '2019-07-18 11:12:53', '2019-07-18 11:13:16');

-- ----------------------------
-- Table structure for home
-- ----------------------------
DROP TABLE IF EXISTS `home`;
CREATE TABLE `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '中文名称',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '英文名称',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片/视频路径',
  `is_video` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是视频（1-是 0-不是-默认）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `home_title_unique` (`title`),
  UNIQUE KEY `home_name_unique` (`name`),
  UNIQUE KEY `home_name_en_unique` (`name_en`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of home
-- ----------------------------
INSERT INTO `home` VALUES ('1', '傅正刚', '1', '1', 'files/1.png', '0', '2019-07-18 11:07:02', '2019-07-18 15:08:07');

-- ----------------------------
-- Table structure for link
-- ----------------------------
DROP TABLE IF EXISTS `link`;
CREATE TABLE `link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of link
-- ----------------------------
INSERT INTO `link` VALUES ('1', '111', 'http', '2019-07-18 10:12:44', '2019-07-18 10:12:44');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2016_01_04_173148_create_admin_tables', '1');
INSERT INTO `migrations` VALUES ('4', '2018_04_20_025431_create_categories_table', '1');
INSERT INTO `migrations` VALUES ('5', '2018_05_03_151443_create_home_table', '1');
INSERT INTO `migrations` VALUES ('6', '2018_05_09_225101_create_genes_table', '1');
INSERT INTO `migrations` VALUES ('7', '2018_05_10_104415_create_teachers_table', '1');
INSERT INTO `migrations` VALUES ('8', '2018_05_10_153115_create_company_table', '1');
INSERT INTO `migrations` VALUES ('9', '2018_05_10_153115_create_course_table', '1');
INSERT INTO `migrations` VALUES ('10', '2018_05_10_153115_create_students_ordinary_table', '1');
INSERT INTO `migrations` VALUES ('11', '2018_05_10_153115_create_students_table', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for story
-- ----------------------------
DROP TABLE IF EXISTS `story`;
CREATE TABLE `story` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `introduce` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介（中文）',
  `introduce_en` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介（英文）',
  `is_video` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '视频/图片',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片/视频',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示',
  `sort` tinyint(1) unsigned NOT NULL DEFAULT '10' COMMENT '手动填写排序，默认排序是数字从小到大排列（数字相同按照ID排序）',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of story
-- ----------------------------
INSERT INTO `story` VALUES ('1', '傅正刚傅正刚傅正刚傅正刚傅正刚傅正刚傅正刚傅正刚傅正刚傅正刚', 'FuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGangFuZhengGang', '0', 'files/b0bb1ebb0214df412beef7ea5a3e7c76.png', '1', '10', '2019-07-18 11:24:40', '2019-07-18 11:24:40');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名-中文',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名-英文',
  `introduce` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介-中文',
  `introduce_en` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介-英文',
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '学员照片/视频',
  `is_video` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是视频（1-视频 0-图片-默认）',
  `sort` int(11) NOT NULL DEFAULT '10' COMMENT '排序',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示（1-显示-默认 0-不显示）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of students
-- ----------------------------

-- ----------------------------
-- Table structure for students_ordinary
-- ----------------------------
DROP TABLE IF EXISTS `students_ordinary`;
CREATE TABLE `students_ordinary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名-中文',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名-英文',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '学员照片',
  `sort` int(11) NOT NULL DEFAULT '10' COMMENT '排序',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示（1-显示-默认 0-不显示）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of students_ordinary
-- ----------------------------

-- ----------------------------
-- Table structure for teachers
-- ----------------------------
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名-中文',
  `name_en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名-英文',
  `introduce` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介-中文',
  `introduce_en` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介-英文',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '教师照片',
  `sort` int(11) NOT NULL DEFAULT '10' COMMENT '排序',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示（1-显示-默认 0-不显示）',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of teachers
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Table structure for web_visit
-- ----------------------------
DROP TABLE IF EXISTS `web_visit`;
CREATE TABLE `web_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of web_visit
-- ----------------------------
INSERT INTO `web_visit` VALUES ('1', '172.19.0.1', 'XX', 'XX', '内网IP', '内网IP', '1563420309');
