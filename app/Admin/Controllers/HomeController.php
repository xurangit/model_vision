<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebVisit;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Grid;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('首页');
            $content->description('欢迎来到傅正刚超模训练营管理后台...');

            //返回各阶段的访问数据
            $list = $this->WebVisitData();

            $content->row(view('title')->with(['list' => $list]));

            $content->body($this->grid());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(WebVisit::class, function (Grid $grid) {
            $grid->id('ID');
            $grid->ip('ip');
            $grid->country('国家');
            $grid->province('省份');
            $grid->city('城市');
            $grid->isp('运营商');
            $grid->time('访问时间')->display(function () {
                return date("Y-m-d H:i:s", $this->time);
            });

            //设置初始排序
            $grid->model()->OrderBy('id','desc');
            //禁用
            $grid->disableFilter();
            $grid->disableExport();
            $grid->disableCreateButton();
            $grid->disableRowSelector();
            $grid->disableActions();
        });
    }

    public function WebVisitData()
    {
        $data      = [];
        $today     = self::today();
        $yesterday = self::yesterday();
        $week      = self::week();
        $lastWeek  = self::lastWeek();
        $month     = self::month();
        $data[0]   = DB::table("web_visit")->whereBetween('time', $today)->count();
        $data[1]   = DB::table("web_visit")->whereBetween('time', $yesterday)->count();
        $data[2]   = DB::table("web_visit")->whereBetween('time', $week)->count();
        $data[3]   = DB::table("web_visit")->whereBetween('time', $lastWeek)->count();
        $data[4]   = DB::table("web_visit")->whereBetween('time', $month)->count();
        $data[5]   = DB::table("web_visit")->count();
        return $data;
    }

    /**
     * 返回今日开始和结束的时间戳
     *
     * @return array
     */
    public static function today()
    {
        return [
            mktime(0, 0, 0, date('m'), date('d'), date('Y')),
            mktime(23, 59, 59, date('m'), date('d'), date('Y'))
        ];
    }

    /**
     * 返回昨日开始和结束的时间戳
     *
     * @return array
     */
    public static function yesterday()
    {
        $yesterday = date('d') - 1;
        return [
            mktime(0, 0, 0, date('m'), $yesterday, date('Y')),
            mktime(23, 59, 59, date('m'), $yesterday, date('Y'))
        ];
    }

    /**
     * 返回本周开始和结束的时间戳
     *
     * @return array
     */
    public static function week()
    {
        $timestamp = time();
        return [
            strtotime(date('Y-m-d', strtotime("this week Monday", $timestamp))),
            strtotime(date('Y-m-d', strtotime("this week Sunday", $timestamp))) + 24 * 3600 - 1
        ];
    }

    /**
     * 返回上周开始和结束的时间戳
     *
     * @return array
     */
    public static function lastWeek()
    {
        $timestamp = time();
        return [
            strtotime(date('Y-m-d', strtotime("last week Monday", $timestamp))),
            strtotime(date('Y-m-d', strtotime("last week Sunday", $timestamp))) + 24 * 3600 - 1
        ];
    }

    /**
     * 返回本月开始和结束的时间戳
     *
     * @return array
     */
    public static function month($everyDay = false)
    {
        return [
            mktime(0, 0, 0, date('m'), 1, date('Y')),
            mktime(23, 59, 59, date('m'), date('t'), date('Y'))
        ];
    }
}
