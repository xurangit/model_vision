<?php

namespace App\Admin\Controllers;

use App\Models\CompanyBasic;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CompanyBasicController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return $this->edit(1);
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('基本设置');
            $content->description('编辑');

            $content->body($this->form()->edit($id));
        });
    }

    public function update()
    {
        return $this->form()->update(1);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CompanyBasic::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(CompanyBasic::class, function (Form $form) {
            $form->setAction('/admin/company_basic/update');
//            $form->display('id', 'ID');
//            $cates = Category::all()->toArray();
//            $catesNew = [];
//            foreach ($cates as $key => $value) {
//                $catesNew[$value['id']] = $value['name'];
//            }
//            $form->select('cate_id', '分类')->options($catesNew);
            $form->text('title','标题（中文）');
//            $form->text('title_en','标题（英文）');
            $form->text('name','名称（中文）');
            $form->text('name_en','名称（英文）');

//            $form->textarea('content','内容（中文）')->rows(5);
//            $form->textarea('content_en','内容（英文）')->rows(5);
//            $states = [
//                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
//                'off' => ['value' => 0, 'text' => '否', 'color' => 'danger'],
//            ];
//            $form->switch('is_video','是否设置视频')->states($states)->help('是（显示视频） 否（显示图片）按钮要与下方图片/视频链接对应');

            $form->file('file','图片')->uniqueName();

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
            $form->tools(function (Form\Tools $tools) {
                // 去掉返回按钮
                $tools->disableBackButton();
                // 去掉跳转列表按钮
                $tools->disableListButton();
            });
        });
    }
}
