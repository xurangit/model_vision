<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" class="ancient-ie old-ie bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" class="ancient-ie old-ie  bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" class="old-ie bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if IE 9]>
<html id="ie9" class="old-ie9 bit-html" dir="ltr" lang="zh-CN">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html class="bit-html" dir="ltr" lang="zh-CN">
<!--<![endif]-->
<head>
    <!-- for 360 -->
    <meta name="renderer" content="webkit">
    <meta name="applicable-device" content="pc,mobile"> <!-- for baidu -->
    <meta http-equiv="Cache-Control" content="no-transform"/> <!-- for baidu -->
    <meta name="MobileOptimized" content="width"/><!-- for baidu -->
    <meta name="HandheldFriendly" content="true"/><!-- for baidu -->
    <meta name="baidu-site-verification" content="ZJTYs4ZAXD"/>
    <meta name="baidu-site-verification" content="QKVudTvBdU"/>
    <meta name="google-site-verification" content="emG8OWshwRUgZawA85PaCbo0RaLf5X8vpqeRuwiTsO8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- customer header -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title>商务合作</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <!--百度地图-->
    <style type="text/css">
        body, html {width: 100%;height: 100%;margin:0;font-family:"微软雅黑";}
        #allmap{width:100%;height:500px;}
        p{margin-left:5px; font-size:14px;}
    </style>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=u1KFfHCGyK4MW0r5kNrGqcB7ltjPy1eZ"></script>

    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="http://www.neverends.cn/xmlrpc.php"/>
    <!--[if lt IE 9]>
    <script src="{{asset('/home/js/html5shiv.min.js')}}"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="{{asset('/home/js/respond.min.js')}}"></script>
    <![endif]-->
    <style type="text/css" id="static-stylesheet"></style>
    <link rel='stylesheet' id='iphorm-css' href='{{asset("/home/css/styles.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dashicons-css' href='{{asset("/home/css/dashicons.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='thickbox-css' href='{{asset("/home/css/thickbox.css")}}' type='text/css' media='all'/>
    <style type='text/css'>
        .qfe_gmaps_widget .qfe_map_wraper {
            background-color: #f7f7f7;
            padding: 0px !important;
        }

    </style>
    <link rel='stylesheet' id='FeiEditor_respond-css' href='{{asset("/home/css/respond.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='open_social_css-css' href='{{asset("/home/css/os.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfy_editor_front-css' href='{{asset("/home/css/qfy_editor_front.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='vc_carousel_css-css' href='{{asset("/home/css/vc_carousel.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfyuser_font-css' href='{{asset("/home/css/qfyuser-font.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfyuser_min-css' href='{{asset("/home/css/qfyuser.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='qfyuser_skin_min-css' href='{{asset("/home/css/style.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='main-fonts-css' href='{{asset("/home/css/main.font.cdn.default.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-main-css' href='{{asset("/home/css/main.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-custom-less-css' href='{{asset("/home/css/custom-d566645bd5.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-bit-custom-less-css' href='{{asset("/home/css/bit-custom-15bb1788e8.css")}}' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='qfy_dynamic_css-css' href='{{asset("/home/css/qfy-custom-style.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='fontawesome_css-css' href='{{asset("/home/css/font-awesome.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-awsome-fonts-boot-css' href='{{asset("/home/css/bootstrap.min.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css' href='{{asset("/home/css/style_1.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-media-op-css-css' href='{{asset("/home/css/opentip.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-main-roya-css-css' href='{{asset("/home/css/royalslider.css")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-main-roya-default-css-css' href='{{asset("/home/css/rs-default.css")}}' type='text/css' media='all'/>
    <script type='text/javascript' src='{{asset("/home/js/jquery.js")}}'></script>
    <script type='text/javascript' src='{{asset("/home/js/jquery-migrate.min.js")}}'></script>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.neverends.cn/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://www.neverends.cn/qfy-includes/wlwmanifest.xml"/>

    <link rel='canonical' href='http://www.neverends.cn/?page_id=13351'/>
    <link rel='shortlink' href='http://www.neverends.cn/?p=13351'/>

    <style class='style_0'>.bitRightSider .widget-title, .bitLeftSider .widget-title {
        padding: 0 0 0 10px;
        height: 28px;
        line-height: 28px;
        background-color: #024886;
        margin: 0px;
        font-family:;
        font-size: px;
        font-weight: normal;
        font-style: normal;
        text-decoration: none;
        color: #fff;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        border-bottom: 0;
        background-image: url(images/bg.png);
        background-repeat: repeat;
        -webkit-border-top-left-radius: 0;
        -webkit-border-top-right-radius: 0;
        -moz-border-radius-topleft: 0;
        -moz-border-radius-topright: 0;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }

    .bitRightSider .bitWidgetFrame, .bitLeftSider .bitWidgetFrame {
        border-top: 0;
        border-bottom: 1px solid #bababa;
        border-left: 1px solid #bababa;
        border-right: 1px solid #bababa;
        padding: 4px 10px 4px 10px;
        -webkit-border-bottom-left-radius: 0;
        -webkit-border-bottom-right-radius: 0;
        -moz-border-radius-bottomleft: 0;
        -moz-border-radius-bottomright: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    .bitRightSider .site_tooler, .bitLeftSider .site_tooler {
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame, .bitRightLeftSiderWidget.site_tooler .bitWidgetFrame {
        background-color: transparent;
        background-image: none;
        -webkit-border-bottom-left-radius: 0px;
        border-bottom-left-radius: 0px;
        -webkit-border-bottom-right-radius: 0px;
        border-bottom-right-radius: 0px;
    }</style>
    <style class='style_0'>.footer .widget-title {
        padding: 0 0 0 10px;
        height: 28px;
        line-height: 28px;
        background-color: #024886;
        margin: 0px;
        font-family:;
        font-size: px;
        font-weight: normal;
        font-style: normal;
        text-decoration: none;
        color: #fff;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        border-bottom: 0;
        background-image: none;
        -webkit-border-top-left-radius: 4px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -moz-border-radius-topright: 4px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }

    .footer .widget-title {
        border-top: 0;
        border-left: 0;
        border-right: 0;
    }

    .footer .bitWidgetFrame {
        border-bottom: 0;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        padding: 4px 10px 4px 10px;
    }

    .footer .site_tooler {
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .footer .site_tooler .bitWidgetFrame {
        background-color: transparent;
        background-image: none;
        -webkit-border-bottom-left-radius: 4px;
        border-bottom-left-radius: 4px;
        -webkit-border-bottom-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }</style>
    <script type="text/javascript">
        var qfyuser_ajax_url = 'http://www.neverends.cn/admin/admin-ajax.php';
    </script>
    <script type="text/javascript">
        var qfyuser_upload_url = 'http://www.neverends.cn/qfy-content/plugins/qfyuser/lib/fileupload/fileupload.php';
    </script>
    <link rel="icon" href="http://www.neverends.cn/qfy-content/uploads/2017/11/3a1f4031747c19d0318da97e1ba753c1.jpg"
          type="image/jpg"/>
    <link rel="apple-touch-icon-precomposed"
          href="http://www.neverends.cn/qfy-content/uploads/2017/11/ad36ec2ac475c2e86e42fdfe9c24da3c.jpg"/>
    <link rel="apple-touch-icon"
          href="http://www.neverends.cn/qfy-content/uploads/2017/11/ad36ec2ac475c2e86e42fdfe9c24da3c.jpg"/>
    <meta name="robots" content="index, follow"/>
    <meta name="description" content="敏感，且富有创意"/>
    <meta name="keywords" content="无尽方式NEVERENDS"/>
    <style type="text/css">
    </style>
    <script type="text/javascript">
        dtGlobals.logoEnabled = 1;
        dtGlobals.curr_id = '13351';
        dtGlobals.logoURL = '';
        dtGlobals.logoW = '0';
        dtGlobals.logoH = '0';
        dtGlobals.qfyname = '起飞页';
        dtGlobals.id = '5a0e68a51e89c';
        dtGlobals.language = '';
        smartMenu = 0;
        document.cookie = 'resolution=' + Math.max(screen.width, screen.height) + '; path=/';
        dtGlobals.gallery_bgcolor = 'rgba(51,51,51,1)';
        dtGlobals.gallery_showthumbs = '0';
        dtGlobals.gallery_style = '';
        dtGlobals.gallery_autoplay = '0';
        dtGlobals.gallery_playspeed = '3';
        dtGlobals.gallery_imagesize = '100';
        dtGlobals.gallery_stopbutton = '';
        dtGlobals.gallery_thumbsposition = '';
        dtGlobals.gallery_tcolor = '#fff';
        dtGlobals.gallery_tsize = '16';
        dtGlobals.gallery_dcolor = '#fff';
        dtGlobals.gallery_dsize = '14';
        dtGlobals.gallery_tfamily = '';
        dtGlobals.gallery_dfamily = '';
        dtGlobals.gallery_blankclose = '0';
        dtGlobals.fm_showstyle = '';
        dtGlobals.fm_showspeed = '';
        dtGlobals.cdn_url = 'https://cdn.goodq.top';
        dtGlobals.qfymodel = "";
        var socail_back_url = '';

    </script>


</head>


<body class="page page-id-13351 page-template-default image-blur  mobilehide_menu  mobilehide_mobile_menu widefull_header2 widefull_footer2 btn-flat content-fullwidth qfe-js-composer js-comp-ver-4.0.1 vc_responsive"
      data-pid="13351" data-pkey="18f5ff452c8ecbeaacccee53cc9b452f">
<div id="page" class=' hidetopbar hideheader hidefooter hidebottombar wide '>


    <!-- left, center, classical, classic-centered -->
    <!-- !Header -->
    <header id="header" class="logo-left wf-mobile-hidden headerPM menuPosition transparent" role="banner">
        <!-- class="overlap"; class="logo-left", class="logo-center", class="logo-classic" -->
        <div class="wf-wrap">
            <div class="wf-table">


                <div id="branding" class="wf-td bit-logo-bar" style="vertical-align: bottom;">
                    <a class="bitem logo small" style="display: table-cell;" href="http://www.neverends.cn/"><span
                            class="logospan"><img class="preload-me"
                                                  src="{{asset('/home/picture/ahr0cdovl3d3dy5uzxzlcmvuzhmuy24vcwz5lwnvbnrlbnqvdxbsb2fkcy8ymde3lzexl2q3zdzlyjg3ndm1mwm5n2iyyte1yta0mde3odi4njvklnbuzw_p_p100_p_3d_p_p100_p_3d.png')}}"
                                                  width="845" height="122" alt="无尽方式NEVERENDS 视觉策划工作室"/></span></a>
                    <div class="bitem text" style="display: table-cell;vertical-align: middle;"><a
                            href='http://www.neverends.cn/' style='text-decoration: none;'>
                        <div id='bit-logoText' data-margin-left=0 style='position:relative;font-size:16px;  '>
                            <div class='logotext_outner'>
                                <div class='logotext_inner'>
                                    <div><span style="color:#FFFFFF;">&nbsp;</span><span style="font-family:黑体;"><span
                                            style="font-family:raleway;"></span></span></div>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <!-- <div id="site-title" class="assistive-text"></div>
	<div id="site-description" class="assistive-text"></div> -->
                </div>
                <div class="wf-mobile-visible wf-td assistive-info    " role="complementary">
                    <div class="top-bar-right right bit_widget_more" bitdatamarker="bitHeader-2"
                         bitdataaction="site_fix_container" bitdatacolor="white">
                    </div>
                </div>


                <!-- !- Navigation -->
                <nav style="vertical-align: bottom;" id="navigation" class="wf-td" bitDataAction="site_menu_container"
                     bitDataLocation="primary">
                    <ul id="main-nav" data-st="0" data-sp="0" data-fh="0" data-mw="0" data-lh="31"
                        class="mainmenu fancy-rollovers wf-mobile-hidden bit-menu-default underline-hover position-text-right"
                        data-bit-menu=underline-hover data-bit-float-menu=underline-hover>
                        <li class=" menu-item menu-item-type-post_type menu-item-object-page bit-menu-post-id-13599 menu-item-13688 first">
                            <a href="http://www.neverends.cn/"><span>HOME</span></a></li>
                    </ul>

                    <a data-padding='' data-top='8' data-right='8' rel="nofollow" id="mobile-menu" style="display:none;"
                       class="glyphicon glyphicon-icon-align-justify dropTopStyle positionFixed center">
                        <span class="menu-open  phone-text">联络</span>
                        <span class="menu-close">关闭</span>
                        <span class="menu-back">返回上一级</span>
                        <span class="wf-phone-visible">&nbsp;</span>
                    </a>

                </nav>
                <div style="display:none;" id="main-nav-slide">
                    <div class="main-nav-slide-inner" data-class="">
                        <div class="floatmenu-bar-right bit_widget_more" bitdatamarker="bitHeader-3"
                             bitdataaction="site_fix_container" bitdatacolor="white">
                        </div>
                    </div>
                </div>


            </div><!-- #branding -->
        </div><!-- .wf-wrap -->
    </header><!-- #masthead -->

    <section class="bitBanner" id="bitBanner" bitdatamarker="bitBanner" bitdataaction="site_fix_container">
    </section>
    <div id="main" class="bit_main_content">


        <div class="main-gradient"></div>

        <div class="wf-wrap">
            <div class="wf-container-main">


                <div id="content" class="content" role="main">


                    <div class="main-outer-wrapper ">
                        <div class="bit_row">

                            <div class="twelve columns no-sidebar-content ">

                                <div class="bit_row">

                                    <div class="content-wrapper twelve columns ">
                                        <!--顶部图片-->
                                        <section data-fixheight=""
                                                 class="qfy-row-1-5aec70151bd2d503883 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal"
                                                 id="bit_rwkws"
                                                 style='margin-bottom:0;border-radius:0px;color:transparent;'>
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-1-5aec70151bd2d503883 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 20vh;
                                                        padding-bottom: 20vh;
                                                        margin-top: 0;
                                                        min-height: 480.8px;
                                                    }

                                                    section.section.qfy-row-1-5aec70151bd2d503883 > .container {
                                                        max-width: 1024px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-1-5aec70151bd2d503883 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 20vh;
                                                        padding-bottom: 20vh;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div data-time="3" data-imagebgs="" class="background-media   "
                                                 backgroundSize='true'
                                                 style="background-image: url('/uploads/{{$basic->file}}'); background-repeat:no-repeat; background-size:cover; background-position:center center;">
                                            </div>

                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: rgba(0,0,0,0.4);"></div>

                                            <div class="container" style="top:-80px">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="" data-delay=""
                                                         class=" qfy-column-1-5aec70151cf23844572 qfy-column-inner  vc_span12  text-Default small-screen-undefined fullrow"
                                                         data-dw="1/1" data-fixheight="">
                                                        <div style="margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:transparent;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_column_text_n8nrq-c-w3"
                                                                     data-anitime='2'
                                                                     class="qfy-element qfy-text qfy-text-45760 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_left-to-right "
                                                                     style="position: relative;line-height:1.5em;background-position:left top;background-repeat: no-repeat;margin-top:30px;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="position: relative;top: 32px;">
                                                                            <div style="text-align: center;"><span
                                                                                        style="color:#ff6d62;"><span
                                                                                            style="font-size: 48px;color: #ffffff;opacity: 0.3"><span
                                                                                                style="font-family: aviano sans;">{{$basic->title}}</span></span></span><br/>
                                                                                &nbsp;
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_column_text_637v2-c-w3"
                                                                     data-anitime='2'
                                                                     class="qfy-element qfy-text qfy-text-73424 qfe_text_column qfe_content_element  qfe_animate_when_almost_visible qfe_right-to-left "
                                                                     style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                                    <div class="qfe_wrapper">
                                                                        <div style="position: relative;">
                                                                            <div style="text-align: center;">
                                                                                <strong>
                                                                                    <span style="font-family:华文仿宋;">
                                                                                        <span style="letter-spacing: 10px;">
                                                                                            <span style="font-size: 36px;">
                                                                                                <div style="border-bottom:2px white solid;width: 3em;opacity: 0.3;position: absolute;left: 250px;top:14px"></div>
                                                                                                <span style="color: rgb(255, 255, 255);">
                                                                                                    <a  style="color: rgb(255, 255, 255);" href="">
                                                                                                        @if($lang == 'en')
                                                                                                            {{$basic->name_en}}
                                                                                                        @else
                                                                                                            {{$basic->name}}
                                                                                                        @endif
                                                                                                    </a>
                                                                                                </span>
                                                                                                <div style="border-bottom:2px white solid;width: 3em;opacity: 0.3;position: absolute;right: 250px;top:14px"></div>
                                                                                            </span>
                                                                                        </span>
                                                                                    </span>
                                                                                </strong>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <!--<div id="qfy-btn-5aec70151df5f2468"
                                                                     style="margin-top:0;margin-bottom:0;padding-top:54px;padding-bottom:0;padding-right:0;padding-left:0;"
                                                                     m-padding="54px 0px 0px 0px" p-padding="54px 0 0 0"
                                                                     css_animation_delay="0"
                                                                     qfyuuid="qfy_btn_ywgbh-c-w3" data-anitime='2'
                                                                     class="qfy-element vc_btn3-container  qfe_animate_when_almost_visible qfe_bottom-to-top vc_btn3-center">
                                                                    <a style="font-family:Aviano Sans; font-size:20px;"
                                                                       class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-square vc_btn3-style-modern vc_btn3-color-danger"
                                                                       href="http://www.neverends.cn/?page_id=13280"
                                                                       target="">Read More</a></div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                            .qfy-column-1-5aec70151cf23844572 > .column_inner {
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 141px;
                                                                padding-bottom: 0;
                                                            }

                                                            .qfe_row .vc_span_class.qfy-column-1-5aec70151cf23844572 {
                                                            }

                                                        ;
                                                        }

                                                        @media only screen and (max-width: 992px) {
                                                            .qfy-column-1-5aec70151cf23844572 > .column_inner {
                                                                margin: 0 auto 0 !important;
                                                                min-height: 0 !important;
                                                                padding-left: 0;
                                                                padding-right: 0;
                                                                padding-top: 0;
                                                                padding-bottom: 0;
                                                            }

                                                            .display_entire .qfe_row .vc_span_class.qfy-column-1-5aec70151cf23844572 {
                                                            }

                                                            .qfy-column-1-5aec70151cf23844572 > .column_inner > .background-overlay, .qfy-column-1-5aec70151cf23844572 > .column_inner > .background-media {
                                                                width: 100% !important;
                                                                left: 0 !important;
                                                                right: auto !important;
                                                            }
                                                        }</style>
                                                </div>
                                            </div>

                                        </section>
                                        {{--<div class="header">--}}
                                            {{--<img src="{{asset('/home/images/5.png')}}" alt="" style="width: auto;--}}
                                                {{--height: auto;--}}
                                                {{--max-width: 100%;--}}
                                                {{--max-height: 100%;">--}}
                                        {{--</div>--}}
                                        <!--百度地图-->
                                        <section data-fixheight=""
                                                 class="qfy-row-8-5aec7054d09a2191401 section     bothfull  section-text-no-shadow section-inner-no-shadow section-normal"
                                                 id="bit_ix813"
                                                 style='margin-bottom:0;border-radius:0px;color:#333333;'>
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-8-5aec7054d09a2191401 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        /*padding-top: 20px;*/
                                                        padding-bottom: 0;
                                                        margin-top: -10px;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-8-5aec7054d09a2191401 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 20px;
                                                        padding-bottom: 0;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                ></div>
                                            <!--百度地图-->
                                            <div class="container">
                                                <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                     style="background:rgba(0,0,0,0.1)"
                                                ></div>
                                                <div id="allmap"></div>
                                            </div>

                                        </section>
                                        <!--表单-->
                                        <section data-fixheight=""
                                                 class="qfy-row-9-5aec7054d1473103794 section   contentsPadding0  smfull  section-text-no-shadow section-inner-no-shadow section-normal"
                                                 id="bit_xg7sg"
                                                 style='margin-bottom:0;border-radius:0px;color:#000000;z-index:10;'>
                                            <style class="row_class qfy_style_class">
                                                @media only screen and (min-width: 992px) {
                                                    section.section.qfy-row-9-5aec7054d1473103794 {
                                                        padding-left: 0;
                                                        padding-right: 0;
                                                        padding-top: 20px;
                                                        padding-bottom: 1px;
                                                        margin-top: -100px;
                                                    }

                                                    section.section.qfy-row-9-5aec7054d1473103794 > .container {
                                                        max-width: 1124px;
                                                        margin: 0 auto;
                                                    }
                                                }

                                                @media only screen and (max-width: 992px) {
                                                    .bit-html section.section.qfy-row-9-5aec7054d1473103794 {
                                                        padding-left: 15px;
                                                        padding-right: 15px;
                                                        padding-top: 0;
                                                        padding-bottom: 1px;
                                                        margin-top: 0;
                                                        min-height: 0;
                                                    }
                                                }
                                            </style>
                                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                 style="background-color: transparent;"></div>

                                            <div class="container">
                                                <div class="row qfe_row">
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="" data-delay=""
                                                         class=" qfy-column-13-5aec7054d22ab485816 qfy-column-inner  vc_span_class  vc_span6  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/2" data-fixheight="">
                                                        <div style="min-height:520px;margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:#f4f4f4;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <section data-fixheight=""
                                                                         class="qfy-row-10-5aec7054d25d3961316 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
                                                                         style='margin-bottom:0;border-radius:0px;color:#333333;'>
                                                                    <style class="row_class qfy_style_class">
                                                                        @media only screen and (min-width: 992px) {
                                                                            section.section.qfy-row-10-5aec7054d25d3961316 {
                                                                                padding-left: 0;
                                                                                padding-right: 0;
                                                                                padding-top: 0;
                                                                                padding-bottom: 0;
                                                                                margin-top: 0;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 992px) {
                                                                            .bit-html section.section.qfy-row-10-5aec7054d25d3961316 {
                                                                                padding-left: 15px;
                                                                                padding-right: 15px;
                                                                                padding-top: 0;
                                                                                padding-bottom: 0;
                                                                                margin-top: 0;
                                                                                min-height: 0;
                                                                            }
                                                                        }
                                                                    </style>
                                                                    <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                                         style="background-color: transparent;"></div>

                                                                    <div class="container">
                                                                        <div class="row qfe_row">
                                                                            <div data-animaleinbegin="90%"
                                                                                 data-animalename="qfyfadeInUp"
                                                                                 data-duration="" data-delay=""
                                                                                 class=" qfy-column-14-5aec7054d2820285325 qfy-column-inner  vc_span12  text-default small-screen-default fullrow"
                                                                                 data-dw="1/1" data-fixheight="">
                                                                                <div style=";position:relative;"
                                                                                     class="column_inner ">
                                                                                    <div class=" background-overlay grid-overlay-"
                                                                                         style="background-color:transparent;width:100%;"></div>
                                                                                    <div class="column_containter"
                                                                                         style="z-index:3;position:relative;">
                                                                                        <div id="vc_header_5aec7054d41ef903"
                                                                                             m-padding="40px 0px 0px 0px"
                                                                                             p-padding="40px 0 0 0"
                                                                                             css_animation_delay="0"
                                                                                             qfyuuid="qfy_header_zdg8s-c-p3"
                                                                                             data-anitime='0.7'
                                                                                             class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                                             style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:20px;padding-top:40px;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                                            <style>@media only screen and (max-width: 760px) {
                                                                                                #vc_header_5aec7054d41ef903 .header_title {
                                                                                                    font-size: 14px !important;
                                                                                                }
                                                                                            }

                                                                                            @media only screen and (max-width: 760px) {
                                                                                                #vc_header_5aec7054d41ef903 .header_subtitle {
                                                                                                    font-size: 12px !important;
                                                                                                }
                                                                                            }</style>
                                                                                            <div class="qfe_wrapper">
                                                                                                <div class="qfy_title left ">
                                                                                                    <div class="qfy_title_image_warpper"
                                                                                                         style="display:inline-block;position:relative;max-width:100%;">
                                                                                                        <div class="qfy_title_inner"
                                                                                                             style="display:inline-block;position:relative;max-width:100%;">
                                                                                                            <div class="header_title"
                                                                                                                 style="font-family:微软雅黑;font-size:16px;font-weight:normal;font-style:normal;color:#333333;padding:0 10px 0 0;display:block;padding:0 10px 0 0;vertical-align:bottom;">
                                                                                                                    {{$info->name}}
                                                                                                            </div>
                                                                                                            <div class="header_subtitle"
                                                                                                                 style="font-family:century gothic;font-size:14px;font-weight:normal;font-style:normal;color:#b5b5b5;display:block;padding:2px;vertical-align:bottom;">
                                                                                                                {{$info->name_en}}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <style></style>
                                                                                                        <div class="button_wrapper"
                                                                                                             style="display:none;">
                                                                                                            <div class="button "
                                                                                                                 style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <span style="clear:both;"></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div m-padding="20px 0px 20px 0px"
                                                                                             p-padding="20px 0 20px 0"
                                                                                             css_animation_delay="0"
                                                                                             qfyuuid="qfy_column_text_g3wr6-c-p3"
                                                                                             data-anitime='0.7'
                                                                                             class="qfy-element qfy-text qfy-text-27040 qfe_text_column qfe_content_element  "
                                                                                             style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:20px;padding-bottom:20px;padding-right:0;padding-left:0;border-radius:0px;">
                                                                                            <div class="qfe_wrapper">
                                                                                                <div style="position: relative;">
                                                                                                    <div><span
                                                                                                            style="font-family:宋体;"><span
                                                                                                            style="color: rgb(105, 105, 105);"><span
                                                                                                            style="letter-spacing: 1px;"><span
                                                                                                            style="font-size: 13px;">
                                                                                                                        @if($lang == 'en')
                                                                                                                            Address
                                                                                                                        @else
                                                                                                                            地址
                                                                                                                        @endif：<span
                                                                                                            style="line-height: 24px;"></span>{{$info->address}}</span></span><br/>
<span style="letter-spacing: 1px;"></span><span style="letter-spacing: 1px;"></span><span style="letter-spacing: 1px;"></span><span
                                                                                                                style="letter-spacing: 1px;"><span
                                                                                                                style="font-size: 13px;">
                                                                                                                        @if($lang == 'en')
                                                                                                                            TEL
                                                                                                                        @else
                                                                                                                            电话
                                                                                                                        @endif：{{$info->tel}}</span></span></span></span>
                                                                                                    </div>

                                                                                                    <div><span
                                                                                                            style="word-spacing: normal; background-color: transparent; color: rgb(105, 105, 105); font-family: 微软雅黑; font-size: 13px; letter-spacing: 1px;"><span
                                                                                                            style="font-family:宋体;">
                                                                                                                @if($lang == 'en')
                                                                                                                    Phone
                                                                                                                @else
                                                                                                                    手机
                                                                                                                @endif：{{$info->phone}} &nbsp;</span> &nbsp;</span>
                                                                                                    </div>

                                                                                                    <div>
                                                                                                        <div style="color: rgb(0, 0, 0);">
                                                                                                            <span style="font-family: 宋体;"><span
                                                                                                                    style="word-spacing: normal; background-color: transparent; font-size: 13px; letter-spacing: 1px; color: rgb(105, 105, 105);">
                                                                                                                    @if($lang == 'en')
                                                                                                                        Fax
                                                                                                                    @else
                                                                                                                        传真
                                                                                                                    @endif：{{$info->fax}} &nbsp; &nbsp;</span></span>
                                                                                                        </div>

                                                                                                        <div style="color: rgb(0, 0, 0);">
                                                                                                            <span style="font-family: 宋体;"><span
                                                                                                                    style="word-spacing: normal; background-color: transparent; font-size: 13px; letter-spacing: 1px; color: rgb(105, 105, 105);"></span><span
                                                                                                                    style="word-spacing: normal; background-color: transparent; font-size: 13px; letter-spacing: 1px; color: rgb(105, 105, 105);">
                                                                                                                    @if($lang == 'en')
                                                                                                                        Email
                                                                                                                    @else
                                                                                                                        邮箱
                                                                                                                    @endif：{{$info->email}}</span></span>
                                                                                                        </div>

                                                                                                        <div style="color: rgb(0, 0, 0);">
                                                                                                            <span style="font-family: 宋体;"><span
                                                                                                                    style="color: rgb(105, 105, 105); font-size: 13px; letter-spacing: 1px;">
                                                                                                                    @if($lang == 'en')
                                                                                                                        Zip Code
                                                                                                                    @else
                                                                                                                        邮编
                                                                                                                    @endif：{{$info->code}}</span></span>
                                                                                                        </div>
                                                                                                        <div  class="col-lg-4 col-md-4"  style="max-width: 286px;float: left;">
                                                                                                            <ul class="qr_code">
                                                                                                                <li style="display: inline-block">
                                                                                                                    <img src="/uploads/{{$info->app_code}}" width="110px" height="110px" alt="">
                                                                                                                    <div class="span" style="text-align: center;font-family: 宋体;font-size: 13px;">
                                                                                                                        @if($lang == 'en')
                                                                                                                            App
                                                                                                                        @else
                                                                                                                            App下载
                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                </li>
                                                                                                                <li style="display: inline-block;margin-left: 35px">
                                                                                                                    <img src="/uploads/{{$info->wechat_code}}" width="110px" height="110px" alt="">
                                                                                                                    <div class="span" style="text-align: center;font-family: 宋体;font-size: 13px;">
                                                                                                                        @if($lang == 'en')
                                                                                                                            WECHAT
                                                                                                                        @else
                                                                                                                            微信公众号
                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                .qfy-column-14-5aec7054d2820285325 > .column_inner {
                                                                                    padding-left: 0;
                                                                                    padding-right: 0;
                                                                                    padding-top: 0;
                                                                                    padding-bottom: 0;
                                                                                }

                                                                                .qfe_row .vc_span_class.qfy-column-14-5aec7054d2820285325 {
                                                                                }

                                                                            ;
                                                                            }

                                                                            @media only screen and (max-width: 992px) {
                                                                                .qfy-column-14-5aec7054d2820285325 > .column_inner {
                                                                                    margin: 0 auto 0 !important;
                                                                                    padding-left: 0;
                                                                                    padding-right: 0;
                                                                                    padding-top:;
                                                                                    padding-bottom:;
                                                                                }

                                                                                .display_entire .qfe_row .vc_span_class.qfy-column-14-5aec7054d2820285325 {
                                                                                }

                                                                                .qfy-column-14-5aec7054d2820285325 > .column_inner > .background-overlay, .qfy-column-14-5aec7054d2820285325 > .column_inner > .background-media {
                                                                                    width: 100% !important;
                                                                                    left: 0 !important;
                                                                                    right: auto !important;
                                                                                }
                                                                            }</style>
                                                                        </div>
                                                                    </div>

                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                        .qfy-column-13-5aec7054d22ab485816 > .column_inner {
                                                            padding-left: 70px;
                                                            padding-right: 70px;
                                                            padding-top: 50px;
                                                            padding-bottom: 50px;
                                                        }

                                                        .qfe_row .vc_span_class.qfy-column-13-5aec7054d22ab485816 {
                                                        }

                                                    ;
                                                    }

                                                    @media only screen and (max-width: 992px) {
                                                        .qfy-column-13-5aec7054d22ab485816 > .column_inner {
                                                            margin: 0 auto 0 !important;
                                                            min-height: 0 !important;
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top: 0;
                                                            padding-bottom: 0;
                                                        }

                                                        .display_entire .qfe_row .vc_span_class.qfy-column-13-5aec7054d22ab485816 {
                                                        }

                                                        .qfy-column-13-5aec7054d22ab485816 > .column_inner > .background-overlay, .qfy-column-13-5aec7054d22ab485816 > .column_inner > .background-media {
                                                            width: 100% !important;
                                                            left: 0 !important;
                                                            right: auto !important;
                                                        }
                                                    }</style>
                                                    <div data-animaleinbegin="90%" data-animalename="qfyfadeInUp"
                                                         data-duration="" data-delay=""
                                                         class=" qfy-column-15-5aec7054d4b65886599 qfy-column-inner  vc_span_class  vc_span6  text-Default small-screen-undefined notfullrow"
                                                         data-dw="1/2" data-fixheight="">
                                                        <div style="min-height:520px;margin-top:0;margin-bottom:0;margin-left:0;margin-right:0;border-radius:0px;;position:relative;"
                                                             class="column_inner ">
                                                            <div class=" background-overlay grid-overlay-0"
                                                                 style="background-color:#333333;width:100%;"></div>
                                                            <div class="column_containter"
                                                                 style="z-index:3;position:relative;">
                                                                <section data-fixheight=""
                                                                         class="qfy-row-11-5aec7054d512910477 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
                                                                         style='margin-bottom:0;border-radius:0px;color:#e8e8e8;'>
                                                                    <style class="row_class qfy_style_class">
                                                                        @media only screen and (min-width: 992px) {
                                                                            section.section.qfy-row-11-5aec7054d512910477 {
                                                                                padding-left: 0;
                                                                                padding-right: 0;
                                                                                padding-top: 0;
                                                                                padding-bottom: 0;
                                                                                margin-top: 0;
                                                                            }
                                                                        }

                                                                        @media only screen and (max-width: 992px) {
                                                                            .bit-html section.section.qfy-row-11-5aec7054d512910477 {
                                                                                padding-left: 15px;
                                                                                padding-right: 15px;
                                                                                padding-top: 0;
                                                                                padding-bottom: 0;
                                                                                margin-top: 0;
                                                                            }
                                                                        }
                                                                    </style>
                                                                    <div class="section-background-overlay background-overlay grid-overlay-0 "
                                                                         style="background-color: #333333;"></div>

                                                                    <div class="container">
                                                                        <div class="row qfe_row">
                                                                            <div data-animaleinbegin="90%"
                                                                                 data-animalename="qfyfadeInUp"
                                                                                 data-duration="" data-delay=""
                                                                                 class=" qfy-column-16-5aec7054d55b8473399 qfy-column-inner  vc_span12  text-default small-screen-default fullrow"
                                                                                 data-dw="1/1" data-fixheight="">
                                                                                <div style=";position:relative;"
                                                                                     class="column_inner ">
                                                                                    <div class=" background-overlay grid-overlay-"
                                                                                         style="background-color:transparent;width:100%;"></div>
                                                                                    <div class="column_containter"
                                                                                         style="z-index:3;position:relative;">
                                                                                        <div id="vc_header_5aec7054d5b1f898"
                                                                                             m-padding="0 0px 0px 0px"
                                                                                             p-padding="40px 0 0 0"
                                                                                             css_animation_delay="0"
                                                                                             qfyuuid="qfy_header_k53gl-c-p3"
                                                                                             data-anitime='0.7'
                                                                                             class="qfy-element minheigh1px qfe_text_column qfe_content_element "
                                                                                             style="position: relative;;;background-repeat: no-repeat;;margin-top:0;margin-bottom:20px;padding-top:40px;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                                            <style>@media only screen and (max-width: 760px) {
                                                                                                #vc_header_5aec7054d5b1f898 .header_title {
                                                                                                    font-size: 14px !important;
                                                                                                }
                                                                                            }

                                                                                            @media only screen and (max-width: 760px) {
                                                                                                #vc_header_5aec7054d5b1f898 .header_subtitle {
                                                                                                    font-size: 12px !important;
                                                                                                }
                                                                                            }</style>
                                                                                            <div class="qfe_wrapper">
                                                                                                <div class="qfy_title left ">
                                                                                                    <div class="qfy_title_image_warpper"
                                                                                                         style="display:inline-block;position:relative;max-width:100%;">
                                                                                                        <div class="qfy_title_inner"
                                                                                                             style="display:inline-block;position:relative;max-width:100%;">
                                                                                                            <div class="header_title"
                                                                                                                 style="font-family:微软雅黑;font-size:16px;font-weight:normal;font-style:normal;color:#ffffff;padding:0 10px 0 0;display:block;padding:0 10px 0 0;vertical-align:bottom;">
                                                                                                                在线表单
                                                                                                            </div>
                                                                                                            <div class="header_subtitle"
                                                                                                                 style="font-family:century gothic;font-size:14px;font-weight:normal;font-style:normal;color:#8e8e8e;display:block;padding:2px;vertical-align:bottom;">
                                                                                                                ONLINE
                                                                                                                FORM
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <style></style>
                                                                                                        <div class="button_wrapper"
                                                                                                             style="display:none;">
                                                                                                            <div class="button "
                                                                                                                 style="display:inline-block;max-width:100%;text-align:center; font-size:14px; color:#333; padding:5px;">
                                                                                                                + 查看更多
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <span style="clear:both;"></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div data-tag="iphorm_vc"
                                                                                             data-id="70"
                                                                                             m-padding="0px 0px 0px 0px"
                                                                                             p-padding="0 0 0 0"
                                                                                             css_animation_delay="0"
                                                                                             qfyuuid="iphorm_vc_dxgy2-c-p3"
                                                                                             class="qfy-element qfe_layerslider_element qfe_content_element"
                                                                                             style="margin-top:40px;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                                                                                            <div id="iphorm-outer-5aec7054d77fe"
                                                                                                 class="bitWidgetFrame iphorm-outer iphorm-outer-70 iphorm-uniform-theme-default">
                                                                                               {{-- <form action="/submit"
                                                                                                      method="post"
                                                                                                      enctype="multipart/form-data">--}}
                                                                                                    <div class="iphorm-inner iphorm-inner-70">
                                                                                                        {{--<input type="hidden"
                                                                                                               name="iphorm_id"
                                                                                                               value="70"/>
                                                                                                        <input type="hidden"
                                                                                                               name="iphorm_uid"
                                                                                                               value="5aec7054d77fe"/>
                                                                                                        <input type="hidden"
                                                                                                               name="form_url"
                                                                                                               value="http://www.neverends.cn/?page_id=13351"/>
                                                                                                        <input type="hidden"
                                                                                                               name="referring_url"
                                                                                                               value="http://www.neverends.cn/?page_id=13280"/>
                                                                                                        <input type="hidden"
                                                                                                               name="post_id"
                                                                                                               value="13351"/>
                                                                                                        <input type="hidden"
                                                                                                               name="post_title"
                                                                                                               value="联络"/>--}}

                                                                                                        <div class="iphorm-success-message iphorm-hidden"></div>
                                                                                                        <div class="iphorm-error-message iphorm-hidden"></div>
                                                                                                        {{--<input type="hidden"--}}
                                                                                                               {{--class="popaction"--}}
                                                                                                               {{--value=""/>--}}
                                                                                                        {{ csrf_field() }}

                                                                                                        <input type="hidden" name="lang" value="{{$lang}}">
                                                                                                        <div class="iphorm-elements iphorm-elements-70 iphorm-clearfix">
                                                                                                            <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_70_1-element-wrap iphorm-clearfix iphorm-labels-left iphorm-element-required">
                                                                                                                <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_70_1-element-spacer">
                                                                                                                    <label for="iphorm_70_1_5aec7054d77fe"
                                                                                                                           style='color: rgb(184, 184, 184);width: 80px;font-size: 14px;line-height:14px;'>
                                                                                                                        @if($lang == 'en')
                                                                                                                            NAME
                                                                                                                        @else
                                                                                                                            姓名
                                                                                                                        @endif
                                                                                                                        <span class="iphorm-required"
                                                                                                                              style='color: #b8b8b8;font-size: 10px;;'>*</span>
                                                                                                                    </label>
                                                                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_70_1-input-wrap"
                                                                                                                         style='margin-left: 80px;'>
                                                                                                                        <input class="iphorm-element-text  iphorm_70_1"
                                                                                                                               id="submit_name"
                                                                                                                               type="text"
                                                                                                                               name="submit_name"
                                                                                                                               value=""
                                                                                                                               placeholder=""
                                                                                                                               style='background-color: rgb(255, 255, 255);border: 1px solid rgb(255, 255, 255);color: rgb(102, 102, 102);height: 24px;font-size: 14px;'/>
                                                                                                                    </div>
                                                                                                                    <div class="iphorm-errors-wrap iphorm-hidden showName" style="margin-left: 80px; display: none;">
                                                                                                                        <div class="iphorm-errors-list iphorm-clearfix">
                                                                                                                            <div class="iphorm-error">
                                                                                                                                <i class="fa fa-exclamation-triangle" aria-hidden="true" style="padding-right:5px;"></i>这是一个必填项哦
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_70_3-element-wrap iphorm-clearfix iphorm-labels-left iphorm-element-required">
                                                                                                                <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_70_3-element-spacer">
                                                                                                                    <label for="iphorm_70_3_5aec7054d77fe"
                                                                                                                           style='color: rgb(184, 184, 184);width: 80px;font-size: 14px;line-height:14px;'>
                                                                                                                        @if($lang == 'en')
                                                                                                                            Email
                                                                                                                        @else
                                                                                                                            邮箱
                                                                                                                        @endif
                                                                                                                        <span class="iphorm-required"
                                                                                                                              style='color: #b8b8b8;font-size: 10px;;'>*</span>
                                                                                                                    </label>
                                                                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_70_3-input-wrap"
                                                                                                                         style='margin-left: 80px;'>
                                                                                                                        <input class="iphorm-element-text  iphorm_70_3"
                                                                                                                               id="submit_email"
                                                                                                                               type="text"
                                                                                                                               name="submit_email"
                                                                                                                               value=""
                                                                                                                               placeholder=""
                                                                                                                               style='background-color: rgb(255, 255, 255);border: 1px solid rgb(255, 255, 255);color: rgb(102, 102, 102);height: 24px;font-size: 14px;'/>
                                                                                                                    </div>
                                                                                                                    <div class="iphorm-errors-wrap iphorm-hidden showEmail" style="margin-left: 80px; display: none;">
                                                                                                                        <div class="iphorm-errors-list iphorm-clearfix">
                                                                                                                            <div class="iphorm-error">
                                                                                                                                <i class="fa fa-exclamation-triangle" aria-hidden="true" style="padding-right:5px;">这是一个必填项哦</i>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="iphorm-element-wrap iphorm-element-wrap-text iphorm_70_2-element-wrap iphorm-clearfix iphorm-labels-left iphorm-element-optional">
                                                                                                                <div class="iphorm-element-spacer iphorm-element-spacer-text iphorm_70_2-element-spacer">
                                                                                                                    <label for="iphorm_70_2_5aec7054d77fe"
                                                                                                                           style='color: rgb(184, 184, 184);width: 80px;font-size: 14px;line-height:14px;'>
                                                                                                                        @if($lang == 'en')
                                                                                                                            TEL
                                                                                                                        @else
                                                                                                                            联系电话
                                                                                                                        @endif
                                                                                                                            <span class="iphorm-required"
                                                                                                                                  style='color: #b8b8b8;font-size: 10px;;'>*</span>
                                                                                                                    </label>
                                                                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-text iphorm_70_2-input-wrap"
                                                                                                                         style='margin-left: 80px;'>
                                                                                                                        <input class="iphorm-element-text  iphorm_70_2"
                                                                                                                               id="submit_tel"
                                                                                                                               type="text"
                                                                                                                               name="submit_tel"
                                                                                                                               value=""
                                                                                                                               placeholder=""
                                                                                                                               style='background-color: rgb(255, 255, 255);border: 1px solid rgb(255, 255, 255);color: rgb(102, 102, 102);height: 24px;font-size: 14px;'/>
                                                                                                                    </div>
                                                                                                                    <div class="iphorm-errors-wrap iphorm-hidden showTel" style="margin-left: 80px; display: none;">
                                                                                                                        <div class="iphorm-errors-list iphorm-clearfix">
                                                                                                                            <div class="iphorm-error">
                                                                                                                                <i class="fa fa-exclamation-triangle" aria-hidden="true" style="padding-right:5px;"></i>这是一个必填项哦
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="iphorm-element-wrap iphorm-element-wrap-textarea iphorm_70_4-element-wrap iphorm-clearfix iphorm-labels-left iphorm-element-required">

                                                                                                                <div class="iphorm-element-spacer iphorm-element-spacer-textarea iphorm_70_4-element-spacer">
                                                                                                                    <label for="iphorm_70_4_5aec7054d77fe"
                                                                                                                           style='color: rgb(184, 184, 184);width: 80px;font-size: 14px;line-height:14px;'>
                                                                                                                        @if($lang == 'en')
                                                                                                                            Message
                                                                                                                        @else
                                                                                                                            内容
                                                                                                                        @endif

                                                                                                                    </label>
                                                                                                                    <div class="iphorm-input-wrap iphorm-input-wrap-textarea iphorm_70_4-input-wrap"
                                                                                                                         style='margin-left: 80px;'>
                                                                                                                        <textarea
                                                                                                                                placeholder=""
                                                                                                                                class="iphorm-element-textarea  iphorm_70_4"
                                                                                                                                id="submit_content"
                                                                                                                                name="submit_content"
                                                                                                                                style='background-color: rgb(255, 255, 255);border: 1px solid rgb(255, 255, 255);color: rgb(102, 102, 102);font-size: 14px;'
                                                                                                                                rows="3"
                                                                                                                                cols="25"></textarea>
                                                                                                                    </div>
                                                                                                                    <div class="iphorm-errors-wrap iphorm-hidden"
                                                                                                                         style='margin-left: 80px;'>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="iphorm-hidden">
                                                                                                                <label>这个输入框是留空的<input
                                                                                                                        type="text"/></label>
                                                                                                            </div>
                                                                                                            <div class="iphorm-submit-wrap iphorm-submit-wrap-70 iphorm-clearfix">

                                                                                                                <div class="iphorm-submit-input-wrap iphorm-submit-input-wrap-70">

                                                                                                                    <button class="iphorm-submit-element"
                                                                                                                            id="submitInfo"
                                                                                                                            type="submit"
                                                                                                                            style='font-size: 14px;line-height:14px;;'>
                                                                                                                        <span style='font-size: 14px;line-height:14px;;padding-left: 20px !important;;background-color: #f1f4f5;border: 1px solid #f1f4f5;'><em
                                                                                                                                style='font-size: 14px;line-height:14px;padding:6px 20px 6px 0;;background-color: #f1f4f5;'>发送</em></span>
                                                                                                                    </button>

                                                                                                                    <div style="display:inline-block;">
                                                                                                                        <div class="iphorm-loading-wrap">
                                                                                                                            <span class="iphorm-loading">请稍候</span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>


                                                                                                            </div>
                                                                                                            <style>.iphorm-elements-70, .iphorm-elements-70 button, .iphorm-elements-70 input, .iphorm-elements-70 select, .iphorm-elements-70 textarea {
                                                                                                                font-family: 微软雅黑;
                                                                                                                font-style: normal
                                                                                                            }

                                                                                                            .iphorm-submit-wrap-70 button.iphorm-submit-element:hover span, .iphorm-submit-wrap-70 button.iphorm-submit-element:hover em {
                                                                                                                background-color: #f1f4f5 !important;
                                                                                                            }

                                                                                                            .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-text input, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-captcha input, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-password input, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap select, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap textarea {
                                                                                                                width: 100%;
                                                                                                                max-width: 100% !important;
                                                                                                            }

                                                                                                            .iphorm-elements.iphorm-elements-70 .iphorm-group-wrap, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-text, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-captcha, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-password, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap {
                                                                                                                width: 100% !important;
                                                                                                                max-width: 100% !important;
                                                                                                            }

                                                                                                            .iphorm-submit-wrap-70 .iphorm-submit-input-wrap {
                                                                                                                width: 100% !important;
                                                                                                                max-width: 100% !important;
                                                                                                            }

                                                                                                            .iphorm-submit-wrap-70 .iphorm-submit-input-wrap button {
                                                                                                                margin: 0;
                                                                                                            }

                                                                                                            .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-text input, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-captcha input, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap-password input, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap select, .iphorm-elements.iphorm-elements-70 .iphorm-element-wrap textarea {
                                                                                                                border-radius: 0px;
                                                                                                            }

                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-text input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-captcha input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-password input,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap-select select,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap select, .iphorm-elements .iphorm-element-wrap textarea {
                                                                                                                box-shadow: none !important;
                                                                                                            }

                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-text input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-captcha input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-password input,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap-select select,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap select,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap-select select,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap textarea {
                                                                                                                background-color: rgba(255, 255, 255, 0) !important;
                                                                                                            }

                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-text input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-captcha input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-password input,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap-select select,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap select,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap-select select,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap textarea,
                                                                                                            .iphorm-elements-70 .iphorm-input-li > label,
                                                                                                            .iphorm-elements-70 .iphorm-datepicker-icon,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap > .iphorm-element-file-inner > div.uploader > span {
                                                                                                                color: #666666;
                                                                                                            }

                                                                                                            .iphorm-elements-70 .iphorm-input-li > label {
                                                                                                                font-size: 14px;
                                                                                                            }

                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-text input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-captcha input,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap-password input,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap-select select,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap select,
                                                                                                            .iphorm-elements-70 .iphorm-input-wrap-select select,
                                                                                                            .iphorm-elements-70 .iphorm-element-wrap textarea {
                                                                                                                border-width: 1px !important;
                                                                                                                border-color: #ffffff !important;

                                                                                                            }</style>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                {{--</form>--}}
                                                                                                <script type="text/javascript">
                                                                                                    <!--
                                                                                                    jQuery('#iphorm-outer-5aec7054d77fe script').remove();
                                                                                                    //-->
                                                                                                </script>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                                                .qfy-column-16-5aec7054d55b8473399 > .column_inner {
                                                                                    padding-left: 0;
                                                                                    padding-right: 0;
                                                                                    padding-top: 0;
                                                                                    padding-bottom: 0;
                                                                                }

                                                                                .qfe_row .vc_span_class.qfy-column-16-5aec7054d55b8473399 {
                                                                                }

                                                                            ;
                                                                            }

                                                                            @media only screen and (max-width: 992px) {
                                                                                .qfy-column-16-5aec7054d55b8473399 > .column_inner {
                                                                                    margin: 0 auto 0 !important;
                                                                                    padding-left: 0;
                                                                                    padding-right: 0;
                                                                                    padding-top:;
                                                                                    padding-bottom:;
                                                                                }

                                                                                .display_entire .qfe_row .vc_span_class.qfy-column-16-5aec7054d55b8473399 {
                                                                                }

                                                                                .qfy-column-16-5aec7054d55b8473399 > .column_inner > .background-overlay, .qfy-column-16-5aec7054d55b8473399 > .column_inner > .background-media {
                                                                                    width: 100% !important;
                                                                                    left: 0 !important;
                                                                                    right: auto !important;
                                                                                }
                                                                            }</style>
                                                                        </div>
                                                                    </div>

                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                                        .qfy-column-15-5aec7054d4b65886599 > .column_inner {
                                                            padding-left: 70px;
                                                            padding-right: 70px;
                                                            padding-top: 50px;
                                                            padding-bottom: 50px;
                                                        }

                                                        .qfe_row .vc_span_class.qfy-column-15-5aec7054d4b65886599 {
                                                        }

                                                    ;
                                                    }

                                                    @media only screen and (max-width: 992px) {
                                                        .qfy-column-15-5aec7054d4b65886599 > .column_inner {
                                                            margin: 0 auto 0 !important;
                                                            padding-left: 0;
                                                            padding-right: 0;
                                                            padding-top: 50px;
                                                            padding-bottom: 50px;
                                                        }

                                                        .display_entire .qfe_row .vc_span_class.qfy-column-15-5aec7054d4b65886599 {
                                                        }

                                                        .qfy-column-15-5aec7054d4b65886599 > .column_inner > .background-overlay, .qfy-column-15-5aec7054d4b65886599 > .column_inner > .background-media {
                                                            width: 100% !important;
                                                            left: 0 !important;
                                                            right: auto !important;
                                                        }
                                                    }</style>
                                                    <div class="wf-mobile-hidden qfy-clumn-clear"
                                                         style="clear:both;"></div>
                                                </div>
                                            </div>

                                        </section>
                                    </div>


                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div><!-- END .page-wrapper -->
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>


                </div>

            </div><!-- .wf-container -->
        </div><!-- .wf-wrap -->
    </div><!-- #main -->
    <footer id="footer" class="footer">
        <div class="wf-wrap">
            <div class="wf-container qfe_row footer1" bitDataAction='site_widget_container' bitdatamarker="sidebar_2">
                <section id="text-11" style="margin-top:0;margin-bottom:0;" class="widget widget_text site_tooler">
                    <style class='style_text-11'>#text-11 .widget-title {
                        padding: 0 0 0 10px;
                        height: 28px;
                        line-height: 28px;
                        background-color: transparent;
                        margin: 0px;
                        font-family:;
                        font-size: px;
                        font-weight: normal;
                        font-style: normal;
                        text-decoration: none;
                        color: #ffffff;
                        border-top: 1px solid transparent;
                        border-left: 1px solid transparent;
                        border-right: 1px solid transparent;
                        border-bottom: 0px solid transparent;
                        background-image: none;
                        -webkit-border-top-left-radius: 4px;
                        -webkit-border-top-right-radius: 4px;
                        -moz-border-radius-topleft: 4px;
                        -moz-border-radius-topright: 4px;
                        border-top-left-radius: 4px;
                        border-top-right-radius: 4px;
                    }

                    #text-11 .bitWidgetFrame {
                        border-top: 0;
                        border-bottom: 1px solid transparent;
                        border-left: 1px solid transparent;
                        border-right: 1px solid transparent;
                        padding: 4px 10px 4px 10px;
                        -webkit-border-bottom-left-radius: 4px;
                        -webkit-border-bottom-right-radius: 4px;
                        -moz-border-radius-bottomleft: 4px;
                        -moz-border-radius-bottomright: 4px;
                        border-bottom-left-radius: 4px;
                        border-bottom-right-radius: 4px;
                    }

                    #text-11 {
                        -webkit-box-shadow: none;
                        box-shadow: none;
                    }

                    #text-11 .bitWidgetFrame {
                        background-color: transparent;
                        background-image: none;
                        -webkit-border-bottom-left-radius: 4px;
                        border-bottom-left-radius: 4px;
                        -webkit-border-bottom-right-radius: 4px;
                        border-bottom-right-radius: 4px;
                    }

                    body #text-11 .bitWidgetFrame {
                        padding-top: 10px !important;
                        padding-bottom: 10px !important;
                    }</style>
                    <div class="textwidget ckeditorInLine bitWidgetFrame" bitRlt="text" bitKey="text" wid="text-11">
                        <div style="text-align: center;"><span style="font-size:16px;"><span style="font-family: 微软雅黑;"><span
                                style="color:#FFFFFF;"></span><a rel="" target="_blank"><span
                                style="color:#FFFFFF;"></span></a></span></span></div>
                    </div>
                </section>
                <section id="simplepage-2" style="margin-bottom:20px;" class="widget simplepage site_tooler">
                    <style class='style_simplepage-2'>#simplepage-2 .widget-title {
                        padding: 0 0 0 10px;
                        height: 28px;
                        line-height: 28px;
                        background-color: transparent;
                        margin: 0px;
                        font-family:;
                        font-size: px;
                        font-weight: normal;
                        font-style: normal;
                        text-decoration: none;
                        color: #ffffff;
                        border-top: 1px solid transparent;
                        border-left: 1px solid transparent;
                        border-right: 1px solid transparent;
                        border-bottom: 0px solid transparent;
                        background-image: none;
                        -webkit-border-top-left-radius: 4px;
                        -webkit-border-top-right-radius: 4px;
                        -moz-border-radius-topleft: 4px;
                        -moz-border-radius-topright: 4px;
                        border-top-left-radius: 4px;
                        border-top-right-radius: 4px;
                    }

                    #simplepage-2 .widget-title {
                        border-top: 0;
                        border-left: 0;
                        border-right: 0;
                    }

                    #simplepage-2 .bitWidgetFrame {
                        border-bottom: 0;
                        border-top: 0;
                        border-left: 0;
                        border-right: 0;
                        padding: 4px 10px 4px 10px;
                    }

                    #simplepage-2 {
                        -webkit-box-shadow: none;
                        box-shadow: none;
                    }

                    #simplepage-2 .bitWidgetFrame {
                        background-color: transparent;
                        background-image: none;
                        -webkit-border-bottom-left-radius: 4px;
                        border-bottom-left-radius: 4px;
                        -webkit-border-bottom-right-radius: 4px;
                        border-bottom-right-radius: 4px;
                    }

                    #simplepage-2 .bitWidgetFrame {
                        padding-left: 0px;
                        padding-right: 0px;
                    }

                    body #simplepage-2 .bitWidgetFrame {
                        padding-top: 0px !important;
                        padding-bottom: 0px !important;
                    }</style>
                    <div class='simplepage_container bitWidgetFrame' data-post_id='10379'>
                        <section data-fixheight=""
                                 class="qfy-row-12-5aec7054e2e90354551 section     no  section-text-no-shadow section-inner-no-shadow section-normal"
                                 id="bit_4ami8" style='margin-bottom:0;border-radius:0px;color:#333333;'>
                            <style class="row_class qfy_style_class">
                                @media only screen and (min-width: 992px) {
                                    section.section.qfy-row-12-5aec7054e2e90354551 {
                                        padding-left: 0;
                                        padding-right: 0;
                                        padding-top: 20px;
                                        padding-bottom: 0;
                                        margin-top: 0;
                                    }

                                    section.section.qfy-row-12-5aec7054e2e90354551 > .container {
                                        max-width: 1280px;
                                        margin: 0 auto;
                                    }
                                }

                                @media only screen and (max-width: 992px) {
                                    .bit-html section.section.qfy-row-12-5aec7054e2e90354551 {
                                        padding-left: 15px;
                                        padding-right: 15px;
                                        padding-top: 20px;
                                        padding-bottom:;
                                        margin-top: 0;
                                    }
                                }
                            </style>
                            <div class="section-background-overlay background-overlay grid-overlay-0 "
                                 style="background-color: #efefef;"></div>

                            <div class="container">
                                <div class="row qfe_row">
                                    <div data-animaleinbegin="bottom-in-view" data-animalename="qfyfadeInUp"
                                         data-duration="" data-delay=""
                                         class=" qfy-column-17-5aec7054e45a317990 qfy-column-inner  vc_span12  text-default small-screen-undefined fullrow"
                                         data-dw="1/1" data-fixheight="">
                                        <div style=";position:relative;" class="column_inner ">
                                            <div class=" background-overlay grid-overlay-"
                                                 style="background-color:transparent;width:100%;"></div>
                                            <div class="column_containter" style="z-index:3;position:relative;">
                                                <div m-padding="0px 0px 0px 0px" p-padding="0 0 0 0"
                                                     css_animation_delay="0" qfyuuid="qfy_column_text_7w7z9"
                                                     data-anitime='0.7'
                                                     class="qfy-element qfy-text qfy-text-4022 qfe_text_column qfe_content_element  "
                                                     style="position: relative;;;line-height:1.5em;;background-position:left top;background-repeat: no-repeat;;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;border-radius:0px;">
                                                    <div class="qfe_wrapper">
                                                        <div style="text-align: center;"><span
                                                                style="font-family:raleway;"><span
                                                                style="display: inline-block; line-height: 1.2;"><span
                                                                style="font-size: 13px; color: rgb(0, 0, 0); word-spacing: normal;">Copyright © Stanley Zhai</span></span></span>
                                                        </div>

                                                        <div style="text-align: center;">
                                                            <div style="background-color: rgb(255, 255, 255); text-align: center;">
                                                                <span style="font-family:raleway;"><span
                                                                        style="display: inline-block; line-height: 1.2;"><span
                                                                        style="color: rgb(0, 0, 0);"><span
                                                                        style="font-size: 13px;">info@StanleyZhai.com</span></span></span></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <style class="column_class qfy_style_class">@media only screen and (min-width: 992px) {
                                        .qfy-column-17-5aec7054e45a317990 > .column_inner {
                                            padding-left: 0;
                                            padding-right: 0;
                                            padding-top: 0;
                                            padding-bottom: 0;
                                        }

                                        .qfe_row .vc_span_class.qfy-column-17-5aec7054e45a317990 {
                                        }

                                    ;
                                    }

                                    @media only screen and (max-width: 992px) {
                                        .qfy-column-17-5aec7054e45a317990 > .column_inner {
                                            margin: 0 auto 0 !important;
                                            padding-left: 0;
                                            padding-right: 0;
                                            padding-top:;
                                            padding-bottom:;
                                        }

                                        .display_entire .qfe_row .vc_span_class.qfy-column-17-5aec7054e45a317990 {
                                        }

                                        .qfy-column-17-5aec7054e45a317990 > .column_inner > .background-overlay, .qfy-column-17-5aec7054e45a317990 > .column_inner > .background-media {
                                            width: 100% !important;
                                            left: 0 !important;
                                            right: auto !important;
                                        }
                                    }</style>
                                </div>
                            </div>

                        </section>
                    </div>
                </section>
            </div><!-- .wf-container -->
        </div><!-- .wf-wrap -->
        <!--  ************begin************* -->
        <style type="text/css" id="static-stylesheet-footer">
            #footer.footer .footer1 .widget {
                width: 99%;
            }

            #footer.footer .footer1 .widget.simplepage {
                width: 100%;
            }

            #footer.footer .footer2 .widget {
                width: 99%;
            }

            #footer.footer .footer2 .widget.simplepage {
                width: 100%;
            }

            #footer.footer .footer3 .widget {
                width: 99%;
            }

            #footer.footer .footer3 .widget.simplepage {
                width: 100%;
            }

            .bit_main_content {
                margin-top: 0px;
                margin-bottom: 50px
            }

            @media screen and (min-width: 760px) {
                .bit_main_content {
                    min-height: 100px;
                }
            }
        </style>
        <!--  ************end************* -->
    </footer>


    <style>
        .iphone{
            display: none;
            width: 240px;
            height: 200px;
            border: 1px solid #cfcfcf;
            position: absolute;
            top:0;
            left: -242px;
            background: #fff;
        }
        .iphone .tit{
            height: 40px;padding: 0 20px;font-size: 14px;background:#f7f7f7;border-bottom: 1px solid #cfcfcf;line-height: 40px;font-family: "Microsoft YaHei";color: #616161;
        }
        .iphone ul li{
            font-size: 15px;width: 100%;padding: 0 20px;box-sizing: border-box;
            line-height: 45px;
        }
        .qr_codes{
            display: none;
            width: 200px;
            height: 200px;
            border: 1px solid #cfcfcf;
            position: absolute;
            top:0;
            left: -202px;
            background: #fff;
        }
    </style>
    @include('common.button')
</div>
<a href="#" class="scroll-top displaynone"></a>

</div><!-- #page -->

<link rel='stylesheet' id='qfy-gf-f8ac7b0ae9ec6d45be568052f50b9149-css' href='{{asset("/home/css/786031f43b3b41b190923e0f06009df0.css")}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='qfyuser_jquery_ui_style-css' href='{{asset("/home/css/qfyuser-jquery-ui.css")}}' type='text/css' media='all'/>
<link rel='stylesheet' id='qtip-css' href='{{asset("/home/css/jquery.qtip.min.css")}}' type='text/css' media='all'/>
{{--<link rel='stylesheet' id='iphorm-uniform-theme-66-css' href='http://www.neverends.cncss/default.css?ver=4.851' type='text/css' media='all'/>--}}
<script type="text/javascript">
    // 百度地图API功能
    var map = new BMap.Map("allmap");
    var point = new BMap.Point(120.258422,30.241991);
    var marker = new BMap.Marker(point);  // 创建标注
    map.addOverlay(marker);              // 将标注添加到地图中
    map.centerAndZoom(point, 15);
    var opts = {
        width : 200,     // 信息窗口宽度
        height: 100,     // 信息窗口高度
        title : "杭州市萧山区钱江世纪城博地中心" , // 信息窗口标题
        enableMessage:true,//设置允许信息窗发送短息
        message:"亲耐滴，晚上一起吃个饭吧？戳下面的链接看下地址喔~"
    }
    var infoWindow = new BMap.InfoWindow("地址：杭州市萧山区钱江世纪城博地中心", opts);  // 创建信息窗口对象
    marker.addEventListener("click", function(){
        map.openInfoWindow(infoWindow,point); //开启信息窗口
    });
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var thickboxL10n = {
        "next": "\u4e0b\u4e00\u5f20 >",
        "prev": "< \u4e0a\u4e00\u5f20",
        "image": "\u56fe\u7247",
        "of": "\/",
        "close": "\u5173\u95ed",
        "noiframes": "\u8fd9\u4e2a\u529f\u80fd\u9700\u8981iframe\u7684\u652f\u6301\u3002\u60a8\u53ef\u80fd\u7981\u6b62\u4e86iframe\u7684\u663e\u793a\uff0c\u6216\u60a8\u7684\u6d4f\u89c8\u5668\u4e0d\u652f\u6301\u6b64\u529f\u80fd\u3002",
        "loadingAnimation": "\/\/fast.qifeiye.com\/FeiEditor\/bitSite\/images\/preloader.gif"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("/home/js/thickbox.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/qfy_editor_front.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/transition.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/vc_carousel.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/waypoints.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.blockui.min.js")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var dtLocal = {
        "passText": "\u67e5\u770b\u8fd9\u4e2a\u52a0\u5bc6\u8d44\u8baf\uff0c\u8bf7\u5728\u4e0b\u9762\u8f93\u5165\u5bc6\u7801\uff1a",
        "moreButtonAllLoadedText": "\u5168\u90e8\u5df2\u52a0\u8f7d",
        "postID": "13351",
        "ajaxurl": "http:\/\/www.neverends.cn\/admin\/admin-ajax.php",
        "contactNonce": "47909ff2a7",
        "ajaxNonce": "72af02f6a2",
        "pageData": {"type": "page", "template": "page", "layout": null},
        "themeSettings": {"smoothScroll": "on"}
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("/home/js/plugins.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/opentip-jquery.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/language.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/main.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.lazy.min.js")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var iphormL10n = {
        "error_submitting_form": "\u5728\u63d0\u4ea4\u8868\u5355\u65f6\u6709\u9519\u8bef",
        "swfupload_flash_url": "http:\/\/www.neverends.cn\/qfy-includes\/js\/swfupload\/swfupload.swf",
        "swfupload_upload_url": "http:\/\/www.neverends.cn\/?iphorm_swfupload=1",
        "swfupload_too_many": "\u961f\u5217\u4e2d\u7684\u6587\u4ef6\u592a\u591a\u4e86",
        "swfupload_file_too_big": "\u6587\u4ef6\u592a\u5927\u4e86",
        "swfupload_file_empty": "\u4e0d\u80fd\u4e0a\u4f20\u7a7a\u6587\u4ef6",
        "swfupload_file_type_not_allowed": "\u8be5\u6587\u4ef6\u7684\u7c7b\u578b\u662f\u4e0d\u5141\u8bb8\u4e0a\u4f20\u7684",
        "swfupload_unknown_queue_error": "\u672a\u77e5\u961f\u5217\u9519\u8bef\uff0c\u8bf7\u7a0d\u5019\u518d\u8bd5",
        "swfupload_upload_error": "\u4e0a\u4f20\u9519\u8bef",
        "swfupload_upload_failed": "\u4e0a\u4f20\u5931\u8d25",
        "swfupload_server_io": "\u670d\u52a1\u5668IO\u9519\u8bef",
        "swfupload_security_error": "\u5b89\u5168\u9519\u8bef",
        "swfupload_limit_exceeded": "\u4e0a\u4f20\u8d85\u8fc7\u9650\u5236",
        "swfupload_validation_failed": "\u9a8c\u8bc1\u5931\u8d25",
        "swfupload_upload_stopped": "\u4e0a\u4f20\u88ab\u7ec8\u6b62",
        "swfupload_unknown_upload_error": "\u6587\u4ef6\u4e0a\u4f20\u88ab\u610f\u5916\u7ec8\u6b62",
        "plugin_url": "http:\/\/www.neverends.cn\/qfy-content\/plugins\/qfy_form",
        "preview_no_submit": "\u5728\u9884\u89c8\u6a21\u5f0f\u4e0b\u4e0d\u80fd\u63d0\u4ea4\u8868\u5355",
        "iphorm_required": "\u8fd9\u662f\u4e00\u4e2a\u5fc5\u586b\u9879\u54e6"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("/home/js/jquery.iphorm.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.form.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.smooth-scroll.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.qtip.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.uniform.min.js")}}'></script>
<script type='text/javascript' src='{{asset("/home/js/jquery.infieldlabel.min.js")}}'></script>
<script>
    $('.wechat').hover(function () {
        $('.qr_codes').show();
    },function () {
        $('.qr_codes').hide();
    })
    $('.phone').hover(function () {
        $('.iphone').show();
    },function () {
        $('.iphone').hide();
    });
    $("#submitInfo").click(function () {
        //获取数据
        var submit_name = $("#submit_name").val();
        var submit_email = $("#submit_email").val();
        var submit_tel = $("#submit_tel").val();
        var submit_content = $("#submit_content").val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url   : '/submit',
            data  :{submit_name:submit_name,submit_email:submit_email,submit_tel:submit_tel,submit_content:submit_content},
            type  : 'post',
            dataType : 'json',
            success : function (data) {
                $(".showName").css('display','none');
                $(".showEmail").css('display','none');
                $(".showTel").css('display','none');
                if(data.code == 400){
                    var name = data.data.name;
                    var email = data.data.email;
                    var tel = data.data.tel;
                    if(name == null){
                        $(".showName").css('display','block');
                    }
                    if(email == null){
                       $(".showEmail").css('display','block');
                    }
                    if(tel == null){
                       $(".showTel").css('display','block');
                    }
                }
                if(data.code == 401){
                    $(".showEmail").css('display','block');
                    $(".showEmail i").html('请填写正确的邮箱！');
                }
                if(data.code == 200){
                    $(".qfy_popinfo .info").html("您的信息已提交成功！");
                    setTimeout(function(){ jQuery(".qfy_popinfo").addClass("qfy_show"); },500);
                    setTimeout(function(){ jQuery(".qfy_popinfo").removeClass("qfy_show"); },2*1000);
                }
                if(data.code == 402){
                    $(".qfy_popinfo .info").html("您的信息已经提交，不用重复提交！");
                    setTimeout(function(){ jQuery(".qfy_popinfo").addClass("qfy_show"); },500);
                    setTimeout(function(){ jQuery(".qfy_popinfo").removeClass("qfy_show"); },2*1000);
                }
            }
        });
    })

</script>
<div style='display:none;'></div>
<div class="qfy_popinfo">
    <div class="md-content">
        <div style="text-align:center;">
            <img class="qfy_pop_checkedimg" src="/home/images/check.png" style="margin-top:20px;margin-bottom:20px;">
            <div class="info" style="padding-bottom:20px;">您的信息已提交成功！</div>
        </div>
    </div>
</div>
</body>

</html>
