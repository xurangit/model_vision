<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255)->unique()->comment('标题');
            $table->string('name',255)->unique()->nullable()->comment('中文名称');
            $table->string('name_en',255)->unique()->nullable()->comment('英文名称');
            $table->string('file',255)->comment('图片/视频路径');
            $table->tinyInteger('is_video')->default(0)->comment("是否是视频（1-是 0-不是-默认）");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home');
    }
}
