<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentOrdinary extends Model
{
    protected $table = 'students_ordinary';
}
