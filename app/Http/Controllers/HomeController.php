<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Company;
use App\Models\CompanyBasic;
use App\Models\Course;
use App\Models\Gene;
use App\Models\Story;
use App\Models\Student;
use App\Models\StudentOrdinary;
use App\Models\Subject;
use App\Models\SubjectBasic;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');

    }

    public function list(Request $request)
    {
        $route = strstr($request->path() . '/', '/', true);
        $cate  = new Category();
        $list  = $cate->getList();
        return view('list', [
            'list'    => $list,
            'lang'    => $request->lang,
            'route'   => $route,
            'company' => $request->input()
        ]);
    }

    public function gene(Request $request)
    {
        $route = strstr($request->path() . '/', '/', true);
        $info  = Gene::first();
        $cate  = Category::where('id', 1)->first();
//        $info->name       = str_replace("\n", '</br>', $info->name);
//        $info->name_en    = str_replace("\n", '</br>', $info->name_en);
//        $info->content    = str_replace("\n", '</br>', $info->content);
//        $info->content_en = str_replace("\n", '</br>', $info->content_en);
        //获取故事列表
        $story = Story::where('is_show', 1)->orderBy('sort', 'asc')->get();
        return view('gene', [
            'cate'    => $cate,
            'info'    => $info,
            'story'   => $story,
            'lang'    => $request->lang,
            'route'   => $route,
            'company' => $request->input()
        ]);
    }

    public function mentor(Request $request)
    {
        $route = strstr($request->path() . '/', '/', true);
        //获取信息
        $cate = Category::where('id', 2)->first();
        //获取导师信息
        $list = Teacher::where('is_show', 1)->orderBy('sort', 'asc')->get();
        foreach ($list as $key => $value) {
            $list[$key]->introduce    = str_replace("\n", '</br>', $value->introduce);
            $list[$key]->introduce_en = str_replace("\n", '</br>', $value->introduce_en);
        }
        return view('mentor', [
            'cate'    => $cate,
            'list'    => $list,
            'lang'    => $request->lang,
            'route'   => $route,
            'company' => $request->input()
        ]);
    }

    public function student(Request $request)
    {
        //路由
        $route = strstr($request->path() . '/', '/', true);
        //获取优秀学员信息
        $student = Student::where('is_show', 1)->orderBy('sort', 'asc')->get();
        foreach ($student as $key => $value) {
            $student[$key]->introduce    = str_replace("\n", '</br>', $value->introduce);
            $student[$key]->introduce_en = str_replace("\n", '</br>', $value->introduce_en);
        }
        //获取普通学员信息
        $student_ordinary = StudentOrdinary::where('is_show', 1)->orderBy('sort', 'asc')->get();
        return view('selected_students', [
            'student'          => $student,
            'student_ordinary' => $student_ordinary,
            'lang'             => $request->lang,
            'route'            => $route,
            'company'          => $request->input()
        ]);
    }

    public function course(Request $request)
    {
        //路由
        $route = strstr($request->path() . '/', '/', true);
        //获取信息
        $cate   = Category::where('id', 4)->first();
        $course = Course::where('is_show', 1)->orderBy('sort', 'asc')->get();
        foreach ($course as $key => $value) {
            $course[$key]->introduce    = str_replace("\n", '</br>', $value->introduce);
            $course[$key]->introduce_en = str_replace("\n", '</br>', $value->introduce_en);
        }
        return view('customized_courses', [
            'cate'    => $cate,
            'course'  => $course,
            'lang'    => $request->lang,
            'route'   => $route,
            'company' => $request->input()
        ]);
    }

    public function contact(Request $request)
    {
        //路由
        $route = strstr($request->path() . '/', '/', true);
        //获取信息
//        $cate = Category::where('id', 5)->first();
        $basic = CompanyBasic::where('id', 1)->first();
        $info  = Company::first();
        return view('info_contact', [
//            'cate'    => $cate,
            'basic'   => $basic,
            'info'    => $info,
            'lang'    => $request->lang,
            'route'   => $route,
            'company' => $request->input()
        ]);
    }

    public function submit(Request $request)
    {
        if ($request->isMethod('post')) {
            $name    = $request->post('submit_name');
            $tel     = $request->post('submit_tel');
            $email   = $request->post('submit_email');
            $content = $request->post('submit_content');
            if (empty($name) || empty($tel) || empty($email)) {
                return json_encode([
                    'code' => 400,
                    'msg'  => '不能为空',
                    'data' => [
                        'name'  => $name,
                        'tel'   => $tel,
                        'email' => $email,
                    ],
                ]);
            }
            if (!preg_match('/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/', $email)) {
                return json_encode([
                    'code' => 401,
                    'msg'  => '不是邮箱'
                ]);
            }
            //插入前先判断是否已经提交了
            $res = DB::table('tourist')->where('email', $email)->orWhere('tel', $tel)->get();
            if (!$res->isEmpty()) {
                return json_encode([
                    'code' => 402,
                    'msg'  => '已存在'
                ]);
            }
            $res2 = DB::table('tourist')
                ->insert([
                    'name'    => $name,
                    'email'   => $email,
                    'tel'     => $tel,
                    'content' => $content,
                    'addtime' => time(),
                ]);
            if ($res2) {
                return json_encode([
                    'code' => 200,
                    'msg'  => '成功'
                ]);
            } else {
                return json_encode([
                    'code' => 404,
                    'msg'  => '失败'
                ]);
            }
        } else {
            return json_encode([
                'code' => 500,
                'msg'  => '错误'
            ]);;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * 超模专题
     */
    public function subject(Request $request)
    {
        //路由
        $route = strstr($request->path() . '/', '/', true);
        //获取信息
        $cate = Category::where('id', 6)->first();
        //获取是视频还是图片
        $subject_basic = SubjectBasic::where('id', 1)->first();
        $subject       = Subject::where('is_show', 1)->orderBy('sort', 'asc')->get();
        foreach ($subject as $key => $value) {
            $subject[$key]->introduce    = str_replace("\n", '</br>', $value->introduce);
            $subject[$key]->introduce_en = str_replace("\n", '</br>', $value->introduce_en);
        }
        return view('subject', [
            'cate'          => $cate,
            'subject'       => $subject,
            'subject_basic' => $subject_basic,
            'lang'          => $request->lang,
            'route'         => $route,
            'company'       => $request->input()
        ]);
    }
}
