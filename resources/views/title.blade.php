<style>
    .title {
        font-size: 50px;
        color: #636b6f;
        font-family: 'Raleway', sans-serif;
        font-weight: 100;
        display: block;
        text-align: center;
        margin: 20px 0 10px 0px;
    }

    .links {
        text-align: center;
        margin-bottom: 20px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }
    .table{
        width: 100%;
        border: 1px;
    }
    .table th{
        width: 100/6%;
    }
</style>

<div class="title">
    网站访问量
</div>
<div class="links">
    <a href="">访问量计算是根据每个访问者的ip地址，每天同一个ip多次访问只计算一次访问量</a>
</div>
<div class="links">
    <table class="table">
        <tr>
            <td style="font-size:16px;font-weight: bold">今日</td>
            <td style="font-size:16px;font-weight: bold">昨日</td>
            <td style="font-size:16px;font-weight: bold">本周</td>
            <td style="font-size:16px;font-weight: bold">上周</td>
            <td style="font-size:16px;font-weight: bold">本月</td>
            <td style="font-size:16px;font-weight: bold">总访问量</td>
        </tr>
        <tr>
            <td>{{$list[0]}}</td>
            <td>{{$list[1]}}</td>
            <td>{{$list[2]}}</td>
            <td>{{$list[3]}}</td>
            <td>{{$list[4]}}</td>
            <td>{{$list[5]}}</td>
        </tr>
    </table>
</div>