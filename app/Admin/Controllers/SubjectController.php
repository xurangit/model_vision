<?php

namespace App\Admin\Controllers;

use App\Models\Subject;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SubjectController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('超模专题');
            $content->description('列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('超模专题');
            $content->description('编辑');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('超模专题');
            $content->description('创建');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Subject::class, function (Grid $grid) {
            $grid->id('ID');
            $grid->name('标题（中文）');
            $grid->name_en('标题（英文）');
            $grid->file('图片/视频')->display(function ($file) {
                return "<image class='images' style='height:30px;' src='".env('APP_URL').'/uploads/'.$file."'>
                    <script>
                        $('.images').mouseover(function() {
                            $(this).attr('style','height:400px;position:absolute;z-index:1;');
                        });
                        $('.images').mouseout(function() {
                            $(this).attr('style','height:30px;');
                        });
                    </script>
                ";
            });
            $states = [
                'on'  => ['value' => 1, 'text' => '显示', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '隐藏', 'color' => 'danger'],
            ];
            $grid->is_show('显示/隐藏')->switch($states);
            $grid->introduce('简介（中文）')->display(function ($text){
                return str_limit($text,30,"...");
            });;
            $grid->introduce_en('简介（英文）')->display(function ($text){
                return str_limit($text,30,"...");
            });;
            $grid->sort('排序')->sortable();

            //禁用
            $grid->disableFilter();
            $grid->disableExport();

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Subject::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', '标题（中文）')->rules('required',['required' => '姓名为必填项']);
            $form->text('name_en', '标题（英文）')->rules('required',['required' => '姓名为必填项']);
            $form->textarea('introduce', '简介（中文）')->rules('required',['required' => '简介为必填项']);
            $form->textarea('introduce_en', '简介（英文）')->rules('required',['required' => '简介为必填项']);
            $states = [
                'on'  => ['value' => 1, 'text' => '视频', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '图片', 'color' => 'danger'],
            ];
            $form->switch('is_video','视频/图片')->states($states)->default(0)->help('按钮要与下方图片/视频链接对应，否则不能展示效果');
            $form->file('file','图片/视频')->uniqueName()->rules('required',['required' => '图片/视频为必填项'])->help('建议图片的尺寸为：700 X 460');
//            $form->radio('is_show','是否显示')->options(['1' => '显示', '0' => '隐藏'])->default(1);
//            $grid->is_show('是否显示')->switch($states);
            $states = [
                'on'  => ['value' => 1, 'text' => '显示', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => '隐藏', 'color' => 'danger'],
            ];
            $form->switch('is_show','是否显示')->states($states)->default(1);
            $form->text('sort','排序')->default(10)->help('手动填写排序，默认排序是数字从小到大排列（数字相同按照ID排序）');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
