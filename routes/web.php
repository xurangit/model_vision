<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//前台
Route::group(['middleware'=>'home'],function (){
    Route::get('/', 'HomeController@index');
    Route::get('/list/{lang?}', 'HomeController@list');
    Route::get('/gene/{lang?}', 'HomeController@gene');
    Route::get('/mentor/{lang?}', 'HomeController@mentor');
    Route::get('/student/{lang?}', 'HomeController@student');
    Route::get('/course/{lang?}', 'HomeController@course');
    Route::any('/contact/{lang?}', 'HomeController@contact');
    Route::post('/submit', 'HomeController@submit');
    Route::get('/subject/{lang?}', 'HomeController@subject');
});
